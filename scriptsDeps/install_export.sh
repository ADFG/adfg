#!/bin/sh
# ------------------------------------------------------------------------------------
# It generates the adfg_sched command to use to call the command line interface
# ------------------------------------------------------------------------------------

RTSCHEDULING_ROOT="$(pwd)"
SCRIPTS_DIR="${RTSCHEDULING_ROOT}/scriptsDeps"

. "${SCRIPTS_DIR}/OSdependent.sh"
. "${SCRIPTS_DIR}/create_cmd.sh"

cmd_file="${RTSCHEDULING_ROOT}/adfg_sched"
adfgExe "${cmd_file}" "${RTSCHEDULING_ROOT}/cli.jar"
chmod +x "${cmd_file}"

echo ""
echo "* Use the command adfg_sched to check the scheduling feasibility of an Ultimately Cyclo-Static DataFlow (UCSDF) graph."
echo "* Use adfg_sched -h for help."
echo ""
