#!/bin/sh

# $1 is the search dir
# $2 is file name
findFrom() {
    jni="`find -L ${1} -type f -name ${2} -print0 -quit`"
    if [ "${jni}_XXX" != "_XXX" ]; then
	echo "`dirname ${jni}`"
    fi
    echo ""
}

