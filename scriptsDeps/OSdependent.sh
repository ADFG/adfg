#!/bin/sh

KERNEL_ADFG=$(uname -s)
MACHINE_ADFG=$(uname -m)

if [ "${KERNEL_ADFG}" = "Darwin" ] ; then
    LIB_SUFF=dylib
    OPT_SHARED="-dynamiclib -undefined dynamic_lookup"
    REL_LINK="-Wl,-install_name,@rpath/\$@ -Wl,-rpath,@loader_path"
else
    LIB_SUFF=so
    OPT_SHARED="-shared"
    REL_LINK="-Wl,-soname=\$@ -Wl,-rpath='\$\$ORIGIN/'. "
fi

if [ "${MACHINE_ADFG}" = "x86_64" ] ; then
    ARCHI_ADFG=64
else
    ARCHI_ADFG=32
fi
