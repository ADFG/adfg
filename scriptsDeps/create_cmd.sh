#!/bin/sh

# $1 = file to create 
# (we assume that the libs/ folder is at the same level)
# $2 = cli jar path

adfgExe() {
    echo "#!/bin/sh"  > "$1"
    echo ""  >> "$1"
    echo "# absolute path to this script file:"  >> "$1"
    echo "SCRIPT_PATH=\"`dirname $1`\""  >> "$1"
    echo ""  >> "$1"
    echo "if [ \"\${SCRIPT_PATH}\" = \"\" ]; then"  >> "$1"
    echo "    echo \"Fill the SCRIPT_PATH variable of this file with an absolute path to its location please.\""  >> "$1"
    echo "    exit 1"  >> "$1"
    echo "fi"  >> "$1"
    echo ". \${SCRIPT_PATH}/scriptsDeps/requires_func.sh"  >> "$1"
    echo "requireFile \"\${SCRIPT_PATH}/libs/liblpsolve55j.${LIB_SUFF}\"" >>  "$1"
    echo "requireFile \"\${SCRIPT_PATH}/libs/libpartition.${LIB_SUFF}\"" >>  "$1"
    echo "java -Djava.library.path=\"\${SCRIPT_PATH}/libs\" -jar $2 \$*" >>  "$1"
}
