#!/bin/sh

requireDir() {
    if [ ! -d "$1" ]; then
	echo "Directory $1 not found ! exiting."
	exit 1
    fi
}

requireFile() {
    if [ ! -f "$1" ]; then
	echo "File $1 not found ! exiting."
	exit 1
    fi
}
