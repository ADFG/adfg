package lpsolve;

public class Demo {

    private static final String file = "ILP-SSA.lp";

    private static class MyLpLogger implements LogListener {
	
	public void logfunc(LpSolve problem, Object userhandle, String buf) throws LpSolveException {
	    System.out.print(buf);
	}

    }

    public static void main(String[] args) {
	try {
	    LpSolve prob = LpSolve.readLp(file, LpSolve.NORMAL, "Demo");
	    // prob.setScaling(LpSolve.SCALE_GEOMETRIC + 
	    // 		LpSolve.SCALE_EQUILIBRATE + 
	    // 		LpSolve.SCALE_INTEGERS);
	    // prob.setImprove(LpSolve.IMPROVE_DUALFEAS + 
	    // 		LpSolve.IMPROVE_THETAGAP);
	    // prob.setSimplextype(LpSolve.SIMPLEX_DUAL_PRIMAL);
	    // prob.setPresolve(LpSolve.PRESOLVE_ROWS + 
	    // 		     LpSolve.PRESOLVE_ROWS +
	    // 		     LpSolve.PRESOLVE_BOUNDS,
	    // 		     prob.getPresolveloops());
	    // System.out.println("Presolve: " + prob.getPresolve() +
	    // 		       "(loops: " + prob.getPresolveloops() + ")");
	    prob.setOutputfile("");
	    prob.putLogfunc(new MyLpLogger(), null);
	    int ret = prob.solve();
	    if (ret == LpSolve.SUBOPTIMAL || ret == LpSolve.OPTIMAL || ret == LpSolve.PRESOLVED) {
		double[] results = prob.getPtrVariables();
		System.out.println("Objective value: "+prob.getObjective());
		for (int i = 0; i < results.length; ++i) {
		    System.out.println("Solution of "+prob.getColName(i+1)+": "+results[i]);
		}
	    } else {
		System.out.println("Oh oh, problem ... ("+ret+")");
	    }
	    prob.deleteLp();
	}
	catch (LpSolveException e) {
	    e.printStackTrace();
	}
    }

}
