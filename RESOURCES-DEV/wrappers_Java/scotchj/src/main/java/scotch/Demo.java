package scotch;

public class Demo {

    public static void main (String[] args) {

	/* use scotch to partition the graph */
	int[] partresult = new Partitioning().graphPart("graph.grf", 57, 2, 0.03);
	for (int i = 0; i < partresult.length; ++i) {
	    System.out.println(i+" --> ["+partresult[i]+"]");
	}
    }

}
