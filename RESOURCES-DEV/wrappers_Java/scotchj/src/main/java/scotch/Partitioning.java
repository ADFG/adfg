package scotch;

/**
 * This class enables to call SCOTCH partitioning.
 * The shared library loading is done at class initialization.
 */
public class Partitioning {

    static {
	System.loadLibrary("partition");
    }


    /**
     * Call SCOTCH partitioner through our shared library (use JNI). 
     * 
     * @param fileName File containing the graph representation (.grf).
     * @param nbNodes Total number of actors.
     * @param nbParts Number of partitions to divide the graph in.
     * @param imbalanceRatio Metric to control the maximum load imbalance.
     * @return Give the partition number of each actor (size is number of actors).
     */
    public native int[] graphPart(String fileName, int nbNodes, int nbParts, double imbalanceRatio);


    /**
     * Call SCOTCH partitioner through our shared library (use JNI). 
     * 
     * @param fileName File containing the graph representation (.grf).
     * @param nbNodes Total number of actors.
     * @param nbParts Number of partitions to divide the graph in.
     * @param imbalanceRatio Metric to control the maximum load imbalance.
     * @param fixedVertices Tabular (of length |V|) specifying each vertex fixed partition,
     * otherwise the vertex cell must contain the value -1.
     * @return Give the partition number of each actor (size is number of actors), input array is not modified.
     */
    public native int[] graphPartFixed(String fileName, int nbNodes, int nbParts, double imbalanceRatio, int[] fixedVertices);

}
