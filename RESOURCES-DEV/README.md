# RESOURCES-DEV

This folder contains extra resources needed for development, 
there cannot be part of the Eclipse repository because of the licences.

## Instructions

Just run ```./install-artifacts.sh```.

This will build and install some dependencies: the java wrappers
for scotch and lpsolve.

## Requirements

mvn, git

## Source code redistribution

- LpSolve java wrapper, made by Juergen Ebert, under LGPL licence.

