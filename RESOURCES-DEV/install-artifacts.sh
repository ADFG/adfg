#!/bin/sh

if [ "$1" = "clean" ] ; then
    directive="clean"
else
    directive="install"
fi

echo ""
echo "==> $mess external dependencies jars .."
echo ""

mvn -q -f wrappers_Java/scotchj/pom.xml ${directive} || exit 1
mvn -q -f wrappers_Java/lpsolvej/pom.xml ${directive} || exit 1

