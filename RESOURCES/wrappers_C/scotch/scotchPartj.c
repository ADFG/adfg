#include <stdio.h>
#include <stdlib.h>

#include <stdint.h>
#include "scotch.h"

#include <locale.h>
#include <string.h>

#include "scotch_Partitioning.h"


/**
 * Last parameter is NULL to call the standard version.
 */
jintArray internal_graphPart
(JNIEnv * env, jobject jobj, jstring jfileName, jint jnbNodes, jint jnbParts, jdouble jimbalanceRatio, jintArray jfixedVertices)
{
  jintArray result = NULL;
  SCOTCH_Graph * grafdat = NULL;
  SCOTCH_Strat * stradat = NULL;
  FILE * fileptr = NULL;
  SCOTCH_Num * partitions = NULL;
  SCOTCH_Num vertnbr;

  char * prevLocale = setlocale (LC_NUMERIC, NULL);
  char * tmp = prevLocale;
  size_t localeLen = 1 + strlen (tmp);
  prevLocale = (char *) malloc (sizeof(char) * localeLen);
  strncpy (prevLocale, tmp, localeLen);
  setlocale (LC_NUMERIC, "C"); 
  // because Scotch internally uses snprintf and only accepts numbers 
  // in format 0.xxx and not 0,xxx

  const char *fileName = (*env)->GetStringUTFChars(env, jfileName, NULL); //"graph.grf";
  SCOTCH_Num nbNodes = (SCOTCH_Num) jnbNodes;
  SCOTCH_Num nbParts = (SCOTCH_Num) jnbParts;
  double imbalanceRatio = (double) jimbalanceRatio;    

  if (fileName == NULL) {
    fprintf(stderr, "SCOTCH: Couldn't get the filename.\n");
    goto end;
  }

  grafdat = (SCOTCH_Graph *) malloc(sizeof(SCOTCH_Graph));
  if (SCOTCH_graphInit (grafdat) != 0) {
    fprintf (stderr, "SCOTCH: Couldn't initialize.\n");
    goto end;
  } else if ((fileptr = fopen(fileName, "r")) == NULL) {
    fprintf (stderr, "SCOTCH: Graph file not found.\n");
    goto end;
  } else if (SCOTCH_graphLoad (grafdat, fileptr, -1, 0) != 0 ) {
    fprintf (stderr, "SCOTCH: Cannot load the graph file.\n");
    goto end;
  }
    
  SCOTCH_graphData (grafdat, NULL, &vertnbr, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
  //fprintf (stderr, stderr, "SCOTCH: nbNodes: %i, nbParts: %i, imbalance ratio: %f\n", nbNodes, nbParts, imbalanceRatio);
  if (vertnbr != nbNodes) {
    fprintf (stderr, "SCOTCH: Invalid number of nodes (found: %i, expected: %i), aborting.\n", vertnbr, nbNodes);
    goto end;
  }
  
  partitions = (SCOTCH_Num *) malloc(sizeof(SCOTCH_Num)*nbNodes);
  if (jfixedVertices != NULL) {
    jint len = (jint) (*env)->GetArrayLength(env, jfixedVertices); // jsize type normally
    if (len != jnbNodes) {
      fprintf (stderr, "SCOTCH: fixed vertices array has different size than expected (found: %i, expected: %i), aborting.\n", len, jnbNodes);
      goto end;
    }
    jint * fixedVert = (*env)->GetIntArrayElements (env, jfixedVertices, 0);
    for (int i = 0; i < jnbNodes; ++i) {
      partitions[i] = (SCOTCH_Num) (fixedVert[i]);
    }
    (*env)->ReleaseIntArrayElements (env, jfixedVertices, fixedVert, JNI_ABORT);
  }

  stradat = (SCOTCH_Strat *) malloc(sizeof(SCOTCH_Strat));

  if (SCOTCH_stratInit (stradat) != 0) {
    fprintf (stderr, "SCOTCH: strategy initialization failed, aborting.\n");
    goto end;
  }

  // otherwise it seems to create a segfault, probably due to Scotch
  if (nbNodes < nbParts) {
    nbParts = nbNodes;
  }
  
  if (SCOTCH_stratGraphMapBuild (stradat, SCOTCH_STRATBALANCE, nbParts, imbalanceRatio) != 0) {
    fprintf (stderr, "SCOTCH: error in strategy.\n");
  } else if (SCOTCH_graphPart (grafdat, nbParts, stradat, partitions) != 0) {
    fprintf (stderr, "SCOTCH: cannot partition the graph.\n");
  } else {
    result = (*env)->NewIntArray (env, jnbNodes);
    if (result != NULL) {
      jint * resVert = (*env)->GetIntArrayElements (env, result, 0);
      for (int i = 0; i < jnbNodes; ++i) {
	resVert[i] = (jint) (partitions[i]);
      }
      (*env)->ReleaseIntArrayElements (env, result, resVert, 0);
    } else {
      fprintf (stderr, "SCOTCH: result array allocation failed.\n");
    }
  }

 end:
  setlocale (LC_NUMERIC, prevLocale);
  free (prevLocale);
  if (partitions != NULL) {
    free (partitions);
  }
  if (fileptr != NULL) {
    fclose (fileptr);
  }
  if (grafdat != NULL) {
    SCOTCH_graphExit (grafdat);
  }
  if (stradat != NULL) {
    SCOTCH_stratExit (stradat);
  }
  return result;
}



JNIEXPORT jintArray JNICALL Java_scotch_Partitioning_graphPart
(JNIEnv * env, jobject jobj, jstring jfileName, jint jnbNodes, jint jnbParts, jdouble jimbalanceRatio)
{
  return internal_graphPart (env, jobj, jfileName, jnbNodes, jnbParts, jimbalanceRatio, NULL);
}



JNIEXPORT jintArray JNICALL Java_scotch_Partitioning_graphPartFixed
(JNIEnv * env, jobject jobj, jstring jfileName, jint jnbNodes, jint jnbParts, jdouble jimbalanceRatio, jintArray jfixedVertices)
{
  return internal_graphPart (env, jobj, jfileName, jnbNodes, jnbParts, jimbalanceRatio, jfixedVertices);
}




