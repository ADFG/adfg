#!/bin/sh

## $1 can be the "clean" directive

RES="$(pwd)"
RTSCHEDULING_ROOT="$(dirname ${RES})"
SCRIPTS="${RTSCHEDULING_ROOT}/scriptsDeps"

# Reading the configuration file
CONFIG_FILE="${RTSCHEDULING_ROOT}/devel.config"
if [ ! -f "${CONFIG_FILE}" ]; then
    echo "Run the main installation script, in the parent folder."
    exit 1
fi

. "${CONFIG_FILE}"


# Compile dependencies if using our archives

if [ "$1" = "clean" ] ; then
    directive="clean"
    mess="Cleaning"
else
    directive=""
    mess="Compiling and installing"
fi

echo ""
echo "==> $mess external dependencies wrappers ..."
echo ""

if [ "${SCOTCH_ADFG}" = "${RES}/libs_to_install/scotch_6.0.6" ] ; then
    echo "[SCOTCH C library] make ${directive}"
    cd "libs_to_install"
    sh ./install-scotch.sh "$1" || exit 1
    cd ..
else
    echo "!!! The repo version of SCOTCH C library will not be installed, check the path of \${SCOTCH_ADFG} in the file devel.config"
fi

if [ "${LPSOLVE_ADFG}" = "${RES}/libs_to_install/lp_solve_5.5" ] ; then
    echo "[LPSOLVE C library] make ${directive}"
    cd "libs_to_install"
    sh ./install-lpsolve.sh "$1" || exit 1
    cd ..
else
    echo "!!! The repo version of LPSOLVE C library will not be installed, check the path of \${LPSOLVE_ADFG} in the file devel.config"
fi

if [ "$1" != "clean" ] ; then
    if [ ! -d "${SCOTCH_ADFG}"  ] || [ ! -d "${LPSOLVE_ADFG}" ] || [ ! -d "${JDK_ADFG}" ] ; then
	echo "Bad devel.config paths, exiting."
	exit 1
    fi
fi


# Create symbolic links on deps and compile libs
cd wrappers_C

. "${SCRIPTS}/find_file.sh"
. "${SCRIPTS}/OSdependent.sh"

if [ ! -f "make.config" ] ; then
    if [ "${JDK_ADFG}" = "" ]; then
	echo 'Empty JDK path, fill the devel.config file and rerun the install.'
	exit 1
    fi
    JNIH1=$(findFrom "${JDK_ADFG}" "jni.h")
    JNIH2=$(findFrom "${JDK_ADFG}" "jni_md.h")
    if [ "${JNIH1}" = "" ] || [ "${JNIH2}" = "" ]; then
	echo 'JNI headers could not be find, check the JDK_ADFG path in devel.config.'
	exit 1
    fi    
    JNIH="JNIH:=-I${JNIH1} -I${JNIH2}"
    echo "# automatically generated file, do not edit" > "make.config"
    echo "${JNIH}" >> "make.config"
    echo "SCOTCH_ADFG:=${SCOTCH_ADFG}" >> "make.config"
    echo "LPSOLVE_ADFG:=${LPSOLVE_ADFG}" >> "make.config"
    echo "OPT_SHARED:=${OPT_SHARED}" >> "make.config"
    echo "LIB_SUFF:=${LIB_SUFF}" >> "make.config"
    echo "REL_LINK=${REL_LINK}" >> "make.config"
    echo "" >> "make.config"
fi

cd scotch
echo "[SCOTCH C wrapper for Java] make ${directive}"

if [ "$1" != "clean" ] ; then
    ln -sf "${SCOTCH_ADFG}/lib/libscotch.${LIB_SUFF}" .
    ln -sf "${SCOTCH_ADFG}/lib/libscotcherr.${LIB_SUFF}" .
    ln -sf "${SCOTCH_ADFG}/lib/libscotcherrexit.${LIB_SUFF}" .
fi
make -s ${directive} || exit 1
cd ..


cd lpsolve
echo "[LPSOLVE C wrapper for Java] make ${directive}"
if [ "${KERNEL_ADFG}" = "Darwin" ] ; then
    OS_LPSOLVE=osx
else
    OS_LPSOLVE=ux
fi
XXLPSOLVE="${OS_LPSOLVE}${ARCHI_ADFG}"
if [ "$1" != "clean" ] ; then
    ln -sf "${LPSOLVE_ADFG}/lpsolve55/bin/${XXLPSOLVE}/liblpsolve55.${LIB_SUFF}"
fi
make -s ${directive} || exit 1
cd ..
if [ "$1" = "clean" ] ; then
   rm -f "make.config"
fi
cd ..

