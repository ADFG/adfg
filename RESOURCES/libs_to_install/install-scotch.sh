#!/bin/sh

SCOTCH_AR=scotch_6.0.6.tar.gz
SCOTCH_DIR=scotch_6.0.6

if [ ! "$1" = "clean" ] ; then
    . ../../scriptsDeps/OSdependent.sh
    if [ ! -f "${SCOTCH_AR}" ] ; then
	echo "${SCOTCH_AR} has disappeard ! Exiting ..."
	exit 1
    fi
    if [ ! -d "${SCOTCH_DIR}" ] ; then
	tar -xf "${SCOTCH_AR}"  || exit 1
    fi

    cd "${SCOTCH_DIR}/src"
    cp ../../adfg_deps_makefiles/Makefile_${KERNEL_ADFG}${ARCHI_ADFG}.inc Makefile.inc
    make -j 2 -s || exit 1
    cd ..
    if [ ! -f "lib/libscotch.${LIB_SUFF}" ] || [ ! -f "lib/libscotcherr.${LIB_SUFF}" ] || \
	[ ! -f "lib/libscotcherrexit.${LIB_SUFF}" ] ; then
	echo "Scotch lib cannot be found after compilation ! Exiting ..."
	exit 1
    fi
    cd ..
else
    rm -rf "scotch_6.0.6"
fi
