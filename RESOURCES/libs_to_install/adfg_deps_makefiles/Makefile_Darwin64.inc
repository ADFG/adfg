EXE             =
LIB             = .dylib
OBJ             = .o

MAKE            = make
AR              = gcc
ARFLAGS         = -Wl,-install_name,@rpath/$@ -Wl,-rpath,@loader_path -dynamiclib -undefined dynamic_lookup -o
CAT             = cat
CCS             = gcc
CCP             = mpicc
CCD             = gcc
CFLAGS          = -O3 -DCOMMON_FILE_COMPRESS_GZ -DCOMMON_PTHREAD -DCOMMON_RANDOM_FIXED_SEED  -DSCOTCH_PTHREAD -Drestrict=__restrict -DIDXSIZE64 -DCOMMON_PTHREAD_BARRIER -DCOMMON_TIMING_OLD -DSCOTCH_RENAME -DSCOTCH_DETERMINISTIC
CLIBFLAGS       =
LDFLAGS         = -lz -lm  -pthread
CP              = cp
LEX             = flex -Pscotchyy -olex.yy.c
LN              = ln
MKDIR           = mkdir
MV              = mv
RANLIB          = echo
YACC            = bison -pscotchyy -y -b y
