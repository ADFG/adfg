#!/bin/sh

LPSOLVE_AR=lp_solve_5.5.2.5_source.tar.gz
LPSOLVE_DIR=lp_solve_5.5

if [ ! "$1" = "clean" ] ; then
    . ../../scriptsDeps/OSdependent.sh
    if [ "${KERNEL_ADFG}" = "Darwin" ] ; then
	OS_LPSOLVE=osx
	OS_SUFF=.osx
    else
	OS_LPSOLVE=ux
	OS_SUFF=
    fi
    XXLPSOLVE="${OS_LPSOLVE}${ARCHI_ADFG}"
    if [ ! -f "${LPSOLVE_AR}" ] ; then
	echo "${LPSOLVE_AR} has disappeard ! Exiting ..."
	exit 1
    fi
    if [ ! -d "${LPSOLVE_DIR}" ] ; then
	tar -xf "${LPSOLVE_AR}"  || exit 1
    fi
    cd "${LPSOLVE_DIR}/lpsolve55"
    if [ ! -f "bin/${XXLPSOLVE}/liblpsolve55.${LIB_SUFF}" ] ; then
	cp ../../adfg_deps_makefiles/adfg_lpsolve_ccc${OS_SUFF} .
	sh ./adfg_lpsolve_ccc${OS_SUFF}  || exit 1
    fi
    if [ ! -f "bin/${XXLPSOLVE}/liblpsolve55.${LIB_SUFF}" ] ; then
	echo "LpSolve lib cannot be found after compilation ! Exiting ..."
	exit 1
    fi
    cd ../..
else
    rm -rf "lp_solve_5.5"
fi
