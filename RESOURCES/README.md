# RESOURCES

This folder contains user resources needed in any case
to run ADFG tool. There cannot be part of the Eclipse 
repository because of the licences.

## Instructions

If *../devel.config* is created, just run ```./install-wrappers.sh```.

This will build and install the scotch and lpsolve shared libraries
to be used by the JNI (these are juste wrappers, not the operational
libraries).

## Requirements

gcc, g++, make

Scotch and lpsolve55 may be installed first with their own
installation process. Otherwise these two libraries will be
automatically installed in *libs_to_install*.

## Source code redistribution

- LpSolve java wrapper, made by Juergen Ebert, under LGPL licence.
- Scotch java wrapper, made by Adnan Bouakaz and Alexandre Honorat.

- (official archive) SCOTCH 6.0.6
- (official archive) LpSolve 5.5
