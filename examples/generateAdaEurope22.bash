#!/bin/bash

rm -rf res-AdaEurope22
mkdir res-AdaEurope22

## 0. call ADFG and generate adfg/cheddar/dot+svg files
##    and parse and aggregate ADFG results with awk/bash


### all sdf files, for 1 and 4 procs
./generateTestFiles.bash sdf3/sdf res-AdaEurope22 sdfAll.conf
./generateTestFiles.bash sdf3/csdf res-AdaEurope22 sdfAll.conf
### only Beamformer, for multiple procs
./generateTestFiles.bash sdf3/sdf res-AdaEurope22 beamformerMult.conf

# ### finally aggregate
./generateDatFiles.bash res-AdaEurope22


## 1. pure ADFG results

./generateChartsFiles.py res-AdaEurope22.dat

### generate each plot
./histogramClustered.bash adfg_SP_UNI_bothBS.dat
./histogramClustered.bash adfgUniBS.dat
./histogramClustered.bash adfg_SP_UNI_bothU.dat
./histogramClustered.bash adfgUniU.dat
# ./histogramClustered.bash adfgMultCrossingEdges.dat
# ./histogramClustered.bash adfgMultEffectiveParts.dat
# ./graphPlot.bash adfgMultExecTime_Beamformer.dat
./graphPlot.bash adfgMultThroughput_Beamformer.dat

### SRTA chart is not generated automatically ...
# ./histogramClustered.bash adfgSRTA.dat

### clean a little bit
mkdir -p res-AdaEurope22/chartsFiles
mv adfg*.dat res-AdaEurope22/chartsFiles
mv adfg*.pdf res-AdaEurope22/chartsFiles
mv res-AdaEurope22.* res-AdaEurope22
mv res-AdaEurope22_escaped.* res-AdaEurope22


## 2. cheddar results (TODO)

## run Cheddar somehow ... and generate res-AdaEurope22-cheddar.csv
# ./runCheddar.bash res-AdaEurope22
# ./generateChartsFile res-AdaEurope22-cheddar.csv
# ./histogramClustered.bash cheddarPreempt_p1.dat
# ./histogramClustered.bash cheddarTokenMax_p4.dat

### clean a little bit
# mkdir -p res-AdaEurope22/chartsFiles
# mv cheddar*.dat res-AdaEurope22/chartsFiles
# mv cheddar*.pdf res-AdaEurope22/chartsFiles
# mv res-AdaEurope22-cheddar.csv res-AdaEurope22

