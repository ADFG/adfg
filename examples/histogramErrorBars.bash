#!/bin/bash

# --------------------------------------------------------
# Usage: 
# >> ./histogramErrorBars.bash $1
#    $1 = data file 
#
# Assumed: 
#    gnuplot installed
# --------------------------------------------------------

if [ $# -lt 1 -o -z "$1" -o ! -f "$1" ]; then
    echo ""
    echo "ERROR, Syntax: $0 <data file>"
    echo ""
    exit 1
fi


pdf=$(basename "$1" ".dat")".pdf"
cols=$(sed -n 2,2p "$1" | awk '{print NF; exit}')
ytitl=$(gawk 'match($0,/^#yaxis: (.*)/,a) {print a[1]; exit}' "$1")

gnuplot <<EOF
set terminal pdf truecolor size 9,3

set style data histograms
set style histogram errorbars gap 1 lw 1 #clustered
set style fill transparent solid 0.95 border lc "black"
set datafile missing 'NA'

set xtics rotate by -30 autojustify font "Verdana,12" 

set key on outside horizontal bottom box font "Verdana,10" autotitle columnhead
set output "${pdf}" 
set ylabel "${ytitl}" font "Verdana,12" offset 2.5,0

set bars front

if (${cols} > 4) {
    set yrange [0:*]
    plot "$1" u 2:3:4:xtic(1) ti columnhead(2), for [i=5:${cols}:3] "" u i:(column(i+1)):(column(i+2)) ti columnhead(i)
} else {
    set yrange [0.1:*]
    set logscale y
    plot "$1" u 2:3:4:xtic(1) ti columnhead(2)
}

EOF
