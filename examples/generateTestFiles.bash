#!/bin/bash

# -----------------------------------------------------------------------------------
# Usage: 
# >> ./generateTestFiles.bash $1 $2 $3
#    $1 = directory to get all SDF3 .xml files 
#    $2 = directory to put the analysed files
#    $3 = configuration file
# 
# Assumed: 
#    the command adfg_sched is known in .bash_aliases or by the PATH shell variable
# ------------------------------------------------------------------------------------


if [ $# -ne 3 -o -z "$1" -o -z "$2" -o -z "$3" ]; then
    echo ""
    echo "ERROR, Syntax: $0 <directory to get all sdf3 files> <directory to put the results> <file.conf>"
    echo ""
    exit 1
fi

trap '
  trap - INT # restore default INT handler
  kill -s INT "$$"
' INT

shopt -s expand_aliases
. ${HOME}/.bash_aliases

## list of common setups:

#delayOpt="" #put "-z" if you want to activate the zero delay option
#declare -a schedUniTypes=("EDF_UNI" "EDF_UNI_DA" "EDF_UNI_HDA" "EDF_UNI_PQPA" "SP_UNI" "SP_UNI_UDH" "SP_UNI_LOP") #all
#declare -a schedUniTypes=("EDF_UNI" "SP_UNI" "SP_UNI_UDH" "SP_UNI_LOP") #all except deadline adjustement
#declare -a schedUniTypes=("EDF_UNI_DA" "EDF_UNI_HDA") #only deadline adjustement
#declare -a schedMultTypes=("SP_MULT_BF_FBBFFD" "SP_MULT_BF_SRTA" "GEDF_MULT" "EDF_MULT_BF_UF_ID" "EDF_MULT_BF_SQPA_CD" "EDF_MULT_BF_PQPA") #all
#declare -a schedMultTypes=("SP_MULT_BF_FBBFFD" "SP_MULT_BF_SRTA" "EDF_MULT_BF_UF_ID")
#declare -a schedMultScotchTypes=("EDF_MULT_MBS_HDA" "EDF_MULT_MBS_ID" "SP_MULT_MBS") #all
#declare -a schedMultScotchTypes=("EDF_MULT_MBS_HDA") #only deadline adjustment
#declare -a stratScotchTypes=("BUFFER_MIN" "THROUGHPUT_MAX" "BALANCED")
#declare -a procs=("4")
#declare -a procs=("5" "10" "15" "20" "25" "30" "35" "40" "45" "50")
#declare -a procs=("8" "12" "16" "20" "24" "28" "32" "36" "40" "44" "48" "52")

# this line is probably better if files have weird names with new lines for example
#declare -a files=$(echo "("; find $1 -maxdepth 1 -name '*.xml' -printf '"%p" '; echo ")")

#declare -a files=( $(find $1 -maxdepth 1 -name '*.xml' -printf '%p ') )
#files+=( $(find $1/hard_acyclic -maxdepth 1 -name '*.xml' -printf '%p ') )
#declare -a files=( $(find $1 -maxdepth 1 -name 'Beamformer.xml' -printf '%p ') )


## in order to generate all data for Ada Europe 22 paper,
## you have to execute this file multiple times with following configurations:

### all sdf files, for 1 and 4 procs
### ./generateTestFiles.bash sdf3/sdf res-AdaEurope22 sdfAll.conf
### ./generateTestFiles.bash sdf3/csdf res-AdaEurope22 sdfAll.conf
### only Beamformer, for multiple procs
### ./generateTestFiles.bash sdf3/sdf res-AdaEurope22 beamformerMult.conf

source "$3"


echo "----- $(date)" >> "$2/analysis.log"
echo "${files[*]}" >> "$2/analysis.log"

# Cleaning
# for file in "${files[@]}"; do
#     if [ -f "${file}" ]; then
#         base=$(basename ${file} .xml)
#         directory="${base}-analyses"
#         rm -rf "$2/${directory}"
#     fi
# done

# $1 is $file, $2 is $base, $3 is $directory, $4 is $schedType, $5 is $nbProcs, $6 is $strat
function execAdfg {
    st=""
    if [ "$6" != "NA" ]; then
        st="-s $6"
    fi
    echo -e "  $4 -p $5 ${st}" | tee -a "$3/../analysis.log"
    fileBase="$3/$2-$4p$5s$6"
    xmlNewFile="${fileBase}.adfg"
    cheddarNewFile="${fileBase}"
    dotNewFile="${fileBase}"
    runCommand="LANG=C adfg_sched -n ${xmlNewFile} -c ${cheddarNewFile} -d ${dotNewFile}.dot -p $5 ${st} $1 $4 ${delayOpt} -v"
    resultFile="${fileBase}.res"
    echo -e "->Commande\t"${runCommand} > "${resultFile}"
    echo -e "->Name\t"$2 >> "${resultFile}"
    echo -e "->SchedType\t"$4 >> "${resultFile}"
    echo -e "->NbProcs\t"$5 >> "${resultFile}"
    echo -e "->Strat\t"$6 >> "${resultFile}"
    eval ${runCommand} >> "${resultFile}" 2>&1
    exitStatus=$?
    if [ ${exitStatus} -ne 0 ]; then
        echo "Something went wrong with the command:" | tee -a "$3/../analysis.log"
        echo ">> ${runCommand}" | tee -a "$3/../analysis.log"
        echo "which exited with status ${exitStatus}." | tee -a "$3/../analysis.log"        
    fi
    dot -Tsvg -o ${dotNewFile}.svg ${dotNewFile}.dot
}


for file in "${files[@]}"; do
    if [ -f "${file}" ]; then
        base=$(basename ${file} .xml)
        directory="$2/${base}-analyses"
        mkdir -p "${directory}"
        echo "===> File ${file}" | tee -a "$2/analysis.log"

        #Uni proc
        for schedType in "${schedUniTypes[@]}"; do
	    execAdfg ${file} ${base} ${directory} ${schedType} "1" "NA"
        done

        for nbProcs in "${procs[@]}"; do
            # Multi proc
            for schedType in "${schedMultTypes[@]}"; do
                execAdfg ${file} ${base} ${directory} ${schedType} ${nbProcs} "NA"
            done

            # Multi proc Scotch
            for schedType in "${schedMultScotchTypes[@]}"; do
                for strat in "${stratScotchTypes[@]}"; do
                    execAdfg ${file} ${base} ${directory} ${schedType} ${nbProcs} ${strat}
                done
            done
        done
    fi
done

