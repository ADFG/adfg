#!/bin/bash

# --------------------------------------------------------
# Usage: 
# >> ./histogramClustered.bash $1
#    $1 = data file 
#
# Assumed: 
#    gnuplot installed
# --------------------------------------------------------

if [ $# -lt 1 -o -z "$1" -o ! -f "$1" ]; then
    echo ""
    echo "ERROR, Syntax: $0 <data file>"
    echo ""
    exit 1
fi


pdf=$(basename "$1" ".dat")".pdf"
cols=$(sed -n 2,2p "$1" | awk '{print NF; exit}')
rows=$(wc -l "$1" | cut -d " " -f 1)
ytitl=$(gawk 'match($0,/^#yaxis: (.*)/,a) {print a[1]; exit}' "$1")

## for smaller histograms, you can reduce size and xrange to avoid blanks:
# set terminal pdf truecolor size 3,3 # default size is 6,3 in inches
# set xrange [-0.5:2.5] # for 3 clusters/applications


gnuplot <<EOF
if (${rows} > 5) {
   set terminal pdf truecolor size 6,3
} else {
   set terminal pdf truecolor size 3,3
   set xrange [-0.5:${rows}-2.5]
}

set style data histograms
set style histogram clustered gap 1
set style fill transparent solid 0.95 border lc "black"
set datafile missing 'NA'

set xtics rotate by -60 autojustify font "Verdana,12" 

set grid
set yrange [0:*]

set key on outside horizontal bottom box font "Verdana,10" autotitle columnhead
set output "${pdf}" 
set ylabel "${ytitl}" font "Verdana,12" offset 2,0

plot "$1" u 2:xtic(1), for [i=3:${cols}] "" u i


EOF
