# Example folder

Contains SDF graph examples in SDF3 and adfg formats.
Provides scripts to generate experiments artefacts.

## Scripts:

Scripts should be called in the following order.

- generateTestFiles.bash: runs ADFG on all configurations specified in the script
- generateDatFiles.bash: generate a dat file containing all results, called after the first script
- generateChartsFiles.py: generate dat file for gnuplot, from the previously computed main dat file

- histogramClustered.bash: generate histogram pdf from gnuplot dat file
- graphPlot.bash: generate graph pdf from gnuplot dat file


## Requirements

- the command adfg_sched is known in .bash_aliases or by the PATH shell variable
- bash / sed / sort /gawk (for bash scripts)
- python3.4+ with [sortedcontainers](https://grantjenks.com/docs/sortedcontainers/) (for Python script)
- gnuplot (for bash scripts generating pdf charts)

## Analysis problems:

- EDF_UNI_DA with h263decoder has a very poor utilization factor,
  probably due to the RWP enforced deadlines.
- EDF_UNI_DA with nodes10 finds preemptions (not the case in the prototype).

