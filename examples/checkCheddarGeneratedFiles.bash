#!/bin/bash

declare -a files=( $(find res-cheddar/ -name '*.xmlv3' -print) )

for file in ${files[@]}; do
    name=$(basename ${file} .xmlv3)
    grep -q "${name}" res-cheddar/Result.txt
    if [ $? -ne 0 ]; then
	echo ${name}
    fi
done 
