#!/bin/bash

# --------------------------------------------------------
# Usage: 
# >> ./graphPlot.bash $1
#    $1 = data file 
#
# Assumed: 
#    gnuplot installed
# --------------------------------------------------------

if [ $# -lt 1 -o -z "$1" -o ! -f "$1" ]; then
    echo ""
    echo "ERROR, Syntax: $0 <data file>"
    echo ""
    exit 1
fi


pdf=$(basename "$1" ".dat")".pdf"
cols=$(sed -n 2,2p "$1" | awk '{print NF; exit}')
ytitl=$(gawk 'match($0,/^#yaxis: (.*)/,a) {print a[1]; exit}' "$1")

gnuplot <<EOF
set terminal pdf truecolor size 6,3

set grid
set yrange [0:*]
set xrange [0:*]

set key on outside horizontal bottom box font "Verdana,10" autotitle columnhead
set output "${pdf}" 
set ylabel "${ytitl}" font "Verdana,12" offset 2,0

plot "$1" u 2:xtic(1) with lines, for [i=3:${cols}] "" u i with lines


EOF
