#!/usr/bin/python3

from sys import argv
from sortedcontainers import SortedDict, SortedSet
from math import log
import statistics
import re

adfgDataNameDico = {("File", "NbProcs", "Algo", "Strat"): ("Cycles", "Hyperperiod", "Maximum Processor Utilization", "SSA buffer size", "Execution time (s)", "Repetition Factor", "Throughput", "Average Porcessor Utilization", "ARS buffer size", "Nb Edges Crossing Part.", "Nb Effective Part.")}
cheddarDataNameDico = {("File", "NbProcs", "Algo", "Strat"): ("Minimum buffer utilization", "Maximum buffer utilization", "Average buffer utilization", "Preemptions", "Minimum token utilization", "Maximum token utilization", "Average token utilization")}

sdf3minBS = {"Beamformer": 254, "BitonicSort": 151, "cd2dat_csdf": 5, "ChannelVocoder": 2618, "DCT": 1792, "DES": 2564, "FFT": 8192, "Filterbank": 680, "FMRadio": 57,
             "h263decoder": 1189, "modem": 16, "mp3decoder": 20, "MPEG2noparser": 6274, "pacemaker": 42, "receiver": 2698, "satellite": 1542, "Serpent": 14249, "Vocoder": 668}
# pacemaker_2 not considered
# cd2dat_sdf not considered
# receiver_2 not considered

retainedFile = {'Serpent', 'Vocoder', 'DES', 'Filterbank', 'ChannelVocoder', 'Beamformer', 'mp3decoder', 'h263decoder', 'cd2dat_csdf', 'pacemaker', 'receiver'}

csdfFile = {'cd2dat_csdf', 'pacemaker', 'receiver'}

spMultPolicies = SortedSet({'SP_MULT_MBS', 'SP_MULT_BF_FBBFFD', 'SP_MULT_BF_SRTA', 'EDF_MULT_BF_UF_ID'})
spMultExtPolicies = SortedSet({'SP_MULT_MBS-BALANCED', 'SP_MULT_MBS-BUFFER_MIN', 'SP_MULT_MBS-THROUGHPUT_MAX', 'SP_MULT_BF_FBBFFD', 'SP_MULT_BF_SRTA', 'EDF_MULT_BF_UF_ID'})
edfMultPolicies = SortedSet({'EDF_MULT_MBS_ID', 'EDF_MULT_BF_UF_ID', 'EDF_MULT_BF_PQPA'})
edfMultExtPolicies = SortedSet({'EDF_MULT_MBS_ID-BALANCED', 'EDF_MULT_MBS_ID-BUFFER_MIN', 'EDF_MULT_MBS_ID-THROUGHPUT_MAX', 'EDF_MULT_BF_UF_ID', 'EDF_MULT_BF_PQPA'})

bufUtExtPolicies = []
for pol in edfMultExtPolicies:
    bufUtExtPolicies.append(pol)
    bufUtExtPolicies.append("Min")
    bufUtExtPolicies.append("Max")    
preemptPolicies = SortedSet({'EDF_UNI', 'EDF_UNI_DA', 'EDF_UNI_HDA', 'EDF_UNI_PQPA'})
tecsUniPolicies = SortedSet({'EDF_UNI', 'SP_UNI', 'SP_UNI_LOP', 'SP_UNI_UDH'})
nbProcs = 4


def parseAdfgData(datFile):
    dico = {}
    with open(datFile) as f:
        for i,line in enumerate(f):
            # skip first line (names)
            if (i == 0):
                continue
            cols = line.split();
            if (len(cols) < 4):
                continue
            tup1 = (cols[0], int(cols[1]), cols[2], cols[3])
            # incomplete data, considered as failure
            tup2 = (0, 0, 0.0, 0, 0.0, 0, 0.0, 0.0, 0, 0, 0)
            if (len(cols) >= 15):
                tup2 = (int(cols[4]), int(cols[5]), float(cols[6]), int(cols[7]), float(cols[8]), int(cols[9]), float(cols[10]), float(cols[11]), int(cols[12]), int(cols[13]), int(cols[14]))
            elif (len(cols) >= 13):
                tup2 = (int(cols[4]), int(cols[5]), float(cols[6]), int(cols[7]), float(cols[8]), int(cols[9]), float(cols[10]), float(cols[11]), int(cols[12]), 0, 0)
            dico[tup1] = tup2
    return dico


def parseCheddarData(csvFile):
    dico = {}
    regex = re.compile('(?P<file>\w+)-(?P<algo>\w+)p(?P<proc>\d+)s(?P<strat>\w+)')
    with open(csvFile) as f:
        for line in f:
            cols = line.split(',')
            if (len(cols) < 8):
                print("Attention : problem on following line\n")
                print("--> {}\n".format(line))
                continue
            cols = [x.strip() for x in cols]
            rawFile = (cols[0])[:-6] # remove.xmlv3
            m = regex.match(rawFile)
            if (not m):
                print("Attention : problem on following line\n")
                print("--> {}\n".format(line))
                continue
            fileName = m.group('file')
            algo = m.group('algo')
            p = int(m.group('proc'))
            strat = m.group('strat')
            tup1 = (fileName, p, algo, strat)
            tup2 = (float(cols[1]), float(cols[2]), float(cols[3]), int(cols[4]), float(cols[5]), float(cols[6]), float(cols[7]))
            dico[tup1] = tup2
    return dico

def escapePolicies(policiesSet):
    res = []
    for x in policiesSet:
        res.append(x.replace('_','\\\\_'))
    return res

        
## used for results of rtns 17 Paper            
def generateCheddarRtnsDatasToWrite(globalDico):
    dicoBufAvgUt = SortedDict()
    dicoPreempt = SortedDict()
    logbasis = 10
    for (fileName, p, algo, strat), value in globalDico.items():
        #if (fileName == 'pacemaker_2' or fileName == 'pipeline'):
        if (fileName not in retainedFile):
            continue
        if (algo in edfMultPolicies and p == nbProcs):
            # fill dicoBufAvgUt
            valuesDico = dicoBufAvgUt.get(fileName)
            if (valuesDico is None):
                valuesDico = SortedDict()
                dicoBufAvgUt[fileName] = valuesDico
            extendedPolicy = algo + "-" + strat
            valuesDico[extendedPolicy] = (value[2], value[1], value[0])
        if (algo in preemptPolicies):
            # fill dicoPreempt
            valuesDico = dicoPreempt.get(fileName)
            if (valuesDico is None):
                valuesDico = SortedDict()
                dicoPreempt[fileName] = valuesDico
            valuesDico[algo] = 0 if (value[3] == 0) else log(value[3], logbasis)
    #now we print lines to file
    with open('cheddarBufferUt_p'+str(nbProcs)+'.dat', 'w') as f:
        f.write("#yaxis: Average buffer utilization (%)\n")
        f.write("{:<20} {:>33} {:>4} {:>4} {:>33} {:>4} {:>4} {:>33} {:>4} {:>4} {:>33} {:>4} {:>4} {:>33} {:>4} {:>4}\n".
                format('File', *(escapePolicies(bufUtExtPolicies))))
        for fileName, value in dicoBufAvgUt.items():
            values=[]
            for x in value.values():
                for i in range(0, 3):
                    values.append(x[i])
            f.write("{:<20} {:>33.0%} {:>4.0%} {:>4.0%} {:>33.0%} {:>4.0%} {:>4.0%} {:>33.0%} {:>4.0%} {:>4.0%} {:>33.0%} {:>4.0%} {:>4.0%} {:>33.0%} {:>4.0%} {:>4.0%}\n".
                    format(fileName.replace('_','\\\\_'), *(values)))
    with open('cheddarPreempt_p1.dat', 'w') as f:
        f.write("#yaxis: Number of preemptions (log_{"+str(logbasis)+"})\n")
        f.write("{:<20} {:>33} {:>33} {:>33} {:>33}\n".format('File', *(escapePolicies(preemptPolicies))))
        for fileName, value in dicoPreempt.items():
            f.write("{:<20} {:>33} {:>33} {:>33} {:>33}\n".format(fileName.replace('_','\\\\_'), *(value.values())))


## used for results of tecs 19 Paper / Ada Europe 22 / SI memocode 23
def generateCheddarTecsDatasToWrite(globalDico):
    dicoMultTU = SortedDict()
    dicoUniP = SortedDict()
    sumTU = 0
    lenTU = 0
    logbasis = 2
    nbProcs = 4
    for (fileName, p, algo, strat), value in globalDico.items():
        if (fileName not in retainedFile):
            continue
        if (algo in tecsUniPolicies):
            # fill dicoUniP
            valuesDico = dicoUniP.get(fileName)
            if (valuesDico is None):
                valuesDico = SortedDict()
                dicoUniP[fileName] = valuesDico
            extendedPolicy = algo
            valuesDico[extendedPolicy] = 0 if value[3] == 0 else log(value[3], logbasis)
        if (algo in spMultPolicies and p == nbProcs):
            # fill dicoMultTU
            valuesDico = dicoMultTU.get(fileName)
            if (valuesDico is None):
                valuesDico = SortedDict()
                dicoMultTU[fileName] = valuesDico
            valuesDico[algo + "-" + strat] = value[5]
            # update acc to get the average at the end
            sumTU += value[5]
            lenTU += 1

    with open('cheddarPreempt_p1.dat', 'w') as f:
        f.write("#yaxis: Number of preemptions (log_{"+str(logbasis)+"})\n")
        f.write("{:<20} {:>33} {:>33} {:>33} {:>33}\n".format('File', *(escapePolicies(tecsUniPolicies))))
        for fileName, value in dicoUniP.items():
            f.write("{:<20} {:>33} {:>33} {:>33} {:>33}\n".format(fileName.replace('_','\\\\_'), *(value.values())))
    with open('cheddarTokenMax_p' + str(nbProcs) + '.dat', 'w') as f:
        f.write("#yaxis: Percentage of maxmimum total buffer size utilization\n")
        f.write("{:<20} {:>33} {:>33} {:>33} {:>33} {:>33} {:>33}\n".format('File', *(escapePolicies(spMultExtPolicies))))
        for fileName, value in dicoMultTU.items():
            f.write("{:<20} {:>33} {:>33} {:>33} {:>33} {:>33} {:>33}\n".format(fileName.replace('_','\\\\_'), *(value.values())))
    print("Average of token max utilization on selected files and analysis is {:.2f}.".format(sumTU/lenTU))

            
            
## used for results of rtns 17 Paper               
def generateAdfgRtnsDatasToWrite(globalDico):
    dicoMultBS = SortedDict()
    dicoMultU = SortedDict()    
    dicoUniU = SortedDict()
    dicoExecTime = SortedDict()
    logbasis = 2
    for (fileName, p, algo, strat), value in globalDico.items():
        #if (fileName == 'pacemaker_2' or fileName == 'pipeline'):
        if (fileName not in retainedFile):
            continue
        if (algo in edfMultPolicies and p == nbProcs):
            if (fileName in sdf3minBS.keys()):
                # fill dicoMultBS
                valuesDico = dicoMultBS.get(fileName)
                if (valuesDico is None):
                    valuesDico = SortedDict({"minBS": log(sdf3minBS[fileName], logbasis)})
                    dicoMultBS[fileName] = valuesDico
                extendedPolicy = algo + "-" + strat
                valuesDico[extendedPolicy] = 0 if (value[3] == 0) else log(value[3], logbasis)
            # fill dicoMultU
            valuesDico = dicoMultU.get(fileName)
            if (valuesDico is None):
                valuesDico = SortedDict()
                dicoMultU[fileName] = valuesDico
            extendedPolicy = algo + "-" + strat
            valuesDico[extendedPolicy] = value[2]
        if (algo in preemptPolicies):
            # fill dicoUniU
            valuesDico = dicoUniU.get(fileName)
            if (valuesDico is None):
                valuesDico = SortedDict()
                dicoUniU[fileName] = valuesDico
            valuesDico[algo] = value[2]
        valuesDico = dicoExecTime.get(algo)
        if (valuesDico is None):
            valuesDico = []
            dicoExecTime[algo] = valuesDico
        valuesDico.append(value[4]*1000) # log(value[4], 10) + 3 # == log_10 (execTime*1000)
    #now we print lines to file
    with open('adfgMultBS_p'+str(nbProcs)+'.dat', 'w') as f:
        f.write("#yaxis: Total buffer size (log_{"+str(logbasis)+"})\n")
        f.write("{:<20} {:>33} {:>33} {:>33} {:>33} {:>33} {:>20}\n".format('File', *(escapePolicies(edfMultExtPolicies)), "minBS"))
        for fileName, value in dicoMultBS.items():
            f.write("{:<20} {:>33} {:>33} {:>33} {:>33} {:>33} {:>20}\n".format(fileName.replace('_','\\\\_'), *(value.values())))
    with open('adfgMultU_p'+str(nbProcs)+'.dat', 'w') as f:
        f.write("#yaxis: Total utilization factor\n")
        f.write("{:<20} {:>33} {:>33} {:>33} {:>33} {:>33}\n".format('File', *(escapePolicies(edfMultExtPolicies))))
        for fileName, value in dicoMultU.items():
            f.write("{:<20} {:>33} {:>33} {:>33} {:>33} {:>33}\n".format(fileName.replace('_','\\\\_'), *(value.values())))
    with open('adfgUniU.dat', 'w') as f:
        f.write("#yaxis: Utilization factor\n")
        f.write("{:<20} {:>33} {:>33} {:>33} {:>33}\n".format('File', *(escapePolicies(preemptPolicies))))
        for fileName, value in dicoUniU.items():
            f.write("{:<20} {:>33} {:>33} {:>33} {:>33}\n".format(fileName.replace('_','\\\\_'), *(value.values())))
    with open('adfgExecTime.dat', 'w') as f:
        f.write("#yaxis: Execution time (ms)\n")
        f.write("{:<33} {:>20} {:>20} {:>20}\n".format('Algo', "Average", "Min", "Max"))
        for algo, value in dicoExecTime.items():
            f.write("{:<33} {:>20.4} {:>20.4} {:>20.4}\n".format(algo.replace('_','\\\\_'), statistics.mean(value), min(value), max(value)))


## used for results of tecs 19 Paper / Ada Europe 22 / SI memocode 23          
def generateAdfgTecsDatasToWrite(globalDico):
    dicoUniBS = SortedDict()
    dicoUniU = SortedDict()
    dicoMultCrossingEdges = SortedDict()
    dicoMultEffectiveParts = SortedDict()
    dicoMultThroughput = SortedDict()    
    dicoMultExecTime = SortedDict()
    dicoAllExecTime = SortedDict()
    dico_SP_UNI_bothBS = SortedDict()
    dico_SP_UNI_bothU = SortedDict()
    logbasis = 2
    for (fileName, p, algo, strat), value in globalDico.items():
        #if (fileName == 'pacemaker_2' or fileName == 'pipeline'):
        if (fileName not in retainedFile):
            continue
        valuesDicoTime = dicoAllExecTime.get(algo+ "-" +strat)
        if (valuesDicoTime is None):
            valuesDicoTime = []
            dicoAllExecTime[algo+ "-" +strat] = valuesDicoTime
        valuesDicoTime.append(value[4])
            
        if (algo in tecsUniPolicies):
            if (fileName in sdf3minBS.keys()):
                # fill dicoUniBS
                valuesDico = dicoUniBS.get(fileName)
                if (valuesDico is None):
                    valuesDico = SortedDict({"minBS": log(sdf3minBS[fileName], logbasis)})
                    dicoUniBS[fileName] = valuesDico
                extendedPolicy = algo
                valuesDico[extendedPolicy] = 0 if (value[3] == 0) else log(value[3], logbasis)
            # fill dicoUniU
            valuesDico = dicoUniU.get(fileName)
            if (valuesDico is None):
                valuesDico = SortedDict()
                dicoUniU[fileName] = valuesDico
            extendedPolicy = algo
            valuesDico[extendedPolicy] = value[2]
        if (algo in spMultPolicies and p == 4):
            # fill dicoMultCrossingEdges
            valuesDico = dicoMultCrossingEdges.get(fileName)
            if (valuesDico is None):
                valuesDico = SortedDict()
                dicoMultCrossingEdges[fileName] = valuesDico
            valuesDico[algo + "-" + strat] = value[9]
            # fill dicoMultEffectiveParts
            valuesDico = dicoMultEffectiveParts.get(fileName)
            if (valuesDico is None):
                valuesDico = SortedDict()
                dicoMultEffectiveParts[fileName] = valuesDico
            valuesDico[algo + "-" + strat] = value[10]
        if (algo in spMultPolicies and fileName == "Beamformer" and p >= 4):
            # fill dicoMultThroughput
            valuesDico = dicoMultThroughput.get(p)
            if (valuesDico is None):
                valuesDico = SortedDict()
                dicoMultThroughput[p] = valuesDico
            valuesDico[algo + "-" + strat] = value[6]
            # fill dicoMultExecTime
            valuesDico = dicoMultExecTime.get(p)
            if (valuesDico is None):
                valuesDico = SortedDict()
                dicoMultExecTime[p] = valuesDico
            valuesDico[algo + "-" + strat] = value[4]*1000 # log(value[4], 10) + 3 # == log_10 (execTime*1000)
        if (fileName in csdfFile and algo == 'SP_UNI'):
            # fill dico_SP_UNI_bothBS
            valuesDico = dico_SP_UNI_bothBS.get(fileName)
            if (valuesDico is None):
                valuesDico = SortedDict({"minBS": log(sdf3minBS[fileName], logbasis)})
                dico_SP_UNI_bothBS[fileName] = valuesDico
            valuesDico['SSA_simu'] = 0 if (value[3] == 0) else log(value[3], logbasis)
            valuesDico['ARS_ILP'] = 0 if (value[8] == 0) else log(value[8], logbasis)
            # fill dico_SP_UNI_bothU
            valuesDico = dico_SP_UNI_bothU.get(fileName)
            if (valuesDico is None):
                valuesDico = SortedDict()
                dico_SP_UNI_bothU[fileName] = valuesDico
            valuesDico['U_max'] = value[2]
            valuesDico['U_avg'] = value[7]

            
            
    #now we print lines to file
    with open('adfgUniBS.dat', 'w') as f:
        f.write("#yaxis: Total buffer size (log_{"+str(logbasis)+"})\n")
        colNames = SortedSet(tecsUniPolicies)
        colNames.add("minBS")
        f.write("{:<20} {:>33} {:>33} {:>33} {:>33} {:>20}\n".format('File', *(escapePolicies(colNames))))
        for fileName, value in dicoUniBS.items():
            f.write("{:<20} {:>33} {:>33} {:>33} {:>33} {:>20}\n".format(fileName.replace('_','\\\\_'), *(value.values())))
    with open('adfgUniU.dat', 'w') as f:
        f.write("#yaxis: Utilization factor\n")
        f.write("{:<20} {:>33} {:>33} {:>33} {:>33}\n".format('File', *(escapePolicies(tecsUniPolicies))))
        for fileName, value in dicoUniU.items():
            f.write("{:<20} {:>33} {:>33} {:>33} {:>33}\n".format(fileName.replace('_','\\\\_'), *(value.values())))
    with open('adfgMultThroughput_Beamformer.dat', 'w') as f:
        f.write("#yaxis: Throughput\n")
        f.write("{:<20} {:>33} {:>33} {:>33} {:>33} {:>33} {:>33}\n".format('nbProcs', *(escapePolicies(spMultExtPolicies))))
        for nbProc, value in dicoMultThroughput.items():
            f.write("{:<20} {:>33} {:>33} {:>33} {:>33} {:>33} {:>33}\n".format(str(nbProc), *(value.values())))
    with open('adfgMultExecTime_Beamformer.dat', 'w') as f:
        f.write("#yaxis: Execution time (ms)\n")
        f.write("{:<33} {:>33} {:>33} {:>33} {:>33} {:>33} {:>33}\n".format('nbProcs', *(escapePolicies(spMultExtPolicies))))
        for nbProc, value in dicoMultExecTime.items():
            f.write("{:<20} {:>33} {:>33} {:>33} {:>33} {:>33} {:>33}\n".format(str(nbProc), *(value.values())))
    with open('adfgMultCrossingEdges.dat', 'w') as f:
        f.write("#yaxis: Nb. of Crossing Edges\n")
        f.write("{:<20} {:>33} {:>33} {:>33} {:>33} {:>33} {:>33}\n".format('File', *(escapePolicies(spMultExtPolicies))))
        for fileName, value in dicoMultCrossingEdges.items():
            f.write("{:<20} {:>33} {:>33} {:>33} {:>33} {:>33} {:>33}\n".format(fileName.replace('_','\\\\_'), *(value.values())))
    with open('adfgMultEffectiveParts.dat', 'w') as f:
        f.write("#yaxis: Nb. of Effective Partitions\n")
        f.write("{:<33} {:>33} {:>33} {:>33} {:>33} {:>33} {:>33}\n".format('File', *(escapePolicies(spMultExtPolicies))))
        for fileName, value in dicoMultEffectiveParts.items():
            f.write("{:<20} {:>33} {:>33} {:>33} {:>33} {:>33} {:>33}\n".format(fileName.replace('_','\\\\_'), *(value.values())))
    with open('adfg_SP_UNI_bothBS.dat', 'w') as f:
        f.write("#yaxis: Total buffer size (log_{"+str(logbasis)+"})\n")
        colNames = SortedSet(['SSA_simu', 'ARS_ILP'])
        colNames.add("minBS")
        f.write("{:<20} {:>33} {:>33} {:>33}\n".format('File', *(escapePolicies(colNames))))
        for fileName, value in dico_SP_UNI_bothBS.items():
            f.write("{:<20} {:>33} {:>33} {:>33}\n".format(fileName.replace('_','\\\\_'), *(value.values())))
    with open('adfg_SP_UNI_bothU.dat', 'w') as f:
        f.write("#yaxis: Utilization factor\n")
        colNames = SortedSet(['U_max', 'U_avg'])
        f.write("{:<20} {:>33} {:>33}\n".format('File', *(escapePolicies(colNames))))
        for fileName, value in dico_SP_UNI_bothU.items():
            f.write("{:<20} {:>33} {:>33}\n".format(fileName.replace('_','\\\\_'), *(value.values())))


            
    print("Exec times: min / avg / max")
    for algo, times in dicoAllExecTime.items():
        print("{}:  {:.3} / {:.3} / {:.3} s".format(algo, min(times), sum(times)/len(times), max(times)))
            
        
if __name__ == '__main__':
    if (len(argv) < 2):
        raise Exception('Adfg(.dat) or Cheddar(.csv) res file to parse needed as 1st arg.')
    fileToParse = argv[1]
    if (fileToParse.endswith(".dat")):
        dicoAdfg = parseAdfgData(fileToParse)
        print("{} adfg results parsed!".format(len(dicoAdfg)))
        # generateAdfRtnsgDatasToWrite(dicoAdfg)
        generateAdfgTecsDatasToWrite(dicoAdfg)
    elif (fileToParse.endswith(".csv")):
        dicoCheddar = parseCheddarData(fileToParse)
        print("{} cheddar results parsed!".format(len(dicoCheddar)))
        # generateCheddarRtnsDatasToWrite(dicoCheddar)
        generateCheddarTecsDatasToWrite(dicoCheddar)
    else:
        raise Exception('Unrecognized file extension (allowed: .dat for adfg or .csv for cehddar)')
        
