#!/bin/bash

# --------------------------------------------------------
# Usage: 
# >> ./generateDatFile $1
#    $1 = results directory 
# 
# Assumed: 
#    the .res files have generated first
# --------------------------------------------------------

if [ $# -ne 1 -o -z "$1" ]; then
    echo ""
    echo "ERROR, Syntax: $0 <results directory>"
    echo ""
    exit 1
fi

folderName="$(basename $1)"

declare -a files=( $(find $1 -name '*.res' -printf '%p ') )

printf "%-35s %7s %35s %20s %6s %20s %25s %20s %20s %25s %25s %25s %20s %20s %20s\n" "Nom" "NbProcs" "SchedAlgo" "Strat" "Cycles" "Hyperperiod" "Utot" "BStot" "Time" "repFactor" "Throughput" "Uavg" "BSars" "CrossEdges" "NbParts"> "${folderName}.dat"
## CrossEdges and NbParts have values only in the multiprocessor case


for file in ${files[@]}; do
    ## each log is parsed as many times as the number of metrics to consider
    ## listed in order of appearance in the log
    
    ## nom
    n=$(gawk 'match($0,/^->Name[ \t](.*)/,a) {print a[1]}' "${file}")
    ## procs
    p=$(gawk 'match($0,/^->NbProcs[ \t]([0-9]+)/,a) {print a[1]}' "${file}")
    ## sched algo
    sa=$(gawk 'match($0,/^->SchedType[ \t](.*)/,a) {print a[1]}' "${file}")
    ## sched strategy
    st=$(gawk 'match($0,/^->Strat[ \t](.*)/,a) {print a[1]}' "${file}")

    ## numer of cycles
    c=$(gawk 'match($0,/^([0-9]+) cycle basis/,a) {print a[1]}' "${file}")
    ## number of edges crossing partitions, not present if unicore
    ce=$(gawk 'match($0,/^Number of affine relations crossing partitions: ([0-9]+)/,a) {print a[1]}' "${file}")
    ## number of effective partitions, not present if unicore
    ep=$(gawk 'match($0,/^Number of effective partitions: ([0-9]+)/,a) {print a[1]}' "${file}")
    
    ## BStot approximated per ARS ILP
    bsa=$(gawk 'match($0,/^ILP Approximate value of the sum of buffer sizes = ([0-9]+)/,a) {print a[1]}' "${file}")
    ## Utot /!\ dans here, "INFOS" prefix seems dependent on a Java lib and could change in the future
    u=$(gawk 'match($0,/^INFO[S]?: Processor utilization factor U = ([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)/,a) {print a[1]}' "${file}")
    ## hyperperiod (one per weakly connected component)
    h=$(gawk 'match($0,/^-- \(basis = (.*)\) hyperperiod = ([0-9]+)/,a) {print a[2]}' "${file}")
    ## repFactor (one per weakly connected component)
    r=$(gawk 'match($0,/, repFactor = ([0-9]+)/,a) {print a[1]}' "${file}")
    ## throughput (one per weakly connected component)
    th=$(gawk 'match($0,/, throughput = ([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)/,a) {print a[1]}' "${file}")

    ## Uavg
    ua=$(gawk 'match($0,/^Average cyclo-processor utilization factor avgCycloU = ([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)/,a) {print a[1]}' "${file}")    
    ## BStot simulated per FIFO
    bss=$(gawk 'match($0,/^Sum of buffer sizes = ([0-9]+)/,a) {print a[1]}' "${file}")
    ## time
    t=$(gawk 'match($0,/^# Time \(s\): ([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)/,a) {print a[1]}' "${file}")

    ## print results
    printf "%-35s %7s %35s %20s %6s %20s %25s %20s %20s %25s %25s %25s %20s %20s %20s\n" ${n} ${p} ${sa} ${st} ${c} ${h} ${u} ${bss} ${t} ${r} ${th} ${ua} ${bsa} ${ce} ${ep} >> "${folderName}_tmp"
    
done

sort -k 1,1 -k 2n,2n -k 3,3 -k 4,4 "${folderName}_tmp" >> "${folderName}.dat"
cp "${folderName}.dat" "${folderName}_escaped.dat"
sed -i 's/_/\\\\_/g' "${folderName}_escaped.dat"
cp "${folderName}.dat" "${folderName}.csv"
sed -i 's/  */;/g' "${folderName}.csv"

rm "${folderName}_tmp"
