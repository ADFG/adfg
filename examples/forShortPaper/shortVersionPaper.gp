set terminal pdf truecolor size 6.5,3.5

set output "adfgMult_p4.pdf"
set multiplot #layout 2,2

set style data histograms
set style fill transparent solid 0.95 border lc "black"
set bars front
set grid
set yrange [0:*]

set size 0.6,0.5
set origin 0.43,0.5
set title "(b)" offset 0,-0.8
set style histogram clustered gap 1
set xrange [-0.75:7.75]
set key on box font "Verdana,10" autotitle columnhead bottom left at screen 0.05, 0.06
unset xtics
set ytics 3
set ylabel "Total buffer size (log_{2})" font "Verdana,11" offset 2,0
plot "adfgMultBS_p4.dat" u 2:xtic(1), for [i=3:7] "" u i

set size 0.46,0.8
set origin 0,0.2
set title "(a)" offset 0,-0.8
set style histogram clustered gap 1
set xrange [-0.75:7.75]
set xtics rotate by -75 autojustify font "Verdana,11"
set ytics 1
set key off
set ylabel "Total utilization factor" font "Verdana,11" offset 2,0
plot "adfgMultU_p4.dat" u 2:xtic(1), for [i=3:6] "" u i

set size 0.6,0.55
set origin 0.43,0.02
set title "(c)" offset 0,-0.8
set style histogram errorbars gap 1 lw 1
set xrange [-0.75:7.75]
unset xtics
set ytics 0,20,80
set key off
set ylabel "Average buffer utilization (%)" font "Verdana,11" offset 2,0
plot for [i=2:16:3] "cheddarBufferUt_p4.dat" u i:(column(i+1)):(column(i+2)) ti columnhead(i)



