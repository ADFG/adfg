package fr.inria.tea.adfg.docandtaglets;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.ElementScanner9;

import com.sun.source.doctree.DocCommentTree;
import com.sun.source.doctree.UnknownBlockTagTree;
import com.sun.source.util.DocTrees;
import com.sun.source.util.SimpleDocTreeVisitor;

import jdk.javadoc.doclet.Doclet;
import jdk.javadoc.doclet.DocletEnvironment;
import jdk.javadoc.doclet.Reporter;

/**
 * A doclet that access todo tags and print a latex file of them. Using API of Java 9+.
 * @see <a href="https://openjdk.java.net/groups/compiler/using-new-doclet.html#tags">Adapted from Open JDK documentation</a>
 */
public class TodoLatexDoclet9 implements Doclet {

	private DocTrees treeUtils;

	@Override
	public void init(Locale locale, Reporter reporter) {
		// do nothing
	}

	@Override
	public String getName() {
		return getClass().getSimpleName();
	}

	@Override
	public Set<? extends Option> getSupportedOptions() {
		return Collections.emptySet();
	}

	@Override
	public SourceVersion getSupportedSourceVersion() {
		return SourceVersion.latest();
	}


	@Override
	public boolean run(DocletEnvironment environment) {
		treeUtils = environment.getDocTrees();
		try (final FileWriter texOutput = new FileWriter("todo9.tex")) {
			texOutput.write("\\section{Todo}\n\n\n");
			
			PrintTodoTagsForLatex st = new PrintTodoTagsForLatex(texOutput);
			st.show(environment.getSpecifiedElements());
		} catch (IOException e) {
			e.printStackTrace();
			return false; // not ok
		}
		return true; // ok
	}

	/**
	 * A scanner to search for elements with documentation comments,
	 * and to examine those comments for custom tags.
	 */
	class PrintTodoTagsForLatex extends ElementScanner9<Void, Integer> {
		final FileWriter fw;
		final LinkedList<Element> stackElements;

		PrintTodoTagsForLatex(final FileWriter fw) {
			this.fw = fw;
			this.stackElements = new LinkedList<>();
		}

		void show(Set<? extends Element> elements) {
			scan(elements, 0);
		}
		
		@Override
		public Void scan(Element e, Integer depth) {
			// first empty the stack if we went back to a parent level
			final int stackGap = stackElements.size() - depth;
			for (int i = 0 ; i < stackGap; i++) {
				stackElements.removeLast();
			}
			stackElements.addLast(e);

			DocCommentTree dcTree = treeUtils.getDocCommentTree(e);
			if (dcTree != null) {
				final List<String> tags = new ArrayList<>();
				new TodoTagScanner(tags).visit(dcTree, null);
				if (!tags.isEmpty()) {
					final String rawTitle = stackElements.stream().map(TodoLatexDoclet9::getSimpleName).collect(Collectors.joining());
					for (final String todo: tags) {
						try {
							String title = shortenAndEscape(rawTitle);
							fw.write("\\subsection*{"+title+"}\n\n");
							fw.write(escapeForLatex(todo));
							fw.write("\n\n");
						} catch (IOException e1) {
							e1.printStackTrace();
						}
					};
				}
			}
			return super.scan(e, depth + 1);
		}
	}

	/**
	 * A visitor to gather the block tags found in a comment.
	 */
	static class TodoTagScanner extends SimpleDocTreeVisitor<Void, List<Element>> {
		private final List<String> tags;

		TodoTagScanner(List<String> tags) {
			this.tags = tags;
		}

		@Override
		public Void visitDocComment(DocCommentTree tree, List<Element> p) {
			return visit(tree.getBlockTags(), null);
		}

		@Override
		public Void visitUnknownBlockTag(UnknownBlockTagTree tree, List<Element> p) {
			final String tagName = tree.getTagName();
			if (tagName.equals("todo")) {
				tags.add(tree.getContent().toString());
			}
			return null;
		}
	}
	
	/**
	 * Retrieve the name of the element and prefix by the right character.
	 * 
	 * @param e Element to consider.
	 * @return Element name.
	 */
	static String getSimpleName(Element e) {
		if (e instanceof PackageElement) {
			final PackageElement qn = (PackageElement) e;
			return qn.getQualifiedName().toString();
		} else if (e instanceof ExecutableElement) {
			final ExecutableElement ee = (ExecutableElement) e;
			String ret = "#"+ee.getSimpleName().toString()+"(";
			ret += ee.getParameters().stream().map(x -> x.getSimpleName().toString()).collect(Collectors.joining(","));
			return ret + ")";
		} else if (e instanceof TypeElement) {
			return "."+e.getSimpleName().toString();
		}
		return "#"+e.getSimpleName().toString();
	}

	/**
	 * Escape the _, &quot;, &amp;, &lt; and &gt; characters, also change
	 * <code>{@code ...\}....}</code> in <code>\texttt{...}</code>.
	 * 
	 * @param text String to escape.
	 * @return New string ready for LaTeX printing.
	 */
	static String escapeForLatex(String text) {
        Pattern pattern = Pattern.compile("\\{@code[ ]?(((\\\\})?[^\\}]?)*)\\}", Pattern.DOTALL);
        Matcher matcher = pattern.matcher(text);
        String tmp = matcher.replaceAll("\\\\texttt{$1}");
		
		tmp = tmp.replace("_", "\\_");
		tmp = tmp.replace("#", "\\#");

		tmp = tmp.replace("&amp;", "\\&");
		tmp = tmp.replace("&lt;", "$<$");
		tmp = tmp.replace("&gt;", "$>$");
		tmp = tmp.replace("&quot;", "\"");
		return tmp;
	}
	
	/**
	 * Remove qualifier prefix of our project:
	 * <code>fr.inria.tea.adfg.</code> and escape it.
	 * 
	 * @param text String to shorten and escape.
	 * @return New string ready for LaTeX printing.
	 */
	static String shortenAndEscape(String text) {
		String shortVersion = text.replace("fr.inria.tea.adfg.", "");
		return escapeForLatex(shortVersion);
	}
	
}

