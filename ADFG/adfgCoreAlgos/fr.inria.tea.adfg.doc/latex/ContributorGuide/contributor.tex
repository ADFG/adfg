%% -*- eval: (flyspell-mode 1); eval: (ispell-change-dictionary "english"); -*-

\chapter{Contributor guide}
\label{chap:contrib}

\gls{ac:adfg} is not a big project but it involves multiple dependencies
and extension so it is a bit difficult to manage. Any contributor needs
some knowledge about the whole project and especially the build process.
The most important thing to know is the project architecture in order
to know what should be modified and where. Then we detail a bit more
the core of the scheduler, where the Adnan's algorithms are located.
The last section is about some future work.

\section{Project architecture}

The project is divided in some different subfolders corresponding to
different technologies (\textsf{C} vs. \java), different origins 
(because we reuse code of some other people), and different purposes
(to be used by the end user or by the contributor). For the contributor,
main information is to know how to navigate in the project, how to install
it, and what are the requirements.

\subsection{Folders}

The project first level should look like this:
\begin{framed}
\begin{verbatim}
.
├── AA_README.md
├── ADFG
├── examples
├── export.sh
├── install.sh
├── RESOURCES
├── RESOURCES-DEV
├── scriptsDeps
└── Site
\end{verbatim}
\end{framed}

\paragraph{Scripts}
The two shell scripts are quite obvious: \texttt{install.sh} builds the project and
\texttt{export.sh} ships it to be used everywhere (although some parts are OS
dependent). Besides, the \texttt{scriptsDeps} folder contains some scripts to do this
installation or shipping. 

\paragraph{Code}
The code is located in the three capitalized folders: \texttt{ADFG} contains the main
\java code, \texttt{RESOURCES} contains extraneous code in \textsf{C} (the dependencies),
and \texttt{RESOURCES-DEV} contains extraneous code in \java (still dependencies). The
last folder is suffixed with \texttt{DEV} because this code is not intended to be exported
as a standalone: the main jar or jars should include it, so this is reserved code for the
developer only. In the next folder tree you can see that all these folders contains some
subfolders, and \texttt{ADFG} is even several different \maven projects.

\paragraph{Others}
A \emph{readme} is also present, and some examples. The \texttt{Site} folder is reserved
for the \gls{ac:adfg} website, which is a small subset of the \textsf{Polychrony} one.\\

Here is the project tree up to the second and the third levels:

\begin{framed}
\begin{verbatim}
.
├── AA_README.md
├── ADFG
│   ├── adfgCoreAlgos
│   │   ├── aggregator
│   │   ├── fr.inria.tea.adfg.algos
│   │   ├── fr.inria.tea.adfg.cli
│   │   ├── fr.inria.tea.adfg.deps
│   │   ├── fr.inria.tea.adfg.doc
│   │   └── parent
│   ├── adfgEclipsePlugins
│   │   ├── aggregator
│   │   ├── feature
│   │   ├── fr.inria.tea.adfg.elauncher.common
│   │   ├── fr.inria.tea.adfg.elauncher.sdf
│   │   ├── parent
│   │   └── site
│   ├── adfgGraphEditor
│   │   ├── aggregator
│   │   ├── feature
│   │   ├── fr.inria.tea.adfg.model
│   │   ├── fr.inria.tea.adfg.model.diagram
│   │   ├── fr.inria.tea.adfg.model.edit
│   │   ├── fr.inria.tea.adfg.model.editor
│   │   └── parent
│   ├── install-projects.sh
│   └── README.md
├── examples
│   ├── generateTestFiles.sh
│   ├── sdf3
│   └── test-aadl
├── export.sh
├── install.sh
├── libs
├── RESOURCES
│   ├── install-wrappers.sh
│   ├── libs_to_install
│   │   ├── adfg_deps_makefiles
│   │   ├── install-lpsolve.sh
│   │   ├── install-scotch.sh
│   │   ├── lp_solve_5.5.2.5_source.tar.gz
│   │   └── scotch_6.0.6.tar.gz
│   ├── README.md
│   └── wrappers_C
│       ├── lpsolve
│       └── scotch
├── RESOURCES-DEV
│   ├── install-artifacts.sh
│   ├── README.md
│   ├── UMLGraph
│   └── wrappers_Java
│       ├── lpsolvej
│       └── scotchj
├── scriptsDeps
│   ├── create_cmd.sh
│   ├── find_file.sh
│   ├── install_export.sh
│   ├── OSdependent.sh
│   └── requires_func.sh
└── Site
    ├── AA_README.txt
    ├── exported
    ├── mkSite.sh
    ├── Site.md
    └── UserGuide.md
\end{verbatim}
\end{framed}


\subsection{Installation}

The installation can be done by the root \texttt{install.sh} script. If it is the
first installation it will create at the same level a \texttt{devel.config} file
that you must fill with some paths (the most important is the \gls{ac:jdk} path,
if the \texttt{JAVA\_HOME} environment variable is not set).
\java 9+ versions should be preferred, but it works with \java 8 when deactivating
the documentation module \texttt{fr.inria.tea.adfg.doc}. Then you can rerun the 
installation process, and everything should be fine (if you have an Internet
connection, and all required softwares: \maven (version \texttt{3.6.2+} advised),
\textsf{Git}, \textsf{Make}, \textsf{C} compiler such as \texttt{gcc/g++},
basic \textsf{Unix} commands such as \texttt{sed/grep}).\\

One can notice that each code folder also has at least one installation script,
which are called by the main one. It is possible to call them directly, once the
project has been build at least once by the main one; but keep in mind that some
stuff is done only by the main script, for example the compiled dynamic shared
\textsf{C} libraries are copied in a folder \texttt{libs/} at the project root only
by the main script (so any direct call to \texttt{RESOURCES/install-wrappers.sh}
will compile the wrappers but not install them).\\

\subsubsection{Compilation time line}

The whole compilation process is a bit complicated,
here are the chronological steps:
\begin{enumerate}
\item{\texttt{/RESOURCES/} Compilation of \scotch and \lpsolve (if not user provided);}
\item{\texttt{/RESOURCES/} Compilation of their wrappers (\textsf{C} part);}
\item{\texttt{/RESOURCES-DEV/} Download and compilation of the \umlgr project;}
\item{\texttt{/RESOURCES-DEV/} Compilation* of \scotch and \lpsolve wrappers (\java part);}
\item{\sout{\texttt{/ADFG/adfgGraphEditor/} Compilation* of the graphical editor of process graphs;}}
\item{\texttt{/ADFG/adfgCoreAlgos/} Compilation* of the core part;}
\item{\texttt{/ADFG/adfgEclipsePlugins/} Compilation* of the \eclipse plugins;}
\item{\texttt{/libs/} Installation of the shared libraries;}
\item{\texttt{/} Installation of the \texttt{adfg\_sched} local script;}
\item{\texttt{/} Installation of the command in \texttt{.bashrc} or \texttt{.tcshrc} (if paths have
been specified in \texttt{devil.config});}
\item{\texttt{/} Installation of the \eclipse specific library path (if path has
been specified in \texttt{devil.config}).}
\end{enumerate}
Normally, every non working step stops the script which exits with the error code $1$.
\emph{Compilation*} (with an extra star) means that the \maven local repository
installation is done at the same time.


\subsubsection{\maven}

The \java code parts are always managed by \maven, more particularly
the same pattern is respected for all projects in the folder \texttt{ADFG}:
\begin{itemize}
  \item{a \texttt{parent} folder to store the parent \texttt{pom.xml};}
  \item{an \texttt{aggregator} folder to store the main \texttt{pom.xml} (the one to use to compile or install);}
  \item{a \texttt{feature} folder if the project contains \eclipse plugins (i. e. \texttt{adfgEclipsePlugins}
    \sout{and \texttt{adfgGraphEditor}}), to store the \eclipse \texttt{feature.xml} file;}
  \item{a \texttt{site} folder for the main \eclipse project (i. e. \texttt{adfgEclipsePlugins}) to manage 
the build of an \eclipse update site;}
  \item{one or more \emph{modules} (starting with \texttt{fr.inria.tea.adfg}) containing the code.}
\end{itemize}
Even if projects have the same layout, they are not compiled the same way.\\

Indeed \maven can be enhanced with \emph{plugins} \footnote{Originally
the word \emph{plugin} comes from the \eclipse \emph{plugin} concept which consists in standard \java jars (code
archive) with some extra information (\texttt{MANIFEST.MF} and \texttt{plugin.xml} files) dedicated to add
the jars functionalities into the \eclipse \gls{ac:ide}. Notice that the \eclipse
plugin jars can also be used as regular \java jars (but not the opposite).}
which provides new rules and new ways of building a tool and we need some to build at the same time regular
\java projects and specific \eclipse \java projects also called \emph{plugins}. Here we use the same
word for the two contexts: \maven and \eclipse.
Shortly we can say that two specific \maven plugins are used in 
order to generate our \eclipse plugins and also the main application. Theses (\maven) plugins are:
\begin{itemize}
  \item{\texttt{maven tycho plugin}: compiles \eclipse compatible 
    (and thus also working with standard \java) code from \eclipse plugin style projects 
    (i. e. which already define \texttt{MANIFEST.MF} and eventually \texttt{plugin.xml});}
  \item{\texttt{maven bundle plugin}: compiles \eclipse compatible code from regular \java projects 
    (so it creates almost automatically a \texttt{MANIFEST.MF} file).}
\end{itemize}
We need to use these \maven plugins for the two following reasons: we want to build \gls{ac:adfg} \eclipse plugins
(so we need \tycho) and we also want to build an \gls{ac:adfg} single jar including its dependencies
(not possible with \tycho\footnote{\tycho provides compatibility for \eclipse plugins projects
which does not permit to build jar with dependencies
because their main principle is to resolve dependencies dynamically,
see the \href{https://www.osgi.org/}{OSGi} standard used by the \eclipse plugins.})
to be used in command line, using the same (Byte)code without duplicating it
(so we need the \bundle plugin).

The main differences between these plugins during the projects building process
is the dependency management: with \bundle they are managed by the standard \maven \texttt{pom.xml}
file whereas with \tycho they are managed by the \texttt{MANIFEST.MF} file; sometimes it is said that the
\bundle dependency management is \emph{pom first} whereas the \tycho one is \emph{manifest first}.
This complicates a lot the dependency management for the \eclipse update site: adding a new dependency
in the \java core code implies to modify all the \texttt{pom.xml} files of the \emph{pom first} projects and
all the \texttt{MANIFEST.MF} files of the \emph{manifest first} projects.

\subsubsection{\eclipse}
\label{sec:eclipse}

\gls{ac:adfg} can also be build by \eclipse, this is not convenient for continuous integration but this can
be for development. To do this, you need to install the \textsf{m2e} (\maven to \eclipse) plugin in
your \eclipse \gls{ac:ide}. Then you can import all the \texttt{ADFG} projects into \eclipse 
as \maven projects. Normally, \eclipse specific project files (as \texttt{.project} and \texttt{.classpath})
are stored in the repository so the installation should be fine, nevertheless don't hesitate to 
\emph{clean} the projects if they are faulty.

To directly test the coded \gls{ac:adfg} \eclipse plugins into an \eclipse instance, run the
\texttt{fr.inria.tea.adfg.elauncher.sdf} project by right-clicking on it and selecting 
\emph{Run As > Eclipse Application}. This will launch another \eclipse window with the plugin installed
in it. At first time, these \emph{Eclipse Application Run configuration} has to be configured:
the \emph{VM arguments} have to include the \eclipse installation line:
\begin{framed}
  \texttt{-Djava.library.path=/ADFG\_ROOT/libs}
\end{framed} 
The compilation is done automatically by \eclipse but is separated from the \maven archive process,
to do that you have to run the specific \maven goal \emph{install} (instead we advise you to use
the provided installation scripts). 


\subsection{Dependencies}
\label{sec:deps}

\gls{ac:adfg} uses several kinds of dependencies: one to build the project (\umlgr),
and the others to use the tool (\scotch and \lpsolve). There use and compilation process is
detailed in the following paragraphs. If you are not familiar with the \gls{ac:jni}, we recall
that a \java program can call a \textsf{C} library given a \java wrapper, 
calling a \textsf{C} wrapper, itself calling the original \textsf{C} library.

\subsubsection{\sout{\textsf{UMLGraph}}}

\textcolor{red!90!black}{This dependency has been deleted (does not work with \java 9+ yet).}

This tiny \java library improves the documentation of the code module, by building the
(experimental but quite precise) class dependency graphs which are displayed in the
\textsf{Javadoc}. The source code is directly downloaded from \textsf{GitHub} because
the current version available on the public \maven archive is not up to date.
\umlgr is thus a documentation time dependency but is also needed at compilation time.

\umlgr is free and open source under a copyright license (Diomidis Spinellis).

\subsubsection{\textsf{LpSolve}}

This linear programming solver is a run time \gls{ac:adfg} dependency.
It is written in \textsf{C} and a \java wrapper already exists. We build both from the sources
(the \java wrapper has been slightly modified), but we have to use our own script for the 
compilation to ensure the library portability\footnote{\label{fn:libport}The standard scripts are
working well for \textsf{Linux} systems, but not on \textsf{Darwin} (\textsf{MacOS} kernel),
so we had to write our own scripts in the folder
\texttt{/RESOURCES/libs\_to\_install/adfg\_deps\_makefiles/}, especially in order to manage
relative paths in the library dependencies. Other \textsf{Unix} systems have not been tested.}. 
Moreover one of the original compilation script has a bug which has been reported (see 
\href{https://beta.groups.yahoo.com/neo/groups/lp\_solve/conversations/topics/16593}{this post}) 
but not fixed yet.

Notice that you can use your own \lpsolve version by setting the path \texttt{LPSOLVE\_ADFG} to
your own installation path in the \texttt{devel.config} file, 
but we don't recommend to do it since we use a specific folder
architecture to search the library and we are restricted to the version \texttt{5.5} for now
because the version is hardcoded in the library name.

\lpsolve is called several times by \gls{ac:adfg}; each time a file suffixed by \texttt{.lp}
containing the linear programming problem to solve is created then read and solved by calling
the \lpsolve \gls{ac:api} methods, and then deleted if no exception occurred.
The results are read programmatically via the \lpsolve \gls{ac:api}.

\lpsolve usually prints some solving information on the standard output, they can be enabled
on the \gls{ac:adfg} command line tool by selecting the verbose option (\texttt{-v}). Such
information are not printed on the \gls{ac:adfg} \eclipse plugin console.

Both \lpsolve and the \java wrapper are free and open source under the \textsf{LGPL} license.


\subsubsection{\textsf{SCOTCH}}

This graph partitioner is a run time dependency.
It is more difficult to install, especially if you use the source directly,
as we do. As for \lpsolve we provide our own \texttt{Makefile} depending on the system and select
the good one automatically\footnote{See footnote \ref{fn:libport} above.}.

We use \scotch version \texttt{6.0.6} and it seems that newer version (\texttt{6.1.0+})
change the API and may not be compatible anymore.
Nevertheless, you can use your own \scotch version by setting the path \texttt{SCOTCH\_ADFG} to
your own installation path in the \texttt{devel.config} file, 
but we don't recommend to do it since we use a specific folder
architecture to search the library.

\scotch is called several times by \gls{ac:adfg}; each time a file suffixed by \texttt{.grf}
containing the graph representation to partition is created then read and partitioned by calling
the \scotch \gls{ac:api} methods, and then deleted if no exception occurred.
The results are read programmatically via the \scotch \gls{ac:api}.

\scotch is free and open source under the \textsf{CeCILL-C V1} license.
For this project the wrapper code is ours and is not related to the \scotch project.


\section{Main code folder: \texttt{ADFG} description}

The \texttt{ADFG} code folder is divided in three projects: the core one (\texttt{adfgCoreAlgos}) is about the
scheduling algorithms, the \eclipse one (\texttt{adfgEclipsePlugins}) is about the tool extension to \eclipse,
\sout{and the graph one (\texttt{adfgGraphEditor}) is about the plugins to design data flow graphs}.

\subsection{\texttt{adfgCoreAlgos} project}

This project is the most important one: it does all the scheduling analysis and computes
the best timing parameters (periods, buffer sizes, initial token on buffers). It is a 
standalone project since it includes the command line tool.

\subsubsection{Main modules}

This project contains the following modules:
\begin{enumerate}
  \item{\texttt{fr.inria.tea.adfg.algos} provides the \gls{ac:adfg} scheduling \gls{ac:api} and implementation;}
  \item{\texttt{fr.inria.tea.adfg.cli} provides the command line tool;}
  \item{\texttt{fr.inria.tea.adfg.deps} provides an \eclipse compatible bundled jar with all dependencies;}
  \item{\texttt{fr.inria.tea.adfg.doc} provides a taglet and a doclet to manage the \texttt{fr.inria.tea.adfg.algos} \textsf{Javadoc}.}
\end{enumerate}

In the following paragraphs, these modules will be referred by their short name (\texttt{.algos} for example)
or by their number.

\subsubsection{\maven configuration}

This project is \emph{pom first} so dependencies are expressed within the \texttt{pom.xml} files only. Thanks to the \bundle
plugin, the \texttt{MANIFEST.MF} files are created almost automatically (we restrict a bit the dependencies to include in the
jar in order to be \eclipse compatible, this configuration being done in the \texttt{pom.xml} of the \texttt{.deps} module).

External dependencies wrapped in the \texttt{.deps} module include well knonw \java libraries (\textsf{JGraphT}, \textsf{JDom2})
and our own dependencies (\scotch and \lpsolve \java wrappers). There is also dependencies between the modules: $1$ depends on
$3$ at compile and run time and on $4$ at documentation generation time, and $2$ depends on $1$ at compile time and run time.

\subsubsection{Documentation}

The present documentation file is maintained in the folder \texttt{.doc/latex}; although it is not \java code it facilitates to include
automatically generated \LaTeX code during the \texttt{.algos} documentation phase. Indeed the \sout{\java taglet and} the \java doclet
in the module \texttt{.doc} enable to handle the \texttt{@todo} annotation tags present in the code of \texttt{.algos}: this tags
are translated to \LaTeX code including their content and the method or class where they appear. To run the \LaTeX code
generation (\texttt{.doc/latex/ContributorGuide/appendix/todo.tex}), just run the command \texttt{mvn site} in the \texttt{.algos} module;
in the same time it will generate the regular \textsf{HTML} \textsf{Javadoc} in the \texttt{.algos/target/site/apidocs} folder. 
It is also possible to compile the present documentation without having run the \texttt{mvn site} command, only the appendix
\ref{chap:devwork} will not be filled. A \texttt{Makefile} is provided to compile this \LaTeX documentation.

\subsection{\texttt{adfgEclipsePlugins} project}

\gls{ac:adfg} is also available as an \eclipse plugin: it is possible to analyze the scheduling of \sdf and \texttt{.adfg}
files by a right click on them. Thus this project is mainly a \gls{ac:gui}, but it contributes to the current design
of the core project because a \gls{ac:gui} needs something more than any other software parts: a separate computation
thread. This induces some complex code in both this project and the core project but it is compulsory to avoid
\eclipse crashes if our computations takes a long time.

\subsubsection{Main modules}

The \gls{ac:adfg} \eclipse plugins are contained in two modules, the first being designed
to be reusable by other software (as the \textsf{ASME2SSME} translator):
\begin{enumerate}
  \item{\texttt{fr.inria.tea.adfg.elauncher.common} provides an \gls{ac:adfg} console and some constants;}
  \item{\texttt{fr.inria.tea.adfg.elauncher.sdf} provides the \gls{ac:adfg} \eclipse \gls{ac:gui}.}
\end{enumerate}
Moreover this project is the only one to contain a \texttt{site} folder; among all the \texttt{ADFG/} projects,
only this one is in charge of building the whole \eclipse plugin, which has \sout{two} one \emph{features}:
itself \sout{and \texttt{adfgGraphEditor}}.

In the following paragraphs, these modules will be referred by their short name (\texttt{.sdf} for example)
or by their number.

\subsubsection{\maven and \eclipse configuration}

This project is \emph{manifest first}, however some dependencies must also be declared in the \texttt{pom.xml}
files: the dependencies on \texttt{fr.inria.tea.adfg.algos} and \texttt{fr.inria.tea.adfg.deps}.
\sout{As this project is also in charge of building the main \eclipse plugin with its own feature and the
\texttt{.adfg} file and graph editor, \texttt{adfgGraphEditor} is also a dependency, listed in the file
\texttt{site/site.xml} as the \texttt{model} feature.}
 
To simulate an \eclipse instance with the current \gls{ac:adfg} plugin being developed, do not
forget to set up the \emph{VM arguments} of the \emph{Eclipse Application Run Configuration}
as stated in section \ref{sec:eclipse}. Besides, we tried to not use deprecated extension points
in \eclipse, but this can quickly evolve so the \eclipse extension point mechanism especially
for consoles and explorer menu must be regularly checked.


\subsubsection{Graphical User Interface}

The graphical user interface is composed of two visual components: an output console displaying logs
and a scheduling analysis selection menu
in the package explorer when right-clicking on a \texttt{.xml} or \texttt{.adfg} file. When clicking on
one of the scheduling analysis in the menu, a dialog window is opened in order to specify extra
parameters (number of core, file to export, keep pre-computed file data, etc.). These graphical
components are all coded with the \eclipse facilities: extension points for the console and the
menu, \eclipse \textsf{Standard Widget Toolkit (SWT)} for the dialog area.  

As the \gls{ac:adfg} computation can be long, it is done in a separate thread than the \gls{ac:gui}
thread; notice that the \gls{ac:gui} thread is managed by \eclipse so when we enter in the
computation thread, access the \gls{ac:gui} thread requires to call some specific methods.
A \emph{cancel} button is also provided in order to stop the computation if it is taking too much
time. This is possible thanks to the thread facility in the core \gls{ac:adfg} \gls{ac:api} which
provides \emph{start, join and cancel} methods on a scheduling analysis problem instance.
We enumerate here the successive main steps of an analysis (\texttt{typewriter} text are classes
from this project, \emph{emphasized} text corresponds to \eclipse classes or visual elements):
\begin{enumerate}
  \item{user left-clicks on a scheduling analysis in the \emph{ADFG for SDF} menu;}
  \item{the \texttt{MenuListener} creates a new \texttt{LaunchAnalysis} thread, separated from the
    \gls{ac:gui} thread, and does not wait to it;}
  \item{the \texttt{LaunchAnalysis} thread gets the \texttt{ProcessConsole}, 
    display the \emph{dialog area} and retrieve its parameters through a \emph{dialog settings} instance;}
  \item{the \texttt{LaunchAnalysis} thread enables the \textsf{ProcessConsole} \emph{Cancel} button and
    starts the scheduling analysis (it automatically starts a new thread) problem and try to join it
    immediately after;}
  \item{when the join operation returns, the \texttt{LaunchAnalysis} thread disables the \emph{Cancel}
    button and executes the export operations;}
  \item{if exceptions occurred during the problem computation, they are thrown again by the join method
    and caught by the \texttt{LaunchAnalysis} thread which displays them.}
\end{enumerate}

\subsection{\sout{\texttt{adfgGraphEditor} project}}

\textcolor{red!90!black}{This plugin has been deleted.}

The \eclipse graph editor is a standalone project: it provides facilities
to design an \texttt{.adfg} file describing an actor system to schedule.
The user can choose to edit directly the \texttt{.adfg} through the
\textsf{Ecore} model editor or can draw a graph (\texttt{.adfg\_diagram} file)
of actors linked by channels and then use the associated \texttt{.adfg} file.

\subsubsection{Main modules}

This project is composed of four modules, although three of the four have
been automatically generated:
\begin{enumerate}
  \item{\texttt{fr.inria.tea.adfg.model} contains the \ecore model, its implementation
    and the other modules generator;}
  \item{\texttt{fr.inria.tea.adfg.model.edit} is the \ecore model editor \gls{ac:api};}
  \item{\texttt{fr.inria.tea.adfg.model.editor} is the \ecore resource file editor;}
  \item{\texttt{fr.inria.tea.adfg.model.diagram} is the \ecore diagram file editor.}
\end{enumerate}

In the following paragraphs, these modules will be referred by their short name (\texttt{.model} for example)
or by their number.

\subsubsection{\maven and \eclipse configuration}

This project is completely \emph{manifest first}. However, even if some of the project modules
have been automatically generated, the simple \tycho \texttt{pom.xml} files have been added
manually. As the inner working of \maven is quite easy
in this project, it is the opposite for \eclipse. Firstly this project has to be opened with
\eclipse \emph{modeling} because we use components from the \textsf{Eclipse Modeling Framework (EMF)}
and the \textsf{Graphical Modeling Framework (GMF)}.
Secondly, in order to be able to generate yourself the modules $2$, $3$ and $4$, you need to install
the \eclipse plugin \eugenia developed by \href{http://www.eclipse.org/epsilon/}{\textsf{Epsilon}}.

Besides, notice that the editor modules generation is not perfect: it will not be done with the
good packages names prefixed by \texttt{fr.inria.tea.adfg}. These changes must be done after the
code generation from the model, using \eclipse refactor capacities and a lot of \texttt{grep} and
\texttt{sed} commands. This is a complicated workflow but there is no other possible way until
the \ecore model can manage packages containing only deeper packages. Moreover, the \textsf{XSD}
\footnote{An \textsf{XSD} file is an (\textsf{XML}) file which describes the model of a class of
\textsf{XML} files.}
files used to check the \gls{ac:adfg} \texttt{*.adfg} input files in the core project are also
generated from the \ecore model and then copied to the core project resources, after some
modifications in order to restrict the generated \textsf{XSD} model (all elements must be
declared at the top level in our version).

\subsubsection{\ecore modelization and editor generation}

The \gls{ac:adfg} model is written in \ecore, and translated to the \emfatic
language. The model only describes the input and output graphs of the tool:
the model elements are the actors and the channels, their mandatory
attributes are the input parameters (\gls{ac:wcet}, consumption and 
production rates) and their optional attributes are the results
(periods, deadlines, partition numbers, channel sizes).

The editors generation is done by \eugenia when right-clicking on the 
\emfatic code file of the model: select \emph{Eugenia > Generate EMF/GMF editor}.
Some annotations are used to specify how to link the elements in the editors
(actors are nodes, channels are edges). Then \eugenia generates several files
and especially the \texttt{*.genmodel} file which can be used to generate all editors.
On this file, right-click on the root package in the editor view and select 
\emph{Generate All}: \texttt{.edit}, \texttt{.editor} \texttt{.diagram} projects have been
generated. 


\section{Work to do}

This section describes the main possible axis of future work concerning
the implementation and also concerning the research.

\subsection{Implementation work}

\begin{itemize}
\item{implement option to keep input actor priorities}
\item{remove IDM from RWP? (maybe possible only if no actor with noDA, and if convergent)}
\item{standardize int/long usage}
\item{export to SimSO}
\end{itemize}

\subsection{Theoretical work}

\begin{itemize}
\item{express \gls{ac:wcet} in \gls{ac:ucsdf} instead of SDF currently}
\item{take into account preemption cost}
\item{take into account communication cost}
\item{check sustainability? (\gls{ac:wcet} less than expected)}
\end{itemize}
