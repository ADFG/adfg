#!/bin/sh

IN="$1"
FILTER="UPIS Pair MathFunction Fraction Metrics Logger ESchedType SymbolicDeadline Actor AffineRelation Channel"

if [ -z ${IN} ];
then
    echo "Enter an input jcg file as first argument."
    exit 
fi

TMP="tmp_${IN}"
OUT="callgraph"
X_GR_SIZE=16.5
Y_GR_SIZE=11.7

FILTERED="grep -Ev"
for i in ${FILTER}
do
    FILTERED="${FILTERED} -e '${i}'"
done
FILTERED="${FILTERED} ${IN} > ${TMP}"
#grep -Ev ${FILTERED} ${IN} > ${TMP}
eval ${FILTERED}

#echo "digraph calls {\n  size=\"${X_GR_SIZE},${Y_GR_SIZE}\";" > ${OUT}.dot
echo "digraph calls {" > ${OUT}.dot
grep -Ev -e '^C:' -e  'java\.' -e 'jdom2\.' -e 'jgrapht\.' ${TMP} | sed -E 's/[a-z]+\.//g; s/M://; s/\([MIOS]\)//; s/[\$|\:]/\\n/g;  s/ / -> /g;  s/$/;/; s/<init>/cstr/g; s/(.*) -> (.*);/\"\1\" -> \"\2\"/ '| awk '!seen[$0]++' >> ${OUT}.dot
echo "}" >> ${OUT}.dot
dot -Tpng ${OUT}.dot -o dot_${OUT}.png

rm -f ${TMP}
