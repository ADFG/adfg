%% -*- eval: (flyspell-mode 1); eval: (ispell-change-dictionary "english"); -*-

\section{Algorithms}
\begin{frame}{Outline}
  \tableofcontents[currentsection]
\end{frame}

\begin{frame}{Input}

  \begin{block}{Architecture specification}
    \begin{itemize}
      \item number of processors;
      \item scheduling policy (mainly EDF and DM).
    \end{itemize}
  \end{block}

  \begin{block}{Processes (actors) data flow graph}
    Weakly connected directed graph:
    \begin{itemize}
      \item \emph{vertices} are the actors, with their WCET;
      \item \emph{edges} are the channels, with their production
        and consumption rate.
    \end{itemize}
    It can be deduced from different representations: \textsf{Ecore} model,
    \textsf{SDF3}, \textsf{AADL} file.
  \end{block}

\end{frame}

\begin{frame}{Common workflow -- overview}
  
  \begin{block}{First: graph and problem construction}
    The user defines a problem with a graph, a scheduling policy and a number of processors (checked wrt. scheduling policy). 
  \end{block}

  \begin{block}{Second: affine relation synthesis (ARS)}
    It partitions the graph, assigns the priorities and compute the affine relation phases.
  \end{block}

  \begin{block}{Third: symbolic schedulability analysis (SSA)}
    It computes and sets the actor periods and phases.
  \end{block}

  \begin{block}{Fourth: buffer utilization computation}
    It computes initial tokens and maximum size.
  \end{block}

\end{frame}

\begin{frame}{Packages and notions of the algorithms' module \textless\emph{implementation details}\textgreater}

  In the inclusive hierarchical order:
  \begin{enumerate}
  \item \textbf{main} bundle public classes/interfaces and enumeramtions;
  \item \textbf{ARS} affine relation synthesis algoithms;
  \item \textbf{SSA} symbolic schedulability algoithms;
  \item \textbf{common\_data\_rep} problem general data containers;
  \item \textbf{maths} basic classes as fractions and ultimately periodic sequences;
  \item \textbf{common\_base} internal runtime exception and scheduling types.
  \end{enumerate}

\end{frame}


\begin{frame}{ADFG Global Chart}
  \begin{center}
    \includegraphics[width=0.85\textwidth]{ADFGchart.pdf}
  \end{center}
\end{frame}


\begin{frame}{Affine Relation Definition}

  \begin{block}{Example (from Adnan Bouakaz PhD thesis)}
    \begin{center}
      \includegraphics[width=0.6\textwidth]{AffineRelationAdnan.png}
    \end{center}
  \end{block}

  \begin{block}{Properties}
    Formally, an affine relation AR between actors $i$ and $k$
    is represented by a tuple $(n,\,\phi,\,d)$
    (it is the reverse $(d,\,-\phi,\,n)$ between $k$ and $i$).\\
    Initially, AR value is $(a_x,\, *,\, a_y)$ with $a_x$ and $a_y$ the average
    production and consumption rates per firing respectively.
  \end{block}

  \begin{block}{Consistency conditions}
    Between two actors: every $\frac{n}{d}$ is the same (one AR for all channels $i \leftrightarrow k$).\\
    For cycles (hint): $\sum_{AR \in Cycles} w_{ARs}\phi_{AR} = 0$
  \end{block}
  
\end{frame}


\begin{frame}{Affine Relation Synthesis}

  \begin{block}{Goal}
    Set affine relations phases in order to minimize buffer total size, and
    do the mapping. Time properties (as WCET) are not used here.
  \end{block}

  \begin{block}{Consistency}
    At the beginning check if the affine graph is weakly connected and if 
    all channels between the two same actors have the same prod./cons. ratio.
  \end{block}

  \begin{block}{Steps}
    \begin{enumerate}
    \item \textbf{priority assignment} if fixed priorities (e. g. DM);
    \item \textbf{partitioning} besft fit heuristics or \textsf{SCOTCH} (may also use \texttt{SRTA/SPQA} of the next step);
    \item \textbf{phases computing} wrt. underflow and overflow constraints and cycles.
    \end{enumerate}
  \end{block}


\end{frame}

\begin{frame}{Symbolic scheduling analysis}

  \begin{block}{Goal}
    Set actor periods and phases in order to maximize the throughput or to
    minimize the total buffer size (maximize period).
  \end{block}

  \begin{block}{Consistency}
    At the end, check if processor utilization is correct ($\le$ \#processors).
  \end{block}

  \begin{block}{Steps}
    \begin{enumerate}
      \item \textbf{coefficients computing} all periods are expressed by a ratio on the
        actor basis period;
      \item \textbf{best period computing} \emph{oracles} derived from RTA and QPA ensuring the max throughput;
      \item \textbf{all periods and phases are set} using \textsf{LpSolve}, wrt. the objective.
    \end{enumerate}
    Steps 1 and 3 do not depend on the scheduling policy.
  \end{block}

\end{frame}

\begin{frame}{Changes}

  \begin{block}{To do:}
    \begin{itemize}
    \item{\texttt{SQPA\_enumerative} is unused, complete the code;}
    \item{homogenize \texttt{int/long}, UNDEFINED values;}
    \item{deterministic cycle analysis (and tests);}
    \item{channel loops, see next slide !}
    \end{itemize}
  \end{block}

  \begin{block}{Done:}
    \begin{itemize}
    \item{use java logger, deep copy to use via API;}
    \item{XSD validation of \textsf{SDF3} files;}
    \item{refactor and documentation (but not finished);}
    \item{callgraphs (by scheduling policy);}
    \item{import and export \textsf{Ecore} data flow graph specification.}
    \end{itemize}
  \end{block}

\end{frame}

\begin{frame}{Loops buffer size}

  \begin{block}{Example}
    \begin{center}
    \begin{tikzpicture}
      \node (a) [draw, circle, align=center] at (0,0) {Actor};
      \node (b) [draw=none, fill=none] at (2,0) {};
      \draw (a) [-latex'] to[out=45, in=90] (2,0) node[above right, align=left] {prod: $u_{1}v_{1}^{\omega}$} node [below right, align=left] {cons: $u_{2}v_{2}^{\omega}$} to[out=279, in=315] (a);
    \end{tikzpicture}
    \end{center}
  \end{block}

  \begin{block}{Conjectures}
    Consistency iff $\frac{||v_{1}||}{|v_{1}|} = \frac{||v_{2}||}{|v_{2}|}$. Lets define $phl = lcm \left\{\left|v_{1}\right|, \left|v_{2}\right|\right\}$,
    \begin{equation*}
      initialSize = max \left\{\oplus (u_{2}v_{2} - u_{1}v_{1}) (i) \left| i \in \llbracket 1, max \left\{\left|u_{1}\right|, \left|u_{2}\right|\right\} + phl\rrbracket\right.\right\}
    \end{equation*}
    \begin{equation*}
      maxSize = max \left\{initialSize, \oplus (u_{1}v_{1} - u_{2}v_{2}) (i) \left| ... (idem\, above)\right.\right\}
    \end{equation*}
    To meet the max length: complete with periodic part. Prefix $u_{1}$ by $0$ ?
  \end{block}


\end{frame}
