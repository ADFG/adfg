/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.cli;

import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.Argument;

import fr.inria.tea.adfg.algos.common_base.ESchedAlgo;
import fr.inria.tea.adfg.algos.common_base.ETradeOffType;
import fr.inria.tea.adfg.algos.common_base.TradeOffUDH;

/**
 * Options offered by the command line interface.
 * 
 * @author ahonorat
 */
public class Options {

	/**
	 * Option to print usage.
	 */
	@Option(name = "-h", aliases = "--help", usage = "Print usage.", help = true)
	private boolean help = false;
	
	/**
	 * Option for verbose mode.
	 */
	@Option(name = "-v", aliases = "--verbose", usage = "Print more detailed information.")
	private boolean verbose = false;

	/**
	 * Option for quiet mode.
	 */
	@Option(name = "-q", aliases = "--quiet", usage = "Print only errors (disable verbose mode).")
	private boolean quiet = false;
	
	/**
	 * Option to divide all WCET by their GCD.
	 */
	@Option(name = "-g", aliases = "--WCETgcd", usage = "Reduce all WCET by their greatest common divisor (for simulation).")
	private boolean reduceWCET = false;

	/**
	 * Option to divide all WCET by their GCD.
	 */
	@Option(name = "-l", aliases = "--tokenLCM", usage = "Force initial number of tokens to be a multiple of the production and consumption rates of the edge.")
	private boolean lcmToken = false;
	
	/**
	 * Option for result comparison.
	 */
	@Option(name = "-r", aliases = "--compareResult", usage = "Compare result with the given input.")
	private boolean compare = false;
	
	/**
	 * Option to enable sdf input file update.
	 */
	@Option(name = "-u", aliases = "--updateXML", usage = "Update intial file with computed results (except *.tur).")
	private boolean updateXML = false;

	/**
	 * Option to disable computation of initial buffer sizes (uses the given one).
	 */
	@Option(name = "-i", aliases = "--keepInitToken", usage = "Use the given initial buffer tokens (i. e. computed only if not provided).")
	private boolean keepInitToken = false;
	
	/**
	 * Option to disable computation of max buffer sizes (uses the given one).
	 */
	@Option(name = "-m", aliases = "--keepMaxToken", usage = "Use the given max buffer sizes (i. e. computed only if not provided).")
	private boolean keepMaxToken = false;

	/**
	 * Option to disable computation of periods (uses the given one).
	 */
	@Option(name = "-t", aliases = "--keepPeriod", usage = "Use the given actor periods (i. e. computed only if not provided).")
	private boolean keepPeriod = false;
	
	/**
	 * Option to disable computation of actors processor binding/partition (uses the given one).
	 */
	@Option(name = "-b", aliases = "--keepMapping", usage = "Use the given actor partition (i. e. computed only if not provided).")
	private boolean keepMapping = false;

	/**
	 * Option to disable computation of actors processor binding/partition (uses the given one).
	 */
	@Option(name = "-z", aliases = "--zeroDelay", usage = "All channel initial number of tokens (delays) must be zero.")
	private boolean zeroDelay = false;

	
	/**
	 * Option to export results in Yartiss format.
	 */
	@Option(name = "-y", aliases = "--yartiss", usage = "Export actors properties in the XML Yartiss format.", metaVar = "FILENAME")
	private String yartissFile;
	
	/**
	 * Option to export results in Cheddar format.
	 */
	@Option(name = "-c", aliases = "--cheddar", usage = "Export actors properties in the XML Cheddar format.", metaVar = "FILENAME")
	private String cheddarFile;

	/**
	 * Option to export results in Dot format.
	 */
	@Option(name = "-d", aliases = "--dot", usage = "Export graph properties in the DOT format.", metaVar = "FILENAME")
	private String dotFile;

	/**
	 * Option to specify tradeoff for SP_UNI_UDH.
	 */
	@Option(name = "-e", aliases = "--UDHtradeoff", usage = "Specify tradeoff percentages for the SP_UNI_UDH scheduling: <maxThroughputLoss>-<minBuffersizeGain>[-<permutationSize>]", metaVar = TradeOffUDH.DEFAULT_TRADEOFF_UDH)
	private String udhString;
	
	/**
	 * Option to write results on a new file.
	 */
	@Option(name = "-n", aliases = "--newFile", usage = "Create a results file (ADFG .adfg format, or SDF3 .xml if constructed by a SDF3 file).", metaVar = "FILENAME")
	private String newFile;
	
	/**
	 * Option to set the number of processors in the modelised system, 1 by default.
	 */
	@Option(name = "-p", aliases = "--nbProcs", usage = "Number of processors.", metaVar = "POS INT")
	private int nbProcs = 1;
	
	/**
	 * Option to set the strategy during solving (throughput maximization, buffer minimization, etc).
	 */
	@Option(name = "-s", aliases = "--strategy", usage = "Solving objective.", metaVar = "STRAT TYPE")
	private ETradeOffType strategy;
	
	/**
	 * Option to disable computation of periods (uses the given one).
	 */
	@Option(name = "-cg", aliases = "--generateLIDERTEMS", usage = "Generate LIDE RTEMS code")
	private boolean generateLIDERTEMS = false;
	
	/**
	 * Required file to analyse.
	 */
	@Argument(index = 0, usage = "ADFG, SDF3, or Turbine  file to analyse (*.adfg, *.xml, or *.tur).", required = true, metaVar = "FILENAME")
	private String file;

	/**
	 * Required scheduling policy to use.
	 */
	@Argument(index = 1, usage = "Name of the analysis to run.", required = true, metaVar = "SCHED TYPE")
	private ESchedAlgo scheduleType;

	/**
	 * User choice about help option.
	 * 
	 * @return True if help has been asked, false otherwise.
	 */
	public boolean isHelp() {
		return help;
	}

	/**
	 * User choice about verbose mode.
	 * 
	 * @return True if verbose mode has been asked, 
	 * false otherwise.
	 */
	public boolean isVerbose() {
		return verbose;
	}
	
	/**
	 * User choice about quiet mode.
	 * 
	 * @return True if quiet mode has been asked, 
	 * false otherwise.
	 */
	public boolean isQuiet() {
		return quiet;
	}

	/**
	 * User choice about WCET reduction.
	 * 
	 * @return True if reduction must be done, false
	 * otherwise.
	 */
	public boolean isReduceWCET() {
		return reduceWCET;
	}
	
	
	/**
	 * User choice about WCET reduction.
	 * 
	 * @return True if init token must be lcm of rates, false
	 * otherwise.
	 */
	public boolean isForceTokenLCM() {
		return lcmToken;
	}
				
	/**
	 * User choice about comparison between input
	 * and output.
	 * 
	 * @return True if comparison has been asked,
	 * false otherwise.
	 */
	public boolean isComparison() {
		return compare;
	}
	
	/**
	 * User choice about sdf3 file update.
	 * 
	 * @return True if update has been asked, false otherwise.
	 */
	public boolean isUpdateXML() {
		return updateXML;
	}
	
	/**
	 * User choice about recomputing provided initial buffer sizes.
	 * 
	 * @return False if all init sizes must be recomputed, true otherwise. 
	 */
	public boolean keepInitToken() {
		return keepInitToken;
	}

	/**
	 * User choice about recomputing provided max buffer sizes.
	 * 
	 * @return False if all max sizes must be recomputed, true otherwise. 
	 */
	public boolean keepMaxToken() {
		return keepMaxToken;
	}

	/**
	 * User choice about recomputing provided periods.
	 * 
	 * @return False if all periods must be recomputed, true otherwise. 
	 */
	public boolean keepPeriod() {
		return keepPeriod;
	}

	/**
	 * User choice about recomputing actors mapping.
	 * 
	 * @return False if all actors processor binding must be computed, true otherwise.
	 */
	public boolean keepMapping() {
		return keepMapping;
	}
	
	/**
	 * User choice about zero delay.
	 * 
	 * @return True if all channels initial number of tokens must be set to zero,
	 * false otherwise.
	 */
	public boolean zeroDelay() {
		return zeroDelay;
	}
	
	/**
	 * User input about a Yartiss file to write results on.
	 * 
	 * @return {@code null} if the option has not been set,
	 * the user input otherwise.
	 */
	public String getYartissFile() {
		return yartissFile;
	}
	
	/**
	 * User input about a Cheddar file to write results on.
	 * 
	 * @return {@code null} if the option has not been set,
	 * the user input otherwise.
	 */
	public String getCheddarFile() {
		return cheddarFile;
	}

	/**
	 * User input about a Dot file to write results on.
	 * 
	 * @return {@code null} if the option has not been set,
	 * the user input otherwise.
	 */
	public String getDotFile() {
		return dotFile;
	}

	/**
	 * User input about tradeoff for SP_UNI_UDH.
	 * 
	 * @return {@code null} if the option has not been set,
	 * the user input otherwise.
	 */
	public String getUDHtradeoff() {
		return udhString;
	}
	
	/**
	 * User input about a new file to write results on.
	 * 
	 * @return {@code null} if the option has not been set,
	 * the user input otherwise.
	 */
	public String getNewFile() {
		return newFile;
	}

	/**
	 * User input about the number of processors
	 * available in the modelised system.
	 * 
	 * @return The number of processors that the user set,
	 * or 1 by default. Is still positive.
	 */
	public int getNbProcs() {
		return nbProcs;
	}

	/**
	 * User input about the strategy type.
	 * 
	 * @return Strategy that the user choose (or null).
	 */
	public ETradeOffType getStrategy() {
		return strategy;
	}

	/**
	 * User input about the file to analyse.
	 * 
	 * @return The file path that the user set.
	 */
	public String getFile() {
		return file;
	}

	/**
	 * User input about the schedule policy.
	 * 
	 * @return The schedule policy that the user choose.
	 */
	public ESchedAlgo getScheduleType() {
		return scheduleType;
	}
	
	/**
	 * User choice about generating LIDE RTEMS code
	 * 
	 */
	public boolean generateLIDERTEMS() {
		return generateLIDERTEMS;
	}

	
}
