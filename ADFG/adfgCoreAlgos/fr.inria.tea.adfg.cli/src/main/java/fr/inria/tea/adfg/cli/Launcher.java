/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.cli;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.ParserProperties;

import fr.inria.tea.adfg.algos.ADFGprob;
import fr.inria.tea.adfg.algos.ADFGraph;
import fr.inria.tea.adfg.algos.CodeGeneratorLIDEC;
import fr.inria.tea.adfg.algos.CodeGeneratorLIDECRTEMS;
import fr.inria.tea.adfg.algos.common_base.AdfgExternalException;
import fr.inria.tea.adfg.algos.common_base.DirectStreamHandler;
import fr.inria.tea.adfg.algos.common_base.MinimalFormatter;
import fr.inria.tea.adfg.algos.common_base.Parameters;
import fr.inria.tea.adfg.algos.common_base.TradeOffUDH;
/**
 * Main class to run the command line interface.
 * Parses options, then run the analysis and returns. 
 * 
 * @author ahonorat
 */
public class Launcher {
    private static final String NUM_VERSION = "3.1 (November 2019).";

    private static final Logger LOGGER = Logger.getLogger("AdfgLog");
    
    public static final int CONSOLE_WIDTH = 128;

    /**
     * Standard constructor, do nothing.
     */
    private Launcher() {
    }
    
    /**
     * Prints the usage then exit.
     * 
     * @param parser The complete usage is printed thanks to the parser.
     * @param exitCode 0 if normal exiting, 1 or more if exiting because of
     * 	an error.
     */
    private static void printUsageExit(CmdLineParser parser, int exitCode) {
    	System.out.println("\n\tSyntax: adfg_sched FILENAME ANALYSIS [OPTIONS]\n");
    	parser.printUsage(System.out);
    	System.exit(exitCode);
    }

    /**
     * Parse argument and run the analysis.
     * 
     * @param args Program arguments.
     */
    public static void main(String[] args) {
    	Options opt = new Options();
		ParserProperties pp = ParserProperties.defaults().withUsageWidth(CONSOLE_WIDTH);
		CmdLineParser parser = new CmdLineParser(opt, pp);	
		System.out.println("ADFG Version: " + NUM_VERSION);
		try {
			parser.parseArgument(args);
		} catch (CmdLineException e) {
			System.out.println("Bad argument format: "+e.getMessage());
			printUsageExit(parser, 1);
		}

		if (opt.isHelp()) {
		    printUsageExit(parser, 0);
		}
						
		if (opt.isVerbose()) {
			Level level = opt.isQuiet() ? Level.SEVERE : Level.FINE;
			LOGGER.setLevel(level);
			for (Handler h: LOGGER.getParent().getHandlers()) {
				h.setLevel(level);
			}
		} else {
			Level level = opt.isQuiet() ? Level.SEVERE : Level.INFO;
			LOGGER.setUseParentHandlers(false);
			Handler handler = new DirectStreamHandler(System.err, new MinimalFormatter());
			handler.setLevel(level);
			LOGGER.addHandler(handler);
			LOGGER.setLevel(level);
		}
			
		long startTime = System.currentTimeMillis();
			
		try {
			ADFGprob.checkSchedulingParams(opt.getScheduleType(), opt.getNbProcs(), opt.getStrategy());
			
			//=================================================
			//         Construction from sdf3/adfg xml format
			//=================================================

			Path currentRelativePath = Paths.get("");
			LOGGER.config("Current relative path is: " + currentRelativePath.toAbsolutePath());

			ADFGraph graph = new ADFGraph(opt.getFile());

			//================================================
			//                 Analysis
			//================================================
			
			Parameters params = new Parameters(opt.keepPeriod(), opt.keepInitToken(), 
												opt.keepMaxToken(), opt.keepMapping(), 
												opt.getStrategy(), opt.isReduceWCET(),
												opt.isForceTokenLCM(), new TradeOffUDH(opt.getUDHtradeoff()));
			ADFGprob prob = new ADFGprob(opt.getScheduleType(), opt.getNbProcs(), graph, params);
			if (opt.zeroDelay()) {
				prob.setAllInitialTokensToZero();
				LOGGER.warning("Channels initial number of tokens (i. e. delays) have been set to zero.");
			}

			
			prob.start();
			prob.join();
			
			boolean isSchedulable = prob.isSchedulable();
//			Actor.resetIDs();
//			AffineRelation.resetIDs();
//			Channel.resetIDs();
				
			// LOGGER.info(prob.getGraphResultRep());
			LOGGER.info("Schedulable system? " + isSchedulable + ".");
			if (isSchedulable) {
				if (opt.isUpdateXML()) {
					prob.updateOrExportFile(null); 
				}
				if (opt.getYartissFile() != null) {
					prob.exportYartissResults(opt.getYartissFile());
				}
				if (opt.getCheddarFile() != null) {
					prob.exportCheddarResults(opt.getCheddarFile());
				}
				if (opt.getDotFile() != null) {
					prob.exportDotResults(opt.getDotFile());
				}
				
				if (opt.generateLIDERTEMS()) {
					LOGGER.info("===LIDE C Code Generation: START===");		
					prob.generateLIDERTEMS();
					
					LOGGER.info("===LIDE C Code Generation: END===");
				}
				
				if (opt.getNewFile() != null) {
					if (opt.getNewFile().equals(opt.getFile())) { 
					    LOGGER.warning(
					    		"You selected the same file for input and copy, rerun with update option instead.");
					} else {
					    prob.updateOrExportFile(opt.getNewFile());
				    }
			    }
			}
			
			if (opt.isComparison()) {
				LOGGER.info("Input=Output? " + prob.compareProperties(graph) + ".");
			}
			
		} catch (AdfgExternalException e) {
			if (opt.isVerbose()) {
				e.printStackTrace();
			}
			System.exit(1);
		}
			
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		double sum = (double) elapsedTime / 1000;
			
		LOGGER.fine("\n# Time (s): "+sum);
			
		
		System.exit(0);
	}
	
}
