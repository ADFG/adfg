/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos;

import java.util.Collection;
import java.util.logging.Logger;

import org.jdom2.Document;
import org.jdom2.Element;

import fr.inria.tea.adfg.algos.common_base.AdfgExternalException;
import fr.inria.tea.adfg.algos.common_base.ESchedAlgo;
import fr.inria.tea.adfg.algos.common_base.ESchedPolicy;
import fr.inria.tea.adfg.algos.common_data_rep.Actor;
import fr.inria.tea.adfg.algos.common_data_rep.Partition;

/**
 * This class can export results in the Yartiss XML format.
 * 
 * @author Alexandre Honorat
 */
class YartissTransformer {

	private static final Logger LOGGER = Logger.getLogger("AdfgLog");
	
	/**
	 * Do nothing.
	 */
	private YartissTransformer() {
	}

	/**
	 * Write a file (or several if partitioned multi-processor) readable by
	 * Yartiss.
	 * 
	 * @param adfgp Results to export.
	 * @param fileName File to write.
	 * @throws AdfgExternalException If file cound't be written by org.jdom2.
	 */
	static void exportSchedResults(ADFGprob adfgp, String fileName) throws AdfgExternalException {
		ESchedAlgo st = adfgp.getScheduleType();
		if (st.schedulingPolicy() == ESchedPolicy.TT) {
			LOGGER.severe("Time Trigerred policies cannot be exported to Cheddar yet.");
			return;
		}
		
		Collection<Partition> parts = adfgp.getPartitions();
		String baseFileName = fileName;
		if (baseFileName.endsWith(".xml")) {
			baseFileName = baseFileName.substring(0, fileName.length()-4);
		}

		for (Partition part: parts) {
			if (part.getActors().isEmpty()) {
				continue;
			}
			int partitionID = part.getID();
			Element root = new Element("dataset");
			Document document = new Document(root);

			Element policy = new Element("policy");
			// GEDF_MULT is a specific case in Yartiss: multi-processor but not partitioned
			if (st.equals(ESchedAlgo.GEDF_MULT)) {
				policy.setAttribute("name", "EDF_multiproc");
			} else if (st.schedulingPolicy() == ESchedPolicy.SP) {
				policy.setAttribute("name", "FP");
			} else {
				policy.setAttribute("name", "EDF");
			}
			policy.setAttribute("nbParams", "0");
			root.addContent(policy);

			Element simulation = new Element("simulation");
			long endValue = getSimulationTime(part);
			simulation.setAttribute("endValue", Long.toString(endValue));
			simulation.setAttribute("nbProcs", Integer.toString(adfgp.getNbProcessor()));
			root.addContent(simulation);

			Element energyProfile = new Element("energyProfile");
			energyProfile.setAttribute("E0", "2147483647");
			energyProfile.setAttribute("Emax", "2147483647");
			energyProfile.setAttribute("Emin", "0");
			energyProfile.setAttribute("pr", "2147483647");
			root.addContent(energyProfile);

			Element tasks = new Element("tasks");
			tasks.setAttribute("nbTasks", Integer.toString(part.getActors().size()));
			tasks.setAttribute("type", "ADFG generated simulation, partition n°"+partitionID);
			for (Actor a: part.getActors()) {
				Element task = new Element("task");
				task.setAttribute("deadline", Long.toString(a.getDeadline()));
				task.setAttribute("firstRelease", Long.toString(a.getPhase()));
				task.setAttribute("period", Long.toString(a.getPeriod()));
				task.setAttribute("priority", Long.toString(a.getPriority()));
				task.setAttribute("type", "simple");
				task.setAttribute("wcee", "0");
				task.setAttribute("wcet", Long.toString(a.getWcet()));
				tasks.addContent(task);
			}
			root.addContent(tasks);
			
			String file = baseFileName+"_p"+partitionID+".xml";
			XMLUtils.writeDocument(document, file, "Yartiss", false);
		}

	}

	/**
	 * Compute an approximation of the simulation time
	 * (which is max of all phases + max of periods) * 10.

	 * @param part Actors being simulated.
	 * @return Approximation of an exhaustive simulation time.
	 */
	static private long getSimulationTime(Partition part) {
		long maxPhasis = 0;
		long maxPeriod = 1;
		for (Actor actor: part.getActors()) {
			if (actor.getPhase() > maxPhasis) {
				maxPhasis = actor.getPhase();
			}
			if (actor.getPeriod() > maxPeriod) {
				maxPeriod = actor.getPeriod();
			}
		}
		return maxPhasis + maxPeriod * 10;
	}

}
