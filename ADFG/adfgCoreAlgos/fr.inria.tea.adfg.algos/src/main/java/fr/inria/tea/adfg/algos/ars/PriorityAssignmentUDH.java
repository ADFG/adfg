/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.ars;

import java.util.Arrays;

import fr.inria.tea.adfg.algos.common_data_rep.Actor;
import fr.inria.tea.adfg.algos.common_data_rep.WeaklyConnectedComponent;
import fr.inria.tea.adfg.algos.maths.Fraction;
import fr.inria.tea.adfg.algos.ssa.Metrics;

/**
 * This class is an helper for the utilization 
 * distance heuristic. 
 * 
 * @author Adnan Bouakaz
 * @author Alexandre Honorat
 */
class PriorityAssignmentUDH {

	private Actor[] order;
	
	/**
	 * Create a new instance.
	 * <p>
	 * The parameter array is not deep
	 * copied.
	 * 
	 * @param actors Ordered actors.
	 */
	PriorityAssignmentUDH(Actor[] actors){
		order = actors;
	}

	/**
	 * @param i Index of the actor to retrieve.
	 * @return Actor in case i.
	 */
	Actor getActor(int i) {
		return order[i];
	}
	
	/**
	 * @param pa Instance to compare to.
	 * @return True if all actors are in the
	 * same order, false otherwise.
	 */
	boolean isEqualTo(PriorityAssignmentUDH pa) {
		for (int i = 0; i < order.length; ++i) {
			if (order[i].getID() != pa.order[i].getID()) { 
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Create a new priority order with the given
	 * permutation applied to the sequence starting
	 * at the index.
	 * 
	 * @param index Starting position to apply the permutation.
	 * @param permSize Size of the permutation
	 * @param perm Permutation to use.
	 * @return A new instance, with priorities ordered wrt.
	 * the given permutation.
	 */
	PriorityAssignmentUDH arranging(int index, int permSize, int[] perm) {

		Actor[] actors = new Actor[order.length];
		for (int j = 0; j < index; ++j) {
			actors[j] = order[j];
		}
		for (int j = index + permSize; j < order.length; ++j) {
			actors[j] = order[j];
		}

		for (int j = 0; j < permSize; ++j) {
			actors[index+j] = order[index+perm[j]-1];
		}		
		return new PriorityAssignmentUDH(actors);
	}

	/**
	 * Compute the utilization distance between the
	 * current priority ordering and the ideal symbolic one.
	 * <p>
	 * See Adnan Bouakaz thesis p. 96 for the whole algorithm
	 * explanation.
	 * 
	 * @param wcp Actors wcp container.
	 * @return Percentage of throughput loss (divided per 100).
	 */
	double getUtilizationDistance(WeaklyConnectedComponent wcp) {
		long basisPeriod = wcp.getBasis().getPeriod();
		long maxT = basisPeriod;
		for (int i = 0; i < order.length; ++i) {
			Actor a = order[i];
			long respTime = new Metrics(Arrays.asList(order), 1).spResponseTime(a);
			/* compute maxT */
			Fraction deadline = a.getSymbolicDeadline().setMUL(a.getCoefficient());
			long temp = wcp.getDivFactor() * (long) Math.ceil(respTime / (deadline.getValue() * wcp.getDivFactor())); 
			if (temp > maxT) {
				maxT = temp;
			}
		}
		return (maxT - basisPeriod) / ((double) basisPeriod);
	}

	/**
	 * Sum all buffers, regarding to the priority
	 * ordering of this instance.
	 * <p>
	 * Only n*n/2 elements of the matrix need to
	 * be summed because each element of the matrix
	 * is already the total approximated sum of all
	 * channels (regardless to the direction) between
	 * two actors. So either {@code sizes[i][j]} is
	 * summed, either {@code sizes[j][i]}, depending
	 * on the ordering of this instance.
	 * 
	 * @param sizes Buffer weighted adjacency matrix
	 * of the actors to consider, previously filled by
	 * {@code PriorityComputing.fillWeightMatrix(int[][], Set<Actor>)}.
	 * @return Approximated sum of all buffers.
	 */
	long getBufferSumApproximation(long[][] sizes) {
		long sum = 0;
		for (int i = 0; i < order.length; ++i) {
			Actor a = order[i]; 
			int ii = a.getIndex(); 
			for (int j = i+1; j < order.length; ++j) {
				Actor b = order[j];
				int jj = b.getIndex(); 
				sum += sizes[ii][jj];
			}
		}
		return sum;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < order.length; ++i) {
			sb.append("p_"+order[i].getID()+"   ");
		}
		return sb.toString();
	}
	
}
