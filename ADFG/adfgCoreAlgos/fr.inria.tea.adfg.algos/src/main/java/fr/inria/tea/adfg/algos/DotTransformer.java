/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos;

import fr.inria.tea.adfg.algos.common_base.AdfgExternalException;
import fr.inria.tea.adfg.algos.common_base.Parameters;
import fr.inria.tea.adfg.algos.common_data_rep.Actor;
import fr.inria.tea.adfg.algos.common_data_rep.Channel;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.jgrapht.Graph;
import org.jgrapht.io.ComponentNameProvider;
import org.jgrapht.io.Attribute;
import org.jgrapht.io.AttributeType;
import org.jgrapht.io.ComponentAttributeProvider;
import org.jgrapht.io.DOTExporter;

/**
 * This class enables to export the adfg graph
 * to the dot file format.
 * 
 * @author Alexandre Honorat
 */
class DotTransformer {

	/**
	 * Problem instance to export.
	 */
	ADFGprob adfgp;
	
	/**
	 * Do nothing.
	 * 
	 * @param adfgp Results to export.
	 */
	public DotTransformer(ADFGprob adfgp) {
		this.adfgp = adfgp;
	}

	/**
	 * Create (or overwrite) the dot file with
	 * the given problem instance properties.
	 * 
	 * @param fileName File to (over)write.
	 * @throws AdfgExternalException If file couldn't be written.
	 */
	public void exportSchedResults(String fileName) throws AdfgExternalException {
		DOTExporter<Actor, Channel> dot = new DOTExporter<>(new ActorIDNameProvider(), 
															new ActorNameProvider(), 
															new ChannelNameProvider(), 
															new ActorAttributeProvider(), 
															new ChannelAttributeProvider(), 
															new GraphNameProvider());
		try (FileWriter writer = new FileWriter(fileName)) {
			dot.exportGraph(adfgp.getAdfgGraph(), writer);
		} catch (IOException e) {
			throw new AdfgExternalException("Problem during dot file <" + fileName + "> creation.", e);
		}
	}
	
	class GraphNameProvider implements ComponentNameProvider<Graph<Actor, Channel>> {
		@Override
		public String getName(Graph<Actor, Channel> component) {
			String sched = adfgp.getScheduleType().name() + "p" + adfgp.getNbProcessor();
			Parameters params = adfgp.getParams();
			String options = "I" + params.isKeepInitToken() + "M" + params.isKeepMaxToken() +
							 "T" + params.isKeepPeriod() + "B" + params.isKeepMapping() + 
							 "G" + params.isReduceWCET();
			return sched+"_"+options;
		}
	}
	
	class ActorIDNameProvider implements ComponentNameProvider<Actor> {
		@Override
		public String getName(Actor component) {
			return "a" + component.getID();
		}
	}

	class ActorNameProvider implements ComponentNameProvider<Actor> {
		@Override
		public String getName(Actor component) {
			StringBuilder sb = new StringBuilder(component.getName()+"\\n");
			sb.append("WCET: " + component.getWcet()+"\\n");
			long p = component.getPeriod();
			sb.append("Period: " + ((p == -1) ? "*" : p));
			return sb.toString();
		}
	}
	
	class ChannelNameProvider implements ComponentNameProvider<Channel> {
		@Override
		public String getName(Channel component) {
			StringBuilder sb = new StringBuilder(component.getName()+"\\n");
			long i = component.getInitial();
			sb.append("Init: " + ((i == -1) ? "*" : i) + "\\n");
			long m = component.getSize();
			sb.append("Max: " + ((m == -1) ? "*" : m));
			return sb.toString();
		}
	}
	
	class ActorAttributeProvider implements ComponentAttributeProvider<Actor> {
		@Override
		public Map<String, Attribute> getComponentAttributes(Actor component) {
			return null;
		}		
	}
	
	class ChannelAttributeProvider implements ComponentAttributeProvider<Channel> {
		
		class StringAttribute implements Attribute {

			private String val;
			
			public StringAttribute(String value) {
				val = value;
			}
			
			@Override
			public String getValue() {
				return val;
			}

			@Override
			public AttributeType getType() {
				return AttributeType.STRING;
			}
			
		}
		
		@Override
		public Map<String, Attribute> getComponentAttributes(Channel component) {
			Map<String, Attribute> map = new HashMap<>();
			map.put("headlabel", new StringAttribute(component.getConsume().toString()));
			map.put("taillabel", new StringAttribute(component.getProduce().toString()));
			return map;
		}		
	}
	
}
