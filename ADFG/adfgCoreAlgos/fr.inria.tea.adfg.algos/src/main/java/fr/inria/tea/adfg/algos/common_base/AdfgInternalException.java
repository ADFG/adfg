/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.common_base;

/**
 * Runtime exception class used for all internal adfg methods.
 * These internal runtime exceptions should be transformed into
 * external adfg exceptions in all methods exposed to the user.
 * 
 * @author Alexandre Honorat
 */
public class AdfgInternalException extends RuntimeException {

	// useful in case of serialization
	private static final long serialVersionUID = 1L;

	/**
	 * Standard constructor, not used.
	 */
	public AdfgInternalException() {
		super();
	}

	/**
	 * Full constructor, not used in practice.
	 * 
	 * @param message Message.
	 * @param cause Cause.
	 * @param enableSuppression Allow to suppress exceptions when several
	 * are thrown in the same time.
	 * @param writableStackTrace If the stack trace must be written or not.
	 */
	public AdfgInternalException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * Constructor to transmit a cause, with a specific message.
	 * 
	 * @param message Message.
	 * @param cause Cause.
	 */
	public AdfgInternalException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructor with specific message.
	 * 
	 * @param message Message.
	 */
	public AdfgInternalException(String message) {
		super(message);
	}

	/**
	 * Constructor to transmit a cause, not used in practice.
	 * 
	 * @param cause Cause.
	 */
	public AdfgInternalException(Throwable cause) {
		super(cause);
	}

}
