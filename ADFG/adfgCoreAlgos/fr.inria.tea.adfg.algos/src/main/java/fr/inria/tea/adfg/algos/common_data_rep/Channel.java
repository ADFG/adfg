/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.common_data_rep;

import java.io.Serializable;

import fr.inria.tea.adfg.algos.common_base.AdfgExternalException;
import fr.inria.tea.adfg.algos.maths.BasicArithmetic;
import fr.inria.tea.adfg.algos.maths.Fraction;
import fr.inria.tea.adfg.algos.maths.UPIS;

/** 
 * Channel for data flow graphs (as edges). 
 * 
 * @author Adnan Bouakaz
 * @author Alexandre Honorat
 */
public class Channel implements IChannel, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The value is not known yet.
	 */
	public static final long UNDEFINED = -1;

	/**
	 * Unique instance identifier.
	 */
	private static int IDcount = 0;

	private int instanceID;
	protected long size;
	protected long initial;
	protected long dataSize; 	
	protected String name;
	protected UPIS produce, consume;
	protected long tokenLcm;

	/**
	 * Construct a channel characterized by its 
	 * production and consumption rates, and its
	 * tokens (maximum size, initial size). 
	 * <p>
	 * The data size should be set to 1 by default.
	 * 
	 * @param name Channel name.
	 * @param prate Production rate (no deep-copy).
	 * @param crate Consumption rate (no deep-copy).
	 * @param maxSize Channel size.
	 * @param initSize Channel initial tokens.
	 * @param dataSize Data size of tokens.
	 * @throws AdfgExternalException If {@code sz &lt; init}.
	 */
	public Channel(String name, UPIS prate, UPIS crate, long maxSize, long initSize, long dataSize) 
			throws AdfgExternalException {
		if (maxSize >= 0 && initSize >= 0 && maxSize < initSize) {
			throw new AdfgExternalException("The size of the buffer must be greater than the number of initial tokens,"
					+ " and both positive integers.");
		}
		if (dataSize < 1) {
			throw new AdfgExternalException("DataSize of a channel cannot be zero or negative.");
		}
		if (prate == null || crate == null || name == null || name.isEmpty()) {
			throw new AdfgExternalException("At least one of the channel constructor parameters was not set.");
		}

		instanceID = ++IDcount;
		size = (maxSize > 0) ? maxSize : UNDEFINED;
		initial = (initSize >= 0) ? initSize : UNDEFINED;
		this.dataSize = dataSize;
		this.name = name;
		produce = prate;
		consume = crate;
		
		Fraction fp = new Fraction(prate.getSumPeriod(), prate.getPeriodLength());
		Fraction fc = new Fraction(crate.getSumPeriod(), crate.getPeriodLength());
		long p = fp.getDenominator()*fp.getNumerator();
		long c = fc.getDenominator()*fc.getNumerator();
		tokenLcm = BasicArithmetic.funcLCM(p, c);
	}

	/**
	 * Copy constructor, {@code instanceID} is kept the same.
	 * <p>
	 * UPIS are deep copied.
	 * 
	 * @param ch Channel to copy.
	 * @param keepInitToken Copy {@code initial} size only if true, else set to {@code UNDEFINED}.
	 * @param keepMaxToken Copy (maximum) {@code size} only if true, else set to {@code UNDEFINED}.
	 */
	public Channel(Channel ch, boolean keepInitToken, boolean keepMaxToken) {
		instanceID = ++IDcount;
		initial = UNDEFINED;
		size = UNDEFINED;
		if (keepInitToken) {
			initial = ch.initial;
		}
		if (keepMaxToken) {
			size = ch.size;
		}
		dataSize = ch.dataSize;
		name = ch.name;
		produce = ch.produce;
		consume = ch.consume;
		tokenLcm = ch.tokenLcm;
	}

	/* (non-Javadoc)
	 * @see fr.inria.tea.adfg.algos.common_data_rep.IChannel#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * Get unique channel identifier.
	 * 
	 * @return Channel's ID.
	 */
	public int getID() {
		return instanceID;
	}

	/**
	 * Set the initial number of tokens present on
	 * the channel.
	 * 
	 * @param initial New initial number of tokens,
	 * should be a strictly positive integer.
	 */
	public void setInitial(long initial) {
		this.initial = initial;
	}

	/* (non-Javadoc)
	 * @see fr.inria.tea.adfg.algos.common_data_rep.IChannel#getInitial()
	 */
	@Override
	public long getInitial() {
		return initial;
	}

	/**
	 * Set maximum channel size (maximum number
	 * of tokens present on the channel at the
	 * same time).
	 * 
	 * @param size New channel maximum size,
	 * should be a strictly positive integer.
	 */
	public void setSize(long size) {
		this.size = size;
	}
	
	/* (non-Javadoc)
	 * @see fr.inria.tea.adfg.algos.common_data_rep.IChannel#getSize()
	 */
	@Override
	public long getSize() {
		return size;
	}

	/* (non-Javadoc)
	 * @see fr.inria.tea.adfg.algos.common_data_rep.IChannel#getDataSize()
	 */
	@Override
	public long getDataSize() {
		return dataSize;
	}

	public long getRatesLCM() {
		return tokenLcm;
	}
	
	/**
	 * Get the production rate.
	 * 
	 * @return Production rate cloned by deep-copy.
	 */
	public UPIS getProduce() {
		return new UPIS(produce);
	}
	
	/**
	 * Get consumption rate.
	 * 
	 * @return Consumption rate cloned by deep-copy.
	 */
	public UPIS getConsume() {
		return new UPIS(consume);
	}

	/**
	 * Reset to 0 the next Channel's ID. This method
	 * must be called between each new Channel's graph
	 * problem representation creation. Indeed Channels'
	 * names are not used to identified them
	 * internally.
	 * 
	 */
	public static void resetIDs() {
		IDcount = 0;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Channel) {
			return ((Channel) obj).getID() == instanceID;
		}
		return false;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode(java.lang.Object)
	 */	
	@Override
	public int hashCode() {
		return instanceID;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "<"+name+": "+((initial != UNDEFINED) ? initial : "*")+", "+((size != UNDEFINED) ? size : "*")+">";
	}
	
}
