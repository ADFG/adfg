/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.ars;

import java.io.FileWriter;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Logger;

import org.jgrapht.graph.DirectedMultigraph;
import org.jgrapht.graph.SimpleGraph;

import fr.inria.tea.adfg.algos.common_base.AdfgInternalException;
import fr.inria.tea.adfg.algos.common_base.ESchedAlgo;
import fr.inria.tea.adfg.algos.common_base.ESchedPolicy;
import fr.inria.tea.adfg.algos.common_base.ETradeOffType;
import fr.inria.tea.adfg.algos.common_data_rep.Actor;
import fr.inria.tea.adfg.algos.common_data_rep.AffineRelation;
import fr.inria.tea.adfg.algos.common_data_rep.GraphProbData;
import fr.inria.tea.adfg.algos.common_data_rep.Partition;
import fr.inria.tea.adfg.algos.common_data_rep.WeaklyConnectedComponent;
import fr.inria.tea.adfg.algos.maths.Fraction;
import fr.inria.tea.adfg.algos.ssa.SSAnalysis;
import fr.inria.tea.adfg.algos.ssa.SchedulingOracleEDF;
import fr.inria.tea.adfg.algos.ssa.SchedulingOracleSP;
import scotch.Partitioning;

/**
 * Partition a graph component by setting each 
 * actor partition number. There is as many partitions
 * as the number of processors. Partitions are then
 * accessible via a map (numbered by processor
 * id), each entry containing its actor set.
 * <p>
 * All the method must be called with only one
 * weakly connected component in the graph,
 * especially for SCOTCH (the others are indirectly
 * calling methods non working with unconnected graph).
 *  
 * @author Adnan Bouakaz
 * @author Alexandre Honorat
 */
class GraphPartitioning {

	private static final Logger LOGGER = Logger.getLogger("AdfgLog");

	private static final double MIN_SCOTCH_IMBALANCE_RATIO = 0.001;

	static final int ALL_MAPPED = 0;
	static final int NONE_MAPPED = 1;
	static final int PARTLY_MAPPED = 2;
	
	// GraphProbData
	private GraphProbData gpr;
	private SimpleGraph<Actor, Object> undirectedGraph;
	private DirectedMultigraph<Actor, AffineRelation> affineGraph;
	private Map<Integer, Partition> partitions; 
	private ESchedAlgo scheduleType;
	private int nbProcessor;
	private int nbActors;
	private Actor[] priorityOrderedActors;

	/**
	 * Create an object to call partitioning algorithms.
	 * 
	 * @param gpr Graph to work on.
	 */
	public GraphPartitioning(GraphProbData gpr) {
		this.gpr = gpr;
		affineGraph = gpr.getAffineGraph();
		undirectedGraph = gpr.getUndirectedGraph();
		partitions = gpr.getMappedPartitions();
		scheduleType = gpr.getScheduleType();
		nbProcessor = gpr.getNbProcessor();
		nbActors = gpr.getNbActors();
		priorityOrderedActors = gpr.getPriorityOrderedActors();
	}


	/**
	 * Apply scotch partitioning.
	 * <p>
	 * See Adnan Bouakaz thesis p. 109-110, point 3.3.3
	 * (the step 2 is not exactly computed the same way:
	 * for now an exact buffer computation is done).
	 * <p>
 	 * See Scotch documentation page 10 for the imbalance 
	 * ratio. See page 21 for the grf format description.
	 */
	public void partitioningSCOTCH() {
		int mapped = checkAndSetMapping(gpr.getActors(), false);
		if (mapped == ALL_MAPPED) {
			return;
		}
		int[] fixedMapping = null;
		if (mapped == PARTLY_MAPPED) {
			fixedMapping = new int[nbActors];
		}
		/* construct the grf graph */
		StringBuilder grf = new StringBuilder();
		TreeMap<Actor, Long> actorScotchLoads = new TreeMap<Actor, Long>();
		int twiceNbEdge = 0;
		grf.append("0   111\n"); // code for SCOTCH parsing
		double imbalanceRatio = computeScotchLoadsMaxImbalanceRatio(actorScotchLoads);

		int i = 0;
		for (Entry<Actor, Long> entry: actorScotchLoads.entrySet()) {
			Actor actor = entry.getKey();
			if (mapped == PARTLY_MAPPED) {
				fixedMapping[i] = actor.getPartitionID();
			}

			grf.append(actor.getID() + "   " + entry.getValue() + "   ");
			Set<AffineRelation> neighbors = affineGraph.outgoingEdgesOf(actor);
			twiceNbEdge += neighbors.size();
			grf.append(neighbors.size()+"   ");

			for (AffineRelation r: neighbors) {
				Actor dest = affineGraph.getEdgeTarget(r); 
				if (r.isNoAdjust()) {
					grf.append("0   "); 
				} else {
					long gain = 0;
					if (scheduleType.schedulingPolicy() == ESchedPolicy.SP) {
						/* compute the gain that comes from being allocated to the same processor */
						gain = Math.max(0, gpr.approximatedSPedgeGain(actor, dest));
					} else if (scheduleType.schedulingPolicy() == ESchedPolicy.EDF) {
						/* compute the gain that comes from adjusting the deadlines */
						gain = Math.max(0, gpr.approximatedEDFedgeGain(actor, dest, r));
					}
					grf.append(gain+"   ");
				}
				grf.append(dest.getID()+"   ");
			}
			grf.append("\n");
			++i;
		}
		try (FileWriter graphSCOTCHFormat = new FileWriter("graph.grf")) {
			graphSCOTCHFormat.write("0\n"+nbActors+"   "+twiceNbEdge+"\n"+grf.toString());
		} catch (Exception e) {
			throw new AdfgInternalException("Problem during grf file writing.", e);
		}

		/* use scotch to partition the graph */
		int[] partresult;
		if (mapped == PARTLY_MAPPED) {
			partresult = new Partitioning().graphPartFixed("graph.grf", nbActors, nbProcessor, imbalanceRatio, fixedMapping);
		} else {
			partresult = new Partitioning().graphPart("graph.grf", nbActors, nbProcessor, imbalanceRatio);			
		}

		try {
			Path pathToFile = FileSystems.getDefault().getPath("graph.grf");
			Files.delete(pathToFile);
		} catch (Exception e) {
			LOGGER.warning("graph.grf file couldn't be deleted:\n"+e);
		}
		if (partresult == null) {
			throw new AdfgInternalException("Scotch returned empty partition.");
		}

		i = 0;
		for (Actor actor: actorScotchLoads.keySet()) {
			if ((mapped == PARTLY_MAPPED  && fixedMapping[i] == -1) || mapped == NONE_MAPPED) {
				partitions.get(partresult[i]).addActor(actor, false);
			}
			++i;
		}
		logPartitioningMessage();
	}

	/**
	 * This method fill the actor map
	 * with appropriate loads and compute the
	 * maxmimum imbalance ratio that we want.
	 * <p>
	 * Definition of the imbalance ratio can
	 * be found in the SCOTCH manual p. 10.
	 * 
	 * @param actorScotchLoads To be filled.
	 * @return The maximum acceptable imbalance ratio.
	 */
	private double computeScotchLoadsMaxImbalanceRatio(TreeMap<Actor, Long> actorScotchLoads) {
		double sum = 0;
		double minSymbolicLoad = Double.MAX_VALUE;
		for (WeaklyConnectedComponent wcp: gpr.getWeaklyConnectedComponents()) {
			sum += wcp.getSymbolicLoad();
			minSymbolicLoad = Math.min(minSymbolicLoad, wcp.getMinimumSymbolicLoad());
		}
		int minActorProc = Math.min(nbProcessor, nbActors);
		double imb = 0;
		ETradeOffType strat = gpr.getStrategy();

		if (strat == null || strat != ETradeOffType.BUFFER_MIN) {
			// throughput maximization or balanced
			ScotchImbHeuristics imbHeur = new ScotchImbHeuristics(gpr.getActors(), minActorProc, sum);
			double imbMin = imbHeur.computeImbMin();
			
			if (strat != null && strat == ETradeOffType.THROUGHPUT_MAX) {
				// more there are procs, more we reduce imbalance ratio
				// otherwise SCOTCH would not use all processors
				imb = imbMin;
			} else {
				double imbMax = imbHeur.computeImbMax();
				imb = imbMin + ((imbMax - imbMin) * minActorProc) / nbActors;
			}
		} else {
			// buffer minimization
			imb = (2*minActorProc - 2) / (double) minActorProc;
		}

		double coef = Math.max(1.0 / minSymbolicLoad, 1.0);
		for (Actor actor: gpr.getActors()) {
			actorScotchLoads.put(actor, Math.round(actor.getSymbolicLoad()*coef));
		}

		LOGGER.info("Scotch imbalance ratio heuristic found is " + imb + " (min: " +
				MIN_SCOTCH_IMBALANCE_RATIO + ").");

		return Math.max(imb, MIN_SCOTCH_IMBALANCE_RATIO);
	}

	/**
	 * Apply partitioning for static priority, choosing the partition
	 * that minimize some metrics (SRTA if {@code SRTA} is true, CPU
	 * utilization otherwise).
	 * <p>
	 * See Adnan Bouakaz thesis p. 101, section 3.2.3 .
	 * 
	 * @param srta True if SRTA metric must be used instead of
	 * CPU utilization factor.
	 */
	public void partitioningBestFitFP(boolean srta) {
		int mapped = checkAndSetMapping(Arrays.asList(priorityOrderedActors), true);
		if (mapped == ALL_MAPPED) {
			return;
		}

		for (Actor actor: priorityOrderedActors) {
			if (actor.getPartitionID() > -1) {
				continue;
			}
			Fraction coef = actor.getCoefficient();
			double deadline = actor.getSymbolicDeadline().setMUL(coef).getValue();
			long bestT = Actor.MAX_PERIOD;
			Partition bestPartition = partitions.get(0);
			for (Partition partition: partitions.values()) {
				long tempT;
				partition.addActor(actor, true);
				if (srta) {
				    tempT = new SchedulingOracleSP(gpr, true).standardSRTA(partition, 0L, bestT); 
					if (tempT == SchedulingOracleSP.incoherentPeriod) {
					        partition.removeActor(actor, true);
					        continue;
					}
				} else {
					/* FBB_FFD */
					double sum = actor.getWcet(); 
					for (Actor a: partition.getActors()) {
						sum += a.getWcet() * (1 + (deadline / a.getCoefficient().getValue()));
					}
					tempT = (long) Math.ceil(sum / deadline);
				}
				partition.removeActor(actor, true);
				if (tempT < bestT) {
					bestPartition = partition; 
					bestT = tempT; 
				} else if (tempT == bestT) {
					/* min load */
					if (bestPartition.getSymbolicLoad() > partition.getSymbolicLoad()) { 
						bestPartition = partition; 
						bestT = tempT; 
					}
				}
			}
			if (bestT == Actor.MAX_PERIOD) {
				throw new AdfgInternalException("The best period found during partition is MAX_INT.");
			}
			bestPartition.addActor(actor, true); 
		}		
		logPartitioningMessage();
	}

	/**
	 * Apply partitioning for EDF policy, choosing the partition
	 * that minimize some metrics (SQPA if {@code SQPA} is true,
	 * component load otherwise).
	 * 
	 * See Adnan Bouakaz thesis p. 109, section 3.3.3 .
	 * 
	 * @param sqpa True if SQPA metric must be used instead of
	 * component load.
	 */
	public void partitioningBestFitEDF(boolean sqpa) {
		int mapped = checkAndSetMapping(Arrays.asList(priorityOrderedActors), false);
		if (mapped == ALL_MAPPED) {
			return;
		}

		for (Actor actor: priorityOrderedActors) {
			if (actor.getPartitionID() > -1) {
				continue;
			}
			long bestT = Actor.MAX_PERIOD;
			Partition bestPartition = partitions.get(0);
			for (Partition partition: partitions.values()) {
				long tempT;
				partition.addActor(actor, false);
				if (sqpa) {
					tempT = new SchedulingOracleEDF(gpr, true).standardSQPA(partition);
					if (tempT == SchedulingOracleSP.incoherentPeriod) {
					        partition.removeActor(actor, false);
						continue;
					}
				} else {
					/* implicit deadlines: utilization-based test U <= 1*/
					double load = partition.getSymbolicLoad();
					tempT = (long) Math.ceil(load);
				}
				partition.removeActor(actor, false);

				if (tempT < bestT) { 
					bestPartition = partition; 
					bestT = tempT; 
				} else if (tempT == bestT) {
					/* min load */
					if (bestPartition.getSymbolicLoad() > partition.getSymbolicLoad()) { 
						bestPartition = partition; 
						bestT = tempT; 
					}
				}
			}
			if (bestT == Actor.MAX_PERIOD) {
				throw new AdfgInternalException("The best period found during partition is MAX_INT.");
			}
			bestPartition.addActor(actor, false); 
		}
		logPartitioningMessage();
	}


	/**
	 * Apply partitioning for EDF with PQPApart algorithm.
	 * <p>
	 * This work is not from the Adnan Bouakaz thesis, is has been
	 * adapted from his article "Earliest-Deadline First Scheduling of Multiple
	 * Independent Dataflow Graphs".
	 */
	public void partitioningEDFmodifiedPQPApart() {
		int mapped = checkAndSetMapping(Arrays.asList(priorityOrderedActors), false);
		if (mapped == ALL_MAPPED) {
			SchedulingOracleEDF oracle = new SchedulingOracleEDF(gpr, true);
			long[] periods = oracle.modifiedPQPA();
			if (periods == null) {
				throw new AdfgInternalException("Provided mapping is not schedulable.");
			}
			return;
		}
		int nbWCPs = gpr.getWeaklyConnectedComponents().size();
		WeaklyConnectedComponent[] indexedWCPs = new WeaklyConnectedComponent[nbWCPs];
		long[] indexedPUBs = new long[nbWCPs];
		long[] indexedDivFactor = new long[nbWCPs];
		long[] indexedBestK = new long[nbWCPs];
		double psiCoef = ((double) nbWCPs / nbProcessor);
		int i = 0;

		for (WeaklyConnectedComponent wcp: gpr.getWeaklyConnectedComponents()) {
			indexedWCPs[i] = wcp;
			indexedPUBs[i] = wcp.getPeriodUpperBound();
			indexedDivFactor[i] = wcp.getDivFactor();
			long inf = Math.max(wcp.getPeriodLowerBound(), (long) Math.floor(wcp.getSymbolicLoad() * psiCoef)); 
			long k = inf / indexedDivFactor[i]; 
			indexedBestK[i] = k;
			long basisPeriod = k * indexedDivFactor[i];
			if (basisPeriod > indexedPUBs[i]) {
				throw new AdfgInternalException("One component period is above the upper bound.");
			}
			wcp.setPeriods(basisPeriod);
			++i;
		}

		SchedulingOracleEDF oracle = new SchedulingOracleEDF(gpr, true);
		for (Actor actor: priorityOrderedActors) {
			if (actor.getPartitionID() > -1) {
				continue;
			}
			Partition bestPartition = null;
			long[] indexedNextIterBestK = indexedBestK;
			double bestU = 0;
			double bestPartitionLoad = 1.0;
			for (Partition partition: partitions.values()) {
				partition.addActor(actor, false);
				long[] tempK = oracle.modifiedPQPA(partition, indexedNextIterBestK, indexedWCPs);
				if (tempK == null) {
				        partition.removeActor(actor, false);
					continue;
				}
				double tempU = gpr.getTotalUtilizationFactor();
				double tempLoad = partition.getLoad();
				if (tempU == SSAnalysis.uncomputedUtilizationFactor) {
					throw new AdfgInternalException("Total utilization factor couldn't be computed.");
				} else if (tempU > bestU || ((Arrays.equals(tempK, indexedBestK) ||
						Arrays.equals(tempK, indexedNextIterBestK)) && 
						bestPartitionLoad > tempLoad)) {
					bestU = tempU;
					indexedBestK = tempK;
					bestPartition = partition;
					bestPartitionLoad = tempLoad;
				}
				partition.removeActor(actor, false);
			}
			if (bestPartition == null) {
				throw new AdfgInternalException("Cannot partition the system with PQPA.");
			}
			bestPartition.addActor(actor, false); 
		}
		for (i = 0; i < nbWCPs; ++i) {
			WeaklyConnectedComponent wcp = indexedWCPs[i];
			indexedDivFactor[i] = wcp.getDivFactor();
			long basisPeriod = indexedDivFactor[i] * indexedBestK[i];
			wcp.setPeriods(basisPeriod);
		}
		
		logPartitioningMessage();
	}	

	/**
	 * Count affine relations linked to actors on different partitions.
	 * 
	 * @return Number of crossing affine relations.
	 */
	private int countCrossingRelation() {
		/* count edges between parts */
		int nbCrossingEdges = 0;
		for (Object o: undirectedGraph.edgeSet()) {
			Actor src = undirectedGraph.getEdgeSource(o); 
			Actor dest = undirectedGraph.getEdgeTarget(o); 
			if (!Actor.onSameProcessor(src, dest)) {
				++nbCrossingEdges;
			}
		}
		return nbCrossingEdges;
	}

	/**
	 * Log the partitioning result: actors' partition number,
	 * number of crossing affine relations among partitions
	 * and number of effective (i.e. non empty) partitions.
	 */
	private void logPartitioningMessage() {
		StringBuilder sb = new StringBuilder("Exiting:\n");
		sb.append(" ========== Partitioning ==========\n");
		sb.append("Number of affine relations crossing partitions: " + countCrossingRelation() + "\n\n");
		int nbEffectivePartitions = 0;
		for (Partition partition: partitions.values()) {
			double load = 0;
			sb.append("Partition " + partition.getID() + " :\n");
			if (partition.getNbActors() > 0) {
			    nbEffectivePartitions += 1;
			}
			for (Actor a: partition.getActors()) { 
				load += a.getSymbolicLoad(); 
				sb.append(a.getName() + "; ");
			}
			sb.append(" <end> (Load = " + load + ")\n\n");
		}
		sb.append("Number of effective partitions: " + nbEffectivePartitions + "\n\n");
		LOGGER.info(sb.toString());
	}

	/**
	 * Check if the mapping is consistent (partition number less
	 * than total available processors), and set the partition if
	 * its specified.
	 * <p>
	 * An exception is thrown if mapping is not consistent.
	 * 
	 * @param actors Actors to check and eventually add into their partition
	 * (if already defined).
	 * @param addInPartitionPrioritiesSet If the actors must be add in the
	 * their partition priority list.
	 * @return ALL_MAPPED, NONE_MAPPED or PARTLY_MAPPED.
	 */
	int checkAndSetMapping(Collection<Actor> actors, boolean addInPartitionPrioritiesSet) {
		int nbMapped = 0;
		for (Actor actor: actors) {
			int part = actor.getPartitionID();
			if (part < nbProcessor && part >= -1) {
				if (part > -1) {
					nbMapped++;
					gpr.getMappedPartitions().get(actor.getPartitionID()).addActor(actor, addInPartitionPrioritiesSet);
				}
			} else {
				throw new AdfgInternalException("Partition number " + part + " out of range for actor <" +
						actor.getName() + ">.");
			}
		}
		if (nbMapped == nbActors) {
			logPartitioningMessage();
			return ALL_MAPPED;
		} else if (nbMapped == 0) {
			return NONE_MAPPED;
		}
		return PARTLY_MAPPED;
	}

}
