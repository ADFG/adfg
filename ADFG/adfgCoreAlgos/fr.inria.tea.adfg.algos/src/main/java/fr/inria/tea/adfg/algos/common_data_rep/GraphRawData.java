/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.common_data_rep;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.jgrapht.graph.DirectedMultigraph;
import org.jgrapht.graph.SimpleGraph;

/**
 * This class stores primary information on an ADFG graph:
 * - dataflow graph
 * - affine graph
 * - file describing the graph (if one)
 * Also it gives acces to actors and channels by different
 * means: graphs or map (by name).
 * 
 * @author Alexandre Honorat
 */
public class GraphRawData implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected DirectedMultigraph<Actor, Channel> adfgGraph;
    protected DirectedMultigraph<Actor, AffineRelation> affineGraph;
    protected SimpleGraph<Actor, Object> undirectedGraph;
	protected Map<String, Actor> mappedActors;
	protected Map<String, Channel> mappedChannels;
	protected Map<String, Loop> mappedLoops;
    /**
     * File name associated to the described graph, may be null.
     */
    protected String fileName;
    /**
     * Whether or not this file is in the standard maven resource folder.
     */
	protected boolean isXMLresource;

	/**
	 * Initialize the object: create internal storage capacities
	 * and copy the two parameters.
	 * 
	 * @param fileName File describing the graph ({@code null} if none).
	 * @param isXMLresource If {@code fileName} is not {@code null},
	 * specify if the file can be found in the standard maven resource folder.
	 */
	protected GraphRawData(String fileName, boolean isXMLresource) {
		adfgGraph = new DirectedMultigraph<>(Channel.class);
		affineGraph = new DirectedMultigraph<>(AffineRelation.class);
		undirectedGraph = new SimpleGraph<>(Object.class);
		mappedActors = new TreeMap<>();
		mappedChannels = new TreeMap<>();
		mappedLoops = new TreeMap<>();
		this.fileName = fileName;
		this.isXMLresource = isXMLresource;
	}

	/**
	 * Clone constructor: all internal variables are copied by
	 * reference.
	 *
	 * @param grd Object to clone.
	 */
	protected GraphRawData(GraphRawData grd) {
		this.adfgGraph = grd.adfgGraph;
		this.affineGraph = grd.affineGraph;
		this.undirectedGraph = grd.undirectedGraph;
		this.mappedActors = grd.mappedActors;
		this.mappedChannels = grd.mappedChannels;
		this.mappedLoops = grd.mappedLoops;
		this.fileName = grd.fileName;
		this.isXMLresource = grd.isXMLresource;
	}	

	/**
	 * @return Data flow graph.
	 */
	public DirectedMultigraph<Actor, Channel> getAdfgGraph() {
		return adfgGraph;
	}

	/**
	 * @return Affine relation graph.
	 */
	public DirectedMultigraph<Actor, AffineRelation> getAffineGraph() {
		return affineGraph;
	}
	
	/**
	 * @return Dataflow graph as undirected, only with actor informations.
	 */
	public SimpleGraph<Actor, Object> getUndirectedGraph() {
		return undirectedGraph;
	}
	
	/**
	 * @return Graph's actors mapped to their name.
	 */
	public Map<String, Actor> getMappedActors() {
		return mappedActors;
	}

	/**
	 * @return Graph's channels mapped to their name.
	 */
	public Map<String, Channel> getMappedChannels() {
		return mappedChannels;
	}
	
	/**
	 * @return Graph's loops (channels with their source equal to
	 * their destination).
	 */
	public Map<String, Loop> getMappedLoops() {
		return mappedLoops;
	}
	
	/**
	 * @return Filename used to load the graph. Null if there is not.
	 */
	public String getFileName() {
		return fileName;
	}	

	/**
	 * @return True if constructed from an XML resource (in maven folder).
	 * False otherwise, especially if no XML has been used to construct the graph.
	 */
	public boolean isXMLresource() {
		return isXMLresource;
	}

}
