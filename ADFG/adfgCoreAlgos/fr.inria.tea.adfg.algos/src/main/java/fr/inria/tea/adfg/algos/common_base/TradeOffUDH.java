/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.common_base;

/**
 * This class stores the user choice for SP_UNI_UDH algorithm.
 * 
 * @author honorat
 */
public class TradeOffUDH {

	private double lossT;
	private double gainB;
	private int permSize;
	
	public final static String DEFAULT_TRADEOFF_UDH = "10-10-4";
	
	public TradeOffUDH(String tradeoffUDH) {
		if (tradeoffUDH == null || tradeoffUDH.isEmpty()) {
			tradeoffUDH = DEFAULT_TRADEOFF_UDH;
		}
		if (!tradeoffUDH.matches("[0-9]+-[0-9]+(-[0-9]+)?")) {
			throw new AdfgInternalException(
					"Tradeoff UDH string does not match the format <int>-<int>-<int>.");
		}
		String[] elts = tradeoffUDH.split("-");
		lossT = Double.parseDouble(elts[0])/100;
		gainB = Double.parseDouble(elts[1])/100;
		if (gainB >= 100.0) {
			throw new AdfgInternalException(
					"Tradeoff UDH buffer gain cannot excess 99 percent.");
		}
		permSize = Integer.parseInt(elts[2]);
		if (permSize < 1 || gainB < 0.0 || lossT < 0.0) {
			throw new AdfgInternalException(
					"Tradeoff UDH values must be strictly positive.");
		}
	}

	/**
	 * @return the lossT
	 */
	public double getLossT() {
		return lossT;
	}

	/**
	 * @return the gainB
	 */
	public double getGainB() {
		return gainB;
	}

	/**
	 * @return the permSize
	 */
	public int getPermSize() {
		return permSize;
	}
			
}
