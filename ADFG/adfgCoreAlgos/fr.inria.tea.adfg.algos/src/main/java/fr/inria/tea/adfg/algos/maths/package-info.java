/**
 * This package contains the base classes to perform mathematical operations.
 */
package fr.inria.tea.adfg.algos.maths;