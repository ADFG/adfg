/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.ssa;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import fr.inria.tea.adfg.algos.common_base.AbstractSolver;
import fr.inria.tea.adfg.algos.common_base.AdfgInternalException;
import fr.inria.tea.adfg.algos.common_data_rep.Actor;
import fr.inria.tea.adfg.algos.common_data_rep.WeaklyConnectedComponent;
import lpsolve.LpSolve;
import lpsolve.LpSolveException;


/**
 * This class in the symbolic schedulability analysis
 * is in charge of calling the ILP solver to compute
 * actors' phases and periods (not the affine relations'
 * one).
 * 
 * @author Adnan Bouakaz
 * @author Alexandre Honorat
 */
class ILPSymbolicSchedulability extends AbstractSolver {

	private static final Logger LOGGER = Logger.getLogger("AdfgLog");	 
	
	protected enum Objective {MAXIMIZE_PERIOD, MINIMIZE_PERIOD, BASIS_PERIOD}
	
	private Objective objective;
	private long periodMin;
	
	// WCP
	private WeaklyConnectedComponent wcp;
	
	/**
	 * Create the object in charge of the ILP resolution (to
	 * set actors' phase and period).
	 * 
	 * @param wcp Connected part of the graph to work with.
	 * @param objective If {@code MAXIMIZE_PERIOD}: maximize the period (bounded to 
	 * {@code periodUpperBound}); else if {@code BASIS_PERIOD}: use the basis actor
	 * period; else minimize the period (so maximize the throughput) but 
	 * ensure that the computed period is greater than the next parameter.
	 * @param periodMin Minimal period authorized.
	 */
	protected ILPSymbolicSchedulability(WeaklyConnectedComponent wcp, Objective objective, long periodMin) {
		super("SSA", "ILP-SSA.lp");
		this.wcp = wcp;
		this.objective = objective;
		this.periodMin = periodMin;
	}
	
	/** 
	 * Solve the problem regarding to the current properties.
	 * Compute actors' phases and periods.
	 * 
	 */
	@Override
	protected void solving() {
		Set<Actor> actors = wcp.getActors();
		Actor basis = wcp.getBasis();
		long periodLowerBound = wcp.getPeriodLowerBound();
		long periodUpperBound = wcp.getPeriodUpperBound();
		String constraints = wcp.getConstraints();
		String general = wcp.getGeneral();
		String userPeriodConstraints = wcp.getUserPeriodConstraints();

		try (FileWriter lpProblem = new FileWriter(fileName)) {
			long plb = Math.max(periodMin, periodLowerBound);

			switch (objective) {
			case MAXIMIZE_PERIOD:
				lpProblem.write("Maximize: \n p_"+basis.getID()+" - r_"+basis.getID()+";\n");
				break;
			case BASIS_PERIOD:
				if (basis.getUserPeriod() != Actor.UNDEFINED && basis.getPeriod() > basis.getUserPeriod()) {
					throw new AdfgInternalException("The basis user period is less than the minimal one,"
							+ " the system is not schedulable.");
				}
			case MINIMIZE_PERIOD: default:
				lpProblem.write("Minimize: \n p_"+basis.getID()+" + r_"+basis.getID()+";\n");
				break;
			}

			lpProblem.write("\n/* Subject To */\n\n\n"+userPeriodConstraints+"\n\n"+constraints);
			lpProblem.write("\n/* Bounds */\n\n"+"p_"+basis.getID()+" >= "+ Math.max(plb, basis.getPeriod())+";\n"+
							"p_"+basis.getID()+" <= "+periodUpperBound+";\n\n");
			lpProblem.write("\n/* General */\n\n"+general+"\n");
		} catch (IOException e) {
			throw new AdfgInternalException("Problem during "+name+" ILP file writing.", e);
		}
				
		LpSolve ilp = solveILP(LOGGER);
			
		/* save the solution */
		Map<String,Long> periods = new HashMap<>(), phases = new HashMap<>();
		try {
			double[] vals = ilp.getPtrVariables(); 
			for (int i = 0; i < vals.length; i++) {
				String var = ilp.getColName(i+1);
				long value = (long) vals[i];
				if (var.charAt(0) == 'p') {
					periods.put(var, value);
				} else if (var.charAt(0) == 'r') {
					phases.put(var, value);
				}
			}
		} catch (LpSolveException e) {
			throw new AdfgInternalException("Problem during "+name+" ILP results reading.", e);
		}

		deleteILP(LOGGER, ilp);

		for (Actor actor: actors) {
			actor.setPeriod(periods.get("p_"+actor.getID()));
			actor.setPhase(phases.get("r_"+actor.getID()));
		}
	}	
		
}
