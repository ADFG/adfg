/**
 * This package contains the classes performing Symbolic Scheduling Analysis (SSA).
 */
package fr.inria.tea.adfg.algos.ssa;