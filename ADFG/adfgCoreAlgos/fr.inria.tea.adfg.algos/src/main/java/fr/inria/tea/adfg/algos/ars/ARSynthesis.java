/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.ars;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jgrapht.GraphPath;
import org.jgrapht.alg.cycle.PatonCycleBase;

import fr.inria.tea.adfg.algos.common_base.AdfgInternalException;
import fr.inria.tea.adfg.algos.common_base.ESchedAlgo;
import fr.inria.tea.adfg.algos.common_base.ESchedPolicy;
import fr.inria.tea.adfg.algos.common_data_rep.Actor;
import fr.inria.tea.adfg.algos.common_data_rep.GraphProbData;
import fr.inria.tea.adfg.algos.common_data_rep.Partition;
import fr.inria.tea.adfg.algos.common_data_rep.WeaklyConnectedComponent;

/**
 * Main class for the affine relation synthesis.
 * Compute affine relations' phases between connected actors.
 * 
 * @author Adnan Bouakaz
 * @author Alexandre Honorat
 */
public class ARSynthesis {

	private static final Logger LOGGER = Logger.getLogger("AdfgLog");	

	/**
	 * Precision to enable hybrid deadline adjustment
	 * (deadline coefficient, between 0 and 1).
	 */
	private static final double HDA_PRECISION = 0.7;

	private Set<WeaklyConnectedComponent> weaklyConnectedComponents;
	private ESchedAlgo scheduleType; 

	private Set<List<Actor>> cycleBasis;
	private GraphProbData gpr;

	/**
	 * Create an ARSynthesis instance which can be solved.
	 * 
	 * @param gpr Problem representation to work on.
	 */
	public ARSynthesis(GraphProbData gpr) {
		this.gpr = gpr;

		weaklyConnectedComponents = gpr.getWeaklyConnectedComponents();
		scheduleType = gpr.getScheduleType();
		
		cycleBasis = new HashSet<>();
	}


	/**
	 * Solve the problem regarding to its scheduling policy.
	 */
	public void solving() {

		LOGGER.info("Entering:\n========== Affine Relation Synthesis ==========");

		decomposition();

		if (!scheduleType.isMultiprocessor() || scheduleType.isGlobal()) {
			/* the only partition must be filled in case of uniprocessor scheduling */
			Partition part = gpr.getMappedPartitions().get(0);
			for (Actor actor: gpr.getActors()) {
				part.addActor(actor, false);						
			}
		}

		GraphPartitioning partitioning = new GraphPartitioning(gpr);
		PriorityComputing pc = new PriorityComputing(gpr);
		/* priority assignment and partitioning */
		switch (scheduleType) {
		case EDF_UNI: case EDF_UNI_DA: case GEDF_MULT: case EDF_UNI_PQPA: break;
		case EDF_UNI_HDA:
			gpr.markNoDeadlineAdjustment(HDA_PRECISION);
			break;
		case EDF_MULT_MBS_HDA:
			gpr.markNoDeadlineAdjustment(HDA_PRECISION);
			partitioning.partitioningSCOTCH();
			break;
		case EDF_MULT_MBS_ID:
			partitioning.partitioningSCOTCH();
			break;
		case EDF_MULT_BF_UF_ID:
			pc.setPrioritiesDM();
			gpr.setPriorityOrderedActors();
			partitioning.partitioningBestFitEDF(false);
			break;	
		case EDF_MULT_BF_SQPA_CD:
			pc.setPrioritiesDM();
			gpr.setPriorityOrderedActors();
			partitioning.partitioningBestFitEDF(true);
			break;
		case EDF_MULT_BF_PQPA:
			pc.setPrioritiesDM();
			gpr.setPriorityOrderedActors();
			partitioning.partitioningEDFmodifiedPQPApart();
			break;
		case SP_UNI:
			pc.setPrioritiesDM();
			gpr.setPartitionPriorityOrderedActors(true);
			break;
		case SP_UNI_LOP:
			pc.setPrioritiesLOP();
			gpr.setPartitionPriorityOrderedActors(true);
			break;
		case SP_MULT_MBS:
			pc.setPrioritiesDM();
			partitioning.partitioningSCOTCH();
			pc.setPrioritiesMLOP();
			gpr.setPartitionPriorityOrderedActors(true);
			break;
		case SP_MULT_BF_FBBFFD:
			pc.setPrioritiesDM();
			gpr.setPriorityOrderedActors();
			partitioning.partitioningBestFitFP(false);
			break;
		case SP_MULT_BF_SRTA:
			pc.setPrioritiesDM();
			gpr.setPriorityOrderedActors();
			partitioning.partitioningBestFitFP(true);
			break;
		case SP_UNI_UDH:
			WeaklyConnectedComponent wcp = weaklyConnectedComponents.iterator().next();
			pc.setPrioritiesUtilizationDistanceHeuristic(wcp, gpr.getUserParams().getTradeOffUDH());
			gpr.setPartitionPriorityOrderedActors(true);
			break;
		default:
			if (scheduleType.schedulingPolicy() == ESchedPolicy.TT) {
				if (GraphPartitioning.ALL_MAPPED != partitioning.checkAndSetMapping(gpr.getActors(), false)) {
					throw new AdfgInternalException("Incomplete Mapping ! "
							+ "All actors must be already mapped for Time Trigerred policies (see -b option).");
				}
				break;
			}
			throw new AdfgInternalException("Schedule type not recognized in ARS.");
		}

		for (WeaklyConnectedComponent wcp: weaklyConnectedComponents) {
			wcp.updatePLBwithNbPartitions(scheduleType.isGlobal());
		}
		
		if (scheduleType.schedulingPolicy() == ESchedPolicy.SP) {
			StringBuilder sb = new StringBuilder(
					"Exiting:\n========== Affine Relation Synthesis -- Fixed priorities ==========\n");
			sb.append("Actor priorities (lower value, higher priority):\n");
			for (Partition partition: gpr.getMappedPartitions().values()) {
				sb.append("Partition " + partition.getID() +" :\n");
				for (Actor actor: partition.getPriorityOrderedActors()) {
					sb.append(actor.getName() + ": n°" + actor.getPriority() + " ;\t");
				}
				sb.append("<end>\n\n");
			}
			LOGGER.info(sb.toString());
		}

		new ILPAffineRelation(gpr, cycleBasis).solving();		

	}

	/**
	 * Compute all cycles in the graph.
	 */
	private void decomposition() {		
		PatonCycleBase<Actor,Object> pcb = new PatonCycleBase<>(gpr.getUndirectedGraph());
		Set<GraphPath<Actor, Object>> cycleBasisI = pcb.getCycleBasis().getCyclesAsGraphPaths();
		for (GraphPath<Actor, Object> gp: cycleBasisI) {
			cycleBasis.add(gp.getVertexList());
		}

		/* print */
		if (LOGGER.isLoggable(Level.FINE)) {
			StringBuilder sb = new StringBuilder();
			sb.append("Graph infos: \n");
			sb.append("========== Weakly connected components ==========\n");
			sb.append(weaklyConnectedComponents.size() + " weakly connected components (one per line):\n");
			for (WeaklyConnectedComponent wcp: weaklyConnectedComponents) {
				for (Actor actor: wcp.getActors()) {
					sb.append(actor.getName()+"; ");
				}
				sb.append("<end> \n");
				sb.append("-- metrics: psi = "+wcp.getSymbolicLoad()+"; divFactor = "+wcp.getDivFactor()+
						"; basis = "+wcp.getBasis().getName()+" (p_"+wcp.getBasis().getID()+")\n");
			}
			sb.append("========== Cycle Basis ==========\n");
			sb.append(cycleBasis.size() + " cycle basis (one per line):\n");
			for (List<Actor> cycle: cycleBasis) {
				for (Actor actor: cycle) {
					sb.append(actor.getName() + " --> ");
				}
				sb.append("<end> \n");
			}
			LOGGER.fine(sb.toString());
		}
	} 

}
