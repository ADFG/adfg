/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.common_data_rep;

import fr.inria.tea.adfg.algos.common_base.AdfgExternalException;
import fr.inria.tea.adfg.algos.maths.BasicArithmetic;

/**
 * This class represents an actor's loop: the production
 * and consumption of an actor in/from its own memory.
 * 
 * @author Alexandre Honorat
 */
public class Loop extends Channel {

	private long maxComparisonLength;
	private Actor actor;
	
	/**
	 * Builds and initializes the loop. Consumption is always
	 * done before production.
	 * <p>
	 * UPIS are not deep copied.
	 * <p>
	 * Production rate (will be prefixed by 0
	 * in order to simulate the consumption precedence).
	 *
	 * @param channel The loop is derived from the channel.
	 * @param actor Actor which has this loop.
	 * @throws AdfgExternalException If the memory usage diverge.
	 */
	public Loop(Channel channel, Actor actor) throws AdfgExternalException {
		super(channel, true, true);
		this.actor = actor;
		long periodicLCM = BasicArithmetic.funcLCM(produce.getPeriodLength(), consume.getPeriodLength());
		maxComparisonLength = Math.max(produce.getPrefixLength(), consume.getPrefixLength()) + periodicLCM;
		if (! produce.getFiringRatio().isEqualTo(consume.getFiringRatio())) {
			throw new AdfgExternalException("Divergent Memory usage for loop: <"+name+">");
		}
	}
	
	/**
	 * Copy constructor.
	 * 
	 * @param loop Loop to copy.
	 * @param actor New actor associated to the loop.
	 */
	public Loop(Loop loop, Actor actor) {
		super(loop, true, true);
		this.actor = actor;
		this.maxComparisonLength = loop.maxComparisonLength;
	}
	
	/**
	 * Compute and set the initial memory size.
	 */
	public void setInitMemSize() {
		if (initial == UNDEFINED) {
			initial = consume.elementAt(0);
			long sum = initial;
			for (int i = 0; i < maxComparisonLength; ++i) {
				sum += consume.elementAt(i+1) - produce.elementAt(i);
			}
			if (sum > initial) {
				initial = sum;
			}
		}
	}

	/**
	 * Compute and set the maximum memory size.
	 */
	public void setMaxMemSize() {
		setInitMemSize();
		if (size == UNDEFINED) {
			size = initial;
			long sum = initial;
			for (int i = 0; i < maxComparisonLength; ++i) {
				sum += produce.elementAt(i) - consume.elementAt(i);
			}			
			if (sum > size) {
				size = sum;
			}
		}
	}
	
	/**
	 * @return Loop's actor owner.
	 */
	public Actor getActor() {
		return actor;
	}
	
}
