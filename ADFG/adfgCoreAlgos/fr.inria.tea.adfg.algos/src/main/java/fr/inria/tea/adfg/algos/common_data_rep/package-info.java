/**
 * This package contains the base classes to represent data.
 */
package fr.inria.tea.adfg.algos.common_data_rep;