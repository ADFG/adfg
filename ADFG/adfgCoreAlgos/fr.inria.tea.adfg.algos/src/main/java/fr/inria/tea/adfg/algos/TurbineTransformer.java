/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import fr.inria.tea.adfg.algos.common_base.AdfgExternalException;
import fr.inria.tea.adfg.algos.common_data_rep.Actor;
import fr.inria.tea.adfg.algos.common_data_rep.Channel;
import fr.inria.tea.adfg.algos.maths.UPIS;

/**
 * This class can import Turbine graphs (.tur) in ADFG,
 * export and update are not supported.
 * 
 * @author ahonorat
 */
public class TurbineTransformer {


	/**
	 * Do nothing.
	 */
	public TurbineTransformer() {
	}

	/**
	 * Parses the Turbine file (.tur) and fill the graph.
	 * <p>
	 * The extension .tur has to be checked before calling this function. 
	 * 
	 * @param adfgr Graph into one adding the Turbine edges and vertices.
	 * 
	 * @throws AdfgExternalException If something went wrong 
	 * (especially if the file is not sdf neither csdf).
	 */
	public static void initGraphFromTurbine(ADFGraph adfgr) throws AdfgExternalException {
		String fileName = adfgr.getFileName();
		int[] bounds = new int[3];
		bounds[0] = 0;
		bounds[1] = 0;
		bounds[2] = 0;
		try (FileReader file = new FileReader(fileName)) {
			BufferedReader br = new BufferedReader(file);
			readBasicProperties(br, bounds);
			readActorProperties(br, bounds, adfgr);
			readArcsProperties(br, bounds, adfgr);
		} catch (FileNotFoundException e) {
			throw new AdfgExternalException("The file "+fileName+" has not been found on disk.", e);
		} catch (IOException e) {
			throw new AdfgExternalException(e);
		} catch (NumberFormatException e) {
			throw new AdfgExternalException("Number parsing error on line "+bounds[2]+".", e);
		}
	}
	
	private static void readBasicProperties(BufferedReader br, int[] bounds) throws IOException, AdfgExternalException {		
		String line = null;
		while ((line = br.readLine()) != null) {
			bounds[2]++;
			if (line.startsWith("#")) {
				continue;
			}
			String[] parse = line.split(" ");
			if (parse.length != 2) {
				throw new AdfgExternalException("Input file format error on line "+bounds[2]+".");
			}
			if (!"CSDFG".equalsIgnoreCase(parse[1]) && !"CSDF".equalsIgnoreCase(parse[1]) &&
					!"SDFG".equalsIgnoreCase(parse[1]) && !"SDF".equalsIgnoreCase(parse[1])) {
				throw new AdfgExternalException("The graph is neither SDF nor CSDF, abandon. (line "+bounds[2]+")");
			}
			break;
		}
		while ((line = br.readLine()) != null) {
			bounds[2]++;
			if (line.startsWith("#")) {
				continue;
			}
			String[] parse = line.split(" ");
			if (parse.length != 2) {
				throw new AdfgExternalException("Input file format error on line "+bounds[2]+".");
			}
			int nbTasks = Integer.parseInt(parse[0]);
			int nbArcs = Integer.parseInt(parse[1]);
			if (nbTasks <= 0 || nbArcs <= 0) {
				throw new AdfgExternalException("The number of tasks/arcs is not correct. (line "+bounds[2]+")");
			}
			bounds[0] = nbTasks;
			bounds[1] = nbArcs;
			break;
		}
				
	}
	
	private static void readActorProperties(BufferedReader br, int[] bounds, ADFGraph adfgr) throws IOException, AdfgExternalException {
		int nbActors = 0;
		
		String line = null;
		while ((line = br.readLine()) != null) {
			bounds[2]++;
			if (line.startsWith("#")) {
				continue;
			}
			nbActors++;
			String[] parse = line.split(" ");
			if (parse.length != 3) {
				throw new AdfgExternalException("Input file format error on line "+bounds[2]+".");
			}
			String name = "t"+parse[0];
			String wcetsStr = parse[2];
			String[] wcetsStrArray = wcetsStr.split(",");
			int lengthCyclo = wcetsStrArray.length;
			long maxWCET = 1;
			long avgWCET = lengthCyclo > 0 ? 0 : 1;
			for (String s: wcetsStrArray) {
				long wcet = (long) Math.ceil(Double.parseDouble(s));
				avgWCET += wcet;
				if (wcet > maxWCET) {
					maxWCET = wcet;
				}
			}
			if (lengthCyclo > 0) {
				avgWCET /= lengthCyclo;
			}
			adfgr.addActor(new Actor(name, maxWCET, avgWCET, Actor.UNDEFINED));
			if (nbActors == bounds[0]) {
				break;
			}
		}
	}
	
	
	private static void readArcsProperties(BufferedReader br, int[] bounds, ADFGraph adfgr) throws IOException, AdfgExternalException {
		int nbArcs = 0;

		String line = null;
		while ((line = br.readLine()) != null) {
			bounds[2]++;
			if (line.startsWith("#")) {
				continue;
			}
			nbArcs++;
			String[] parse = line.split(" ");
			if (parse.length != 4 || !parse[0].matches("\\([0-9]+,[0-9]+\\)")) {
				throw new AdfgExternalException("Input file format error on line "+bounds[2]+".");
			}
			String[] srcDst = parse[0].split(",");
			String src = "t"+srcDst[0].replace("(", "");
			String dst = "t"+srcDst[1].replace(")", "");
			if (!adfgr.getMappedActors().containsKey(src) || !adfgr.getMappedActors().containsKey(dst)) {
				throw new AdfgExternalException("Unknown actor on line "+bounds[2]+".");
			}
			long initSize = Long.parseLong(parse[1]);
			UPIS prate = new UPIS("("+parse[2].replace(",", " ")+")");
			UPIS crate = new UPIS("("+parse[3].replace(",", " ")+")");
			Channel chan = new Channel(src+"_to_"+dst+"--"+nbArcs, prate, crate, Channel.UNDEFINED, initSize, 1);
			adfgr.addChannel(chan, adfgr.getMappedActors().get(src), adfgr.getMappedActors().get(dst));
			if (nbArcs == bounds[1]) {
				break;
			}
		}
	}
	
}
