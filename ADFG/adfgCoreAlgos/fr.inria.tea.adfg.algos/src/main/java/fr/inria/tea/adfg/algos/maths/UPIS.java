/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.maths;

import java.util.Arrays;

import fr.inria.tea.adfg.algos.common_base.AdfgInternalException;

/**
 * UPIS stands for Ultimately Periodic Integer Sequence s=u(v).
 * This class handle their representation.
 * 
 * @author Adnan Bouakaz
 * @author Alexandre Honorat
 */
public class UPIS {

	/**
	 * The value is not known yet.
	 */
	public static final int UNDEFINED = -1;

	private long[] sequence;
	private int prefixLength = 0; 
	private int periodLength = 0;

	private long sumPeriod = UNDEFINED;
	private long sumPrefix = UNDEFINED;
	private long sumTotal = UNDEFINED;
	private Fraction firingRatio = null;
	private Pair<Fraction> linearBounds = null;
	private Pair<Long> constantBounds = null;

	/**
	 * Construct an UPIS thanks to a string of integer numbers. An example is
	 * "0 2 9 10 (0 3 9 9)".
	 * 
	 * @param str UPIS string representation.
	 */
	public UPIS(String str) {
		if (!str.matches("[0-9 ]*\\([0-9 ]+\\) *")) {
			throw new AdfgInternalException("Error in syntax of a sequence.");
		}

		String[] strs = str.split("\\("); 
		strs[1] = strs[1].replaceAll("\\)", " ");		
		String u = strs[0].trim();
		String v = strs[1].trim();
		String[] uu = u.split(" +");
		String[] vv = v.split(" +");
		
		if (u.length() != 0) {
			prefixLength = uu.length;			
		}
		if (v.length() != 0) {
			periodLength = vv.length;
		} else {
			throw new AdfgInternalException("UPIS cannot have an empty periodic part.");
		}

		sequence = new long[prefixLength + periodLength];		
		for (int i = 0; i < prefixLength; ++i) {
			sequence[i] = Long.parseLong(uu[i]);
		}		
		for (int i = 0; i < periodLength; ++i){
			sequence[i+prefixLength] = Long.parseLong(vv[i]); 
		}  
	}

	/**
	 * Construct a new UPIS by deep copy.
	 * 
	 * @param upis UPIS to copy.
	 */
	public UPIS(UPIS upis) {
		prefixLength = upis.prefixLength;
		periodLength = upis.periodLength;
		sequence = upis.sequence;
	}

	/**
	 * Construct an UPIS directly with an integer array,
	 * copied by reference.
	 * <p> 
	 * Be aware that some computations are cached
	 * (sums, ratio, and bounds), you need to reset
	 * the object if you modify the array after the 
	 * object creation.
	 * 
	 * @param seq Array containing prefix and period.
	 * @param prefixLength Length of the prefix in the array.
	 */
	public UPIS(long[] seq, int prefixLength) {
		this.prefixLength = prefixLength;
		periodLength = seq.length - prefixLength;
		sequence = seq;
	}
	
	/**
	 * Reset all cached computations: sums, ratio and bounds.
	 */
	public void resetComputations() {
		sumPeriod = UNDEFINED;
		sumPrefix = UNDEFINED;
		sumTotal = UNDEFINED;
		firingRatio = null;
		linearBounds = null;
		constantBounds = null;
	}
	
	
	/**
	 * Create new UPIS with increased prefix part size.
	 * e. g. 1 (0 2) ==&gt; 1 0 2 0 (2 0)
	 * <p>
	 * Runtime exception thrown for negative parameters.
	 * 
	 * @param h Number of elements to be added at the end of the prefix part.
	 * @return New UPIS with extended prefix part, filled thanks to the
	 * periodic part.
	 */
	public UPIS shift(int h) {
		if (h < 0) {
			throw new AdfgInternalException("Cannot shift sequence with a negative parameter.");
		}
		long[] shiftedSequence = new long[sequence.length+h];
		System.arraycopy(sequence, 0, shiftedSequence, 0, sequence.length);
		for (int i = 0; i < h; ++i) {
			shiftedSequence[i+sequence.length] = elementAt(i+sequence.length);
		}
		return new UPIS(shiftedSequence, prefixLength+h);
	}

	/**
	 * Create new UPIS with unfolded period (new period will be
	 * duplicated as many times as the parameter value).
	 * e. g. 1 (0 2) ==&gt; 1 (0 2 0 2) with h = 2.
	 * <p>
	 * Runtime exception thrown if non strictly positive parameter.
	 * 
	 * @param h Number of times the period should appear (&gt; 0).
	 * @return New UPIS with extended period part, filled by
	 * duplication of the previous period part.
	 */
	public UPIS unfold(int h) {
		if (h < 0) {
			throw new AdfgInternalException("Cannot unfold sequence with negative parameter or zero.");
		}
		long[] unfoldedSequence = new long[sequence.length+(h-1)*getPeriodLength()];
		System.arraycopy(sequence, 0, unfoldedSequence, 0, sequence.length);
		for (int i = 1; i < h; ++i) {
			System.arraycopy(sequence, prefixLength, unfoldedSequence, 
					sequence.length + (i-1)*getPeriodLength(), getPeriodLength());
		}
		return new UPIS(unfoldedSequence, prefixLength);
	}

	/**
	 * Create a new UPIS that is shifted with the first parameter
	 * and then is unfolded with the second parameter.
	 * Currently not called.
	 * 
	 * @param h1 Shift parameter.
	 * @param h2 Unfold parameter.
	 * @return New UPIS that is shifted and then unfolded.
	 */
	public UPIS shiftUnfold(int h1, int h2) {
		return shift(h1).unfold(h2);
	}

	/**
	 * Distribute parameter values over
	 * strictly positive elements of the current
	 * object. e.g. 2 (1 3 7) over 1 0 (1 0 0 1) 
	 * ==&gt; 2 0 (1 0 0 3)
	 *
	 * @param w UPIS to distribute.
	 * @return New UPIS that is the distribution of
	 * the parameter over the current object (being 
	 * usually a binary activation clock).
	 */
	public UPIS distributOver(UPIS w) {
		long[] seq = new long[sequence.length];
		System.arraycopy(sequence, 0, seq, 0, sequence.length);
		int nbNonZero = 0;
		for (int i = 0; i < seq.length; ++i){
			if (seq[i] > 0) {
				seq[i] = w.elementAt(nbNonZero);
				nbNonZero++;
			}
		}
		return new UPIS(seq, prefixLength);
	}

	/**
	 * Compute the hyper binary encoding (which is an activation
	 * clock) adapted to the source and target rates' sequences, 
	 * and derived from the standard binary encoding of the source and 
	 * the target. 
	 * <p>
	 * The hyper encoding ensures that there is enough activations of the
	 * source and target on both their prefix and periodic part to meet
	 * all rates at least once.
	 * <p>
	 * Example: source and target are affine related by (1, 1, 4).
	 * The encoding is composed of seq1 = 0 (1 1 1 1) and seq2 = 
	 * 1 (0 0 1 0). w1 and w2 are user specified rates' sequences of a
	 * channel between the source and the target. w1 = 0 0 0 (0 2) and
	 * w2 = 2 2 2 (10 0 1 5). With these inputs, the new encoding is the 
	 * original one shifted 7 (sh = 7) times and unfolded 2 (k = 2) times.
	 * This give: seq1' = 1 0 0 1 0 0 0 1 (0 0 0 1 0 0 0 1) and seq2' = 
	 * 0 1 1 1 1 1 1 1 (1 1 1 1 1 1 1 1)
	 * <p>
	 * Note that if this method is called with an affine relation
	 * binary encoding,, the result sequences do not contain
	 * instants i such that seq1[i] = 0 and seq2[i] = 0.
	 *
	 * @param encoding Binary sequence of the affine relation
	 * between {@code w1} and {@code w2}. 
	 * @param w1 Data production rates of the source.
	 * @param w2 Data consumption rates of the target.
	 * @return The pair of binary sequences (activation clocks)
	 * shifted and unfolded accordingly to the hyperperiod
	 * of {@code w1} and {@code w2}.
	 */
	public static Pair<UPIS> hyperPeriod(Pair<UPIS> encoding, UPIS w1, UPIS w2) {
		UPIS seq1 = encoding.getFirst();
		UPIS seq2 = encoding.getSecond();
		
		/* shift */
		
		/* u1 is the number of 1's in the prefix of seq1
		 * notice that if u1 > 0, then u1 = seq1.PrefixLength,
		 * because this method is always called with <seq1,seq2>
		 * being the binary encoding of an affine relation.
		 */
		int u1 = 0; 
		for (int i = 0; i < seq1.getPrefixLength(); ++i) {
			if (seq1.elementAt(i) == 1) {
				++u1; 
			}
		}
		/* Compute sh1 such as sh1 is the necessary number of seq1
		 * shifts (which gives the new UPIS seq1') in order to have 
		 * l1 equal to the number of 1's in the prefix part of seq1'.
		 */
		int l1 = w1.getPrefixLength(), sh1 = 0;
		if (l1 > 0 && l1 > u1) {
			int nbOf1 = u1;
			while (nbOf1 < l1) { 
				if (seq1.elementAt(sh1+seq1.prefixLength) == 1) {
					++nbOf1; 
				}
				++sh1;
			}
		}

		/* same computation, for seq2 and w2 */
		int u2 = 0; 
		for (int i = 0; i < seq2.getPrefixLength(); ++i) {
			if (seq2.elementAt(i) == 1) {
				++u2; 
			}
		}
		int l2 = w2.getPrefixLength(), sh2 = 0;
		if (l2 > 0 && l2 > u2) {
			int nbOf1 = u2;
			while (nbOf1 < l2) { 
				if (seq2.elementAt(sh2+seq2.prefixLength) == 1) {
					++nbOf1; 
				}
				++sh2; 
			}
		}
		
		int sh; 
		if (sh1 >= sh2)	{
			sh = sh1; 
		} else { 
			sh = sh2;
		}
		UPIS res1 = seq1.shift(sh);
		UPIS res2 = seq2.shift(sh);
		
		/* unfold */
		
		/* v1 (resp. v2) is the number of 1's in the period of seq1 (resp. seq2) */
		int v1 = 0; 
		for (int i = 0; i < seq1.getPeriodLength(); ++i) {
			if (seq1.elementAt(i+seq1.getPeriodLength()) == 1) {
				++v1; 
			}
		}
		int v2 = 0; 
		for (int i = 0; i < seq2.getPeriodLength(); ++i) {
			if (seq2.elementAt(i+seq2.getPeriodLength()) == 1) {
				++v2; 
			}
		}
		
		/* sort of LCM, but not exactly.
		 * Let's m1 = w1.periodicLength (resp. m2 for w2).
		 * k ensures that m1 divides v1' (the number of 1's in the
		 * periodic part of seq1' = res1) and respectively
		 * m2 divides v2'.
		 */
		int k = (w1.getPeriodLength() * w2.getPeriodLength()) / (int) 
				BasicArithmetic.funcGCD((long) w1.getPeriodLength()*v2, (long) w2.getPeriodLength()*v1);
		if (k > 1) {
			res1 = seq1.unfold(k);
			res2 = seq2.unfold(k);
		}
		return new Pair<>(res1, res2);
	}
	
	/**
	 * Return element of the UPIS sequence (including
	 * prefix) at position {@code i}, starting from 0.
	 * 
	 * @param i Positive index to reach.
	 * @return Element at the given index.
	 */
	public long elementAt(int i) {
		if (i < 0) {
			throw new AdfgInternalException("Trying to reach negative index in UPIS.");
		} else if (i < sequence.length) {
			return sequence[i];
		}
		int index = (i - prefixLength) % (periodLength);
		return sequence[index + prefixLength];
	}

	/**
	 * Prefix length in the UPIS sequence.
	 * 
	 * @return Prefix length.
	 */
	public int getPrefixLength() {
		return prefixLength;
	}
	
	/**
	 * Periodic part length of the UPIS sequence.
	 * 
	 * @return Periodic part length.
	 */
	public int getPeriodLength() {
		return periodLength;
	}

	/**
	 * Total length of the UPIS sequence (so
	 * including prefix and periodic part). 
	 * 
	 * @return Total length.
	 */
	public int getLength() {
		return sequence.length;
	}

	/**
	 * Sum from 0 to {@code j} all sequence
	 * elements. 
	 * 
	 * @param j Final index of the sum.
	 * @return Partial sequence sum over [0--{@code j}].
	 */
	public long getCumulative(int j) {
		long sum = 0;
		for (int i = 0; i <= j; ++i) {
			sum += elementAt(i);
		}
		return sum;
	}
	
	/**
	 * Sum elements over the period part of the
	 * UPIS sequence.
	 * 
	 * @return Period elements' sum.
	 */
	public long getSumPeriod() {
		if (sumPeriod != UNDEFINED) {
			return sumPeriod;
		}
		sumPeriod = 0; 
		for (int i = prefixLength; i < sequence.length; ++i) {
			sumPeriod += sequence[i];
		}
		return sumPeriod;
	}

	
	/**
	 * Sum elements over the prefix part of the
	 * UPIS sequence.
	 * 
	 * @return Prefix elements' sum.
	 */
	public long getSumPrefix() {
		if (sumPrefix != UNDEFINED) {
			return sumPrefix;
		}
		sumPrefix = 0; 
		for (int i = 0; i < prefixLength; ++i) {
			sumPrefix += sequence[i];
		}
		return sumPrefix;
	}

	/**
	 * Sum all elements of the UPIS sequence
	 * (so including prefix and periodic part).
	 * 
	 * @return Elements' sum.
	 */
	public long getSum() {
		if (sumTotal != UNDEFINED) {
			return sumTotal;
		}
		sumTotal = getSumPrefix() + getSumPeriod(); 
		return sumTotal;
	}

	/**
	 * Compute the firing ratio of the periodic part.
	 * <p>
	 * See Adnan Bouakaz thesis p. 80 (a_x and a_y).
	 * 
	 * @return Ratio of the period sum over the period length.
	 */
	public Fraction getFiringRatio() {
		if (firingRatio == null) {
			firingRatio = new Fraction(getSumPeriod(), periodLength);
		}
		return firingRatio;
	}
	
	/**
	 * Compute the minimum and maximum lambda of the UPIS.
	 * 
	 * @return Pair of lambdas, the first being the minimum
	 * and the second begin the maximum.
	 */
	public Pair<Fraction> linearBounds() {
		if (linearBounds != null) {
			return linearBounds;
		}
		int vLength = getPeriodLength();
		long vSum = getSumPeriod(); 
		Fraction slope = new Fraction(vSum, vLength); 
		Fraction maxLambda = new Fraction(0,1);
		Fraction minLambda = new Fraction(0,1);
		long sum = 0;
		for (int i = 0; i < sequence.length; ++i){
			sum += sequence[i];
			Fraction temp = new Fraction(-(i+1), 1).setMUL(slope);
			temp.setADD(new Fraction(sum, 1));
			
			if (maxLambda.isLessOrEqual(temp)) {
				maxLambda = temp;
			}
			if (!minLambda.isLessOrEqual(temp)) {
				minLambda = temp;
			}
		}
		linearBounds = new Pair<>(minLambda, maxLambda);
		return linearBounds;
	}

	/**
	 * Compute minimum and maximum element values of
	 * the UPIS sequence (including both prefix and 
	 * periodic part).
	 * 
	 * @return Pair of bounds, minimum being the first,
	 * and maximum being the second.
	 */
	public Pair<Long> constantBounds() {
		if (constantBounds != null) {
			return constantBounds;
		}
		long min = Long.MAX_VALUE;
		long max = Long.MIN_VALUE;
		for (int i = 0; i < sequence.length; ++i) {
			if (min > sequence[i]) {
				min = sequence[i];
			}
			if (max < sequence[i]) {
				max = sequence[i];
			}
		}
		constantBounds = new Pair<>(min, max);
		return constantBounds;
	}

	/**
	 * Export the UPIS formatted for Cheddar
	 * parsing: 1_2_3(4_6) for example.
	 * 
	 * @return UPIS with underscore separators
	 * instead of spaces, there is still parenthesis
	 * around the periodic part.
	 */
	public String toCheddarString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < prefixLength - 1; ++i) {
			sb.append(sequence[i] + "_");
		}
		if (prefixLength > 0) {
			sb.append(sequence[prefixLength - 1]);
		}
		sb.append("(");
		for (int i = prefixLength; i < sequence.length - 1; ++i) {
			sb.append(sequence[i] + "_");
		}
		sb.append(sequence[sequence.length - 1]);
		sb.append(")");
		return sb.toString();		
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof UPIS) {
			return Arrays.equals(sequence, ((UPIS) obj).sequence);
		}
		return false;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < prefixLength; ++i) {
			sb.append(sequence[i] + " ");
		}
		sb.append("( ");
		for (int i = prefixLength; i < sequence.length; ++i) {
			sb.append(sequence[i] + " ");
		}
		sb.append(")");
		return sb.toString();
	}

}
