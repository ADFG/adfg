/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.ars;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import org.jgrapht.graph.SimpleGraph;

import fr.inria.tea.adfg.algos.common_base.AdfgInternalException;
import fr.inria.tea.adfg.algos.common_base.TradeOffUDH;
import fr.inria.tea.adfg.algos.common_data_rep.Actor;
import fr.inria.tea.adfg.algos.common_data_rep.GraphProbData;
import fr.inria.tea.adfg.algos.common_data_rep.Partition;
import fr.inria.tea.adfg.algos.common_data_rep.WeaklyConnectedComponent;
import fr.inria.tea.adfg.algos.maths.Fraction;
import fr.inria.tea.adfg.algos.maths.PermutationTrotter;
import fr.inria.tea.adfg.algos.ssa.SchedulingOracleSP;

/**
 * This class contain all methods computing priorities.
 * 
 * @author Adnan Bouakaz
 * @author Alexandre Honorat
 */
class PriorityComputing {

	private static final Logger LOGGER = Logger.getLogger("AdfgLog");
	
	// GraphProbData
	private GraphProbData gpr;
	

	/**
	 * Link an actor to a deadline (which is
	 * not related to its current deadline).
	 * 
	 * @author Adnan Bouakaz
	 */
	private class DeadlineInfo {
		Actor actor;
		Fraction deadline;

		DeadlineInfo(Actor a, Fraction f) {
			actor = a;
			deadline = f;
		}
	}
	
	/**
	 * Create an instance that enables to call
	 * priority computing methods.
	 * 
	 * @param gpr Problem to work on.
	 */
	public PriorityComputing(GraphProbData gpr) {
		this.gpr = gpr;
	}

	/**
	 * Insert a deadline into a list of increasing
	 * deadlines (when tie, sort by increasing topological order, 
	 * and when tie again, sort by decreasing symbolic load).
	 * 
	 * @param list Deadline info list to modify.
	 * @param deadline Element to add.
	 */
	private void insertOrderedSet(List<DeadlineInfo> list, DeadlineInfo deadline) {
		int i = 0;
		for (DeadlineInfo element: list) {
			if (deadline.deadline.isLess(element.deadline)) {
				break;
			} else if (deadline.deadline.isEqualTo(element.deadline)) {
				Actor elToAdd = deadline.actor;
				Actor currentEl = element.actor;
				int elOrder = elToAdd.getDAGorder();
				int curOrder = currentEl.getDAGorder();
				if (elOrder < curOrder) {
					break; 
				} else if (elOrder == curOrder) {
					double x = elToAdd.getSymbolicLoad();
					double y = currentEl.getSymbolicLoad();
					if (x > y) {
						break;
					}
				}
			}
			++i;
		}
		list.add(i, deadline);
	}

	/**
	 * @return Deadline info list, ordered by increasing deadline.
	 */
	private List<DeadlineInfo> getOrderedDeadlineInfo() {
		List<DeadlineInfo> priorityOrder = new ArrayList<>(gpr.getNbActors());
		for (Actor actor: gpr.getActors()) {
			Fraction coef = actor.getCoefficient();
			Fraction deadline = actor.getSymbolicDeadline().setMUL(coef);
			DeadlineInfo D = new DeadlineInfo(actor, deadline);
			insertOrderedSet(priorityOrder, D);
		}
		return priorityOrder;
	}

	/**
	 * Set priorities to maximize the processor utilization, using
	 * Deadline Monotonic algorithm.
	 * <p>
	 * See Adnan Bouakaz thesis p. 92.
	 */
	public void setPrioritiesDM() {
		List<DeadlineInfo> priorityOrder = getOrderedDeadlineInfo();
		int i = 1;
		for (DeadlineInfo element: priorityOrder) {
			element.actor.setPriority(i);
			++i;
		}
	}
	
	/**
	 * Fill a buffer sizes weighted matrix.
	 * <p>
	 * {@code sizes[i][j]} gives the total buffer
	 * size between actors i and j (indexes are fixed
	 * during the fill, and stored in the returned
	 * array), considering i priority higher than j
	 * (i priority is one and j is 2). So this matrix
	 * is not symmetric.
	 * 
	 * @param sizes Weight matrix (buffer sizes).
	 * @param actors Actors to fill the matrix weights,
	 * there must be as much actors as row/column entries
	 * in the matrix.
	 * @return Indexed actor array.
	 */
	private Actor[] fillWeightMatrix(long sizes[][], Collection<Actor> actors) {
		int ii = 0, jj;
		SimpleGraph<Actor, Object> undirectedGraph = gpr.getUndirectedGraph();
		Actor[] index = new Actor[actors.size()];
		for (Actor a: actors) {
			jj = 0;
			index[ii] = a;
			a.setIndex(ii);
			a.setPriority(1);
			for (Actor b: actors) {
				Object o = undirectedGraph.getEdge(a, b); 
				if (o != null) {
					b.setPriority(2);
					sizes[ii][jj] = gpr.minimumSPedgeSize(a, b);
				} else {
					sizes[ii][jj] = 0;
				}
				++jj;
			}
			++ii;
		}
		return index;
	}
	
	/**
	 * See Adnan Bouakaz thesis p. 91.
	 *
	 * @todo
	 * Better understand {@code base}.
	 * What was the compromise? (removed dead code)
	 * 
	 * @param vertexSet Actor to prioritize.
	 * @param base Priority offset (in order to ensure
	 * a global priority ordering).
	 */
	private void setPrioritiesLOPintern(Collection<Actor> vertexSet, int base) {		
		int nbActors = vertexSet.size();
		/* weight matrix */
		long[][] sizes = new long[nbActors][nbActors];
		Actor[] indexedActors = fillWeightMatrix(sizes, vertexSet);

		if (nbActors == 0) {
			return;
		} else if (nbActors == 1) {
			indexedActors[0].setPriority(base+nbActors);
			return;
		}

		
		/* generate ILP program */
		StringBuilder constraints = new StringBuilder("\n/* Subject To */\n"), 
				binary = new StringBuilder("\n/* Binary */\n"),
				objective = new StringBuilder("Minimize: \n");

		for (int i = 0; i < nbActors; ++i) {
			for (int j = 0; j < nbActors; ++j) {
				if (i != j) {
					binary.append("bin x_"+i+"_"+j+";\n");
					if (sizes[i][j] != 0) {
						objective.append(sizes[i][j]+" x_"+i+"_"+j+" +");
					}
					if (i < j) {
						constraints.append("x_"+i+"_"+j+" + x_"+j+"_"+i+" = 1;\n");
						for (int k = i+1; k < nbActors; ++k) {
							if (k != j) {
								constraints.append("x_"+i+"_"+j+" + "+
										"x_"+j+"_"+k+" + "+"x_"+k+"_"+i+" <= 2;\n");
							}
						}
					}
				}
			}
		}
		
		objective.deleteCharAt(objective.length()-1).append(" ;\n"); 
		binary.append("\n");

		new ILPpriorityLOP(indexedActors, base, objective.toString(), 
				constraints.toString(), binary.toString()).solving();
		
	}

	/**
	 * Set priorities to minimize the buffer requirements, using
	 * Linear Ordering Problem.
	 * <p>
	 * See Adnan Bouakaz thesis p. 91.
	 */
	public void setPrioritiesLOP() {
		/* This method is called for uni-processor scheduling
		 * so the only partition is not filled yet and it is
		 * exactly the graph vertex set.
		 */
		setPrioritiesLOPintern(gpr.getActors(), 0);
	}
	
	/**
	 * Compute priorities with LOP algorithm
	 * applied on each partitions of a multiprocessor
	 * problem.
	 * 
	 * @todo 
	 * Is it global scheduling as suggested in the
	 * Adnan Bouakaz thesis p. 102 (paragraph above 
	 * lemma 3.1)?
	 * 
	 */
	public void setPrioritiesMLOP() {
		int base = 0;
		for (Partition partition: gpr.getMappedPartitions().values()) {
			setPrioritiesLOPintern(partition.getActors(), base);
			base += partition.getActors().size();
		}
	}

	/**
	 * Find the best priority order by testing some permutations
	 * (actually little order permutation, sliding along the
	 * full priorities array).
	 * <p>
	 * This method requires the actor to have a concrete period.
	 * <p>
	 * See Adnan Bouakaz thesis p. 97.
	 * 
	 * @param wcp Specific component to work on.
	 * @param initialPa Initial priority order.
	 * @param sizes Buffer sizes (as a matrix).
	 * @param tradeOffUDH Trade off between throughout and buffer size.
	 * @return The best priority order found.
	 */
	private PriorityAssignmentUDH findBestPermutation(WeaklyConnectedComponent wcp,
														PriorityAssignmentUDH initialPa, 
														long[][] sizes, TradeOffUDH tradeOffUDH) {

		double thresholdThroughput = tradeOffUDH.getLossT();
		double thresholdBuffersize = tradeOffUDH.getGainB();
		int windowWidth = Math.min(tradeOffUDH.getPermSize(), wcp.getActors().size());

		int nbActors = sizes.length;
		windowWidth = Math.min(nbActors, windowWidth);
		PriorityAssignmentUDH prevPerm;
		PriorityAssignmentUDH curPerm = initialPa;
		double curDistance = 0.0;
		long initialBufferSum = curPerm.getBufferSumApproximation(sizes);
		long curSize = initialBufferSum; 		
		int iterations = 0;
		
		do {
			iterations++;
			prevPerm = curPerm;
			for (int i = 0; i <= (nbActors-windowWidth); ++i) {
				PermutationTrotter permTrotter = new PermutationTrotter(windowWidth);
				while (permTrotter.hasNext()) {
					PriorityAssignmentUDH pa = curPerm.arranging(i, windowWidth, permTrotter.next());
					double dis = pa.getUtilizationDistance(wcp);
					if (dis <= thresholdThroughput) {
						long size = pa.getBufferSumApproximation(sizes); 
						if ((size < curSize && thresholdBuffersize < (initialBufferSum - size)/((double) initialBufferSum)) 
							|| (size == curSize && dis < curDistance)) { 
							curSize = size;
							curDistance = dis;
							curPerm = pa;
						}
					}
				}
			}
		} while (!curPerm.isEqualTo(prevPerm));
		LOGGER.info("UDH total iterations: "+ iterations + "; new buffer size: " + curSize + 
				" (initially " + initialBufferSum + "); utilization distance (throuhput loss factor): " + curDistance);
		return curPerm;
	}
	
	
	/**
	 * Compute priorities with utilization distance
	 * heuristic (for a weakly connected dataflow graph
	 * on uni-processor).
	 * <p>
	 * See Adnan Bouakaz p. 94-97.
	 * 
	 * @param wcp Specific component to work on.
	 * @param tradeOffUDH Trade off between throughput and buffer size, given by the user.
	 */
	public void setPrioritiesUtilizationDistanceHeuristic(WeaklyConnectedComponent wcp, TradeOffUDH tradeOffUDH) {
		/* the wcp actor set could be a subset of the gpr one */
		Set<Actor> actors = wcp.getActors();

		/* Approximate buffer sizes */
		int nbActors = actors.size();
		long[][] sizes = new long[nbActors][nbActors];		
		fillWeightMatrix(sizes, actors);

		/* compute DM priorities and set all periods */
		setPrioritiesDM();
		gpr.setPartitionPriorityOrderedActors(true);
		long dmBasisPeriod = new SchedulingOracleSP(gpr,true).standardSRTA(gpr.getMappedPartitions().get(0), 0L, Actor.MAX_PERIOD);
		if (dmBasisPeriod < 0) {
			throw new AdfgInternalException("The task system is not schedulable with DM priorities.");
		}
		LOGGER.info("SRTA best basis period before UDH: "+dmBasisPeriod);
		
		PriorityAssignmentUDH DMPermutation = new PriorityAssignmentUDH(gpr.getPriorityOrderedActors()); 
		PriorityAssignmentUDH bestPerm = findBestPermutation(wcp, DMPermutation, sizes, tradeOffUDH);

		/* setting priorities */
		for (int j = 0; j < nbActors; ++j) {
			bestPerm.getActor(j).setPriority(j+1);
		}
	}	
	
}
