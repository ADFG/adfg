/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos;

import java.io.FileWriter;
import java.io.InputStream;

import org.jdom2.Document;
import org.jdom2.input.SAXBuilder;
import org.jdom2.input.sax.XMLReaderJDOMFactory;
import org.jdom2.input.sax.XMLReaderXSDFactory;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import fr.inria.tea.adfg.algos.common_base.AdfgExternalException;

/**
 * This class provides helpers for XML parsing:
 * - a XSD validator method
 * 
 * @author Alexandre Honorat
 */
class XMLUtils {

	/**
	 * Do nothing.
	 */
	private XMLUtils() {
	}

	/**
	 * This class enclosed a document with the log
	 * eventually generated when parsing failed.
	 * <p>
	 * The normal use of this class would be:
	 * either the document is {@code null},
	 * either the log is {@code null},
	 * and never both of them are {@code null} or defined.
	 * 
	 * @author Alexandre Honorat
	 */
	static class ParsedDocument {
		private Document doc;
		private String log;
		private ParsedDocument(Document doc, String log) {
			this.doc = doc;
			this.log = log;
		}
		Document getDocument() {
			return doc;
		}
		String getLog() {
			return log;
		}
	}
	
	
	/**
	 * Validate an XML file thanks to the XSD corresponding schema
	 * and construct the associated jdom2 document.
	 * 
	 * @param xmlPath Path to the XML file.
	 * @param isXMLresource True if XML file is located in maven main resource folder.
	 * @param xsdPath Paths to the XSD files (located in the 
	 * standard resources folder).
	 * @return Null document if XML file does not respect XSD schema or if something went wrong,
	 * in this case you must log the log string at a higher level; the document otherwise.
	 */
	static ParsedDocument validateXMLSchema(String xmlPath, boolean isXMLresource, String... xsdPath) {   
	    Document doc = null;
	    ClassLoader classLoader = XMLUtils.class.getClassLoader();
	    String[] xsdURLS = new String[xsdPath.length];
	    for (int i = 0; i < xsdPath.length; ++i) {
		    xsdURLS[i] = classLoader.getResource(xsdPath[i]).toString();
		}
	    
	    try {
        	XMLReaderJDOMFactory factory = new XMLReaderXSDFactory(xsdURLS);
        	SAXBuilder builder = new SAXBuilder(factory);
        	if (isXMLresource) {
        		InputStream xmlIn = classLoader.getResourceAsStream(xmlPath);
        		doc = builder.build(xmlIn);
        		xmlIn.close();
		    } else {
		    	doc = builder.build(xmlPath);
		    }
	    } catch (Exception e) {
		    return (new ParsedDocument(null,
		    		"The xml file could not been validated with schema (" + xsdPath+").\n" + e.getMessage()));
		}      
	    return (new ParsedDocument(doc, null));
	}

	/**
	 * Write an XML document into the file system.
	 * 
	 * @param doc Document to write.
	 * @param fileName File name (and path) to use.
	 * @param fileTypeName Type of file.
	 * @param showEmpty Whether or not empty xml elements have
	 * to be expanded (like {@code <balise></balise>}.
	 * @throws AdfgExternalException If something went wrong.
	 */
	static void writeDocument(Document doc, String fileName, String fileTypeName, boolean showEmpty) throws AdfgExternalException {
		XMLOutputter xmlOutput = new XMLOutputter();
		xmlOutput.setFormat(Format.getPrettyFormat().setExpandEmptyElements(showEmpty));
		try (FileWriter fw = new FileWriter(fileName)) {
			xmlOutput.output(doc, fw);
		} catch (Exception e) {
			throw new AdfgExternalException("Problem during " + fileTypeName + " file update (writing).", e);
		} 		
	}
}
