/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos;

import fr.inria.tea.adfg.algos.common_data_rep.Actor;
import fr.inria.tea.adfg.algos.common_data_rep.Channel;
import fr.inria.tea.adfg.algos.common_base.AdfgExternalException;


/**
 * This interface provides simple methods to construct
 * data flow graph representing a schedulability problem.
 * The graph may be firstly loaded from a sdf3 file.
 * 
 * @author Alexandre Honorat
 */
public interface IADFGr {
    
    /**
     * Add an actor to a the graph.
     * 
     * @param actor Periodic actor to add.
     * @return Boolean specifying if the actor has been added.
     * @throws AdfgExternalException If an actor with the same
     * name has already been added.
     */
    public boolean addActor(Actor actor) throws AdfgExternalException;

    /**
     * Add a channel between two actors in the graph.
     * <p>
     * See Adnan Bouakaz thesis p. 80 for the boundedness
     * criterion and the way affine relation are deduced 
     * from UPIS.
     *  
     * @param channel Channel to be added.
     * @param src Source Actor.
     * @param dest Destination Actor.
     * @return Boolean specifying if the channel has been added.
     * @throws AdfgExternalException Thrown if a non consistent channel between the
     * two actors already exist.
     */
    public boolean addChannel(Channel channel, Actor src, Actor dest) throws AdfgExternalException;

    /**
     * Provides the current graph representation.
     * 
     * @return A string representing the graph.
     */
    public String getGraphRep();

}
