/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos;

import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;

import fr.inria.tea.adfg.algos.XMLUtils.ParsedDocument;
import fr.inria.tea.adfg.algos.common_base.AdfgExternalException;
import fr.inria.tea.adfg.algos.common_data_rep.Actor;
import fr.inria.tea.adfg.algos.common_data_rep.Channel;
import fr.inria.tea.adfg.algos.maths.UPIS;

/**
 * This class can import a graph from the adfg ecore model
 * (stored in *.adfg) and also export a graph into this model.
 * <p>
 * The xmi file (*.adfg) describing the graph is validated
 * against an xsd. But notice there is no dependency with any emf 
 * object: if the model is modified, this class must be modified too,
 * by hand (by analysing the composition of the *.adfg files).
 * 
 * @author Alexandre Honorat
 */
class XMITransformer {

	private static final Logger LOGGER = Logger.getLogger("AdfgLog");
	
	/**
	 * Do nothing.
	 */
	private XMITransformer() {
	}

	/**
	 * Parses the *.adfg file and add the actors and the channels to
	 * the graph parameter.
	 * 
	 * @param adfgr Graph into one adding the xmi edges and vertices.
	 * @throws AdfgExternalException If something went wrong (especially 
	 * if the file cannot be validated against our schema).
	 */
	static void initGraphFromXMI(ADFGraph adfgr) throws AdfgExternalException {
		String fileName = adfgr.getFileName();
		Boolean isXMLresource = adfgr.isXMLresource();
		ParsedDocument pdoc = XMLUtils.validateXMLSchema(fileName, isXMLresource, 
				"ucsdf/adfgGraphModelXMI.xsd", "ucsdf/XMI.xsd");
		if (pdoc.getDocument() == null) {
			LOGGER.severe(pdoc.getLog());
			throw new AdfgExternalException("File is not in format UCSDF (adfg xmi model),"+
					" or something went wrong during file parsing.");
		}

		Element root = pdoc.getDocument().getRootElement();

		List<Element> elActors = root.getChildren("actors");
		for (Element element: elActors) {
			String name = element.getAttributeValue("Name");
			// WCET default value is handled by the xsd schema
			String WCET = element.getAttributeValue("WCET");
			String period = element.getAttributeValue("Period");
			long userPeriod = (period == null) ? Actor.UNDEFINED : Long.parseLong(period);
			long wcet = Long.parseLong(WCET);
			Actor actor = new Actor(name, wcet, wcet, userPeriod);
			String deadline = element.getAttributeValue("Deadline");
			if (deadline != null) {
				actor.setDeadline(Long.parseLong(deadline));
			}
			String phase = element.getAttributeValue("Phase");
			if (phase != null) {
				actor.setPhase(Long.parseLong(phase));
			}
			String proc = element.getAttributeValue("ProcBinding");
			if (proc != null) {
				actor.setPartitionID(Integer.parseInt(proc));
			}
			String priority = element.getAttributeValue("Priority");
			if (priority != null) {
				actor.setPriority(Integer.parseInt(priority));
			}
			adfgr.addActor(actor);
		}

		List<Element> elChannels = root.getChildren("channels");
		for (Element element: elChannels) {
			String name = element.getAttributeValue("Name");
			String pRate = element.getAttributeValue("ProdRate");
			String cRate = element.getAttributeValue("ConsRate");
			String dSize = element.getAttributeValue("DataSize");
			String tInit = element.getAttributeValue("InitialToken");
			String tMax = element.getAttributeValue("MaxSize");
			String srcName = element.getAttributeValue("source");
			String tgtName = element.getAttributeValue("target");			

			UPIS prate = new UPIS(pRate);
			UPIS crate = new UPIS(cRate);
			Channel channel = new Channel(name, prate, crate, Long.parseLong(tMax), 
					Long.parseLong(tInit), Long.parseLong(dSize));
			Actor src = adfgr.getMappedActors().get(srcName);
			Actor tgt = adfgr.getMappedActors().get(tgtName);
			if (src == null || tgt == null) {
				throw new AdfgExternalException("Source/target unknown: channel <"+name+">.");
			}
			adfgr.addChannel(channel, src, tgt);
		}

	}

	/**
	 * Update the original file if {@code newFileName} is {@code null},
	 * else create a new one. 
	 * 
	 * @param adfgp Problem containing the results to update.
	 * @param newFileName *.adfg file to create or {@code null}
	 * if updating the orignal file.
	 * @throws AdfgExternalException If something went wrong.
	 */
	static void udpateXMIFromGraph(ADFGprob adfgp, String newFileName) throws AdfgExternalException {
		Document document = null;
		String originalFileName = adfgp.getFileName();
		boolean isXMLresource = adfgp.isXMLresource();

		if (originalFileName != null && originalFileName.endsWith(".adfg")) {
			SAXBuilder sxb = new SAXBuilder();
			InputStream xmlIn = null;
			try {
				if (isXMLresource) {
					ClassLoader classLoader = XMITransformer.class.getClass().getClassLoader();
					xmlIn = classLoader.getResourceAsStream(originalFileName);
					document = sxb.build(xmlIn);
					xmlIn.close();
				} else {
					document = sxb.build(new File(originalFileName));
				}
			} catch (Exception e) {
				throw new AdfgExternalException("Problem during XMI file update (opening).", e);
			}
			
		} else {
			Element root = new Element("Graph", "adfg", "http://www.irisa.fr/espresso/polychrony/adfgGraphModel");
			document = new Document(root);
			root.setAttribute("version", "2.0", Namespace.getNamespace("xmi", "http://www.omg.org/XMI"));
			
			for (Actor actor: adfgp.getMappedActors().values()) {
				Element element = new Element("actors");
				element.setAttribute("Name", actor.getName());
				element.setAttribute("WCET", Long.toString(actor.getWcet()));
				root.addContent(element);
			}

			for (Channel channel: adfgp.getMappedChannels().values()) {
				Element element = new Element("channels");
				element.setAttribute("Name", channel.getName());
				element.setAttribute("ProdRate", channel.getProduce().toString());
				element.setAttribute("ConsRate", channel.getConsume().toString());
				element.setAttribute("DataSize", Long.toString(channel.getDataSize()));
				Actor source = adfgp.getAdfgGraph().getEdgeSource(channel);
				element.setAttribute("source", source.getName());
				Actor target = adfgp.getAdfgGraph().getEdgeTarget(channel);
				element.setAttribute("target", target.getName());				
				root.addContent(element);
			}

		}

		setDocument(adfgp, document);		
		String file = (newFileName == null) ? originalFileName : newFileName;
		XMLUtils.writeDocument(document, file, "ADFG", false);
	}

	private static void setDocument(ADFGprob adfgp, Document doc) throws AdfgExternalException {
		Element root = doc.getRootElement();
		
		String graphName = root.getAttributeValue("Name");
		if (graphName != null && !graphName.endsWith("- analysed by ADFG")) {
			root.setAttribute("Name", graphName+" - analysed by ADFG");
		} else if (graphName == null) {
			root.setAttribute("Name", " - analysed by ADFG");
		}
		root.setAttribute("NbProcs", Integer.toString(adfgp.getNbProcessor()));
		root.setAttribute("SchedulingPolicy", adfgp.getScheduleType().name());

		Map<String, Actor> mapActors = adfgp.getMappedActors();
		Map<String, Channel> mapChannels = adfgp.getMappedChannels();

		if (adfgp.isSchedulable()) {
			root.setAttribute("IsSchedulable", "true");
			root.setAttribute("UtilizationFactor", Double.toString(adfgp.getGlobalUtilizationFactor()));

			for (Element element: root.getChildren("actors")) {
				String name = element.getAttributeValue("Name");
				Actor actor = mapActors.get(name);
				if (actor != null) {
					element.setAttribute("Period", Long.toString(actor.getPeriod()));
					element.setAttribute("Deadline", Long.toString(actor.getDeadline()));
					element.setAttribute("Phase", Long.toString(actor.getPhase()));
					element.setAttribute("ProcBinding", Integer.toString(actor.getPartitionID()));
					element.setAttribute("Priority", Integer.toString(actor.getPriority()));
				} else {
					throw new AdfgExternalException("An actor disappeared during analysis: <"+name+">.");
				}
			}

			for (Element element: root.getChildren("channels")) {
				String name = element.getAttributeValue("Name");
				Channel channel = mapChannels.get(name);
				if (channel != null) {
					element.setAttribute("InitialToken", Long.toString(channel.getInitial()));
					element.setAttribute("MaxSize", Long.toString(channel.getSize()));
				} else {
					throw new AdfgExternalException("A channel disappeared during analysis: <"+name+">.");
				}
			}

		} else {
			root.setAttribute("IsSchedulable", "false");

			for (Element element: root.getChildren("actors")) {
				String name = element.getAttributeValue("Name");
				Actor actor = mapActors.get(name);
				if (actor != null) {
					element.removeAttribute("Period");
					element.removeAttribute("Deadline");
					element.removeAttribute("Phase");
					element.removeAttribute("ProcBinding");
					element.removeAttribute("Priority");
				} else {
					throw new AdfgExternalException("An actor disappeared during analysis: <"+name+">.");
				}
			}

			for (Element element: root.getChildren("channels")) {
				String name = element.getAttributeValue("Name");
				Channel channel = mapChannels.get(name);
				if (channel != null) {
					element.removeAttribute("InitialToken");
					element.removeAttribute("MaxSize");
				} else {
					throw new AdfgExternalException("A channel disappeared during analysis: <"+name+">.");
				}
			}

		}

	}
	
}
