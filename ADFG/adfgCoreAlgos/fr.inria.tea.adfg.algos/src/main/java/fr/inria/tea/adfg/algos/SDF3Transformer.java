/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

import fr.inria.tea.adfg.algos.XMLUtils.ParsedDocument;
import fr.inria.tea.adfg.algos.common_base.AdfgExternalException;
import fr.inria.tea.adfg.algos.common_data_rep.Actor;
import fr.inria.tea.adfg.algos.common_data_rep.Channel;
import fr.inria.tea.adfg.algos.maths.UPIS;

/**
 * This class is in charge of the SDF3 file format import and
 * export.
 * 
 * @author Alexandre Honorat
 */
class SDF3Transformer {

	private static final Logger LOGGER = Logger.getLogger("AdfgLog");	 
	
	private static class ActorInfo {
		Actor actor;
		Map<String, UPIS> portRates;

		ActorInfo(Actor a) {
			actor = a;
			portRates = new HashMap<>();
		}
	}
	
	/**
	 * Do nothing, but is useful in order to apply the {@code getClass()} method.
	 */
	private SDF3Transformer() {
	}


	/**
	 * Parses the sdf3 file and add the actors and the channels to
	 * the graph parameter.
	 * <p>
	 * SDF3 file format doesn't support UCSDF graphs, only CSDF and SDF
	 * are available.
	 * <p>
	 * Channels starting with the name '_ch' are ignored for historical
	 * reasons (they were used to code constraints in loops for some
	 * other tools reading SDF3 file format).
	 * 
	 * @param adfgr Graph into one adding the SDF3 edges and vertices.
	 * 
	 * @throws AdfgExternalException If something went wrong 
	 * (especially if the file is not sdf neither csdf).
	 */
	static void initGraphFromSDF3(ADFGraph adfgr) throws AdfgExternalException {
			String fileName = adfgr.getFileName();
			Boolean isXMLresource = adfgr.isXMLresource();
			ParsedDocument pdocSDF = XMLUtils.validateXMLSchema(fileName, isXMLresource, "sdf3/sdf3-sdf.xsd");
			ParsedDocument pdocCSDF = XMLUtils.validateXMLSchema(fileName, isXMLresource, "sdf3/sdf3-csdf.xsd");		
			ParsedDocument pdoc = null;
			

			if (pdocSDF.getDocument() != null) {
			    pdoc = pdocSDF;
			    LOGGER.fine("SDF3 xml file recognized as SDF file.");
			}
			if (pdocCSDF.getDocument() != null) {
				pdoc = pdocCSDF;
				LOGGER.fine("SDF3 xml file recognized as CSDF file.");
			}
			
			if (pdoc == null) {
				LOGGER.severe(pdocSDF.getLog());
				LOGGER.severe(pdocCSDF.getLog());
				throw new AdfgExternalException("SDF3 file is neither of type sdf nor csdf,"+
						" or something went wrong during file parsing.");
			}
			
			Element racine = pdoc.getDocument().getRootElement();
			Element applicationGraph = racine.getChild("applicationGraph");
			Element sdf = null;

			if (pdoc == pdocSDF) { 
				sdf = applicationGraph.getChild("sdf");
			} else {
				sdf = applicationGraph.getChild("csdf"); 
			}

			Map<String,ActorInfo> actorsInfo = new HashMap<>();

			List<Element> elActors = sdf.getChildren("actor");
			for (Element element: elActors) {
				String name = element.getAttributeValue("name");
				Actor actor = new Actor(name, 1, 1, Actor.UNDEFINED);
				adfgr.addActor(actor);
				ActorInfo actorInfo = new ActorInfo(actor);
				List<Element> elPorts = element.getChildren("port");
				for (Element port: elPorts) {
					String str = checkAndSetPeriodicString(port.getAttributeValue("rate"), fileName);
					actorInfo.portRates.put(port.getAttributeValue("name"), new UPIS("("+str+")")); 
				}
				actorsInfo.put(name, actorInfo);
			}

			List<Element> elChannels = sdf.getChildren("channel");
			for (Element element: elChannels) {
				String srcActor = element.getAttributeValue("srcActor");
				String dstActor = element.getAttributeValue("dstActor");
				String strInitial = element.getAttributeValue("initialTokens");
				String strSize = element.getAttributeValue("size");
				String name = element.getAttributeValue("name");
				long initial = (strInitial != null) ? Long.parseLong(strInitial) : Channel.UNDEFINED;
				long size = (strSize != null) ? Long.parseLong(strSize) : Channel.UNDEFINED;
				UPIS prate = actorsInfo.get(srcActor).portRates.get(element.getAttributeValue("srcPort"));
				UPIS crate = actorsInfo.get(dstActor).portRates.get(element.getAttributeValue("dstPort")); 
				Channel channel = new Channel(name, prate, crate, size, initial, 1);
				adfgr.addChannel(channel, actorsInfo.get(srcActor).actor, actorsInfo.get(dstActor).actor);
			}

			Element sdfProperties = null;
			if (pdoc == pdocSDF) {
				sdfProperties = applicationGraph.getChild("sdfProperties"); 
			} else if (pdoc == pdocCSDF) {
				sdfProperties = applicationGraph.getChild("csdfProperties"); 
			}


			if (sdfProperties == null) {
				LOGGER.warning("No (c)sdfProperties have been found, actors' WCET is 1 by default.");
			} else {
				List<Element> actorProperties = sdfProperties.getChildren("actorProperties");
				for (Element element: actorProperties) {
					String name = element.getAttributeValue("actor");
					Element processor = element.getChild("processor");
					Element executionTime = processor.getChild("executionTime");
					String time = checkAndSetPeriodicString(executionTime.getAttributeValue("time"), fileName);
					long max = 1;
					long avg = 1;
					if (time.length() > 0) {
						avg = 0;
						String[] times = time.split(" +");
						for (int i = 0; i < times.length; ++i) {
							long temp = Long.parseLong(times[i]); 
							avg += temp;
							if (temp > max) {
								max = temp;
							}
						}
						avg /= times.length;
					}
					Actor a = actorsInfo.get(name).actor;
					a.setWcet(max);
					a.setAvgCycloWcet(avg);
				}
			}
	}
	
	/**
	 * Update SDF3 file if {@code newFileName} is {@code null},
	 * else create a copy of the file used to construct the current
	 * object. 
	 * <p>
	 * Only channels information are updated.
	 * Use only if the problem was created from a SDF3 file.
	 * 
	 * @param adfgp Problem containing the results to update.
	 * @param newFileName SDF file to create (it only modifies
	 * the channels' initial token and size numbers of the
	 * original one).
	 * @throws AdfgExternalException If something went wrong.
	 */
	static void updateSDF3FromGraph(ADFGprob adfgp, String newFileName) throws AdfgExternalException {
		Document document = null;
		String originalFileName = adfgp.getFileName();
		boolean isXMLresource = adfgp.isXMLresource();
		Map<String, Channel> channels = adfgp.getMappedChannels();
		SAXBuilder sxb = new SAXBuilder();
		InputStream xmlIn = null;
		try {
			if (isXMLresource) {
				ClassLoader classLoader = SDF3Transformer.class.getClass().getClassLoader();
				xmlIn = classLoader.getResourceAsStream(originalFileName);
				document = sxb.build(xmlIn);
				xmlIn.close();
			} else {
				document = sxb.build(new File(originalFileName));
			}
		} catch (Exception e) {
			throw new AdfgExternalException("Problem during SDF3 file update (opening).", e);
		}
		Element root = document.getRootElement();
		Element applicationGraph = root.getChild("applicationGraph");
		Element sdf = applicationGraph.getChild("sdf"); 
		if (sdf == null) {
			// if it is not sdf, then it is csdf
			sdf = applicationGraph.getChild("csdf");
		}

		List<Element> elChannels = sdf.getChildren("channel");
		for (Element element: elChannels) {
			String name = element.getAttributeValue("name");
			if (!name.startsWith("_ch") && 
					!element.getAttributeValue("srcActor").equals(element.getAttributeValue("dstActor"))) {
				Channel ch = channels.get(name);
				if (ch != null) {
					element.setAttribute("initialTokens", Long.toString(ch.getInitial()));	
					element.setAttribute("size", Long.toString(ch.getSize()));
				} else {
					throw new AdfgExternalException("A channel disappeared during analysis: <"+name+">.");
				}
			}
		}    

		String file = (newFileName == null) ? originalFileName : newFileName;
		XMLUtils.writeDocument(document, file, "SDF3", false);
	}
	
	/**
	 * Check and transform ( *[0-9 ]+ *,)* *[0-9]+ * strings by removing all colons.
	 * 
	 * @param str String to validate and transform.
	 * @param fileName File being parsed.
	 * @return New string containing only numbers and spaces characters.
	 * @throws AdfgExternalException If the string doesn't match the regex.
	 */
	private static String checkAndSetPeriodicString(String str, String fileName) throws AdfgExternalException {
		String regex = "( *[0-9 ]+ *,)* *[0-9]+ *";
		if (!str.matches(regex)) {
			throw new AdfgExternalException("While parsing file "+fileName+
						", the periodic rate/wcet \""+str+"\"doesn't match the regex "+regex+".");
		}
		return str.replace(',', ' ').trim();
	}

}

