/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.common_data_rep;

import java.io.Serializable;

import fr.inria.tea.adfg.algos.common_base.AdfgExternalException;
import fr.inria.tea.adfg.algos.common_base.AdfgInternalException;
import fr.inria.tea.adfg.algos.maths.Fraction;

/** 
 * An actor is a part of code to execute (it can be executed
 * several times: task/job firings in a scheduling). It is the
 * element used as nodes in data flow graph and affine relation
 * graph of ADFG. Time unit is not present, all durations are 
 * assumed to be expressed in the same implicit unit.
 * 
 * @author Adnan Bouakaz
 * @author Alexandre Honorat
 */
public class Actor implements IActor, Comparable<Actor>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The value is not known yet.
	 */
	public static final long UNDEFINED = -1;

	/**
	 * All periods greater than this value will generate an exception.
	 */
	public static final long MAX_PERIOD = Long.MAX_VALUE/100;
	
	/**
	 * Unique instance identifier.
	 */
	private static int IDcount = 0;

	private int instanceID;
	private String name;
	private long wcet;
	private long avgCycloWcet;
	private long period, phase;
	private long userPeriod;
	private long deadline;

	private long nbFirings;
	
	/**
	 * Deadline relative to the actor period,
	 * default is 1/1 (i. e. the deadline
	 * equals the period).
	 */
	private Fraction symbolicDeadline;
	/**
	 * Symbolic deadlines computed over the hyper
	 * period of the actor and its neighbors.
	 */
	private FiringDeadlines firingDeadlines;
	/**
	 * Period ratio with the wcp actor basis.
	 */
	private Fraction coefficient;
	/**
	 * Weakly Connected Component containing this actor;
	 */
	private WeaklyConnectedComponent wcp;
	/**
	 * Partition (processor) containing this actor.
	 */
	private int partitionID;
	/**
	 * Used only in case of static-priority scheduling.
	 */
	private int priority;
	/**
	 * Cached value to avoid computations.
	 */
	private double symbolicLoad;
	private double avgCycloSymbolicLoad;
	/**
	 * May be used to index actors on a specific order in
	 * some algorithms.
	 */
	private int index;
	/**
	 * Used for graph order, 0 by default.
	 */
	private int dagOrder;
	
	
	
	/**
	 * Construct a new Actor characterized by a name and
	 * a worst case execution time (WCET), without unit.
	 * 
	 * @param name Actor name.
	 * @param wcet Actor WCET.
	 * @param avgCycloWcet Actor worst case average execution time according to the cyclo-WCET.
	 * @param userPeriod Actor period, or {@code UNDEFINED}.
	 * @throws AdfgExternalException If parameters are not consistent. 
	 */
	public Actor(String name, long wcet, long avgCycloWcet, long userPeriod) throws AdfgExternalException {
		if (wcet <= 0) {
			throw new AdfgExternalException("The worst-case execution time must be strictly positive.");
		}
		if (userPeriod < UNDEFINED) {
			throw new AdfgExternalException("The user period must be positive or undefined.");
		}
		instanceID = ++IDcount;
		this.name = name;
		this.wcet = wcet;
		this.avgCycloWcet = avgCycloWcet;
		this.userPeriod = userPeriod;
		/* deadline = period at begining */
		symbolicDeadline = new Fraction(1,1);
		partitionID = -1;
		cstrSubroutine();
	}

	/**
	 * Deep copy constructor, with {@code instanceID} increased.
	 * Computed attributes are not copied except the period
	 * and the mapping, accordingly to the parameter.
	 * <p>
	 * FiringDeadlines is also set to {@code null}, this constructor
	 * is not intended to be called by an external user.
	 * 
	 * @param actor Actor to copy.
	 * @param keepPeriod Whether or not user periods must be used.
	 * @param keepMapping Whether or not user partiion must be used.
	 */
	public Actor(Actor actor, boolean keepPeriod, boolean keepMapping) {
		instanceID = ++IDcount;
		name = actor.name;
		wcet = actor.wcet;
		avgCycloWcet = actor.avgCycloWcet;
		if (keepPeriod) {
			userPeriod = actor.userPeriod;			
		} else {
			userPeriod = UNDEFINED;
		}
		if (keepMapping) {
			partitionID = actor.getPartitionID();
		} else {
			partitionID = -1;
		}
		// copy of this two should be managed by an option one day
		symbolicDeadline = actor.symbolicDeadline;
		cstrSubroutine();
	}

	/**
	 * Initialize results data.
	 */
	void cstrSubroutine() {
		/* not set at this time */
		deadline = UNDEFINED;
		period = UNDEFINED;
		phase = UNDEFINED;
		dagOrder = 0;
		firingDeadlines = null;
		index = (int) UNDEFINED;
		coefficient = null;
		firingDeadlines = null;
		wcp = null;
		symbolicLoad = UNDEFINED;
		avgCycloSymbolicLoad = UNDEFINED;
		nbFirings = UNDEFINED;
		priority = 0;
	}
	
	/**
	 * Each actor receives a unique ID in ADFG.
	 * It enables to break tie during scheduling
	 * priority assignment for example.
	 * <p>
	 * ID are not the same between adfgr abd adfgp.
	 * 
	 * @return Unique Actor ID.
	 */
	public int getID() {
		return instanceID;
	}

	/**
	 * Set the period and update the symbolic
	 * deadline value.
	 * 
	 * @param p New period.
	 */
	public void setPeriod(long p) {
		period = p;
		concretizeSymbolicDeadline();
	}

	/* (non-Javadoc)
	 * @see fr.inria.tea.adfg.algos.common_data_rep.IActor#getPeriod()
	 */
	@Override
	public long getPeriod() {
		return period;
	}
	
	/**
	 * Set the phase.
	 * 
	 * @param ph New phase.
	 */
	public void setPhase(long ph){
		phase = ph;
	}

	/* (non-Javadoc)
	 * @see fr.inria.tea.adfg.algos.common_data_rep.IActor#getPhase()
	 */
	@Override
	public long getPhase() {
		return phase;
	}

	/* (non-Javadoc)
	 * @see fr.inria.tea.adfg.algos.common_data_rep.IActor#getUserPeriod()
	 */
	@Override
	public long getUserPeriod() {
		return userPeriod;
	}
	
	/**
	 * Set the WCET, without unit.
	 * 
	 * @param time New WCET.
	 */
	public void setWcet(long time){
		wcet = time;
	}

	/* (non-Javadoc)
	 * @see fr.inria.tea.adfg.algos.common_data_rep.IActor#getWcet()
	 */
	@Override
	public long getWcet() {
		return wcet;
	}

	/**
	 * Set the average time of cyclo WCET, without unit.
	 * 
	 * @param time New average cyclo-WCET.
	 */
	public void setAvgCycloWcet(long time){
		avgCycloWcet = time;
	}

	/**
	 * 
	 * @return Average cyclo-WCET.
	 */
	public long getAvgCycloWcet() {
		return avgCycloWcet;
	}

	
	/**
	 * Set the symbolic deadline (as a fraction of the actor period).
	 * <p>
	 * See Adnan Bouakaz thesis p. 89, second paragraph.
	 * 
	 * @param sd New symbolic deadline (will be cloned
	 * by deep-copy constructor).
	 */
	public void setSymbolicDeadline(Fraction sd) {
		double value = sd.getValue();
		if (value > 1.0 || value < 0.0) {
			throw new AdfgInternalException(
					"Symbolic deadline not in range [0;1] in the actor <"+name+">!");
		}
		symbolicDeadline = new Fraction(sd);
	}

	/**
	 * Get the symbolic deadline (as a fraction).
	 * The result is a clone, so it can be modified
	 * without modifying the fraction of the current object.
	 * 
	 * @return Cloned symbolic deadline.
	 */
	public Fraction getSymbolicDeadline() {
		return new Fraction(symbolicDeadline);
	}

	/* (non-Javadoc)
	 * @see fr.inria.tea.adfg.algos.common_data_rep.IActor#getFiringDeadline(int)
	 */
	@Override
	public long getFiringDeadline(int i){
		Fraction sd = firingDeadlines.get(i);
		return sd.getNumerator() * period / sd.getDenominator();
	}

	/**
	 * Set the firing deadlines.
	 * 
	 * @param fd New firing deadlines.
	 */
	public void setFiringDeadlines(FiringDeadlines fd) {
		firingDeadlines = fd;
	}

	/**
	 * Get the firing deadlines.
	 * 
	 * @return Actor's firing deadlines.
	 */
	public FiringDeadlines getFiringDeadlines() {
		return firingDeadlines;
	}

	/**
	 * 
	 * @return The instance ID.
	 */
	public int getInstanceID() {
		return instanceID;
	}

	/**
	 * 
	 * @param instanceID The new instance ID.
	 */
	public void setInstanceID(int instanceID) {
		this.instanceID = instanceID;
	}
	
	/* (non-Javadoc)
	 * @see fr.inria.tea.adfg.algos.common_data_rep.IActor#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name The new name.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Set the deadline, without referring to the
	 * symbolic deadline (relative to the period).
	 * 
	 * @param deadline New deadline.
	 */
	public void setDeadline(long deadline) {
		this.deadline = deadline;
	}
	
	/* (non-Javadoc)
	 * @see fr.inria.tea.adfg.algos.common_data_rep.IActor#getDeadline()
	 */
	@Override
	public long getDeadline() {
		return deadline;
	}

	/**
	 * Update deadline thanks to the period and
	 * the symbolic deadline.
	 */
	private void concretizeSymbolicDeadline() {
		deadline = period*symbolicDeadline.getNumerator()/symbolicDeadline.getDenominator();
	}

	/**
	 * Set the partition (identified by its number between 0 and
	 * nbProcs - 1) which contains the actor.
	 * 
	 * @param pi Partition identifier.
	 */
	public void setPartitionID(int pi) {
		partitionID = pi;
	}

	/* (non-Javadoc)
	 * @see fr.inria.tea.adfg.algos.common_data_rep.IActor#getPartitionID()
	 */
	@Override
	public int getPartitionID() {
		return partitionID;
	}

	/**
	 * Set the wcp container of this actor.
	 * <p>
	 * Set also the cached value of the symbolic load.
	 * 
	 * @param wcp Container.
	 */
	public void setWCPcontainer(WeaklyConnectedComponent wcp) {
		this.wcp = wcp;
	}
	
	/**
	 * @return Container of this actor in the graph.
	 */
	public WeaklyConnectedComponent getWCPcontainer() {
		return wcp;
	}
	
	/**
	 * This method set the coefficient and also the
	 * symbolic load. No deep copy is made of the
	 * coefficient.
	 * 
	 * @param coefficient Actor period coefficient
	 * relative to its wcp actor basis.
	 */
	public void setCoefficient(Fraction coefficient) {
		this.coefficient = coefficient;
		double value = coefficient.getValue();
		symbolicLoad = wcet / value;
		avgCycloSymbolicLoad = avgCycloWcet / value;
	}
	
	/**
	 * Each coefficient is the current actor period fraction
	 * over the {@code basis} actor period of its wcp.
	 * <p>
	 * See Adnan Bouakaz thesis p. 88.
	 * The original coefficient is returned, no deep copy is
	 * made.
	 * 
	 * @return Actor (original) coefficient in its wcp container.
	 */
	public Fraction getCoefficient() {
		return coefficient;
	}
	
	/**
	 * @return Symbolic load of the actor: wcet over
	 * its period coefficient.
	 */
	public double getSymbolicLoad() {
		return symbolicLoad;
	}
	
	/**
	 * @return Symbolic load of the actor: wcet over
	 * its period coefficient.
	 */
	public double getAvgCycloSymbolicLoad() {
		return avgCycloSymbolicLoad;
	}
	
	/**
	 * Set actor's priority in the current
	 * scheduling.
	 * 
	 * @param pr Priority.
	 */
	public void setPriority(int pr) {
		priority = pr;
	}

	/* (non-Javadoc)
	 * @see fr.inria.tea.adfg.algos.common_data_rep.IActor#getPriority()
	 */
	@Override
	public int getPriority() {
		return priority;
	}
	
	/**
	 * Used for fixed priorities algorithm
	 * purpose.
	 * <p>
	 * It is advised to not use negative values.
	 * 
	 * @param index New actor index.
	 */
	public void setIndex(int index) {
		this.index = index;
	}
	
	/**
	 * @return The index set by a fixed
	 * priorities algorithm, or {@code UNDEFINED}.
	 */
	public int getIndex() {
		return index;
	}
	
	/**
	 * Set the number of firings of the actor in one
	 * graph iteration.
	 * 
	 * @param nbFirings New number of firings.
	 */
	public void setNbFirings(long nbFirings) {
		this.nbFirings = nbFirings;
	}
	
	/**
	 * 
	 * @return Number of firings of the actor in one graph iteration.
	 */
	public long getNbFirings() {
		return nbFirings;
	}
	
	/**
	 * Color the actor.
	 * 
	 * @param dagOrder New color (as integer).
	 */
	public void setDAGorder(int dagOrder) {
		this.dagOrder = dagOrder;
	}
	
	/**
	 * Get the actor color, which is 0
	 * if none has been set.
	 * 
	 * @return Actor color.
	 */
	public int getDAGorder() {
		return dagOrder;
	}
	
	/**
	 * Reset to 0 the next Actor's ID. This method
	 * must be called between each new Actor's graph
	 * problem representation creation. Indeed Actors'
	 * names are not used to identified them
	 * internally.
	 * 
	 */
	public static void resetIDs() {
		IDcount = 0;
	}
	
	/**
	 * Compare actor's membership of same partition
	 * or not.
	 * 
	 * @param a1 Actor 1.
	 * @param a2 Actor 2.
	 * @return True if both actors belong the same partition,
	 * false otherwise.
	 */
	public static boolean onSameProcessor(IActor a1, IActor a2) {
		return a1.getPartitionID() == a2.getPartitionID();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Actor) {
			return ((Actor) obj).getID() == instanceID;
		}
		return false;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode(java.lang.Object)
	 */	
	@Override
	public int hashCode() {
		return instanceID;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	/* (non-Javadoc)
	 * @see fr.inria.tea.adfg.algos.common_data_rep.IActor#toString()
	 */
	@Override
	public String toString() {
		return "("+name+", "+wcet+", "+((period!=UNDEFINED)?period:"*")+", "+
				((phase!=UNDEFINED)?phase:"*")+", "+((deadline!=UNDEFINED)?deadline:"*")+")";
	}

	/**
	 * Sort actors regarding to their IDs 
	 * (natural integer order).
	 */
	@Override
	public int compareTo(Actor o) {
		return Integer.compare(instanceID, o.getID());
	}

}
