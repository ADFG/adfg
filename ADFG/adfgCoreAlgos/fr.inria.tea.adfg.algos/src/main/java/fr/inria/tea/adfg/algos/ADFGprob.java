/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jgrapht.graph.DirectedMultigraph;

import de.vandermeer.asciitable.AsciiTable;
import fr.inria.tea.adfg.algos.ars.ARSynthesis;
import fr.inria.tea.adfg.algos.common_base.AdfgExternalException;
import fr.inria.tea.adfg.algos.common_base.AdfgInternalException;
import fr.inria.tea.adfg.algos.common_base.ESchedAlgo;
import fr.inria.tea.adfg.algos.common_base.ESchedPolicy;
import fr.inria.tea.adfg.algos.common_base.ETradeOffType;
import fr.inria.tea.adfg.algos.common_base.Parameters;
import fr.inria.tea.adfg.algos.common_base.PrettyTable;
import fr.inria.tea.adfg.algos.common_data_rep.Actor;
import fr.inria.tea.adfg.algos.common_data_rep.AffineRelation;
import fr.inria.tea.adfg.algos.common_data_rep.Buffer;
import fr.inria.tea.adfg.algos.common_data_rep.Channel;
import fr.inria.tea.adfg.algos.common_data_rep.GraphProbData;
import fr.inria.tea.adfg.algos.common_data_rep.GraphRawData;
import fr.inria.tea.adfg.algos.common_data_rep.Loop;
import fr.inria.tea.adfg.algos.common_data_rep.Partition;
import fr.inria.tea.adfg.algos.maths.BasicArithmetic;
import fr.inria.tea.adfg.algos.ssa.SSAnalysis;

/**
 * Basic implementation to call schedulability analysis
 * and get the results. Once constructed, the analysis is done
 * by calling the run method (only first call will trigger it).
 * 
 * @todo
 * Cancel method is not complete since every inner loop 
 * (potentially infinite) should carry the isInterrupted boolean, 
 * and this is not the case yet (such loops must be identified,
 * it includes specifically fixed points computation loops).
 * 
 * @author Adnan Bouakaz
 * @author Alexandre Honorat
 */
public class ADFGprob extends GraphRawData implements IADFGp {

	private static final Logger LOGGER = Logger.getLogger("AdfgLog");

	private GraphProbData gpr;

	private ESchedAlgo scheduleType;
	private int nbProcessor;

	private Parameters params;
	
	private Collection<Partition> partitions;
	private Double globalUtilizationFactor;

	private volatile Thread thread;
	private volatile State state;
	private Throwable internalException;
		
	private boolean isSchedulable;

	// Load scotch wrapper
	static {

		String libPathProperty = System.getProperty("java.library.path");
		String classPathProperty = System.getProperty("java.class.path");
		LOGGER.config("Configuration:" + 
					"\nCurrent library path is: " + libPathProperty +
					"\nCurrent class path is: "+ classPathProperty);

		// lpsolve55j is already loaded by the LpSolve class
		// partition is already loaded by the Partitioning class
		// an error is thrown if loaded in another classpath
	}


	/**
	 * Construct a schedulability problem and solve it.
	 * Do graphs' deep copies.
	 * 
	 * @param scheduleType Scheduling algorithm to be used.
	 * @param nbProcessor Number of processors available in the problem.
	 * @param adfgr Data flow graph to schedule.
	 * @param params Optional solving parameters (mainly to not compute again some user inputs).
	 * @throws AdfgExternalException Thrown if a problem occurred during resolution.
	 */
	public ADFGprob(ESchedAlgo scheduleType, int nbProcessor, ADFGraph adfgr, 
					Parameters params) throws AdfgExternalException {
		super(adfgr.getFileName(), adfgr.isXMLresource());
		this.scheduleType = scheduleType;
		this.nbProcessor = nbProcessor;
		thread = null;
		state = State.CREATED;
		internalException = null;
		isSchedulable = false;
		gpr = null;
		this.params = params;

		checkSchedulingParams(scheduleType, nbProcessor, params.getStrategy());
		if (adfgr.getAffineGraph().vertexSet().isEmpty()) {
			throw new AdfgInternalException("Empty graph, exiting.");
		}
		
		try {		    		    
			initCopyGraph(adfgr);
			if (params.isReduceWCET()) {
				long gcdWCET = reduceWCET();
				LOGGER.warning("Actors WCET have been divided by their gcd: " + gcdWCET + ".");
			}
			gpr = new GraphProbData(this, scheduleType, nbProcessor, params);
		} catch (AdfgInternalException e) {
			throw new AdfgExternalException("An exception occured during problem construction.", e);
		}
		
	}

	/**
	 * Copy the graphs' problem into the instance's ones.
	 * AffineRelation, Actors and Channel are deep copied.
	 * 
	 * @param adfgr Initial graph to copy.
	 */
	private void initCopyGraph(ADFGraph adfgr) {
		DirectedMultigraph<Actor, Channel> oldAdfgGraph = adfgr.getAdfgGraph();
		DirectedMultigraph<Actor, AffineRelation> oldAffineGraph = adfgr.getAffineGraph();
		
		Actor.resetIDs();
		for (Actor actor: adfgr.getMappedActors().values()) {
			Actor a = new Actor(actor, params.isKeepPeriod(), params.isKeepMapping());
			this.adfgGraph.addVertex(a);
			this.affineGraph.addVertex(a);
			this.undirectedGraph.addVertex(a);
			mappedActors.put(actor.getName(), a);
		}
		Channel.resetIDs();
		for (Channel ch: adfgr.getMappedChannels().values()) {
			String oldSrc = oldAdfgGraph.getEdgeSource(ch).getName();
			String oldTarget = oldAdfgGraph.getEdgeTarget(ch).getName();
			Channel channel = new Channel(ch, params.isKeepInitToken(), params.isKeepMaxToken());
			this.adfgGraph.addEdge(mappedActors.get(oldSrc), mappedActors.get(oldTarget), channel);
			mappedChannels.put(channel.getName(), channel);
		}
		AffineRelation.resetIDs();
		for (AffineRelation r: oldAffineGraph.edgeSet()) {
		    String oldSrc = oldAffineGraph.getEdgeSource(r).getName();
		    String oldTarget = oldAffineGraph.getEdgeTarget(r).getName();
		    Actor newSrc = mappedActors.get(oldSrc);
		    Actor newTarget = mappedActors.get(oldTarget);
		    this.affineGraph.addEdge(newSrc, newTarget, new AffineRelation(r));
		    if (undirectedGraph.getEdge(newSrc, newTarget) == null) {		    	
			    this.undirectedGraph.addEdge(newSrc, newTarget, new Object());
		    }
		}
		
		for (Entry<String, Loop> loope: adfgr.getMappedLoops().entrySet()) {
			Loop loop = loope.getValue();
			mappedLoops.put(loope.getKey(), new Loop(loop, mappedActors.get(loop.getActor().getName())));
		}
	}
    
	/**
	 * Check if the schedule type and the number of processors
	 * are compatible.
	 * <p>
	 * Uniprocessor algorithms must have only one
	 * processor, and multiprocessors must have an integer greater
	 * than one.
	 * <p> 
	 * This method is automatically called when creating an ADFGprob instance but
	 * can also be useful before constructing the graph (particularly for the CLI).
	 * 
	 * @param scheduleType Scheduling algorithm.
	 * @param nbProcessor Number of processors wanted for this scheduling.
	 * @param strategy Strategy specified by the user. 
	 * @throws AdfgExternalException If the number of processor is not correct.
	 */
	public static void checkSchedulingParams(ESchedAlgo scheduleType, int nbProcessor, ETradeOffType strategy) 
			throws AdfgExternalException {
		if (!scheduleType.isMultiprocessor() && nbProcessor > 1) {
			throw new AdfgExternalException("Multiprocessor not allowed for this sheduling policy.");
	    } else if (scheduleType.isMultiprocessor() && nbProcessor == 1) {
	    	throw new AdfgExternalException(
	    			"The number of processors must be greater than 1 in case of multiprocessor scheduling.");
	    } else if (nbProcessor < 1) {
	    	throw new AdfgExternalException("Negative number of processor not allowed.");
	    }
		if (!scheduleType.implementsStrategies() && strategy != null) {
			LOGGER.warning("A strategy has been specified but this scheduling algorithm does not support it,"
					+ " strategy ignored.");
		}
	}
	
	/**
	 * This method sets all buffer initial number of tokens to zero.
	 * <p>
	 * This method have to be called before the problem analysis,
	 * otherwise it only modifies the results whereas the analysis
	 * have been done with undefined initial number of tokens and
	 * hence has computed possibly strictly positives values.
	 */
	public void setAllInitialTokensToZero() {
		for (Channel ch: getMappedChannels().values()) {
			ch.setInitial(0);
		}
	}
	
	/**
	 * Divide all actors WCET by their greatest
	 * common divisor.
	 * <p>
	 * This method have to be called before the problem analysis.
	 * 
	 * @return The greatest found common divisor.
	 */
	private long reduceWCET() {
		Iterator<Actor> it = mappedActors.values().iterator();
		if (!it.hasNext()) {
			return 1;
		}
		long pgcd = it.next().getWcet();
		while (it.hasNext()) {
			pgcd = BasicArithmetic.funcGCD(pgcd, it.next().getWcet());
		}
		if (pgcd > 1 ) {
			LOGGER.log(Level.WARNING, "All wcet have been divided by a factor " + pgcd + ".");
			for (Actor actor: mappedActors.values()) {
				actor.setWcet(actor.getWcet() / pgcd);
				actor.setAvgCycloWcet(actor.getAvgCycloWcet() / pgcd);
			}
		}
		return pgcd;
	}

	
	/**
	 * Runnable class to launch the analysis.
	 * Should check periodically if the thread hasn't been
	 * interrupted (using java native methods, not using
	 * {@code State} enumeration).
	 * 
	 * @author ahonorat
	 */
	class RunAnalyse implements Runnable {

		@Override
		public void run() {
			AdfgInternalException ex = new 
					AdfgInternalException("Something went wrong during analysis (possible user interruption).");
			LOGGER.fine("NB nodes: " + adfgGraph.vertexSet().size() + "\tNB edges: " + adfgGraph.edgeSet().size());
			
			StringBuilder  sb = new StringBuilder("Repetition vector:\n");
			AsciiTable atFirings = PrettyTable.create();
			atFirings.addRule();
			atFirings.addRow("Actor name", "nbFirings");
			atFirings.addStrongRule();
			for (Entry<String, Actor> e: mappedActors.entrySet()) {
				atFirings.addRow(e.getKey(), e.getValue().getNbFirings());
			}
			atFirings.addRule();
			sb.append(atFirings.render());
			LOGGER.info(sb.toString());
			
			ARSynthesis ars = new ARSynthesis(gpr);
			ars.solving();
			if (Thread.interrupted()) {
				throw ex;
			}

			partitions = gpr.getMappedPartitions().values();

			SSAnalysis ssa = new SSAnalysis(gpr);
			ssa.solving();
			globalUtilizationFactor = ssa.getGlobalUtilizationFactor();
			if (Thread.interrupted()) {
				throw ex;
			}
			isSchedulable = true;

			sb = new StringBuilder("Exiting:\n");

			if (scheduleType.schedulingPolicy() == ESchedPolicy.TT) {
				LOGGER.warning("Buffer sizes are not computed yet for Time Triggered policies, "
						+ "do not take into account these buffer sizes and initial number of tokens.");
			}
			
			sb.append("========== Buffer sizes ==========\n");
			AsciiTable atBuffers = PrettyTable.create();
			atBuffers.addRule();
			atBuffers.addRow("direction", "channel ID", "channel name", "initial tokens", "size");
			atBuffers.addStrongRule();
			long sumBufferSize = 0;
			for (Channel edge: mappedChannels.values()) {
				if (Thread.interrupted()) {
					throw new AdfgInternalException("User interruption.");
				}
				Actor src = adfgGraph.getEdgeSource(edge), dest = adfgGraph.getEdgeTarget(edge);
				AffineRelation r = affineGraph.getEdge(src, dest); 
				long bufferSize = new Buffer(edge, src, dest, r).
						bufferSize(scheduleType, true, params.isForceTokenLCM());
				sumBufferSize += bufferSize;
				atBuffers.addRow(src.getName() + " --> " + dest.getName(), edge.getID(),
						edge.getName(), edge.getInitial() * edge.getDataSize(), bufferSize);
			}
			atBuffers.addRule();
			sb.append(atBuffers.render());
			sb.append("\nSum of buffer sizes = " + sumBufferSize + "\n");


			sb.append("========== Memory sizes ==========\n");
			AsciiTable atLoops = PrettyTable.create();
			atLoops.addRule();
			atLoops.addRow("actor", "channel name", "initial tokens", "size");
			atLoops.addStrongRule();
			int sumMemSize = 0;
			for (Loop loop: mappedLoops.values()) {
				if (Thread.interrupted()) {
					throw new AdfgInternalException("User interruption.");
				}
				loop.setInitMemSize();
				loop.setMaxMemSize();
				long loopSize = loop.getSize()*loop.getDataSize(); 
				sumMemSize += loopSize;
				atLoops.addRow(loop.getActor().getName(), loop.getName(),
						loop.getInitial() * loop.getDataSize(), loopSize);
			}
			atLoops.addRule();
			sb.append(atLoops.render());
			sb.append("\nSum of memory sizes = " + sumMemSize);
			LOGGER.info(sb.toString());
		}	    
	}
	
	/**
	 * Class to report exceptions in the analysis thread.
	 * 
	 * @author ahonorat
	 */
	class HandledThread implements Thread.UncaughtExceptionHandler {
		@Override
		public void uncaughtException(Thread t, Throwable e) {
		    System.out.println(t.getName() + " throws exception: " + e);
		    internalException = e;
		}
	}
	
	/* (non-Javadoc)
	 * @see fr.inria.tea.adfg.algos.IADFGp#start()
	 */
	@Override
	public synchronized void start() {
		if (state == State.CREATED) {
			thread = new Thread(new RunAnalyse());
			thread.setUncaughtExceptionHandler(new HandledThread());
			state = State.STARTED;
			thread.start();
		}
	}
	
	/* (non-Javadoc)
	 * @see fr.inria.tea.adfg.algos.IADFGp#join()
	 */
	@Override
	public void join() throws AdfgExternalException {
		if (thread != null) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				thread = null;
				state = State.INTERRUPTED;
				throw new AdfgExternalException("Computation thread has been interrupted.", e);
			}
			thread = null;
			state = State.JOINED;
		}
		if (internalException != null) {
			throw new AdfgExternalException("Computation thread encountered an exception.", internalException);
		}
	}
	
	/* (non-Javadoc)
	 * @see fr.inria.tea.adfg.algos.IADFGp#cancel()
	 */
	@Override
	public boolean cancel() {
		if (thread != null) {
			try {
				thread.interrupt();
				thread = null;
				state = State.INTERRUPTED;
				return true;
			} catch (SecurityException ex) {
				return false;
			}
		}
		return false;
	}
	
	/**
	 * Compare computed properties with the
	 * given graph ones.
	 * <p>
	 * This method is affected by the choice
	 * of keepPeriod, keepInitToken and
	 * keepMaxToken.
	 * <p>
	 * If the current problem is not schedulable,
	 * the result is always false.
	 * 
	 * @param initialGraph Graph to compare to.
	 * @return True if all properties are equal,
	 * false otherwise.
	 */
	public boolean compareProperties(ADFGraph initialGraph) {
		if (!isSchedulable) {
			return false;
		}
		boolean isStaticPriority = scheduleType.schedulingPolicy() == ESchedPolicy.SP;
		String initialFile = initialGraph.getFileName();
		boolean isFileAdfg = initialFile == null || initialFile.endsWith(".adfg");
		if (isFileAdfg) {
			for (Entry<String, Actor> entry: mappedActors.entrySet()) {
				Actor resultActor = entry.getValue();
				Actor initialActor = initialGraph.getMappedActors().get(entry.getKey());
				if (initialActor == null) {
					return false;
				}
				if ((!params.isKeepPeriod() && resultActor.getPeriod() != initialActor.getUserPeriod()) ||
					(!params.isKeepMapping() &&	resultActor.getPartitionID() != initialActor.getPartitionID()) ||
					resultActor.getWcet() != initialActor.getWcet() ||
					resultActor.getDeadline() != initialActor.getDeadline() ||
					resultActor.getPhase() != initialActor.getPhase()) {
					return false;
				}
				if (isStaticPriority && resultActor.getPriority() != initialActor.getPriority()) {
					return false;
				}
			}
		}
		for (Entry<String, Channel> entry: mappedChannels.entrySet()) {
			Channel resultChannel = entry.getValue();
			Channel initialChannel = initialGraph.getMappedChannels().get(entry.getKey());
			if (initialChannel == null) {
				return false;
			}
			if (!resultChannel.getConsume().equals(initialChannel.getConsume()) ||
				!resultChannel.getProduce().equals(initialChannel.getProduce()) ||
				resultChannel.getDataSize() != initialChannel.getDataSize() ||
				(!params.isKeepMaxToken() && resultChannel.getSize() != initialChannel.getSize()) ||
				(!params.isKeepInitToken() && resultChannel.getInitial() != initialChannel.getInitial())) {
				return false;
			}
		}
		return true;
	}
	
	
	/**
	 * Partitions: one per processor.
	 * 
	 * @return The partitions.
	 */
	public Collection<Partition> getPartitions() {
		return partitions;
	}

	/**
	 * The original parameters instance.
	 * 
	 * @return The parameters instance.
	 */
	public Parameters getParams() {
		return params;
	}
	
	/* (non-Javadoc)
	 * @see fr.inria.tea.adfg.algos.main.IADFGp#getScheduleType()
	 */
	@Override
	public ESchedAlgo getScheduleType() {
		return scheduleType;
	}

	/* (non-Javadoc)
	 * @see fr.inria.tea.adfg.algos.main.IADFGp#getNbProcessor()
	 */
	@Override
	public int getNbProcessor() {
		return nbProcessor;
	}

	/* (non-Javadoc)
	 * @see fr.inria.tea.adfg.algos.main.IADFGp#getGraphResultRep()
	 */
	@Override
	public State getState() {
		return state;
	}
	
	/* (non-Javadoc)
	 * @see fr.inria.tea.adfg.algos.main.IADFGp#isSchedulable()
	 */
	@Override
	public boolean isSchedulable() {
		return isSchedulable;
	}

	/* (non-Javadoc)
	 * @see fr.inria.tea.adfg.algos.main.IADFGp#getUtilizationFactor()
	 */
	@Override
	public Double getGlobalUtilizationFactor() {
		return globalUtilizationFactor;
	}

	/* (non-Javadoc)
	 * @see fr.inria.tea.adfg.algos.main.IADFGp#getGraphResultRep()
	 */
	@Override
	public String getGraphResultRep() {
		return adfgGraph.toString();
	}

	/* (non-Javadoc)
	 * @see fr.inria.tea.adfg.algos.main.IADFGp#updateOrExportFile(java.lang.String)
	 */
	@Override
	public void updateOrExportFile(String newFileName) throws AdfgExternalException {
		if (fileName != null) {
			if (fileName.endsWith(".xml") && (newFileName == null || newFileName.endsWith(".xml"))) {
				SDF3Transformer.updateSDF3FromGraph(this, newFileName);
			} else if (fileName.endsWith(".adfg") && (newFileName == null || newFileName.endsWith(".adfg"))) {
				XMITransformer.udpateXMIFromGraph(this, newFileName);
			} else if (fileName.endsWith(".xml") && newFileName != null && newFileName.endsWith(".adfg")) {
				XMITransformer.udpateXMIFromGraph(this, newFileName);
			} else if (fileName.endsWith(".adfg") && newFileName != null && newFileName.endsWith(".xml")) {
				throw new AdfgExternalException("Cannot create SDF3 (.xml) file from ADFG (.adfg) file.");
			} else if (fileName.endsWith(".tur")) {
				LOGGER.warning("Turbine files cannot be updated.");
				return;
			} else {
				throw new AdfgExternalException("File extension not recognized "
						+ "(can update only SDF3 or ADFG files).");
			}
		} else {
			if (newFileName != null) {
				if (newFileName.endsWith(".adfg")) {
					XMITransformer.udpateXMIFromGraph(this, newFileName);
				} else {
					throw new AdfgExternalException("File extension not recognized (can export only ADFG files).");
				}
			} else {
				throw new AdfgExternalException("Cannot update file of a manually constructed graph.");
			}
		}
	}

	
	/**
	 * Export the results (actors properties) in the Yartiss XML format.
	 * 
	 * @param fileName Yartiss file name to write.
	 * @throws AdfgExternalException If the XML writer encountered a problem.
	 */
	public void exportYartissResults(String fileName) throws AdfgExternalException {
		YartissTransformer.exportSchedResults(this, fileName);
	}

	/**
	 * Export the results (actors anc channels properties) in the Cheddar XMLv3 format.
	 * 
	 * @param fileName Cheddar file name to write.
	 * @throws AdfgExternalException If the XML writer encountered a problem.
	 */
	public void exportCheddarResults(String fileName) throws AdfgExternalException {
		CheddarTransformer.exportSchedResults(this, fileName);
	}
	
	
	/**
	 * Generate LIDE RTEMS code
	 */
	public void generateLIDERTEMS() {
		CodeGeneratorLIDECRTEMS cgl = new CodeGeneratorLIDECRTEMS(gpr);
		cgl.generateLIDECRTEMS();
	}
	/**
	 * Export the results (main graph properties) in the DOT format.
	 * 
	 * @param fileName Dot file name to write.
	 * @throws AdfgExternalException If the XML writer encountered a problem.
	 */
	public void exportDotResults(String fileName) throws AdfgExternalException {
		DotTransformer dot = new DotTransformer(this);
		dot.exportSchedResults(fileName);
	}

}
