/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.common_base;

import java.io.OutputStream;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;
import java.util.logging.StreamHandler;

/**
 * This class forces the stream to be flush after each message.
 * 
 * @author Alexandre Honorat
 */
public class DirectStreamHandler extends StreamHandler {

	/**
	 * Same as StreamHandler.
	 */
	public DirectStreamHandler() {
		super();
	}

	/**
	 * Same as StreamHandler.
	 * 
	 * @param out Output Stream
	 * @param formatter Log record formatter.
	 */
	public DirectStreamHandler(OutputStream out, Formatter formatter) {
		super(out, formatter);
	}

	@Override
	public void publish(LogRecord record) {
		super.publish(record);
		flush();
	}
	
}
