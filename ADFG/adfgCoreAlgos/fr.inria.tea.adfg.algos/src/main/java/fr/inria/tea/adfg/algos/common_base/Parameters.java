/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.common_base;

/**
 * This class holds several parameters used
 * during problem solving.
 * 
 * @author Alexandre Honorat
 */
public class Parameters {

	private final boolean keepPeriod;
	private final boolean keepInitToken;
	private final boolean keepMaxToken;
	private final boolean keepMapping;
	private final ETradeOffType strategy;
	private final boolean reduceWCET;
	private final boolean lcmToken;
	private final TradeOffUDH toUDH;
	
	/**
	 * Creates a parameters instance with the given values.
	 * 
	 * @param keepPeriod Whether or not period must'nt be computed.
	 * @param keepInitToken Whether or not initial buffer sizes must'nt be computed.
	 * @param keepMaxToken Whether or not max buffer sizes must'nt be computed.
	 * @param keepMapping Whether or not mapping must'nt be computed.
	 * @param strategy The strategy to use while solving (ignored if the scheduling
	 * algorithm do not implement it).
	 * @param reduceWCET Reduce all WCET if true.
	 * @param lcmToken Whether or not delays must be a multiple of lcm.
	 * @param toUDH Trade off for SP_UNI_UDH.
	 */
	public Parameters(boolean keepPeriod, boolean keepInitToken, boolean keepMaxToken, boolean keepMapping,
						ETradeOffType strategy, boolean reduceWCET, boolean lcmToken, TradeOffUDH toUDH) {
		this.keepPeriod = keepPeriod;
		this.keepInitToken = keepInitToken;
		this.keepMaxToken = keepMaxToken;
		this.keepMapping = keepMapping;
		this.strategy = strategy;
		this.reduceWCET = reduceWCET;
		this.lcmToken = lcmToken;
		this.toUDH = toUDH;
	}

	public static Parameters getDefault() {
		return new Parameters(false, false, false, false, 
				ETradeOffType.getDefault(), false, false, 
				new TradeOffUDH(TradeOffUDH.DEFAULT_TRADEOFF_UDH));
	}
	
	/**
	 * @return Whether or not the user actor periods must be kept.
	 */
	public boolean isKeepPeriod() {
		return keepPeriod;
	}

	/**
	 * @return Whether or not the user initial buffer tokens must be kept.
	 */
	public boolean isKeepInitToken() {
		return keepInitToken;
	}

	/**
	 * @return Whether or not the user maximum buffer sizes must be kept.
	 */
	public boolean isKeepMaxToken() {
		return keepMaxToken;
	}

	/**
	 * @return Whether or not the user mapping must be kept.
	 */
	public boolean isKeepMapping() {
		return keepMapping;
	}
	
	/**
	 * @return Strategy used during analysis.
	 */
	public ETradeOffType getStrategy() {
		return strategy;
	}
	
	/**
	 * @return If all WCET must be reduced or not.
	 */
	public boolean isReduceWCET() {
		return reduceWCET;
	}

	/**
	 * @return True if init token must be lcm of rates, false
	 * otherwise.
	 */
	public boolean isForceTokenLCM() {
		return lcmToken;
	}
	
	/**
	 * @return Trade off UDH object.
	 */
	public TradeOffUDH getTradeOffUDH() {
		return toUDH;
	}
}
