/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.common_base;

import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.logging.Logger;

import lpsolve.LpSolve;
import lpsolve.LpSolveException;

/**
 * This class is upon all solver classes in order
 * to provide some helpers.
 * 
 * @author Alexandre Honorat
 */
public abstract class AbstractSolver {

	protected String name;
	protected String fileName;
	
	/**
	 * Create a new instance with specific information.
	 * 
	 * @param name Name of the ILP.
	 * @param fileName Name of the ILP file.
	 */
	protected AbstractSolver(String name, String fileName) {
		this.name = name;
		this.fileName = fileName;		
	}

	/**
	 * Solve the problem: basically call the two method of this class,
	 * and store the results in the right objects.
	 */
	protected abstract void solving();
	
	/**
	 * Call the ILP solver.
	 * 
	 * @param logger Where messages must be logged.
	 * @return The ILP problem solved.
	 */
	protected LpSolve solveILP(Logger logger) {
		/* read the generated linear problem from a file and solve it */
		LpSolve ilp = null;
		int res = LpSolve.USERABORT;
		StringBuilder lpLog = new StringBuilder("LpSolve information.");
		try {
			ilp = LpSolve.readLp(fileName, LpSolve.NORMAL, name+" ADFG"); 
			ilp.setOutputfile(""); // indicate to not print output
			ilp.putLogfunc(new LpSolveLogger(), lpLog);
			ilp.putAbortfunc(new LpSolveAborter(), Thread.currentThread());
			res = ilp.solve();
		} catch (LpSolveException e) {
			throw new AdfgInternalException("Problem during "+name+" ILP solving.\n"
					+ "Check the standard output and the generated .lp file, "
					+ "it may come from incompatible bounds "
					+ "on the ILP file, resulting from not consistent user periods.", e);
		}

		logger.fine(lpLog.toString());
		if (res == LpSolve.SUBOPTIMAL) {
			logger.warning(name+" ILP solution is suboptimal.");
		} else if (res != LpSolve.OPTIMAL) {				
			throw new AdfgInternalException(
					"No solution found for "+name+" ILP (lp_solve.solve() returned "+res+").");
		}
		return ilp;
	}
	
	/**
	 * Delete the ILP instance and the file used
	 * to describe the problem.
	 * 
	 * @param logger Where messages must be logged.
	 * @param ilp The solved problem to delete.
	 */
	protected void deleteILP(Logger logger, LpSolve ilp) {
		ilp.deleteLp();
		try {
			Path pathToFile = FileSystems.getDefault().getPath(fileName);
			Files.delete(pathToFile);
		} catch (Exception e) {
			logger.warning(fileName+" file couldn't be deleted:\n"+e);
		}	
	}
	
	
}
