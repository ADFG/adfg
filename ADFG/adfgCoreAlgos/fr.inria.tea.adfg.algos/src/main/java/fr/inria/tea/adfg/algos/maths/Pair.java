/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.maths;

/** 
 * Pair of elements.
 * 
 * @author Adnan Bouakaz
 */
public class Pair<T> {

    private T first, second;
	
    /**
     * Construct a pair from the parameters.
     * 
     * @param a First element.
     * @param b Second element.
     */
    public Pair(T a, T b){
    	first = a;
    	second = b;
    }
	
    /**
     * Copy a pair (by reference if not elementary type).
     * 
     * @param p Pair to be copied.
     */
    public Pair(Pair<T> p){
    	first = p.first;
    	second = p.second;
    }

    /**
     * @return First element in the pair.
     */
    public T getFirst() {
    	return first;
    }

    /**
     * @return Second element in the pair.
     */
    public T getSecond() {
    	return second;
    }
	
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
	@Override
   public String toString(){
    	return "<"+first+", "+second+">";
    }
}
