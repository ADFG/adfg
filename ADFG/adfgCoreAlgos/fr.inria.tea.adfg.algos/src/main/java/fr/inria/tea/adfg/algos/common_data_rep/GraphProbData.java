/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.common_data_rep;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.jgrapht.alg.connectivity.ConnectivityInspector;

import fr.inria.tea.adfg.algos.common_base.AdfgInternalException;
import fr.inria.tea.adfg.algos.common_base.ESchedAlgo;
import fr.inria.tea.adfg.algos.common_base.ESchedPolicy;
import fr.inria.tea.adfg.algos.common_base.ETradeOffType;
import fr.inria.tea.adfg.algos.common_base.Parameters;
import fr.inria.tea.adfg.algos.maths.Fraction;
import fr.inria.tea.adfg.algos.maths.Pair;
import fr.inria.tea.adfg.algos.maths.UPIS;
import fr.inria.tea.adfg.algos.ssa.SSAnalysis;

/**
 * Storage class of all shared data used for the
 * schedule computation.
 * <p>
 * This class also contains some helpers.
 * 
 * @author Adnan Bouakaz
 * @author Alexandre Honorat
 */
public class GraphProbData extends GraphRawData {

    protected final Set<WeaklyConnectedComponent> wcps;
    protected final Map<Integer, Partition> mappedPartitions; 
    protected final ESchedAlgo scheduleType;
    protected final int nbProcessor;
    
    protected final Parameters params;
    protected final ETradeOffType strategy;
    
    protected final int nbActors;
	protected final Actor[] priorityOrderedActors;


    
	/**
	 * Constructor of the global problem internal data.
	 * Copy by reference the first parameter and thus
	 * enable side effects on it.
	 * 
	 * @param grd Graph implicitly constructed by the user.
	 * @param scheduleType Scheduling policy.
	 * @param nbProcessor Number of processors in the modelised system.
	 * @param params The optional parameters given by the user.
	 */
	public GraphProbData(GraphRawData grd, ESchedAlgo scheduleType, int nbProcessor, Parameters params) {
		super(grd);
		this.scheduleType = scheduleType;
		this.nbProcessor = nbProcessor;
		this.params = params;
		this.strategy = params.getStrategy();
		wcps = new HashSet<>();
		mappedPartitions = new HashMap<>();
		nbActors = mappedActors.size();
		priorityOrderedActors = new Actor[nbActors];
	
		
		ConnectivityInspector<Actor,AffineRelation> inspector = new ConnectivityInspector<>(affineGraph);
		for (Set<Actor> connectedSet: inspector.connectedSets()) {
			WeaklyConnectedComponent wcp = new WeaklyConnectedComponent(this, connectedSet);
			wcps.add(wcp);
			for (Actor actor: connectedSet) {
				actor.setWCPcontainer(wcp);
			}
		}
		
		int nbPartitions = scheduleType.isGlobal() ? 1 : nbProcessor;
		for (int i = 0; i < nbPartitions; ++i) {
			mappedPartitions.put(i, new Partition(i));
		}
		
		if (!scheduleType.allowsWCPs() && wcps.size() > 1) { 
		    throw new AdfgInternalException("The dataflow graph must be connected (found "+
		    		wcps.size()+" parts)."); 
		}

	}

	/**
	 * @return Weakly connected components of the affine graph.
	 */
	public Set<WeaklyConnectedComponent> getWeaklyConnectedComponents() {
		return wcps;
	}

	/**
	 * The partition are mapped with their ID.
	 * <p>
	 * Even if the result is a collection, all
	 * partitions are unique.
	 * 
	 * @return Partitions on processors.
	 */
	public Map<Integer, Partition> getMappedPartitions() {
		return mappedPartitions;
	}

	/**
	 * @return Schedule type used for the current analysis.
	 */
	public ESchedAlgo getScheduleType() {
		return scheduleType;
	}

	/**
	 * @return The optional parameters set by the user.
	 */
	public Parameters getUserParams() {
		return params;
	}
	
	/**
	 * @return Strategy used during analysis.
	 */
	public ETradeOffType getStrategy() {
		return strategy;
	}
	
	/**
	 * @return Number of processors available for the current analysis.
	 */
	public int getNbProcessor() {
		return nbProcessor;
	}	
	
	/**
	 * @return Number of actors in the problem;
	 */
	public int getNbActors() {
		return nbActors;
	}
	
	/** 
	 * Actors are ID ordered and unique.
	 * 
	 * @return Actors in the problem.
	 */
	public Collection<Actor> getActors() {
		return mappedActors.values();
	}
	
	/**
	 * Compute the number of partitions actually
	 * used, i.e. non empty.
	 * 
	 * @return Number of non empty partitions.
	 */
	public int getNumberOfUsedProcessors() {
		int nb = 0;
		for (Partition p: getMappedPartitions().values()) {
			if (p.getNbActors() > 0) {
				++nb;
			}
		}
		return nb;
	}
	
	
	public void generateILPforSSA() {
		for (WeaklyConnectedComponent wcp: wcps) {
			wcp.generateILPssa();
		}
	}
	
	/**
	 * @param precision Factor on the period lower
	 * bound to enable the {@code noAdjustment} flag
	 * on actors.
	 */
	public void markNoDeadlineAdjustment(double precision) {
		for (Object o: undirectedGraph.edgeSet()) {
			Actor src = undirectedGraph.getEdgeSource(o);
			Actor dest = undirectedGraph.getEdgeTarget(o);
			AffineRelation r = affineGraph.getEdge(src, dest);
			AffineRelation rReversed = affineGraph.getEdge(dest, src);
			long newBound = 0;
			if (r.getN() <= r.getD() - 2) {
				Fraction coefDest = dest.getCoefficient();
				newBound = (long) Math.ceil(
						new Fraction(dest.getWcet()*r.getD(),r.getN()).getValue() / coefDest.getValue());
			} else if (r.getD() <= r.getN() - 2) {
				Fraction coefSrc = src.getCoefficient();
				newBound = (long) Math.ceil(
						new Fraction(src.getWcet()*r.getN(),r.getD()).getValue() / coefSrc.getValue());
			}
			double reducedBound = newBound * precision;
			if (src.getWCPcontainer().getPeriodLowerBound() < reducedBound &&
				dest.getWCPcontainer().getPeriodLowerBound() < reducedBound) { 
				r.setNoAdjust(true); 
				rReversed.setNoAdjust(true);
			}
		}
	}
	
	/**
	 * Set the periods of the specified actors according
	 * to the basis period {@code T} and their coefficient.
	 * <p>
	 * This method will also set the deadline,
	 * relative to the period.
	 * <p>
	 * This method is intended to be called with a subset
	 * of the same weakly connected component.
	 * 
	 * @param T is the period of the basis actor.
	 * @param componentSet Actors to set.
	 */
	public static void setPeriods(long T, Collection<Actor> componentSet){
		for (Actor actor: componentSet) {
			Fraction coef = actor.getCoefficient();
			actor.setPeriod(T * coef.getNumerator() / coef.getDenominator()); 
		}
	}
		
	/**
	 * Approximate the size of the channel between the
	 * two actors.
	 * 
	 * @param src Source actor.
	 * @param dest Destination actor.
	 * @param ch Channel to consider.
	 * @return The minimum channel size between the two actors (NOT considering channel datatype size).
	 */
	private long minimumSPchannelSize(Actor src, Actor dest, Channel ch) {
		Fraction ax = ch.getProduce().getFiringRatio();
		Fraction ay = ch.getConsume().getFiringRatio();
		Pair<Fraction> lx = ch.getProduce().linearBounds();
		Pair<Fraction> ly = ch.getConsume().linearBounds();
		Fraction l = ly.getSecond().getADD(lx.getSecond()).setSUB(
				ly.getFirst().getADD(lx.getFirst()));
		Fraction res = l.setADD(ax).setADD(ay);
		if (!scheduleType.isGlobal() && Actor.onSameProcessor(src, dest)) {
			if (src.getPriority() < dest.getPriority()) {
				res = l.setADD(ay);
			} else {
				res = l.setADD(ax);
			}
		}
		return (res.getNumerator() + res.getDenominator() - 1) / res.getDenominator();
	}
	
	/**
	 * Approximate the total size of the channels between
	 * two actors. The right channels are used automatically.
	 * 
	 * @param src Source actor.
	 * @param dest Destination actor.
	 * @return The minimum size of all channels between the two actors (considering channel datatype size).
	 */
	public long minimumSPedgeSize(Actor src, Actor dest) {	
		long result = 0;
		for (Channel ch: adfgGraph.getAllEdges(src, dest)) {
		    result += minimumSPchannelSize(src, dest, ch)*ch.getDataSize();
		}
		for (Channel ch: adfgGraph.getAllEdges(dest, src)) {
		    result += minimumSPchannelSize(dest, src, ch)*ch.getDataSize();
		}
		return result;
	}

	/**
	 * Compute the gain that comes 
	 * from allocating the two actors
	 * on the same partition for a
	 * specific channel (SP).
	 * 
	 * @param src Source actor.
	 * @param dest Destination actor.
	 * @param ch Channel to consider.
	 * @return The approximated gain of allocating the
	 * actors on the same processor for the channel.
	 */
	public long approximatedSPchannelGain(Actor src, Actor dest, Channel ch) {
		Fraction ax = ch.getProduce().getFiringRatio();
		Fraction ay = ch.getConsume().getFiringRatio();
		Fraction res;
		if (!scheduleType.isGlobal()) {
			if (src.getPriority() < dest.getPriority()) {
				res = ax;
			} else {
				res = ay;
			}
			return (res.getNumerator() + res.getDenominator() - 1) / res.getDenominator();
		}
		return 0;
	}
	
	/**
	 * Compute the gain that comes 
	 * from allocating the two actors
	 * on the same partition (SP).
	 * 
	 * @param src Source actor.
	 * @param dest Destination actor.
	 * @return The approximated gain of allocating the
	 * actors on the same processor.
	 */
	public long approximatedSPedgeGain(Actor src, Actor dest) {
		long result = 0;
		for (Channel ch: adfgGraph.getAllEdges(src, dest)) {
			result += approximatedSPchannelGain(src, dest, ch);
		}
		for (Channel ch: adfgGraph.getAllEdges(dest, src)) {
			result += approximatedSPchannelGain(dest, src, ch);
		}
		return result;
	}

	/**
	 * Compute the gain that comes 
	 * from allocating the two actors
	 * on the same partition for a
	 * specific channel (EDF).
	 * 
	 * @param src Source actor.
	 * @param dest Destination actor.
	 * @param ch Channel to consider.
	 * @param r Affine relation between the two actors.
	 * @return The approximated gain of allocating the
	 * actors on the same processor for the channel.
	 */
	public long approximatedEDFchannelGain(Actor src, Actor dest, Channel ch, AffineRelation r) {
		Fraction ax = ch.getProduce().getFiringRatio();
		Fraction ay = ch.getConsume().getFiringRatio();
		Fraction res;
		if (!scheduleType.isGlobal()) {
			int dMINUSn = r.getD() - r.getN();
			if (dMINUSn > 1 || (dMINUSn == 0 && src.getID() < dest.getID())) {
				res = ax;
			} else if (dMINUSn < 2 && dMINUSn > -2) {
				res = ax.getADD(ay);
			} else {
				res = ay;
			}
			return (res.getNumerator() + res.getDenominator() - 1) / res.getDenominator();
		}
		return 0;
	}
	
	
	/**
	 * Compute the gain that comes 
	 * from allocating the two actors
	 * on the same partition (SP).
	 * 
	 * @param src Source actor.
	 * @param dest Destination actor.
	 * @param r Affine relation between the two actors.
	 * @return The maximum gain of deadline adjustment
	 * (buffer size sum with EDF_UNI minus with EDF_UNI_DA).
	 */
	public long approximatedEDFedgeGain(Actor src, Actor dest, AffineRelation r) {
		long result = 0;
		for (Channel ch: adfgGraph.getAllEdges(src, dest)) {
			result += approximatedEDFchannelGain(src, dest, ch, r);
		}
		for (Channel ch: adfgGraph.getAllEdges(dest, src)) {
			result += approximatedEDFchannelGain(dest, src, ch, r.getReverse());
		}
		return result;
	}

	/**
	 * Compute the overfolw or the underflow bound on
	 * theta(e) + phi * a_y / d. For EDF, the totally
	 * ordered communications case is handled the same
	 * as the implicit task case.
	 * 
	 * @param src Source actor (productor).
	 * @param dest Destination actor (consumer).
	 * @param r Affine relation between the actors (already reversed).
	 * @param prate Source rate.
	 * @param crate Destination rate.
	 * @param isOver {@code true} is the overflow bound is wanted,
	 * otherwise the underflow bound will be returned.
	 * @return The equation bound. 
	 */
	public Fraction getUOequation(Actor src, Actor dest, AffineRelation r,
									UPIS prate, UPIS crate, boolean isOver) {
		Pair<Fraction> pLambdas = prate.linearBounds();
		Pair<Fraction> cLambdas = crate.linearBounds();
		Fraction result = null;
		if (isOver) {
			result = pLambdas.getSecond().getSUB(cLambdas.getFirst());
		} else {
			result = pLambdas.getFirst().getSUB(cLambdas.getSecond());			
		}
		
		Fraction pRatio = prate.getFiringRatio();
		Fraction cRatio = crate.getFiringRatio();
		if (scheduleType.schedulingPolicy() == ESchedPolicy.SP) {
			if (isOver) {
				Fraction ayPartRatio = new Fraction((r.getN() - 1), r.getD());
				result.setADD(ayPartRatio.getMUL(cRatio));
				if (src.getPriority() < dest.getPriority() ||
						!Actor.onSameProcessor(src, dest) ||
						scheduleType == ESchedAlgo.SP_MULT_MBS) {
					result.setADD(cRatio);
				}
			} else {
				Fraction axPartRatio = new Fraction((1 - r.getD()), r.getN());
				result.setADD(axPartRatio.getMUL(pRatio));
				if (src.getPriority() > dest.getPriority() ||
						!Actor.onSameProcessor(src, dest) ||
						scheduleType == ESchedAlgo.SP_MULT_MBS) {
					result.setSUB(pRatio);								
				}
			}
		} else if (scheduleType.schedulingPolicy() == ESchedPolicy.EDF) {
			if (isOver) {
				Fraction ayPartRatio = new Fraction((r.getN() - 1), r.getD());
				result.setADD(ayPartRatio.getMUL(cRatio));
				if (r.getD() - r.getN() >= 2 || !Actor.onSameProcessor(src, dest) ||
						(r.getD() == r.getN() && src.getID() < dest.getID()) ||
						scheduleType == ESchedAlgo.GEDF_MULT) {
					result.setADD(cRatio);					
				}	
			} else {
				Fraction axPartRatio = new Fraction((1 - r.getD()), r.getN());
				result.setADD(axPartRatio.getMUL(pRatio));
				if (r.getN() - r.getD() >= 2 || !Actor.onSameProcessor(src, dest) ||
						(r.getD() == r.getN() && src.getID() > dest.getID()) ||
						scheduleType == ESchedAlgo.GEDF_MULT) {
					result.setSUB(pRatio);													
				}					
			}
		}
		return result;
	}
	
	
	/**
	 * Order actors by their increasing priorities,
	 * result is undefined if priorities haven't be set yet.
	 * Priorities must have been set globally: two actors on
	 * different partitions must have different priorities
	 * (there is as much priorities as actors, starting from 1).
	 * <p>
	 * Each actor priority is its index in the array + 1.
	 * The actors need not to be mapped on partition yet.
	 */
	public void setPriorityOrderedActors() {
		for (Actor actor: mappedActors.values()) {
			priorityOrderedActors[actor.getPriority()-1] = actor;
		}
	}	

	/**
	 * See {@link #setPriorityOrderedActors()}.
	 * 
	 * @return Array of actors ordered by increasing priority number,
	 * or {@code null} if set has not been called.
	 */
	public Actor[] getPriorityOrderedActors() {
		return priorityOrderedActors;
	}	

	/**
	 * Set the priority ordered actor lists in each partition.
	 * <p>
	 * The method {@link #setPriorityOrderedActors()} has to be
	 * called first (done automatically if {@code doGlobalOrdering}
	 * is {@code true}).
	 * 
	 * @param doGlobalOredering Do a global ordering before the 
	 * partition ordering.
	 */
	public void setPartitionPriorityOrderedActors(boolean doGlobalOredering) {
		if (doGlobalOredering) {
			setPriorityOrderedActors();
		}
		for (Partition partition: mappedPartitions.values()) {
			partition.getPriorityOrderedActors().clear();
		}
		for (Actor actor: priorityOrderedActors) {
			mappedPartitions.get(actor.getPartitionID()).getPriorityOrderedActors().add(actor);
		}
	}
	
	
	/**
	 * Compute the utilization factor over all
	 * partitions and weakly connected components.
	 * <p>
	 * Components basis period must be set.
	 * 
	 * @return Total utilization factor.
	 */
	public double getTotalUtilizationFactor() {
		double totalU = 0;
		for (WeaklyConnectedComponent wcp: wcps) {
			double wcpU = wcp.getComponentUtilizationFactor();
			if (wcpU != SSAnalysis.uncomputedUtilizationFactor) {
				totalU += wcpU;
			} else {
				return SSAnalysis.uncomputedUtilizationFactor;
			}
		}
		return totalU;
	}
	
}

