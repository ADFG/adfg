/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.common_data_rep;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jgrapht.alg.connectivity.KosarajuStrongConnectivityInspector;
import org.jgrapht.graph.DirectedMultigraph;
import org.jgrapht.graph.SimpleDirectedGraph;
import org.jgrapht.graph.SimpleGraph;

import fr.inria.tea.adfg.algos.common_base.AdfgInternalException;
import fr.inria.tea.adfg.algos.maths.BasicArithmetic;
import fr.inria.tea.adfg.algos.maths.Fraction;
import fr.inria.tea.adfg.algos.maths.UPIS;
import fr.inria.tea.adfg.algos.ssa.SSAnalysis;

/**
 * Storage class of one weakly component
 * properties.
 * 
 * @author Adnan Bouakaz
 * @author Alexandre Honorat
 */
public class WeaklyConnectedComponent {
		
	protected int nbProcessor;	
	protected DirectedMultigraph<Actor, AffineRelation> affineGraph;
	protected DirectedMultigraph<Actor, Channel> adfgGraph;
	protected GraphProbData gpr;
	protected SortedSet<Actor> actors;
	/**
	 * WCP affine graph representation.
	 */
	protected SimpleGraph<Actor, Object> wcpUndirectedGraph;
	/** 
	 * Basis actor: periods of other actors are a fraction 
	 * of the basis actor period. 
	 */
	protected Actor basis;
	/**
	 * Lower bound to respect utilization factor and integer relation
	 * constraints between periods and phases.
	 */
	protected long periodUpperBound;
	/**
	 * Always set to MAX_INT for now.
	 */
	protected long periodLowerBound;
	/**
	 * The basis period must be a multiple of {@code divFactor}.
	 * <p>
	 * See Adnan Bouakaz thesis p. 88, first point
	 * ({@code divFactor} is denoted {@code B} in the thesis).
	 */
	protected long divFactor;
	/**
	 * LCM of all coefs denominators, used to compute repetition vector.
	 */
	protected long repFactor;
	/**
	 * LCM of all coefs denominators, used to compute repetition vector.
	 */
	protected long repFactorRates;
	/**
	 * The utilization factor can be expressed as {@code psi/T}
	 * (with T being the basis period equal to {@code 
	 * basis.getPeriod() * divFactor}.
	 * <p>
	 * See Adnan Bouakaz thesis p. 88, first point
	 * ({@code psi} is denoted by sigma in the thesis). 
	 */
	protected double psi;
	protected double avgCycloPsi;
	/**
	 * Used in Scotch partitioning.
	 */
	protected double minSymbolicLoad;
	protected StringBuilder constraints, general;
	protected StringBuilder userPeriodConstraints;
	
	private boolean isILPgenerated;
	private Set<Actor> unconsideredActors;

	
	
	
	/**
	 * Define a weakly connected component from the problem shared data.
	 * <p>
	 * See Adnan Bouakaz thesis p. 88 for ILP constraints (especially
	 * equations 3.1 and 3.2).
	 * <p>
	 * Currently maxmimum allowed period for an actor is Long.MAX_VALUE/100.
	 * 
	 * @param gpr Problem shared data.
	 * @param actorSet Actors in the weakly connected component.
	 */
	public WeaklyConnectedComponent(GraphProbData gpr, Set<Actor> actorSet) {
		nbProcessor = gpr.getNbProcessor();
		affineGraph = gpr.getAffineGraph();
		adfgGraph = gpr.adfgGraph;
		this.gpr = gpr;
		actors = new TreeSet<>(actorSet);
		unconsideredActors = new HashSet<>(gpr.getActors());
		unconsideredActors.removeAll(actorSet);
		wcpUndirectedGraph = (SimpleGraph<Actor, Object>) gpr.getUndirectedGraph().clone();
		for (Actor a: unconsideredActors) {
			wcpUndirectedGraph.removeVertex(a);
		}
		
		isILPgenerated = false;
		constraints = new StringBuilder();
		userPeriodConstraints = new StringBuilder();
		general = new StringBuilder();
		divFactor = 1;
		repFactor = 1;
		repFactorRates = 1;
		periodUpperBound = Actor.MAX_PERIOD;
		periodLowerBound = 0;
		
		/* construction */
		basis = graphOrdering(); 
		basis.setCoefficient(new Fraction(1,1)); 
		psi = basis.getWcet();
		avgCycloPsi = basis.getAvgCycloWcet();
		minSymbolicLoad = basis.getSymbolicLoad();
		
		
		Set<Object> remainingEdges = new HashSet<>(wcpUndirectedGraph.edgeSet());
		dfsCoefficients(basis, remainingEdges, actors.size());
		
		for (Object o: remainingEdges) {
			Actor src = wcpUndirectedGraph.getEdgeSource(o);
			Actor dest = wcpUndirectedGraph.getEdgeTarget(o);
			AffineRelation r = affineGraph.getEdge(src, dest);
			Fraction frac = new Fraction(r.getPhi(), r.getN()).setMUL(src.getCoefficient());
			divFactor = BasicArithmetic.funcLCM(divFactor, frac.getDenominator());
		}
		
		repFactorRates = computeRepetitionVector();
		
		/* U <= m */
		long plb = (long) Math.ceil(psi / nbProcessor); 
		checkAndSetPeriodLB(plb);
		
		checkAndSetPeriodLB(divFactor);
	}

	
	public void generateILPssa() {
		if (isILPgenerated) {
			return;
		}
		isILPgenerated = true;

		for (Actor a: actors) {
			a.setIndex(0);
		}
		
		general.append("int p_"+basis.getID()+";\n"+"int r_"+basis.getID()+";\n");
		if (basis.getUserPeriod() != Actor.UNDEFINED) {
			userPeriodConstraints.append("p_"+basis.getID()+" = "+basis.getUserPeriod()+";\n");
		}

		Set<Object> remainingEdges = new HashSet<>(wcpUndirectedGraph.edgeSet());
		dfsCoefficients(basis, remainingEdges, actors.size());

		for (Object o: remainingEdges) {
			Actor src = wcpUndirectedGraph.getEdgeSource(o);
			Actor dest = wcpUndirectedGraph.getEdgeTarget(o);
			AffineRelation r = affineGraph.getEdge(src, dest);
			Fraction frac = new Fraction(r.getPhi(), r.getN()).setMUL(src.getCoefficient());
			
			String bp = " p_"+basis.getID();
			String x = " r_"+dest.getID(), y = " r_"+src.getID();
			if (r.getPhi() != 0) {
				constraints.append(frac.getDenominator()+ x + " - " + frac.getDenominator() + y +
						" - " + frac.getNumerator()+ bp + " = 0;\n\n");
			} else {
				constraints.append(x + " = " + y + ";\n\n");
			}
		}
		
	}
	
	/**
	 * Compute minimal repetition vector (nbFirings of each actor)
	 * of the component.
	 * <p>
	 * The repetition vector ensures that all rates in the cyclo-static expressions
	 * are used at least once per graph iteration.
	 * 
	 * @return The factor that was needed to ensure that all rates in the cyclo-static
	 * expressions are used at least once.
	 */
	public long computeRepetitionVector() {
		for (Actor a: actors) {
			Fraction f = a.getCoefficient();
			a.setNbFirings(f.getDenominator() * (repFactor / f.getNumerator()));
		}
		long lcm = 1;
		long totFirings = 0;
		for (Actor a: actors) {
			long lcmRates = 1;
			for (Channel c: adfgGraph.outgoingEdgesOf(a)) {
				UPIS upis = c.produce;
				long length = upis.getPeriodLength();
				lcmRates = BasicArithmetic.funcLCM(length, lcmRates);
			}
			for (Channel c: adfgGraph.incomingEdgesOf(a)) {
				UPIS upis = c.consume;
				long length = upis.getPeriodLength();
				lcmRates = BasicArithmetic.funcLCM(length, lcmRates);
			}
			long nbFirings = a.getNbFirings();
			totFirings += nbFirings;
			lcmRates = BasicArithmetic.funcLCM(nbFirings, lcmRates);
			lcm = BasicArithmetic.funcLCM(lcmRates/nbFirings, lcm);
		}
		if (lcm > 1) {
			Logger.getLogger("AdfgLog").log(Level.INFO, "Repetition factor has been multiplied by a factor " 
					+ lcm + " due to the cyclo-rates.");
			for (Actor a: actors) {
				a.setNbFirings(lcm*a.getNbFirings());
			}
		}
		Logger.getLogger("AdfgLog").log(Level.INFO, "Total number of firings in wcp: " + totFirings);
		return lcm;
	}
	
	
	private static class DAGnode {
		private int index;
		private Set<Actor> set;
		private boolean isThereIncomingEdges;
		private int order;
		
		private DAGnode(int index, Set<Actor> set) {
			this.index = index;
			this.set = set;
			isThereIncomingEdges = false;
			order = -1;
		}
	}
	
	
	/**
	 * Order the adfg graph (set actor DAG order)
	 * and return a list of starting points actor.
	 * 
	 * @return One starting point actor (i. e. whose
	 * order is 0).
	 */
	private Actor graphOrdering() {
		DirectedMultigraph<Actor, Channel> wcpAdfgGraph = (DirectedMultigraph<Actor, Channel>) gpr.adfgGraph.clone();
		for (Actor a: unconsideredActors) {
			wcpAdfgGraph.removeVertex(a);
		}
		KosarajuStrongConnectivityInspector<Actor, Channel> inspector = 
				new KosarajuStrongConnectivityInspector<>(wcpAdfgGraph);
		List<Set<Actor>> strongParts = inspector.stronglyConnectedSets();
		SimpleDirectedGraph<DAGnode, Object> inducedDAG = new SimpleDirectedGraph<>(Object.class);
		DAGnode[] dagNodes = new DAGnode[strongParts.size()];

		int index = 0;
		for (Set<Actor> set: strongParts) {
			dagNodes[index] = new DAGnode(index, set);
			inducedDAG.addVertex(dagNodes[index]);
			for (Actor a: set) {
				a.setIndex(index);
			}
			index++;
		}
		for (DAGnode dagNode: dagNodes) {
			index = dagNode.index;
			for (Actor src: dagNode.set) {
				Set<Channel> chans = wcpAdfgGraph.outgoingEdgesOf(src);
				for (Channel chan: chans) {
					Actor dest = wcpAdfgGraph.getEdgeTarget(chan);
					int destIndex = dest.getIndex();
					if (destIndex != index && inducedDAG.getEdge(dagNodes[index], dagNodes[destIndex]) == null) {
						inducedDAG.addEdge(dagNodes[index], dagNodes[destIndex], new Object());
						dagNodes[destIndex].isThereIncomingEdges = true;
					}
				}
			}
		}
		Deque<DAGnode> toVisit = new ArrayDeque<>(index+1);
		for (DAGnode dagNode: dagNodes) {
			if (dagNode.isThereIncomingEdges == false) {
				toVisit.addLast(dagNode);
				dagNode.order = 0;
			}
		}
		Actor result = toVisit.getFirst().set.iterator().next();
		while (!toVisit.isEmpty()) {
			DAGnode dagNode = toVisit.removeFirst();
			for (Object o: inducedDAG.outgoingEdgesOf(dagNode)) {
				DAGnode dest = inducedDAG.getEdgeTarget(o);
				if (dest.order == -1) {
					int order = dagNode.order + 1;
					dest.order = order;
					for (Actor a: dest.set) {
						a.setDAGorder(order);
					}
					toVisit.addLast(dest);
				}
			}
		}
		return result;
	}
	
	/**
	 * Performs a Depth First Search in order to 
	 * set all actor coefficients (and related affine
	 * relation constraints).
	 * 
	 * @param start Start actor (the basis actor at first).
	 * @param remainingEdges Visited edges are removed from this
	 * list to generate later lacking constraints.
	 * @param unusedIndex Available index to mark visited nodes.
	 */
	private void dfsCoefficients(Actor start, Set<Object> remainingEdges, int unusedIndex) {
		start.setIndex(unusedIndex);
		for (AffineRelation r: affineGraph.outgoingEdgesOf(start)) {
			Actor next = affineGraph.getEdgeTarget(r);
			if (next.getIndex() != unusedIndex) {
				setCoefficientAndConstraint(start, next, r);				
				remainingEdges.remove(wcpUndirectedGraph.getEdge(start, next));
				dfsCoefficients(next, remainingEdges, unusedIndex);
			}
		}
	}
	
	/**
	 * Set the coefficients of the actors, and some constraints.
	 * 
	 * @todo Check divFactor computation when symbolic deadline.
	 * 
	 * @param src Visited actor (which has a coefficient).
	 * @param dest To visit actor (which does not have yet a coefficient).
	 * @param r Affine relation from {@code src} to {@code dest}.
	 */
	private void setCoefficientAndConstraint(Actor src, Actor dest, AffineRelation r) {
		Fraction coefNeighbor = src.getCoefficient();
		Fraction coef = new Fraction(r.getD(), r.getN()).setMUL(coefNeighbor); 
		Fraction frac = new Fraction(r.getPhi(), r.getN()).setMUL(coefNeighbor);

		if (!isILPgenerated) {
			dest.setCoefficient(coef);
			double actorSymbolicLoad = dest.getSymbolicLoad();

			/* divFactor and psi */
			divFactor = BasicArithmetic.funcLCM(divFactor, coef.getDenominator());
			repFactor = BasicArithmetic.funcLCM(repFactor, coef.getNumerator());

			psi += actorSymbolicLoad;
			avgCycloPsi += dest.getAvgCycloSymbolicLoad();
			minSymbolicLoad = Math.min(minSymbolicLoad, actorSymbolicLoad);

			/* considering deadline (useless for now)*/ 
			Fraction temp = dest.getSymbolicDeadline().setMUL(coef);
			divFactor = BasicArithmetic.funcLCM(divFactor, temp.getDenominator());
			long plb = (long) Math.ceil(dest.getWcet() / temp.getValue());
			checkAndSetPeriodLB(plb);

			divFactor = BasicArithmetic.funcLCM(divFactor, frac.getDenominator());

		} else {
			/* ILP generation */

			general.append("int p_"+dest.getID()+";\n"+"int r_"+dest.getID()+";\n");
			/* constraint on periods */
			String a = " p_"+dest.getID(), b = " p_"+basis.getID();
			constraints.append(coef.getDenominator() + a + " - "+coef.getNumerator() + b + " = 0;\n");
			if (dest.getUserPeriod() != Actor.UNDEFINED) {
				userPeriodConstraints.append("p_"+dest.getID()+" = "+dest.getUserPeriod()+";\n");
			}

			/* constraint on phasis */
			String x = " r_"+dest.getID(), y = " r_"+src.getID();
			if (r.getPhi() != 0) {
				String bp = " p_"+basis.getID();
				constraints.append(frac.getDenominator()+ x + " - " + frac.getDenominator() + y +
						" - " + frac.getNumerator() + bp + " = 0;\n\n");
			} else {
				constraints.append(x + " = " + y + ";\n\n");
			}
		}
	}
	
	
	/**
	 * Clone by deep copy an existing weakly connected component.
	 * 
	 * @param wcp Original weakly connected component.
	 */
	public WeaklyConnectedComponent(WeaklyConnectedComponent wcp) {
		actors = new TreeSet<>(wcp.actors);
		nbProcessor = wcp.nbProcessor;
		affineGraph = wcp.affineGraph;
		gpr = wcp.gpr;
		wcpUndirectedGraph = wcp.wcpUndirectedGraph;
		isILPgenerated = wcp.isILPgenerated;
		constraints = new StringBuilder(wcp.constraints);
		userPeriodConstraints = new StringBuilder(wcp.userPeriodConstraints);
		general = new StringBuilder(wcp.general);
		divFactor = wcp.divFactor;
		repFactor = wcp.repFactor;
		repFactorRates = wcp.repFactorRates;
		psi = wcp.psi;
		avgCycloPsi = wcp.avgCycloPsi;
		basis = wcp.basis;
		periodLowerBound = wcp.periodLowerBound;
		periodUpperBound = wcp.periodUpperBound;
		minSymbolicLoad = wcp.minSymbolicLoad;
	}

	
	/**
	 * Compute the new period lower bound and set it.
	 * <p>
	 * An exception is thrown if the new lower bound
	 * is greater than the upper bound.
	 * 
	 * @param nextPeriodLowerBound Next lower bound.
	 */
	private void checkAndSetPeriodLB(long nextPeriodLowerBound) {
		if (nextPeriodLowerBound > periodLowerBound) {
			periodLowerBound = nextPeriodLowerBound;
		}
		if (periodLowerBound > periodUpperBound) {
			throw new AdfgInternalException("Bounds on periods are not consistent.");
		}
	}
	
	
	/**
	 * 	
	 * @return True if ILP constraints are generated for this
	 * instance, false otherwise.
	 */
	public boolean isILPgenerated() {
		return isILPgenerated;
	}
	
	/**
	 * Fusion of the weakly connected component in parameter with the
	 * current one (side effects on the current object). ILP constraints
	 * are also merged if any.
	 * <p>
	 * This fusion method is not described in the Adnan Bouakaz thesis,
	 * currently the fusion factor is the symbolic load (psi) ratio of 
	 * the two wcp to fusion (component.psi/this.psi).
	 * 
	 * @param component Component to fusion with.
	 * @param factor Quotient of the {@code 
	 * component psi} over the current object
	 * {@code psi}. This factor will multiple the
	 * new component actor coefficients.
	 * 
	 */
	public void fusion(WeaklyConnectedComponent component, Fraction factor) {
		/* fusion coefficients */
		for (Actor a: component.actors) { 
			a.getCoefficient().setMUL(factor);
		}
		actors.addAll(component.getActors());
		unconsideredActors = new HashSet<>(gpr.getActors());
		unconsideredActors.removeAll(actors);
		unconsideredActors.removeAll(component.actors);
		wcpUndirectedGraph = (SimpleGraph<Actor, Object>) gpr.getUndirectedGraph().clone();
		for (Actor a: unconsideredActors) {
			wcpUndirectedGraph.removeVertex(a);
		}

		/* fusion divFactor */
		divFactor = BasicArithmetic.funcLCM(divFactor, factor.getDenominator());
		divFactor = BasicArithmetic.funcLCM(divFactor, component.divFactor 
				/ BasicArithmetic.funcGCD(component.divFactor, factor.getNumerator()));

		repFactor = BasicArithmetic.funcLCM(repFactor, factor.getDenominator());
		repFactor = BasicArithmetic.funcLCM(repFactor, component.repFactor 
				/ BasicArithmetic.funcGCD(component.repFactor, factor.getNumerator()));
		
		repFactorRates = computeRepetitionVector();
		
		/* fusion of psi */
		double factorValue = factor.getValue();
		psi += component.psi / factorValue;
		avgCycloPsi += component.avgCycloPsi / factorValue;
		minSymbolicLoad = Math.min(minSymbolicLoad, component.minSymbolicLoad*factor.getValue());
		
		/* fusion bounds */
		if (component.periodUpperBound != Actor.MAX_PERIOD) {
			long pub = (long) Math.floor(component.periodUpperBound / factor.getValue());
			if (pub < periodUpperBound) {
				periodUpperBound = pub;
			}
		}
		checkAndSetPeriodLB((long) Math.ceil(component.periodLowerBound / factor.getValue()));  
		checkAndSetPeriodLB((long) Math.ceil(psi / nbProcessor)); 
		checkAndSetPeriodLB(divFactor);
		
		/* fusion ILP problems */
		if (isILPgenerated && component.isILPgenerated) {
			constraints.append(component.constraints); 
			constraints.append(factor.getDenominator()+" p_"+component.basis.getID()+
					" - "+factor.getNumerator()+" p_"+basis.getID()+" = 0;\n");
			general.append(component.general);
		}
	}
	
	/**
	 * The period lower bound must be greater than psi over nbProcessors.
	 * However the number of non empty partitions can be greater than
	 * the number of available processors, and so PLB must be
	 * computed again.
	 * <p>
	 * This method is designed to be called after partitioning
	 * and after a fusion.
	 *
	 * @param mustUseAllProcessors True if the analysis must be done with
	 * all available processors, or false if with the number of non empty partitions.
	 */
	public void updatePLBwithNbPartitions(boolean mustUseAllProcessors) {
		int nbProcs = gpr.getNbProcessor();
		if (!mustUseAllProcessors) {
			nbProcs = gpr.getNumberOfUsedProcessors();
		}
		long newPLB = (long) Math.ceil(psi/nbProcs);
		checkAndSetPeriodLB(newPLB);
	}
	
	/**
	 * This method computes the component utilization factor thanks
	 * to the basis period.
	 * 
	 * @return Processor utilization factor of this component.
	 */
	public double getComponentUtilizationFactor() {
		return (basis.getPeriod() == Actor.UNDEFINED) ? 
					SSAnalysis.uncomputedUtilizationFactor : (psi / basis.getPeriod());
	}

	/**
	 * This method computes the component average cyclo utilization factor thanks
	 * to the basis period.
	 * 
	 * @return Processor utilization factor (from average cyclo-WCET) of this component.
	 */
	public double getComponentAvgCycloUtilizationFactor() {
		return (basis.getPeriod() == Actor.UNDEFINED) ? 
					SSAnalysis.uncomputedUtilizationFactor : (avgCycloPsi / basis.getPeriod());
	}
	
	/**
	 * @return Actors present in this weakly components.
	 */
	public Set<Actor> getActors() {
		return actors;
	}

	/**
	 * @return The undirected graph of this wcp.
	 */
	public SimpleGraph<Actor, Object> getWcpUndirectedGraph() {
		return wcpUndirectedGraph;
	}
	
	/**
	 * @return Component basis.
	 */
	public Actor getBasis() {
		return basis;
	}

	/**
	 * See Adnan Bouakaz thesis p. 88, first point
	 * ({@code divFactor} is {@code B}).
	 * 
	 * @return The division factor.
	 */
	public long getDivFactor() {
		return divFactor;
	}

	/**
	 * See Adnan Bouakaz thesis p. 88, first point
	 * ({@code psi} is denoted by sigma in the thesis). 
	 * 
	 * @return Symbolic load of the component, being
	 * the sum of its actor symbolic load.
	 */
	public double getSymbolicLoad() {
		return psi;
	}

	/**
	 * See Adnan Bouakaz thesis p. 88, first point
	 * ({@code psi} is denoted by sigma in the thesis). 
	 * 
	 * @return Average cyclo-symbolic load of the component, being
	 * the sum of its actor average cyclo-symbolic load.
	 */
	public double getAvgCycloSymbolicLoad() {
		return avgCycloPsi;
	}

	
	/**
	 * Used in Scotch partitioning.
	 * 
	 * @return The minimum symbolic load among all actors
	 * in this component.
	 */
	public double getMinimumSymbolicLoad() {
		return minSymbolicLoad;
	}
	
	/**
	 * @return Component period upper bound.
	 */
	public long getPeriodUpperBound() {
		return periodUpperBound;
	}

	/**
	 * Not called.
	 * @param periodUpperBound New period upper bound.
	 */
	public void setPeriodUpperBound(long periodUpperBound) {
		this.periodUpperBound = periodUpperBound;
	}

	/**
	 * @return Component period lower bound.
	 */
	public long getPeriodLowerBound(){
		return periodLowerBound;
	}

	/**
	 * Called only by {@code deadlineCalculus}.
	 * @param periodLowerBound New period lower bound.
	 */
	public void setPeriodLowerBound(long periodLowerBound) {
		this.periodLowerBound = periodLowerBound;
	}
	
	/**
	 * @return Constraints generated for the ILP solver,
	 * empty string if none.
	 */
	public String getConstraints() {
		return constraints.toString();
	}

	/**
	 * @return User period constraints generated for the
	 * ILP solver, empty string if none.
	 */
	public String getUserPeriodConstraints() {
		return userPeriodConstraints.toString();
	}
	
	/**
	 * @return General information generated for ILP solver,
	 * empty string if none.
	 */
	public String getGeneral() {
		return general.toString();
	}

	/**
	 * @return Factor multiplying the repetition vector in order
	 * to ensure that each rate is used once.
	 */
	public long getRepetitionFactorScale() {
		return repFactorRates;
	}
	
	/**
	 * Set the periods of all actors according
	 * to {@code basisPeriod} and their coefficient.
	 * <p>
	 * This method will also set the deadline,
	 * relative to the period.
	 * 
	 * @param basisPeriod is the period of the basis actor.
	 */
	public void setPeriods(long basisPeriod) {
		GraphProbData.setPeriods(basisPeriod, actors);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder("WCP relative period infos and abstract utilization factor:\n");
		sb.append(periodLowerBound+" <= ( p_"+basis.getID()+" = k * "+divFactor+" ) <= "+periodUpperBound+
				"\n U = "+psi+"/p_"+basis.getID()+" \n"); 
		for (Actor actor: actors) {
			sb.append("p_"+actor.getID()+" = "+actor.getCoefficient()+" p_"+basis.getID()+";\t"); 
		}
		return sb.toString();
	}

}
