/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.ssa;

import java.util.logging.Logger;

import fr.inria.tea.adfg.algos.common_base.AdfgInternalException;
import fr.inria.tea.adfg.algos.common_data_rep.GraphProbData;

/**
 * This class contains several methods to compute
 * the best period of a system
 * for Time Triggered scheduling, and ensures that the system
 * is schedulable.
 * 
 * @author Alexandre Honorat
 * @author Hai Nam Tran
 */
public class SchedulingOracleTT {

	private static final Logger LOGGER = Logger.getLogger("AdfgLog");	
	
	public static final long incoherentPeriod = -1;
	
	// GraphProbData
	private int nbProcessor;
	private GraphProbData gpr;
	
	/**
	 * Store the graph problem useful information.
	 * 
	 * @param gpr Graph to work with.
	 * @param mustUseAllProcessors True if the analysis must be done with
	 * all available processors, or false if with the number of non empty partitions.
	 */
	public SchedulingOracleTT(GraphProbData gpr, boolean mustUseAllProcessors) {
		nbProcessor = gpr.getNbProcessor();
		if (!mustUseAllProcessors) {
			nbProcessor = gpr.getNumberOfUsedProcessors();
			
		}
		this.gpr = gpr;
	}	

	
	/**
	 * This method should compute the basis actor best period in
	 * each weakly connected component (only one considered for now).
	 * 
	 * @todo
	 * Code this method.
	 * 
	 * @return Weakly connected component basis actor best period.
	 */
	public long experimentalMethod() {
		throw new AdfgInternalException("TT_MC_RTA is not fully implemented, abandon.");
	}
	

}
