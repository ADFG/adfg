/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.common_base;

import java.util.logging.Logger;

/**
 * External exception can be thrown by adfg,
 * especially during schedulability analysis. 
 * The user must caught them. All the exceptions are
 * logged when constructed.
 * 
 * @author Alexandre Honorat
 */
public class AdfgExternalException extends Exception {

	// useful in case of serialization
	private static final long serialVersionUID = 1L;
	
	private static final Logger LOGGER = Logger.getLogger("AdfgLog");

    private String getCause(Throwable cause) {
    	StringBuilder sb = new StringBuilder();
    	Throwable t = cause;
    	while (t != null) {
    		sb.append("\n" + t.toString());
    		t = t.getCause();
    	}
    	return sb.toString();
    }

	/**
	 * Standard constructor, not used.
	 */
	public AdfgExternalException() {
		super();
	}

	/**
	 * Full constructor, not used in practice.
	 * 
	 * @param message Message.
	 * @param cause Cause.
	 * @param enableSuppression Allow to suppress exceptions when several
	 * are thrown in the same time.
	 * @param writableStackTrace If the stack trace must be written or not.
	 */
	public AdfgExternalException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		LOGGER.severe(message + getCause(cause));
	}

	/**
	 * Constructor to transmit a cause, with a specific message.
	 * 
	 * @param message Message.
	 * @param cause Cause.
	 */
	public AdfgExternalException(String message, Throwable cause) {
		super(message, cause);
		LOGGER.severe(message + getCause(cause));
	}

	/**
	 * Constructor with specific message.
	 * 
	 * @param message Message.
	 */
	public AdfgExternalException(String message) {
		super(message);
		LOGGER.severe(message);
	}

	/**
	 * Constructor to transmit a cause, may be
	 * used to transmit adfg internal exception.
	 * 
	 * @param cause Cause.
	 */
	public AdfgExternalException(Throwable cause) {
	    super(cause);
	    LOGGER.severe("Externalisation of exception:" + getCause(cause));
	}
	
}
