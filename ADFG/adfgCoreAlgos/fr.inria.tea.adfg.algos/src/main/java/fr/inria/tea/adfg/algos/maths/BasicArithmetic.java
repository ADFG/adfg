/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.maths;

/** 
 * Basic functions on long.
 * 
 * @author Adnan Bouakaz
 */
public final class BasicArithmetic {
	
	private BasicArithmetic() {}
	
    /**
     * Greatest common divisor (euclidean algorithm).
     * Undefined result if parameters are negative numbers.
     * 
     * @param a First number.
     * @param b Second number.
     * @return Greatest common divisor of the parameters.
     */
    public static long funcGCD(long a, long b) {
    	if (a == 0 && b == 0) {
    		return 1;
    	} else if (a == 0) {
    		return b;
    	} else {
    		return funcGCD(b%a, a);
    	}
    }
	
    /**
     * Lowest common multiple (multiplication of the numbers,
     * divided by GCD).
     * Undefined result if parameters are negative numbers.
     * 
     * @param a First number.
     * @param b Second number.
     * @return Lowest common multiple of the parameters.
     */
    public static long funcLCM(long a, long b){
    	return a * (b / funcGCD(a, b));
    }
	
}
