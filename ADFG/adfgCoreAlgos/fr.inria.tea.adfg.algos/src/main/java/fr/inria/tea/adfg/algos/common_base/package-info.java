/**
 * This package contains the base classes for exceptions and parameters.
 */
package fr.inria.tea.adfg.algos.common_base;