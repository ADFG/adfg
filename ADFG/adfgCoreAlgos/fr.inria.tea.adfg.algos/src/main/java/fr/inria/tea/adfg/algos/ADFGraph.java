/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos;

import java.util.logging.Logger;

import fr.inria.tea.adfg.algos.common_base.AdfgExternalException;
import fr.inria.tea.adfg.algos.common_data_rep.Actor;
import fr.inria.tea.adfg.algos.common_data_rep.AffineRelation;
import fr.inria.tea.adfg.algos.common_data_rep.Channel;
import fr.inria.tea.adfg.algos.common_data_rep.GraphRawData;
import fr.inria.tea.adfg.algos.common_data_rep.Loop;
import fr.inria.tea.adfg.algos.maths.Fraction;
import fr.inria.tea.adfg.algos.maths.UPIS;

/**
 * Basic implementation to construct a data flow graph,
 * and its related affine relation graph.
 * 
 * @author Adnan Bouakaz
 * @author Alexandre Honorat
 */
public class ADFGraph extends GraphRawData implements IADFGr  {

	private static final Logger LOGGER = Logger.getLogger("AdfgLog");	 

	private boolean isMutable;

	/**
	 * Construct an empty graph.
	 */
	public ADFGraph() {
		super(null, false);
		isMutable = true;
	}

	/**
	 * Construct a graph from a SDF3 file. 
	 * <p>
	 * It is not possible to call setter on this object if instantiated with
	 * this constructor.
	 * 
	 * @param fileName sdf3 file to load.
	 * @throws AdfgExternalException Thrown if problem occurred during file reading.
	 */
	public ADFGraph(String fileName) throws AdfgExternalException {
		this(fileName, false);
	}

	/**
	 * Construct a graph from a SDF3 file. Enable to specify an sdf3 file from
	 * the maven main resource folder, for tests purposes.
	 * <p>
	 * It is not possible to call setter on this object if instantiated with
	 * this constructor.
	 * 
	 * @param fileName sdf3 file to load.
	 * @param isXMLresource True if XML file is located in maven main resource folder.
	 * @throws AdfgExternalException Thrown if problem occurred during file reading.
	 */
	public ADFGraph(String fileName, boolean isXMLresource) throws AdfgExternalException {
		super(fileName, isXMLresource);
		isMutable = true;
		if (fileName != null ) {
			if (fileName.endsWith(".xml")) {
				SDF3Transformer.initGraphFromSDF3(this);
			} else if (fileName.endsWith(".adfg")) {
				XMITransformer.initGraphFromXMI(this);
			} else if (fileName.endsWith(".tur")) {
				TurbineTransformer.initGraphFromTurbine(this);
			} else {
				throw new AdfgExternalException("File extension not recognized (accepts *.xml and *.adfg).");
			}
			isMutable = false;
		}
	}

	/* (non-Javadoc)
	 * @see fr.inria.tea.adfg.algos.main.IADFGr#getGraphRep()
	 */
	@Override
	public String getGraphRep() {
		return adfgGraph.toString();
	}

	/* (non-Javadoc) 
	 * @see fr.inria.tea.adfg.algos.main.IADFGr#addActor(fr.inria.tea.adfg.algos.elements.Actor)
	 */
	@Override
	public boolean addActor(Actor actor) throws AdfgExternalException {
		boolean inserted = false;
		if (isMutable) {
			if (mappedActors.containsKey(actor.getName())) {
				throw new AdfgExternalException("An actor already exists with the same name (<" + actor.getName() + ">).");
			} else {
				if (inserted = adfgGraph.addVertex(actor)) {
					// if insertion worked for one graph, it should work for all
					affineGraph.addVertex(actor);
					undirectedGraph.addVertex(actor);
					mappedActors.put(actor.getName(), actor);					
				}
			}
		} else {
			LOGGER.warning("Attempting to modify a graph contructed by a file, ignored.");
		}
		return inserted;
	}

	/* (non-Javadoc)
	 * @see fr.inria.tea.adfg.algos.main.IADFGr#addChannel(fr.inria.tea.adfg.algos.elements.Channel, 
	 * fr.inria.tea.adfg.algos.elements.Actor, fr.inria.tea.adfg.algos.elements.Actor)
	 */
	@Override
	public boolean addChannel(Channel ch, Actor src, Actor dest) throws AdfgExternalException {
		boolean inserted = false;
		if (isMutable) {
			if (ch.getName().startsWith("_ch")) {
				LOGGER.warning("Channel <" + ch.getName() + "> cannot be added (unauthorized _ch* name), ignored.");
			    return inserted;
			}
			if (mappedChannels.containsKey(ch.getName())) {
				throw new AdfgExternalException("A channel already exists with the same name (<"+ch.getName()+">).");
			} else if (src.getID() == dest.getID()) {
				if (mappedActors.containsKey(src.getName())) {
					if (mappedLoops.containsKey(ch.getName())) {
						throw new AdfgExternalException("A loop already exists with the same name (<"+ch.getName()+">).");						
					} else {
						mappedLoops.put(ch.getName(), new Loop(ch, src));
						// Self loop are allowed only with the Pseudograph class of JGraphT
						// adfgGraph.addEdge(src, dest, ch); // not allowed, and not needed
						return true;						
					}
				}
				return false;
			} else {
				try {
					inserted = adfgGraph.addEdge(src, dest, ch);
				} catch (IllegalArgumentException e) {
					LOGGER.warning("The channel couldn't be added (" + e.getMessage() + ").");
				}
				if (inserted) {					
					// check boundedness criterion and update affineGraph
					// if insertion worked for one graph, it should work for all
					UPIS prate = ch.getProduce(), crate = ch.getConsume(); 
					Fraction nOVERd = new Fraction(prate.getSumPeriod()*crate.getPeriodLength(), 
							prate.getPeriodLength()*crate.getSumPeriod());
					AffineRelation rSrcToDest = 
							new AffineRelation((int) nOVERd.getNumerator(), (int) nOVERd.getDenominator());

					if (undirectedGraph.getEdge(src, dest) == null) {
						affineGraph.addEdge(src, dest, rSrcToDest);
						AffineRelation rDestToSrc = 
								new AffineRelation((int) nOVERd.getDenominator(), (int) nOVERd.getNumerator());
						affineGraph.addEdge(dest, src, rDestToSrc);
						undirectedGraph.addEdge(src, dest, new Object());
					} else {
						AffineRelation r = affineGraph.getEdge(src, dest);
						if (!rSrcToDest.isConsistentWith(r)) {
							throw new AdfgExternalException(
										"Two channels between actors with different boundedness criterion.");
						}
					}
					mappedChannels.put(ch.getName(), ch);
				}
			}
		} else {
			LOGGER.warning("Attempting to modify a graph contructed by a file, ignored.");
		}
		return inserted;
	}
	
}
