/**
 * This package contains the classes performing Affine Relation Synthesis (ARS).
 */
package fr.inria.tea.adfg.algos.ars;