/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos;

import fr.inria.tea.adfg.algos.common_base.ESchedAlgo;
import fr.inria.tea.adfg.algos.common_base.AdfgExternalException;


/**
 * This interface provides methods to get the results of the
 * schedulability analysis. Be aware that the solution is computed
 * during constructor operations.
 * 
 * @author Alexandre Honorat
 */
public interface IADFGp {

	/**
	 * Internal state of the analysis thread. 
	 * 
	 * @author ahonorat
	 */
	public enum State {
		/**
		 * The object has been correctly instantiated.
		 */
		CREATED,
		/**
		 * The thread has been started.
		 */
		STARTED,
		/**
		 * The thread has been notified to be interrupted.
		 */
		INTERRUPTED,
		/**
		 * The thread has been joined (set once the join method returned).
		 */
		JOINED;
	}	
	
	/**
	 * Start the analysis, non blocking operation. Only the first call
	 * will start the analysis, next one will do nothing.
	 * 
	 */
	void start();

	/**
	 * Wait for the analysis to complete. The analysis cannot be run anymore
	 * after this call. Throws the exception that occurred during
	 * analysis if there is one.
	 * 
	 * @throws AdfgExternalException If something went wrong
	 * (during the analysis, or while attempting to interrupt it).
	 */
	void join() throws AdfgExternalException;

	/**
	 * Cancel the analysis. The analysis cannot be run anymore if no
	 * exception has been raised. If the problem has been canceled
	 * and an exception occurred before that, it will be thrown when
	 * calling the join method.
	 * 
	 * @return True if analysis have been cancelled, false if a
	 * SecurityExceotion occurred or if the analysis is already finished.
	 */
	boolean cancel();
	
	/**
	 * 
	 * @return The schedule type used for this schedulability analysis.
	 */
	ESchedAlgo getScheduleType();

	/**
	 * 
	 * @return The number of processors used for this schedulability analysis.
	 */
	int getNbProcessor();
	
	/**
	 * 
	 * @return The current state of this problem analysis instance.
	 */
	public State getState();
	
	/**
	 * 
	 * @return True if the system is schedulable, false otherwise.
	 */
	public boolean isSchedulable();
	
	/**
	 * @return The utilization factor computed by the analysis
	 * (among all weakly connected components). Null if the analyses
	 * went wrong or if the system is not schedulable.
	 */
	public Double getGlobalUtilizationFactor();
	
	/**
	 * @return The data flow graph with parameters computed during the
	 * schedulability analysis.
	 */
	String getGraphResultRep();

	/**
	 * Write results to the file used to construct the graph
	 * if it exists, else if {@code newFileName}
	 * is not {@code null}, copy the result in the new file instead of
	 * the one used for graph construction.
	 * <p>
	 * SDF3 files (*.xml) cannot be created, only updated; at the opposite
	 * ADFG files (*.adfg) can be created even if the graph has been 
	 * constructed manually (without loading a file).
	 * 
	 * @param newFileName File to write, if none try to update files
	 * used to construct the graph.
	 * 
	 * @throws AdfgExternalException Thrown if a problem occurred during
	 * the results write or if the file extension is not correct.
	 */
	void updateOrExportFile(String newFileName) throws AdfgExternalException;

}
