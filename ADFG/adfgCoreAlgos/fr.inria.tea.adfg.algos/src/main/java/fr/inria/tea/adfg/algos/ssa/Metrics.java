/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.ssa;

import java.util.Collection;

import fr.inria.tea.adfg.algos.common_data_rep.Actor;
import fr.inria.tea.adfg.algos.maths.Fraction;

/**
 * This class enable to compute some metrics on
 * actor sets. U is needed several times
 * (to be reused in other metrics) and hence is computed
 * only once and stored as a class member.
 * <p>
 * If the actor periods are modified,
 * you must create a new object of this class.
 * 
 * @author Adnan Bouakaz
 * @author Alexandre Honorat
 */
public class Metrics {

	private Collection<Actor> actors;
	private int nbProcessor;

	private double savedU;
	private boolean isSavedU;

	/**
	 * Construct a metrics object characterized by
	 * an actors's set and the number of processors.
	 * 
	 * @param actors Actors's set to compute metrics on.
	 * @param nbProcessor Available number of processors.
	 */
	public Metrics(Collection<Actor> actors, int nbProcessor) {
		this.actors = actors;
		this.nbProcessor = nbProcessor;

		isSavedU = false;
	}

	/**
	 * Total processor demand during a time interval,
	 * applied on the instance actor set.
	 * <p>
	 * See Adnan Bouakaz thesis p. 40.
	 * 
	 * @param timeInterval Time interval duration.
	 * @return Processor demand of the task system,
	 * homogeneous to the time unit.
	 */
	long edfH(long timeInterval) {
		long h = 0;
		for (Actor actor: actors) {
			long rest = timeInterval % actor.getPeriod();
			h += (rest >= actor.getDeadline()) ? actor.getWcet() : 0; 
			h += actor.getWcet() * (timeInterval / actor.getPeriod());
		}
		return h;
	}

	/**
	 * Compute processor utilization factor
	 * of the actor set, with real periods. 
	 * <p>
	 * See Adnan Bouakaz thesis p. 36.
	 * 
	 * @return Processor utilization factor.
	 */
	double edfU() {
		if (isSavedU) {
			return savedU;
		}
		savedU = 0;
		for (Actor actor: actors) {
			savedU += (double) actor.getWcet() / actor.getPeriod();
		}
		isSavedU = true;
		return savedU;
	}

	/**
	 * Compute L bound to ensure schedulability of
	 * constrained systems.
	 * <p>
	 * See Adnan Bouakaz thesis p. 40.
	 * 
	 * @return L metric, homogeneous to the time unit.
	 */
	long edfL() {
		double sum = 0;
		for (Actor actor: actors) {
			sum += actor.getWcet() * ((double) (actor.getPeriod() - actor.getDeadline()) / actor.getPeriod());
		}
		sum = sum / (1 - edfU()); 
		return (long) Math.ceil(sum);
	}

	/**
	 * Compute the maximum of the absolute deadlines
	 * lesser or equal than the time interval. 
	 * <p>
	 * See Adnan Bouakaz thesis p. 107.
	 * DMax is not written explicitly, it corresponds to
	 * max{d|d &lt; t} with t being 
	 * {@code timeInterval here}.
	 * 
	 * @param timeInterval Time interval duration.
	 * @param strictInequality If {@code true}, returns
	 * the maximum deadline strictly less than {@code timeInterval}.
	 * @return The maximum absolute deadline less than 
	 * {@code timeInterval}.
	 */
	long edfDMax(long timeInterval, boolean strictInequality) {
		long dMax = 0;
		for (Actor actor: actors) {
			long absoluteDeadline = 0;
			long rest = timeInterval % actor.getPeriod(); 
			if (rest > actor.getDeadline()) { 
				absoluteDeadline = actor.getDeadline();				
			}
			absoluteDeadline += actor.getPeriod() * (timeInterval / actor.getPeriod());
			if (absoluteDeadline > dMax) {
				if (absoluteDeadline < timeInterval || (!strictInequality && absoluteDeadline == timeInterval)) {
					dMax = absoluteDeadline;
				}
			}
		}
		return dMax;
	}

	/**
	 * Compute the minimum relative deadline in the
	 * actor set.
	 * 
	 * @return The minimum relative deadline.
	 */
	long edfDMin() {
		long dMin = Actor.MAX_PERIOD;
		for (Actor actor: actors) {
			if (dMin > actor.getDeadline()) {
				dMin = actor.getDeadline();				
			}
		}
		return dMin;
	}

	/**
	 * Compute the gamma upper bound: (M-U)/(M-1). 
	 * <p>
	 * See Adnan Bouakaz thesis p. 42 (theorem 1.2).
	 * 
	 * @return Maximum gamma.
	 */
	double edfGammaMax() {
		return ((double) nbProcessor - edfU()) / (nbProcessor - 1);
	}

	/**
	 * Compute the gamma lower bound: max mu_i. 
	 * <p>
	 * See Adnan Bouakaz thesis p. 42 (mu* in theorem 1.2).
	 * 
	 * @return Minimum gamma.
	 */
	double edfGammaMin() {
		double gamma = 0;
		for (Actor actor: actors){
			double density = (double) actor.getWcet() / actor.getDeadline();
			if (density > gamma) {
				gamma = density;
			}
		}
		return gamma;
	}

	/**
	 * Compute L bound to ensure schedulability of
	 * constrained systems.
	 * <p>
	 * See Adnan Bouakaz thesis p. 42 (theorem 1.2).
	 * 
	 * @param gamma Gamma factor to be used.
	 * @return L metric using gamma in the
	 * denominator, homogeneous to the time unit.
	 */
	long edfL(double gamma) {
		double denominator = nbProcessor - ((nbProcessor-1) * gamma) - edfU();
		double sum = 0;
		for (Actor actor: actors) {
			sum += ((double) (actor.getWcet() * (actor.getPeriod() - actor.getDeadline()))) / actor.getPeriod();
		}
		return (long) Math.floor(sum / denominator);
	}

	/**
	 * Total processor demand during the time interval,
	 * applied on the instance actor set.
	 * <p>
	 * See Adnan Bouakaz thesis p.42 (h(t,gamma), theorem 1.2).
	 * 
	 * @param ffdbf ffdbf value tu use (typically 
	 * 	{@code edfFFDBF(t, gamma)}).
	 * @param gamma Gamma factor to be used.
	 * @return Processor demand of the task system,
	 * using gamma in the denominator,
	 * homogeneous to the time unit.
	 */
	long edfH(double ffdbf, double gamma) {
		double denominator = nbProcessor - ((nbProcessor-1) * gamma);
		return (long) Math.floor(ffdbf / denominator);
	}

	/**
	 * Compute the forced-forward demand bound.
	 * <p>
	 * See Adnan Bouakaz thesis p. 42.
	 * 
	 * @param timeInterval Time interval duration.
	 * @param gamma Gamma factor to be used.
	 * @return ffdbf value.
	 */
	double edfFFDBF(long timeInterval, double gamma) {
		double ffdbf = 0;
		for (Actor actor: actors) {
			ffdbf += (timeInterval / actor.getPeriod()) * actor.getWcet();
			long rest = timeInterval % actor.getPeriod();
			if (rest >= actor.getDeadline()) {
				ffdbf += actor.getWcet();
			} else if (rest < actor.getDeadline() && 
						rest >= (actor.getDeadline() - (actor.getWcet() / gamma))) {
				ffdbf += actor.getWcet() - ((actor.getDeadline() - rest) * gamma);
			}
		}
		return ffdbf;
	}

	/**
	 * Computes the F* metric applied on
	 * the instance actor set.
	 * <p>
	 * See Adnan Bouakaz thesis p. 111 (first paragraph).
	 * 
	 * @param gamma Gamma factor to be used.
	 * @return F* metric.
	 */
	double edfF(double gamma) {
		long sum = 0;
		for (Actor actor: actors) {
			sum += actor.getWcet();
		}
		double denominator = gamma * (nbProcessor-1); 
		double numerator = nbProcessor - denominator;
		return sum * numerator / denominator ;
	}

	/**
	 * Compute the maximum ratio among the 
	 * instance actor set of: WCET divided by the 
	 * deadline (which is expressed as a fraction
	 * of the unknown actor basis period).
	 * <p>
	 * See Adnan Bouakaz thesis p. 89 (scond paragraph)
	 * for the beta explanation. However this function
	 * itself is not referenced in the thesis.
	 * 
	 * @return Maximum ratio (among the actors) of:
	 * WCET divided by the symbolic deadline fraction value
	 * relative to the actor basis period.
	 */
	double edfWCEToverBetaMax() {
		double max = 0;
		for (Actor actor: actors) {
			Fraction deadline = actor.getSymbolicDeadline().setMUL(actor.getCoefficient()); 
			double temp = actor.getWcet() / deadline.getValue();
			if (temp > max) {
				max = temp;
			}
		}
		return max;
	}	

	/**
	 * Compute the response time of the actor i. 
	 * <p>
	 * The actor collection passed to the constructor is
	 * assumed to be be priority ordered.
	 * <p>
	 * See Adnan Bouakaz thesis p. 43 (equation 1.9).
	 * 
	 * @param actor Actor to consider in the 
	 * list of actors sorted by priorities.
	 * @return Response time of actor i.
	 */
	public long spResponseTime(Actor actor) {
		long respTime, nextRespTime = actor.getWcet();
		do {
			respTime = nextRespTime;
			nextRespTime = actor.getWcet();
			for (Actor actorTemp: actors) {
				if (actor.equals(actorTemp)) {
					break;
				}
				nextRespTime += (long) Math.ceil((double) respTime / actorTemp.getPeriod()) * actorTemp.getWcet();
			}
		} while (nextRespTime != respTime);
		return respTime;
	}

}
