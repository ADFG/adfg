/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.common_data_rep;

import java.util.logging.Logger;

import fr.inria.tea.adfg.algos.common_base.AdfgInternalException;
import fr.inria.tea.adfg.algos.common_base.ESchedAlgo;
import fr.inria.tea.adfg.algos.common_base.ESchedPolicy;
import fr.inria.tea.adfg.algos.maths.Pair;
import fr.inria.tea.adfg.algos.maths.UPIS;

/**
 * Compute buffer sizes.
 * 
 * @author Adnan Bouakaz
 * @author Alexandre Honorat
 */
public final class Buffer {

	private static final Logger LOGGER = Logger.getLogger("AdfgLog");
	
	private Channel edge;
	private Actor src, dest;
	private AffineRelation r;
	
	private UPIS prate, crate;
	private UPIS pClock, cClock;
	
	private boolean setSizes;
	private long result;
	private long lcm;
	
	/**
	 * 
	 * @param edge Data flow channel between the actors..
	 * @param src Source actor.
	 * @param dest Destination actor.
	 * @param r AffineRelation between the actors.
	 */
	public Buffer(Channel edge, Actor src, Actor dest, AffineRelation r) {
		this.edge = edge;
		this.src = src;
		this.dest = dest;
		this.r = r;
		// get firing rates
		prate = edge.getProduce();
		crate = edge.getConsume();
		Pair<UPIS> encoding = r.binaryEncoding();
		Pair<UPIS> hyperEncoding = UPIS.hyperPeriod(encoding, prate, crate);
		// get firing clocks
		pClock = hyperEncoding.getFirst();
		cClock = hyperEncoding.getSecond();
		// init res
		result = 0;
		lcm = edge.tokenLcm;
	}
		
	/**
	 * Compute and set the buffer size and the initial token number of a channel
	 * in the data flow graph.
	 * <p>
	 * This method call should follow the object creation.
	 * 
	 * @param st Scheduling policy to use (need not to be the gpr one).
	 * @param setSizes If true enable to set the size and the initial
	 * token of the buffer, otherwise this method is without side effects.
	 * @param multipleLCM Whether or not init must be a multiple of edge lcm.
	 * @return Buffer size (if the channel has a preset size, it is not
	 * modified but the computed one is returned instead of the original
	 * one when {@code setSizes} is true).
	 */
	public long bufferSize(ESchedAlgo st, boolean setSizes, boolean multipleLCM) {
		this.setSizes = setSizes;
		if (st == ESchedAlgo.EDF_UNI || st == ESchedAlgo.EDF_UNI_PQPA || r.isNoAdjust() || 
			(Actor.onSameProcessor(src, dest) && (
					st == ESchedAlgo.EDF_MULT_MBS_ID ||
					st == ESchedAlgo.EDF_MULT_BF_SQPA_CD ||
					st == ESchedAlgo.EDF_MULT_BF_UF_ID ||
					st == ESchedAlgo.EDF_MULT_BF_PQPA))) {
			sizeEDF1(multipleLCM);
		} else if (!Actor.onSameProcessor(src, dest) || st == ESchedAlgo.GEDF_MULT) {
			sizeGlob(multipleLCM);
		} else if (st == ESchedAlgo.EDF_UNI_DA || st == ESchedAlgo.EDF_UNI_HDA || st == ESchedAlgo.EDF_MULT_MBS_HDA) {
			sizeEDF2(multipleLCM);
		} else if (st.schedulingPolicy() == ESchedPolicy.SP) { 
			sizeSP(multipleLCM);
		}
		return result;
	}

	/**
	 * Set properties, only if they were previously undefined.
	 * 
	 * @param initial Initial token needed on the channel.
	 * @param maxSize Maximum needed channel size.
	 */
	private void setEdge(long initial, long maxSize) {
		if (setSizes) {
			long oriInitial = edge.getInitial();
			if (oriInitial == Channel.UNDEFINED) {
				edge.setInitial(initial);
			} else if (initial > oriInitial) {
				LOGGER.warning("Unsafe assignement of <" + edge.getName() + "> delay ("
						+ oriInitial + "): smaller than expected (" + initial + ").");				
			}
			long oriSize = edge.getSize();
			if (oriSize == Channel.UNDEFINED) {
				edge.setSize(maxSize);
			} else if (maxSize > oriSize) {
				LOGGER.warning("Unsafe assignement of <" + edge.getName() + "> buffer size (" 
						+ oriSize + "): smaller than expected (" + maxSize + ").");
			}
			result = edge.getSize() * edge.getDataSize();
		} else {
			result = maxSize * edge.getDataSize();
		}
	}
	

	/**
	 * Normalize the underflow size into a number of initial tokens.
	 * 
	 * @param rawUnderflowSize Minimum measured underflow (may be negative).
	 * @param multipleLCM If {@code true}, then take the closest integer above the number of token,
	 * being a multiple of {@link #lcm}. 
	 * @return A positive number of initial token, or 0 if there was no underflow (if {@code rawUnderflowSize} is positive).
	 */
	private long normalizeInitSize(long rawUnderflowSize, boolean multipleLCM) {
		final long absoluteSize = Math.abs(Math.min(0L, rawUnderflowSize));
		if (multipleLCM && absoluteSize != 0L) {
			long ceilDiv = (absoluteSize + lcm - 1L) / lcm;
			return ceilDiv*lcm;
		}
		return absoluteSize;
	}
	
	
	/**
	 * Code snippet returning clock tick.
	 * <p>
	 * {@code nb} will be decreased until an activation is found,
	 * i.e. such that {@code clock[nb+1] == 1}.
	 * If no preceding activation is found (or if it is the first one),
	 * it will return -1.
	 * 
	 * @param nb Current clock tick.
	 * @param clock Clock to use as activation condition.
	 * @return Final value of {@code nb}, in the range [-1,nb-1].
	 */
	private int getBackBeforePreviousClockActivation(int nb, UPIS clock) {
		int temp = nb;
		while (temp >= 0) {
			if (clock.elementAt(temp) == 1) {
				--temp;
				break;
			} else {
				--temp;
			}
		}
		return temp;
	}
	
	/**
	 * Compute minimum/maximum size of buffer for EDF1.
	 * 
	 * @param init Initial buffer size.
	 * @param isUnderflow Whether we compute underflow or overflow problem.
	 * @param produce Production activation clock.
	 * @param consume Consumption activation clock.
	 * @return The size of the buffer (min if underflow, max otherwise).
	 */
	private long internEDF1(long init, boolean isUnderflow, UPIS produce, UPIS consume) {
		int c = 0;
		long size = init;
		UPIS lClock = isUnderflow ? cClock : pClock;
		UPIS rClock = isUnderflow ? pClock : cClock;
		while (c < lClock.getLength()) {
			if (lClock.elementAt(c) == 1) {
				int p = 0;
				// clocks are only 0 and 1, so no need for long (for now)
				int lClockNext = (int) lClock.elementAt(c+1);
				int rClockNext = (int) rClock.elementAt(c+1);
				if (rClock.elementAt(c) == 1) {
					if (lClockNext == 0 && rClockNext == 1) { 
						p = c;
					} else if (lClockNext == 1 && rClockNext == 0) {
						p = c-1;
					} else if (lClockNext == 1 && rClockNext == 1) {
						p = ((src.getID() <= dest.getID()) ^ isUnderflow) ? c : c-1;
					}	
				} else {
					/* only if (lClockNext != 0 || rClockNext != 0), which is
					 * always true in such an hyperencoding
					 */
					p = c-1; 
					if (lClockNext == 1 && rClockNext == 0) { 
						p = getBackBeforePreviousClockActivation(p, rClock);
					}
				}
				long inBuffer = produce.getCumulative(isUnderflow ? p : c) 
								- consume.getCumulative(isUnderflow ? c : p);
				if (isUnderflow && size > inBuffer || !isUnderflow && size < (inBuffer+init)) {
					size = inBuffer+init;
				}
			}
			++c;
		}
		return size;
	}
	
	/**
	 * Implicit deadlines uniprocessor case.
	 * @param multipleLCM Whether or not init must be a multiple of edge lcm.
	 */
	private void sizeEDF1(boolean multipleLCM) {
		UPIS produce = pClock.distributOver(prate);
		UPIS consume = cClock.distributOver(crate);
		/* underflow analysis */
		long initial = normalizeInitSize(internEDF1(0, true, produce, consume), multipleLCM); 

		/* overflow analysis */
		long maxSize = internEDF1(initial, false, produce, consume);
		setEdge(initial, maxSize);
	}

	
	/**
	 * If global scheduling or not on the same processor.
	 * @param multipleLCM Whether or not init must be a multiple of edge lcm.
	 */
	private void sizeGlob(boolean multipleLCM) {
		UPIS produce = pClock.distributOver(prate); 
		UPIS consume = cClock.distributOver(crate);
		/* edge crossing processors */
		long minSize = 0;
		int c = 0;
		while (c < cClock.getLength()) {
			if (cClock.elementAt(c) == 1) {
				int p = c-1;
				if (pClock.elementAt(c) != 1) {
					p = getBackBeforePreviousClockActivation(p, pClock);
				}
				long inBuffer = produce.getCumulative(p) - consume.getCumulative(c);
				if (minSize > inBuffer) {
					minSize = inBuffer;
				}
			}
			++c;
		}
		long initial = normalizeInitSize(minSize, multipleLCM); 
		long maxSize = initial;
		int p = 0;
		while (p < pClock.getLength()) {
			if (pClock.elementAt(p) == 1) {
				c = p-1;
				if (cClock.elementAt(p) != 1) { 
					c = getBackBeforePreviousClockActivation(c, cClock);
				}
				long inBuffer = initial + produce.getCumulative(p) - consume.getCumulative(c);
				if (maxSize < inBuffer) {
					maxSize = inBuffer;
				}
			}
			++p;
		}
		setEdge(initial, maxSize);
	}
	
	/**
	 * Basic elements and operations for the EDF2
	 * buffer size computation model without deadlines.
	 * 
	 * @author Alexandre Honorat
	 */
	private class InternEDF2 {
		private long minSize = 0, maxSize = 0;
		private long inBuffer = 0;
		int p = 0, c = 0;
		
		private void increaseP() {
			++p;
			inBuffer += prate.elementAt(p);
			if (maxSize < inBuffer) {
				maxSize = inBuffer;
			}
		}
		
		private void increaseC() {
			++c;
			inBuffer -= crate.elementAt(c);
			if (minSize > inBuffer) {
				minSize = inBuffer;			
			}
		}
		
	}
	
	/**
	 * Constrained deadlines case.
	 * @param multipleLCM Whether or not init must be a multiple of edge lcm.
	 */
	private void sizeEDF2(boolean multipleLCM) {
		if (src.getDeadline() == Actor.UNDEFINED && dest.getDeadline() == Actor.UNDEFINED) {
			InternEDF2 bufferDatas = new InternEDF2();
			int i = 0;
			while (i < cClock.getLength()) { 
				// clocks are only 0 and 1, so no need for long (for now)
				int pClockNow = (int) pClock.elementAt(i);
				int cClockNow = (int) cClock.elementAt(i);
				if (pClockNow == 1 && cClockNow == 0) {
					bufferDatas.increaseP();
				} else if (pClockNow == 0 && cClockNow == 1) {
					bufferDatas.increaseC();
				} else { // cClockNow == pClockNow == 1
					int pClockNext = (int) pClock.elementAt(i+1);
					int cClockNext = (int) cClock.elementAt(i+1);
					if (pClockNext == 1 && cClockNext == 1) {
						if (src.getID() > dest.getID()) {
							bufferDatas.increaseP();
							bufferDatas.increaseC();
						} else {
							bufferDatas.increaseC();
							bufferDatas.increaseP();
						}
					} else if (pClockNext == 1 && cClockNext == 0) {
						bufferDatas.increaseP();
						bufferDatas.increaseC();
					} else {
						bufferDatas.increaseC();
						bufferDatas.increaseP();
					}
				}
				++i;
			}
			long initToken = normalizeInitSize(bufferDatas.minSize, multipleLCM); 
			setEdge(initToken, bufferDatas.maxSize + initToken);
		} else { 
			long rp = src.getPhase(), rc = dest.getPhase();
			int nbjobp = (int) pClock.getSum(), nbjobc = (int) cClock.getSum(), p = 0, c = 0;
			long minSize = 0, maxSize = 0, inBuffer = 0;
			while (p < nbjobp && c < nbjobc) { 
				boolean producerFirst = true;
				if (rp < rc) {
					if (rp + src.getFiringDeadline(p) > rc + dest.getFiringDeadline(c)) {
						LOGGER.severe("1: preemption occurs on channel [p_"+src.getID()+"--> p_"+dest.getID()+"]");
					}
				} else if (rp == rc) {
					if (src.getFiringDeadline(p) > dest.getFiringDeadline(c) || 
						(src.getFiringDeadline(p) == dest.getFiringDeadline(c) && src.getID() < dest.getID())) {
						producerFirst = false;
					}
				} else {
					if (rc + dest.getFiringDeadline(c) > rp + src.getFiringDeadline(p)) { 
						LOGGER.severe("2: preemption occurs on channel [p_"+src.getID()+"--> p_"+dest.getID()+"]");
					}
					producerFirst = false;
				}
				if (producerFirst) {
					inBuffer += prate.elementAt(p); 
					if (maxSize < inBuffer) {
						maxSize = inBuffer;
					}
					++p;
					rc -= rp; 
					rp = src.getPeriod();
				} else {
					inBuffer -= crate.elementAt(c); 
					if (minSize > inBuffer) {
						minSize = inBuffer;
					}
					++c;
					rp -= rc; 
					rc = dest.getPeriod();
				}
			}
			if (p != nbjobp && c != nbjobc) {
				throw new AdfgInternalException("Buffer computation error.");
			}
			long initToken = normalizeInitSize(minSize, multipleLCM); 
			setEdge(initToken, maxSize + initToken);
		}		
	}

	/**
	 * If static priority scheduling.
	 * @param multipleLCM Whether or not init must be a multiple of edge lcm.
	 */
	private void sizeSP(boolean multipleLCM) {
		UPIS produce = pClock.distributOver(prate); 
		UPIS consume = cClock.distributOver(crate);
		/* underflow analysis */
		long minSize = 0;
		int c = 0, p;
		while (c < cClock.getLength()) {
			if (cClock.elementAt(c) == 1) {
				if (pClock.elementAt(c) == 1) {
					p = (src.getPriority() < dest.getPriority()) ? c : c-1;
				} else {
					p = c-1;
					if (src.getPriority() >= dest.getPriority()) {
						p = getBackBeforePreviousClockActivation(p, pClock);
					}
				}
				long inBuffer = produce.getCumulative(p) - consume.getCumulative(c);
				if (minSize > inBuffer) {
					minSize = inBuffer;
				}
			}
			++c;
		}
		long initial = normalizeInitSize(minSize, multipleLCM);

		/* overflow analysis */
		long maxSize = initial;
		p = 0;
		while (p < pClock.getLength()) {
			if (pClock.elementAt(p) == 1) {
				if (cClock.elementAt(p) == 1) {
					c = (src.getPriority() < dest.getPriority()) ? p-1 : p;
				} else {
					c = p-1;
					if (src.getPriority() < dest.getPriority()) {
						c = getBackBeforePreviousClockActivation(c, cClock);
					}
				}
				long inBuffer = initial + produce.getCumulative(p) - consume.getCumulative(c);
				if (maxSize < inBuffer) {
					maxSize = inBuffer;
				}
			}
			++p;
		}
		setEdge(initial, maxSize);
	}
	
}
