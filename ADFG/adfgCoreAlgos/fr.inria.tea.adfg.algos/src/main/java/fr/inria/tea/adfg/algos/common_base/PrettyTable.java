/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.common_base;

import de.vandermeer.asciitable.AsciiTable;
import de.vandermeer.asciitable.CWC_LongestLine;
import de.vandermeer.asciithemes.a7.A7_Grids;

/**
 * This class is intend to not be instantiated.
 * The provided renderer is a singleton.
 * 
 * @author Alexandre Honorat
 */
public class PrettyTable {


	/**
	 * Do nothing.
	 */
	private PrettyTable() {
	}
	
	
	/**
	 * AsciiTable creator, preset to have a pretty rendering.
	 * 
	 * @return An AsciiTable, each column being
	 * as width as the longest line in it.
	 */
	public static AsciiTable create() {
		AsciiTable at = new MyAsciiTable();
		at.getRenderer().setCWC(new CWC_LongestLine());
		at.getContext().setGrid(A7_Grids.minusBarPlusEquals());
		// old version
		// tr. setTheme(V2_E_TableThemes.PLAIN_7BIT_STRONG.get());
		return at;
	}
		

	public static class MyAsciiTable extends AsciiTable {
		
		public String render() {
			setPaddingLeftRight(1);
			return super.render();
		}
		
	}

	
}
