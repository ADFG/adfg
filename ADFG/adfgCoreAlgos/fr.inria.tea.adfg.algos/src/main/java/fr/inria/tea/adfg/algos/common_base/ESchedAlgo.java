/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.common_base;

/**
 * List the implemented scheduling algorithms.
 * First enum name part is SP (static priority) or
 * EDF (earliest deadline first). Second part is
 * UNI (uni-processor) or MULT (multi-processor).
 * 
 * @author Alexandre Honorat
 */
public enum ESchedAlgo {

	EDF_UNI("0: uniprocessor EDF", ESchedPolicy.EDF, false, false, true, true, false),
	EDF_UNI_DA("1: uniprocessor EDF with deadline adjustment", ESchedPolicy.EDF, false, false, true, true, false),
	EDF_UNI_HDA("2: uniprocessor EDF with hybrid deadline adjustment", ESchedPolicy.EDF, false, false, true, true, false),
	EDF_MULT_MBS_HDA("3: partitioned EDF multiprocessor with hybrid deadline adjustment "
			+ "M-balanced partitioning (Scotch)", ESchedPolicy.EDF, true, false, true, false, true),
	EDF_MULT_MBS_ID("4: partitioned EDF multiprocessor with impicit deadlines: "
			+ "M-balanced partitioning (Scotch)", ESchedPolicy.EDF, true, false, true, false, true),
	EDF_MULT_BF_UF_ID("5: best fit partitioned EDF (utilization factor) with implicit deadlines "
			+ "(throughput maximization)", ESchedPolicy.EDF, true, false, true, false, false),
	EDF_MULT_BF_SQPA_CD("6: (!sort of new!) best fit partitioned EDF (SQPA) with constrained deadlines "
			+ "(throughput maximization)", ESchedPolicy.EDF, true, false, true, false, false),
	GEDF_MULT("7: global EDF (FFDBF-SQPA)", ESchedPolicy.EDF, true, true, true, false, false),
	EDF_UNI_PQPA("x: (!new!) uniprocessor EDF with modified PQPA schedulability test.",
			ESchedPolicy.EDF, false, false, true, true, false),
	EDF_MULT_BF_PQPA("x: (!new!) best fit partitioned EDF multiprocessor with modified PQPA schedulability test.",
			ESchedPolicy.EDF, true, false, true, true, false),

	SP_UNI("10: uniprocessor static-priority (deadline monotonic) scheduling", 
			ESchedPolicy.SP, false, false, true, false, false),
	SP_UNI_LOP("11: uniprocessor static-priority (deadline monotonic) scheduling: "
			+ "LOP priority assignment", ESchedPolicy.SP, false, false, true, false, false),
	SP_MULT_MBS("12: multiprocessor static-priority (deadline monotonic) scheduling: "
			+ "M-balanced partitioning (Scotch)", ESchedPolicy.SP, true, false, true, false, true),
	SP_MULT_BF_FBBFFD("13: multiprocessor static-priority (deadline monotonic) scheduling: "
			+ "Best fit Fisher Baruah Baker First Fit Deacreasing partitioning", 
			ESchedPolicy.SP, true, false, true, false, false),
	SP_MULT_BF_SRTA("14: multiprocessor static-priority (deadline monotonic) scheduling: "
			+ "Best fit SRTA partitioning", ESchedPolicy.SP, true, false, true, false, false),
	SP_UNI_UDH("15: (!completed!) uniprocessor static-priority (deadline monotonic) scheduling: "
			+ "Utilization Distance Heuristic assignment", ESchedPolicy.SP, false, false, true, false, false),

	TT_MC_RTA("16: time triggered synthesis for many core architectures (non preemptive) using RTA analysis",
			ESchedPolicy.TT, true, false, false, false, false);


	private final String fullName;
	private final ESchedPolicy schedulingPolicy;
	private final boolean isMultiprocessor;
	private final boolean isGlobal;
	private final boolean isPreemptive;
	private final boolean allowsWCPs;
	private final boolean implementsStrategies;


	/**
	 * Construct a new scheduling type.
	 * 
	 * @param scheduleDesc Message describing the scheduling.
	 * @param schedPolicy The scheduling policy used in the targeted system.
	 * @param isMult True if schedule policy can be used on multi-processor,
	 * false otherwise.
	 * @param isGlob True if only one scheduler for all processors, false
	 * otherwise (false if only one processor).
	 * @param isPreempt True if preemptive scheduling, false otherwise.
	 * @param wcps True if scheduling policy allow to have several 
	 * weakly connected components in the data flow graph, false otherwise.
	 * @param haveStrat True if scheduling policy can adapt itself to an
	 * user specified strategy (of type ETradeOffType), false otherwise.
	 */
	ESchedAlgo(String scheduleDesc, ESchedPolicy schedPolicy, 
			boolean isMult, boolean isGlob, boolean isPreempt, boolean wcps, boolean haveStrat) { 
		fullName = scheduleDesc;
		schedulingPolicy = schedPolicy;
		isMultiprocessor = isMult;
		isGlobal = isGlob;
		isPreemptive = isPreempt;
		allowsWCPs = wcps;
		implementsStrategies = haveStrat;
	}

	/**
	 * @return The scheduling description.
	 */
	public String getFullName() { 
		return fullName; 
	}

	/**
	 * @return The scheduling policy assumed for this
	 * algorithm: EDF, SP or TT.
	 */
	public ESchedPolicy schedulingPolicy() { 
		return schedulingPolicy; 
	}

	/**
	 * @return True if the schedule policy can be used on multi-processor,
	 * false otherwise.
	 */
	public boolean isMultiprocessor() { 
		return isMultiprocessor; 
	}

	/**
	 * @return True if only one scheduler for all processors, false 
	 * otherwise (false if only one processor).
	 */
	public boolean isGlobal() { 
		return isGlobal; 
	}

	/**
	 * @return True if preemption may occur in the synthesized schedule,
	 * false otherwise (so if the algorithm ensures there will be no preemption).
	 */
	public boolean isPreemptive() {
		return isPreemptive;
	}

	/**
	 * @return True if the schedule type can be used on several 
	 * weakly connected subgraphs, false otherwise.
	 */
	public boolean allowsWCPs() {
		return allowsWCPs;
	}

	/**
	 * 
	 * @return True if scheduling policy can adapt itself to an
	 * user specified strategy (of type ETradeOffType), false otherwise.
	 */
	public boolean implementsStrategies() {
		return implementsStrategies;
	}

}


