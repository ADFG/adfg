/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.common_data_rep;

/**
 * User methods for actors.
 * <p>
 * Unlike for channels, period are always computed by the
 * tool even if user provided; that's why there is a two 
 * different period getters: one for the computed period,
 * and one for the user period.
 * 
 * @author Alexandre Honorat
 */
public interface IActor {

	/**
	 * Get the actor's period.
	 * 
	 * @return Period.
	 */
	long getPeriod();

	/**
	 * Get the phase.
	 * 
	 * @return Phase.
	 */
	long getPhase();

	/**
	 * Get the worst case execution time
	 * (it is the same for all task/job firing).
	 * No unit.
	 * 
	 * @return Actor's WCET.
	 */
	long getWcet();

	/**
	 * Get the original user defined period.
	 * No unit.
	 * 
	 * @return User period if defined,
	 * a negative value otherwise.
	 */
	long getUserPeriod();

	/**
	 * Get the deadline of a specific firing.
	 * 
	 * @param i Firing index.
	 * @return Deadline of the {@code i}-th job firing.
	 */
	long getFiringDeadline(int i);

	/**
	 * Get the actor's name.
	 * 
	 * @return Actor's name.
	 */
	String getName();

	/**
	 * Get the current deadline value.
	 * 
	 * @return Deadline.
	 */
	long getDeadline();

	/**
	 * Get the partition (identified by a number)
	 * which contains the actor.
	 * 
	 * @return Partition identifier, or -1 if not defined yet.
	 */
	int getPartitionID();

	/**
	 * Get actor's priority in the current
	 * scheduling.
	 * 
	 * @return Priority.
	 */
	int getPriority();

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	String toString();

}
