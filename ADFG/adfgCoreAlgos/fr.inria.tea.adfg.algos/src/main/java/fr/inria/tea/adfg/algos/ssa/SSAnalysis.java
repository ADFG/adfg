/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.ssa;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Logger;

import de.vandermeer.asciitable.AsciiTable;
import fr.inria.tea.adfg.algos.common_base.AdfgInternalException;
import fr.inria.tea.adfg.algos.common_base.ESchedAlgo;
import fr.inria.tea.adfg.algos.common_base.PrettyTable;
import fr.inria.tea.adfg.algos.common_data_rep.Actor;
import fr.inria.tea.adfg.algos.common_data_rep.FiringDeadlines;
import fr.inria.tea.adfg.algos.common_data_rep.GraphProbData;
import fr.inria.tea.adfg.algos.common_data_rep.Partition;
import fr.inria.tea.adfg.algos.common_data_rep.WeaklyConnectedComponent;
import fr.inria.tea.adfg.algos.maths.BasicArithmetic;
import fr.inria.tea.adfg.algos.maths.Fraction;
import fr.inria.tea.adfg.algos.ssa.ILPSymbolicSchedulability.Objective;


/**
 * Main class for the symbolic schedulability analysis.
 * Compute initial and maximum number of tokens on every channel.
 * 
 * @author Adnan Bouakaz
 * @author Alexandre Honorat
 */
public class SSAnalysis {

	private static final Logger LOGGER = Logger.getLogger("AdfgLog");	 

	public static final double uncomputedUtilizationFactor = -1.0;
	public static final double incoherentUtilizationFactor = -2.0;
	
	
    private Set<WeaklyConnectedComponent> weaklyConnectedComponents;
    private Map<Integer, Partition> partitions;
    private ESchedAlgo scheduleType;
    private GraphProbData gpr;
    
    private double globalUtilizationFactor;
    
    /**
     * Create a SSAnalysis instance which can be solved.
     * 
     * @param gpr Problem representation to work on.
     */
    public SSAnalysis(GraphProbData gpr) {
    	weaklyConnectedComponents = gpr.getWeaklyConnectedComponents();
    	partitions = gpr.getMappedPartitions();
    	scheduleType = gpr.getScheduleType();
    	this.gpr = gpr;
    	
    	globalUtilizationFactor = uncomputedUtilizationFactor;
    }

	/**
	 * Solve the problem regarding to its scheduling policy.
	 */
	public void solving() {
		LOGGER.info("Entering:\n========== Symbolic Schedulability Analysis ==========");
		
		gpr.generateILPforSSA();
		switch (scheduleType) {
		case EDF_UNI: 
			globalUtilizationFactor = ssaImplicitDeadlines(); 
			break;
		case EDF_UNI_DA: case EDF_UNI_HDA: case EDF_MULT_MBS_HDA:
			globalUtilizationFactor = ssaRWPrecedences();  
			break;
		case EDF_MULT_MBS_ID: case EDF_MULT_BF_UF_ID:
			globalUtilizationFactor = ssaImplicitDeadlinesMultiprocessor(); 
			break;
		case EDF_MULT_BF_SQPA_CD:
			globalUtilizationFactor = ssaConstrainedDeadlinesMultiprocessor();
			break;
		case GEDF_MULT: 
			globalUtilizationFactor = ssaGlobalEDF(); 
			break;
		case EDF_UNI_PQPA:
			globalUtilizationFactor = ssaPermissiveEDF(true);
			break;
		case EDF_MULT_BF_PQPA:
			globalUtilizationFactor = ssaPermissiveEDF(false);
			break;
		case SP_UNI: case SP_UNI_LOP: case SP_UNI_UDH:
		case SP_MULT_MBS: case SP_MULT_BF_FBBFFD: case SP_MULT_BF_SRTA:
			globalUtilizationFactor = ssaSP(); 
			break;
		case TT_MC_RTA:
			globalUtilizationFactor = ssaTT();
			break;
		default:
			throw new AdfgInternalException("Schedule type not recognized in SSA.");
		}
		if (globalUtilizationFactor < 0.0) {
			throw new AdfgInternalException("The task system is not schedulable (negative utilization factor).");
		}

		StringBuilder sb = new StringBuilder();
		sb.append("Processor utilization factor U = "+ globalUtilizationFactor + "\n");
		sb.append("details per weakly connected component:\n");
		for (WeaklyConnectedComponent comp: weaklyConnectedComponents) {
			long hyperperiod = 1;
			for (Actor a: comp.getActors()) {
				hyperperiod = BasicArithmetic.funcLCM(hyperperiod, a.getPeriod());
			}
			long repFactor = comp.getRepetitionFactorScale();
			sb.append("-- (basis = " + comp.getBasis().getName() + ") hyperperiod = " + hyperperiod + ", divFactor = " + comp.getDivFactor() + 
				  ", repFactor = " + repFactor + ", throughput = " + 1.0/hyperperiod +
				  ", throughput with repetition factor = " + 1.0/(hyperperiod*repFactor) + 
				  ", load = " + comp.getSymbolicLoad()/comp.getBasis().getPeriod() + 
				  ", avg cyclo-load = " + comp.getAvgCycloSymbolicLoad()/comp.getBasis().getPeriod() + ";\n");
		}
		sb.append("details per partition:\n");
		double totalAvgCycloLoad = 0;
		if (partitions.size() > 1 && scheduleType.isGlobal()) {
			throw new AdfgInternalException("Schedule policy is global but several partitions have been found, error.");
		}
		
		for (Entry<Integer,Partition> part: partitions.entrySet()) {
			Partition p = part.getValue();
			double load = p.getLoad();
			double avgCycloLoad = p.getAvgCycloLoad();
			totalAvgCycloLoad += avgCycloLoad;
			if (load > 1.0 && !scheduleType.isGlobal()) {
				double fracLoad = p.getFractionalLoad().getValue();
				if (fracLoad > 1.0) {
					throw new AdfgInternalException("Processor n°" + part.getKey() + 
							" has a utilization processor more than 1.0 (" + fracLoad + "), aborting.");
				}
				LOGGER.warning("Processor n°" + part.getKey() + " has a utilization processor more than 1.0 "
						+ "due to computation imprecision, more precise computation found " + fracLoad
						+ " so everything is fine.");
			}
			sb.append("-- proc n° " + part.getKey() + ", processor utilization = " + load + 
					", avg cyclo-processor utilization = " + avgCycloLoad + ";\n");
		}
		sb.append("Average cyclo-processor utilization factor avgCycloU = " + totalAvgCycloLoad + ";\n");
		
		AsciiTable atSSA = PrettyTable.create();
		atSSA.addRule();
		atSSA.addRow("actor", "period", "phase",  "deadline");
		atSSA.addStrongRule();
		for (Actor actor: gpr.getActors()) {
			atSSA.addRow(actor.getName(), "p_" + actor.getID() + " = " + actor.getPeriod(),
										  "r_" + actor.getID() + " = " + actor.getPhase(),
										  "d_" + actor.getID() + " = " + actor.getDeadline());
		}
		atSSA.addRule();
		LOGGER.info(sb.toString() + atSSA.render());
	}


	/**
	 * @return {@code uncomputedUtilizationFactor} if the solution hasn't been computed yet,
	 * {@code incoherentUtilizationFactor} if the computed solution is wrong (not in [0;1]),
	 * the global processor utilization factor otherwise.
	 */
	public double getGlobalUtilizationFactor() {
		return globalUtilizationFactor;
	}

	/**
	 * Helper to call the ILP solver, check the parameters and
	 * the result.
	 * 
	 * @param wcp WeaklyConnectedComponent to solve.
	 * @param objective Minimization/Maximization objective.
	 * @param periodMin Minimal period to find.
	 * @return The utilization factor if not negative.
	 */
	private double solveILP(WeaklyConnectedComponent wcp, Objective objective, long periodMin) {
		if (periodMin < 0) {
			throw new AdfgInternalException("The task system is not schedulable (negative best period).");
		}
		new ILPSymbolicSchedulability(wcp, objective, periodMin).solving();
		double utilizationFactor = wcp.getComponentUtilizationFactor();
		if (utilizationFactor < 0) {
			throw new AdfgInternalException("The task system is not schedulable (negative utilization factor).");
		}
		return utilizationFactor;
	}
	
	/**
	 * Solve the problem for uniprocessor EDF policy with implicit deadlines.
	 * <p>
	 * See the Adnan Bouakaz thesis p. 106 (first paragraph after point 3.3.2).
	 * More details can be found in the article "Affine Data-Flow Graphs for
	 * the Synthesis of Hard Real-Time Applications" (Algo.1 section IV)
	 * of A. Bouakaz, J.-P. Talpin and Jan Vitek.
	 * <p>
	 * This algorithm does not involve other schedulability test because
	 * the implcit deadlines case can be test only with the
	 * utilization factor (see Adnan Bouakaz thesis p. 39, equation 1.2).
	 * 
	 * @todo Adapt to use the average cyclo-WCET ?
	 * 
	 * @return {@code incoherentUtilizationFactor} if no solution, 
	 * global processor utilization factor otherwise.
	 */
	private double ssaImplicitDeadlines() { 
		double alpha = 1;
		/* ordered by divFactor decreasing */
		List<WeaklyConnectedComponent> orderedWCPs = new ArrayList<>();
		for (WeaklyConnectedComponent originalWCP: weaklyConnectedComponents) {
			int index = 0; 
			/* This loop will respect the order. */
			for (WeaklyConnectedComponent addedWCP: orderedWCPs) {
				if (addedWCP.getDivFactor() <= originalWCP.getDivFactor()) {
					break;
				}
				++index;
			}
			orderedWCPs.add(index, originalWCP); 
		}
		
		while (!orderedWCPs.isEmpty()) {
			double avgAlpha = alpha/orderedWCPs.size();
			boolean allWCPbelowAvgUF = true;
			/* We need an iterator to remove elements during the loop. */
			Iterator<WeaklyConnectedComponent> itc = orderedWCPs.iterator();
			while (itc.hasNext()) {
				WeaklyConnectedComponent wcp = itc.next();
				long period = Math.max(wcp.getPeriodLowerBound(), (long) Math.ceil(wcp.getSymbolicLoad() / avgAlpha));
				if (wcp.getPeriodUpperBound() < period) {
					/* this component contribute by more than avgAlpha */
					allWCPbelowAvgUF = false;
					alpha -= solveILP(wcp, Objective.MAXIMIZE_PERIOD, 0);
					/* unschedulable system */
					if (alpha < 0.0) {
						return incoherentUtilizationFactor; 
					}
					itc.remove();
				}
			}			
			/* all others weakly connected components can be satisfied for avgAlpha */
			if (allWCPbelowAvgUF) {
				break; 
			}
		}
		
		if (!orderedWCPs.isEmpty()) {
			int nb = orderedWCPs.size(); 
			for (WeaklyConnectedComponent wcp: orderedWCPs) {
				double avgAlpha = alpha / nb;
				long pinf = Math.max(wcp.getPeriodLowerBound(), (long) Math.ceil(wcp.getSymbolicLoad() / avgAlpha));
				long period = wcp.getDivFactor() * (long) Math.ceil(pinf / (wcp.getDivFactor()*avgAlpha));
				alpha -= solveILP(wcp, Objective.MINIMIZE_PERIOD, period);
				--nb;
			}
		}	
		return (1 - alpha);
	}

	/**
	 * Solve the problem for multiprocessor EDF policy with implicit deadlines,
	 * available for only one weakly connected component.
	 * <p>
	 * This algorithm is not described in the Adnan Bouakaz thesis.
	 * <p>
	 * This algorithm does not involve other schedulability test because
	 * the implcit deadlines case can be test only with the
	 * utilization factor (see Adnan Bouakaz thesis p. 39, equation 1.2).
	 * 
	 * @todo Adapt to use the average cyclo-WCET ?
	 * 
	 * @return {@code incoherentUtilizationFactor} if no solution, 
	 * global processor utilization factor otherwise.
	 */
	private double ssaImplicitDeadlinesMultiprocessor() {
		WeaklyConnectedComponent wcp = weaklyConnectedComponents.iterator().next();
		long divFactor = wcp.getDivFactor();
		long supBound = wcp.getPeriodUpperBound(), infBound = wcp.getPeriodLowerBound();
		long period =  0;
		for (Partition partition: partitions.values()) {
			long inf = Math.max(infBound, (long) Math.ceil(partition.getSymbolicLoad()));
			long tempT = divFactor * ((inf + divFactor - 1) / divFactor);
			if (tempT > supBound) {
				return incoherentUtilizationFactor;
			}
			period = Math.max(period, tempT);
		}
		return solveILP(wcp, Objective.MINIMIZE_PERIOD, period);
	}
	
	/**
	 * Solve the problem for EDF scheduling wih deadline adjustment,
	 * fusion weakly connected components if more than one.
	 * <p>
	 * See Adnan Bouakaz thesis p. 42 point C for the precedences
	 * explanations, and p. 102-106 point 3.3.1 for more details.
	 * However the whole algorithm is not described.
	 * 
	 * @todo 
	 * {@code deadlineCalculus} returns always true, 
	 * so when call {@code ssaImplicitDeadlines}?
	 * When stabilization is not reached after a given number of iterations?
	 * 
	 * @return {@code incoherentUtilizationFactor} if no solution, 
	 * global processor utilization factor otherwise.
	 */
	private double ssaRWPrecedences() {
		/* fusion of weakly connected components */
		Iterator<WeaklyConnectedComponent> it = weaklyConnectedComponents.iterator();
		WeaklyConnectedComponent wcp = new WeaklyConnectedComponent(it.next());
		while (it.hasNext()) {
			WeaklyConnectedComponent wcpTemp = it.next();
			Fraction factor = new Fraction(wcpTemp.getSymbolicLoad() / wcp.getSymbolicLoad());
			wcp.fusion(wcpTemp, factor);
		}
		wcp.updatePLBwithNbPartitions(false);
		if (!deadlineCalculus(wcp)) { 
			return ssaImplicitDeadlines();
		} else {
			SchedulingOracleEDF oracle = new SchedulingOracleEDF(gpr, false);
			long period = SchedulingOracleEDF.incoherentPeriod;
			if (scheduleType.isMultiprocessor()) {
				period = oracle.standardPSQPA();
			} else {
				period = oracle.standardSQPA(partitions.get(0));
			}				
			return solveILP(wcp, Objective.BASIS_PERIOD, period);
		}
	}
	
	/**
	 * Solve the problem for multiprocessor EDF policy
	 * with constrained deadlines,
	 * available for only one weakly connected component.
	 * 
	 * @todo
	 * Link to the thesis. Why specifically constrained deadlines?
	 * 
	 * @return {@code incoherentUtilizationFactor} if no solution, 
	 * global processor utilization factor otherwise.
	 */
	private double ssaConstrainedDeadlinesMultiprocessor() {
		WeaklyConnectedComponent wcp = weaklyConnectedComponents.iterator().next();
		SchedulingOracleEDF oracle = new SchedulingOracleEDF(gpr, false);
		return solveILP(wcp, Objective.BASIS_PERIOD, oracle.standardPSQPA());
	}

	/**
	 * Solve the problem for multiprocessor EDF policy,
	 * with the ffdbf SQPA method,
	 * available for only one weakly connected component.
	 * <p>
	 * See Adnan Bouakaz thesis p. 113 (paragraph above Lemma 3.4).
	 * 
	 * @return {@code incoherentUtilizationFactor} if no solution, 
	 * global processor utilization factor otherwise.
	 */
	private double ssaGlobalEDF() {
		WeaklyConnectedComponent wcp = weaklyConnectedComponents.iterator().next();
		SchedulingOracleEDF oracle = new SchedulingOracleEDF(gpr, true);
		return solveILP(wcp, Objective.BASIS_PERIOD, oracle.ffdbfSQPA(wcp));
	}
	
	private double ssaPermissiveEDF(boolean mustComputeAgain) {
		if (mustComputeAgain) {
			SchedulingOracleEDF oracle = new SchedulingOracleEDF(gpr, false);
			long[] periods = oracle.modifiedPQPA();
			if (periods == null) {
				return incoherentUtilizationFactor;
			}
		}
		double utilizationFactor = 0.0;
		for (WeaklyConnectedComponent wcp: weaklyConnectedComponents) {
			utilizationFactor += solveILP(wcp, Objective.BASIS_PERIOD, wcp.getBasis().getPeriod());
		}
		return utilizationFactor;
	}
	
	/**
	 * Solve the problem for TT_MC_RTA.
	 * 
	 * @todo
	 * Code this method.
	 * 
	 * @return {@code incoherentUtilizationFactor} if no solution, 
	 * global processor utilization factor otherwise.
	 */
	private double ssaTT() {
		SchedulingOracleTT oracle = new SchedulingOracleTT(gpr, false);
		oracle.experimentalMethod();
		return uncomputedUtilizationFactor;
	}
	
	/**
	 * Solve the problem for static priority policies,
	 * available for only one weakly connected component.
	 * <p>
	 * There is no specific description in the Adnan Bouakaz
	 * thesis, however the static priorities schedulability tests
	 * are depicted in section 3.2 p. 90-102.
	 * 
	 * @return {@code incoherentUtilizationFactor} if no solution, 
	 * global processor utilization factor otherwise.
	 */
	private double ssaSP() {
		WeaklyConnectedComponent wcp = weaklyConnectedComponents.iterator().next();
		SchedulingOracleSP oracle = new SchedulingOracleSP(gpr, false);
		long period = 0;
		if (scheduleType.isMultiprocessor()) {
			period = oracle.standardPSRTA();
		} else {
		    period = oracle.standardSRTA(partitions.get(0), 0L, Actor.MAX_PERIOD);
		}
		return solveILP(wcp, Objective.BASIS_PERIOD, period);
	}

	/**
	 * Set the symbolic deadlines.
	 * <p>
	 * See Adnan Bouakaz thesis p. 42 point C for the precedences
	 * explanations, and p. 102-106 point 3.3.1 for more details.
	 * However the whole algorithm is not described.
	 * 
	 * @todo
	 * Remove commented code?
	 * 
	 * @param wcp WeaklyConnectedComponent to work on.
	 * @return False if there was no need for deadline adjustment, 
	 * true otherwise.
	 */
	private boolean deadlineCalculus(WeaklyConnectedComponent wcp) {		
		for (Actor actor: wcp.getActors()) {
			FiringDeadlines firingDeadlines = new FiringDeadlines(gpr, wcp, actor);
			actor.setFiringDeadlines(firingDeadlines);
		}

		boolean stable = false;
		while (!stable) {
			stable = true;
			for (Actor actor: wcp.getActors()) {
				boolean change = actor.getFiringDeadlines().update();
				/* unify the deadlines: take the minimum */
				//change=A.getSymbolicDeadlines().updateMinimum(); 
				/* used in case we compute the approximate deadlines using fixed point computation */
				if (change) {
					stable = false;
				}
			}
		}

		/* set the minimum symbolic deadline */
		boolean warningDependencies = false;
		for (Actor actor: wcp.getActors()) {
			actor.setSymbolicDeadline(actor.getFiringDeadlines().getMinimum());
			/* the deadline must be greater than the worst-case execution time */
			Fraction coef = actor.getCoefficient();
			long period = (long) Math.ceil((double) actor.getWcet() / 
					(actor.getSymbolicDeadline().setMUL(coef)).getValue());
			if (period > wcp.getPeriodLowerBound()) {
				wcp.setPeriodLowerBound(period);
				warningDependencies = true;
			}
		}
		if (warningDependencies) {
			LOGGER.warning("Some deadline dependencies jeopradize the throughput.");
		}
		
		StringBuilder sb = new StringBuilder("Deadlines infos:\n");
		sb.append("========== Deadlines adjustment ==========\n");
		for (Actor actor: wcp.getActors()) {
			sb.append("d_" + actor.getID() + " = " + actor.getSymbolicDeadline() +
					" p_" + actor.getID() + ";\n");
		}
		LOGGER.fine(sb.toString());
		return true;
	}
	
}
