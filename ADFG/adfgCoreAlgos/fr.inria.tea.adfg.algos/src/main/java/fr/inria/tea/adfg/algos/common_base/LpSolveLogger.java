/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.common_base;

import lpsolve.LogListener;
import lpsolve.LpSolve;
import lpsolve.LpSolveException;

/**
 * This class report messages of LpSolve into a StrinBuilder.
 * 
 * @author Alexandre Honorat
 */
public class LpSolveLogger implements LogListener {

	/**
	 * Do nothing.
	 */
	public LpSolveLogger() {
	}

	/**
	 * arg1 is the StringBuilder to write..
	 */
	@Override
	public void logfunc(LpSolve arg0, Object arg1, String arg2) throws LpSolveException {
		if (arg1 != null && arg1 instanceof StringBuilder) {
			((StringBuilder) arg1).append(arg2);
		}
		
	}

}
