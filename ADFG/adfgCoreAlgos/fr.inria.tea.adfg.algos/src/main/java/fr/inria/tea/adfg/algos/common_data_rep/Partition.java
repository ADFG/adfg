/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.common_data_rep;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.inria.tea.adfg.algos.maths.Fraction;

/**
 * This represents a partition over the different
 * processors.
 * <p>
 * There is a cyclic definition with actors.
 * 
 * @author Alexandre Honorat
 */
public class Partition {
	
	
	int partitionID;
	int nbActors;
	Set<Actor> actors;
	List<Actor> priorityOrderedActors;
	double symbolicLoad;
	double avgCycloSymbolicLoad;
	
	/**
	 * The map is not used yet, remove it if not used in some months...
	 */
	Map<WeaklyConnectedComponent, Set<Actor>> wcpsActors;
	
	
	
	/**
	 * Creates a new empty partition.
	 * 
	 * @param partitionID Partition index.
	 */
	public Partition(int partitionID) {
		this.partitionID = partitionID;
		nbActors = 0;
		wcpsActors = new HashMap<>();
		actors = new HashSet<>();
		priorityOrderedActors = new ArrayList<>();
		symbolicLoad = 0;
		avgCycloSymbolicLoad = 0;
	}

	/**
	 * Add an actor to the partition.
	 * <p>
	 * This operation automatically set the
	 * actor partitionID.
	 * 
	 * @param actor Element to add.
	 * @param addInPriorityList Whether or not the actor
	 * must be added in the priority list. If {@code true}, the
	 * actor is assumed to have a weaker priority than all
	 * previously added actors in this partition, so it is added
	 * to the end.
	 */
	public synchronized void addActor(Actor actor, boolean addInPriorityList) {
		WeaklyConnectedComponent wcp = actor.getWCPcontainer();
		Set<Actor> set = null;
		if (!wcpsActors.containsKey(wcp)) {
			set = new HashSet<>();
			wcpsActors.put(wcp, set);
		} else {
			set = wcpsActors.get(wcp);
		}
		set.add(actor);
		actors.add(actor);
		actor.setPartitionID(partitionID);
		if (addInPriorityList) {
			priorityOrderedActors.add(actor);
		}
		symbolicLoad += actor.getSymbolicLoad();
		avgCycloSymbolicLoad += actor.getAvgCycloSymbolicLoad();
		++nbActors;
	}
	
	/**
	 * Remove an actor from the partition.
	 * <p>
	 * PartitionID of the actor must have been set.
	 * 
	 * @param actor Actor to remove.
	 * @param removeFromPriorityList Whether or not the actor
	 * must be removed from the priority list. If true, the
	 * actor is assumed to have a weaker priority than all
	 * previously added actors in this partition, so it is removed
	 * from the end.
	 */
	public synchronized void removeActor(Actor actor, boolean removeFromPriorityList) {
		if (actors.contains(actor)) {
			WeaklyConnectedComponent wcp = actor.getWCPcontainer();
			Set<Actor> wcpActors = wcpsActors.get(wcp);
			wcpActors.remove(actor);
			if (wcpActors.isEmpty()) {
				wcpsActors.remove(wcp);
			}
			actors.remove(actor);
			actor.setPartitionID(-1);
			if (removeFromPriorityList && !priorityOrderedActors.isEmpty()) {
				priorityOrderedActors.remove(priorityOrderedActors.size()-1);
			}
			symbolicLoad -= actor.getSymbolicLoad();
			avgCycloSymbolicLoad -= actor.getAvgCycloSymbolicLoad();
			--nbActors;
		}
	}
	
	/**
	 * @return Partition ID.
	 */
	public int getID() {
		return partitionID;
	}
	
	/**
	 * @return Number of actors in this partition.
	 */
	public int getNbActors() {
		return nbActors;
	}
	
	/**
	 * @return Actors present in the partition.
	 */
	public synchronized Set<Actor> getActors() {
		return actors;
	}

	/**
	 * This method return an actor list, which
	 * has been set only after a call of 
	 * {@link GraphProbData#setPartitionPriorityOrderedActors(boolean)}
	 * or if actors have been added with
	 * {@code addInpriorityList = true}.
	 * <p>
	 * The ordering may be done in another class in order to 
	 * decrease the global complexity: an empty list is returned
	 * if the sort has not been done yet.
	 * 
	 * @return Actor list, ordered by priorities.
	 */
	public List<Actor> getPriorityOrderedActors() {
		return priorityOrderedActors;
	}
	
	/**
	 * Symbolic load of the partition, being the
	 * sum of its actor symbolic loads, disregarding
	 * to any proportional metric.
	 * 
	 * @return Symbolic load sum of all actors in the partition.
	 */
	public double getSymbolicLoad() {
		return symbolicLoad;
	}
		
	/**
	 * Average cyclo symbolic load of the partition, being the
	 * sum of its actor avergae cyclo-symbolic loads, disregarding
	 * to any proportional metric.
	 * 
	 * @return Average cyclo-symbolic load sum of all actors in the partition.
	 */
	public double getAvgCycloSymbolicLoad() {
		return avgCycloSymbolicLoad;
	}
		
	/**
	 * Compute the processor utilization factor on
	 * this partition. 
	 * <p>
	 * Actor periods must have been set.
	 * 
	 * @return Processor utilization factor of this partition.
	 */
	public double getLoad() {
		double utilization = 0;
		for (Actor a: actors) {
			utilization += a.getWcet() / (double) a.getPeriod();
		}
		return utilization;
	}

	/**
	 * Compute the processor utilization factor on
	 * this partition (based on average cyclo-WCET).
	 * <p>
	 * Actor periods must have been set.
	 * 
	 * @return Processor utilization factor of this partition.
	 */
	public double getAvgCycloLoad() {
		double utilization = 0;
		for (Actor a: actors) {
			utilization += a.getAvgCycloWcet() / (double) a.getPeriod();
		}
		return utilization;
	}
	
	/**
	 * Compute the exact processor utilization factor on
	 * this partition. 
	 * <p>
	 * Actor periods must have been set.
	 * 
	 * @return Processor utilization factor of this partition
	 * expressed as an integer fraction.
	 */
	public Fraction getFractionalLoad() {
		Fraction utilization = new Fraction(0, 1);
		for (Actor a: actors) {
			utilization.setADD(new Fraction(a.getWcet(), a.getPeriod()));
		}
		return utilization;		
	}
	
	/**
	 * @return WeaklyConnectedComponents containing at least
	 * one actor also present in this partition.
	 */
	public synchronized Set<WeaklyConnectedComponent> getWCPs() {
		return wcpsActors.keySet();
	}
	
	/**
	 * Retrieve the actors of the partition which
	 * belongs to a specific weakly connected
	 * component.
	 * 
	 * @param wcp Component containing the wanted actors.
	 * @return Actors belonging to the intersection of the
	 * given weakly connected component and the partition. 
	 */
	public synchronized Set<Actor> getWCPActors(WeaklyConnectedComponent wcp) {
		return wcpsActors.get(wcp);
	}	
	
}
