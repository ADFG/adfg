/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.ars;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import fr.inria.tea.adfg.algos.common_data_rep.Actor;
import fr.inria.tea.adfg.algos.maths.Pair;

/**
 * This class implements two heuristics to find the least
 * and the greater Scotch imbalance ratio, in quadratic time.
 * 
 * @author Alexandre Honorat
 */
class ScotchImbHeuristics {

	Collection<Actor> actors;
	int minActorProc;
	double totalLoad;
	double avg;
	
	double imbMin;
	double imbMax;
	
	/**
	 * Natural comparator for pair of double,
	 * regarding to their first content.
	 * <p>
	 * In this class, the first pair element
	 * is the metric derived fomr the load,
	 * while the second element is the original load.
	 * 
	 * @author Alexandre Honorat
	 */
	private class PairLoadComparator implements Comparator<Pair<Double>> {
		@Override
		public int compare(Pair<Double> arg0, Pair<Double> arg1) {
			return Double.compare(arg0.getFirst(), arg1.getFirst());
		}		
	}
	
	
	
	/**
	 * Computes the imbalance ratio.
	 * 
	 * @param actors Actors to partition.
	 * @param minActorProc Number of partitions (less than number of actors).
	 * @param totalLoad Sum of all actors symbolic loads.
	 */
	protected ScotchImbHeuristics(Collection<Actor> actors, int minActorProc, double totalLoad) {
		this.actors = actors;
		this.minActorProc = minActorProc;
		this.totalLoad = totalLoad;
		avg = totalLoad / minActorProc;		
		imbMin = 0;
		imbMax = 2;
	}

	/**
	 * Method to compute the minimum imbalance ratio (heuristic).
	 * 
	 * @return Minimum imbalance ratio.
	 */
	protected double computeImbMin() {
		LinkedList<Double> heuristicMinLoads = new LinkedList<>();
		for (Actor actor: actors) {
			double load = actor.getSymbolicLoad();
			insertDescending(heuristicMinLoads, load);
		}
		LinkedList<Double> bestPartitionLoads = new LinkedList<>();
		for (int i = 0; i < minActorProc; ++i) {
			bestPartitionLoads.add(0.0);
		}
		for (double load: heuristicMinLoads) {
			insertAscending(bestPartitionLoads, bestPartitionLoads.pop() + load);
		}
		return computeImb(bestPartitionLoads);
	}

	/**
	 * Helper to insert an element in a list
	 * ordered in ascending order.
	 * 
	 * @param partitionLoads Sorted list of loads, 
	 * @param element Value to add.
	 */
	protected void insertAscending(LinkedList<Double> partitionLoads, double element) {
		ListIterator<Double> it = partitionLoads.listIterator();
		while (it.hasNext()) {
			Double load = it.next();
			if (element < load) {
				it.previous();
				it.add(element);
				return;
			}
		}
		it.add(element);
	}
	
	/**
	 * Helper to insert an element in a list
	 * ordered in descending order.
	 * 
	 * @param partitionLoads Sorted list of loads, 
	 * @param element Value to add.
	 */
	protected void insertDescending(LinkedList<Double> partitionLoads, double element) {
		ListIterator<Double> it = partitionLoads.listIterator();
		while (it.hasNext()) {
			Double load = it.next();
			if (element > load) {
				it.previous();
				it.add(element);
				return;
			}
		}
		it.add(element);
	}

	/**
	 * Method to compute the maximum imbalance ratio (heuristic).
	 * 
	 * @return Maximum imbalance ratio.
	 */
	protected double computeImbMax() {
		List<Pair<Double>> heuristicMaxLoads = new ArrayList<>(actors.size());
		for (Actor actor: actors) {
			double load = actor.getSymbolicLoad();
			heuristicMaxLoads.add(new Pair<>(Math.abs(load - avg) / load, load));
		}
		Collections.sort(heuristicMaxLoads, new PairLoadComparator());
		
		Double[] worstPartitionLoads = new Double[minActorProc];
		ListIterator<Pair<Double>> it = heuristicMaxLoads.listIterator(heuristicMaxLoads.size());
		int index = 0;
		int maxLoadIndex = 0;
		double maxLoad = 0;
		while (it.hasPrevious() && index < worstPartitionLoads.length) {
			double load = it.previous().getSecond();
			worstPartitionLoads[index] = load;
			if (load > maxLoad) {
				maxLoadIndex = index;
				maxLoad = load;
			}
			index++;
		}
		while (it.hasPrevious()) {
			worstPartitionLoads[maxLoadIndex] += it.previous().getSecond();
		}
		return computeImb(Arrays.asList(worstPartitionLoads));
	}	
	
	/**
	 * Compute the imbalance ratio from a specific mapping.
	 * 
	 * @param mapping Loads of each partition.
	 * @return Imbalance ratio.
	 */
	private double computeImb(List<Double> mapping) {
		double imb = 0;
		for (Double load: mapping) {
			imb += Math.abs(load - avg);
		}
		return imb / totalLoad;
	}
	

}
