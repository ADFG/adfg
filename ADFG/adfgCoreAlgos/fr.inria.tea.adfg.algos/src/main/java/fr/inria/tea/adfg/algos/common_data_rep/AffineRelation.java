/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.common_data_rep;

import java.io.Serializable;

import fr.inria.tea.adfg.algos.common_base.AdfgInternalException;
import fr.inria.tea.adfg.algos.maths.BasicArithmetic;
import fr.inria.tea.adfg.algos.maths.Pair;
import fr.inria.tea.adfg.algos.maths.UPIS;

/** 
 * Affine relation express the abstract time 
 * relation between two actors. There are used
 * as edges in affine relation graphs.
 * 
 * @author Adnan Bouakaz
 * @author Alexandre Honorat
 */
public class AffineRelation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Unique instance identifier.
	 */
	private static int IDcount = 1;

	private int instanceID;
	/**
	 * If true then no deadline dependency should be added in the hybrid case. 
	 */
	private boolean noAdjust;
	/**
	 * If false then phi has to be computed in ARS.
	 */
	private boolean isPhiDefined;
	private int n, phi, d;

	/**
	 * Construct a new affine relation characterized
	 * by the source actor relative clock rate, and the destination
	 * actor relative clock rate. The clock rates are relative to a
	 * global clock, at least common to the both involved
	 * actors. Phase is zero. 
	 * 
	 * See Adnan Bouakaz thesis p. 68-70.
	 * 
	 * @param n Source clock relative rate.
	 * @param d Destination clock relative rate.
	 */
	public AffineRelation(int n, int d) {
		instanceID = ++IDcount;
		isPhiDefined = false;
		noAdjust = false;
		cstrSubroutine(n, 0, d);
	}
	
	/**
	 * Copy constructor.
	 * 
	 * @param r AffineRelation to copy (notice that the ID is copied too).
	 */
	
	public AffineRelation(AffineRelation r) {
		instanceID = ++IDcount;
		isPhiDefined = r.isPhiDefined;
		noAdjust = r.noAdjust;
		n = r.n;
		phi = r.phi; 
		d = r.d;
	}
	
	/**
	 * Create a new AffineRelation regarding to the
	 * previous object and the new phase.
	 * 
	 * @param r Relation to copy (notice that the ID is copied too).
	 * @param phi New phase.
	 */
	public AffineRelation(AffineRelation r, int phi) {
		instanceID = r.instanceID;
		isPhiDefined = true;
		noAdjust = r.noAdjust;
		cstrSubroutine(r.n, phi, r.d);
	}

	/**
	 * 
	 * @param a Source activation relative frequency.
	 * @param b Phase between source and target.
	 * @param c	Target activation relative frequency.
	 */
	private void cstrSubroutine(int a, int b, int c) {
		if (a <= 0 || c <= 0) {
			throw new AdfgInternalException("Parameters N and D of an affine relation must be stricly positive.");
		}
		int gcd = (int) BasicArithmetic.funcGCD(a, c); 
		/*Canonical form*/
		int tmpa, tmpc;
		tmpa = a / gcd;
		tmpc = c / gcd;
		if (!isPhiDefined){  
			n = tmpa;  
			phi = b; 
			d = tmpc; 
		} else if (b % gcd == 0) { 
			n = tmpa; 
			phi = b / gcd; 
			d = tmpc; 
		} else { 
			n = 2*tmpa; 
			phi = (b > 0) ? 1 + 2 * (b / gcd) : -1 + 2 * (b / gcd);
			d = 2*tmpc; 
		}		
	}
	
	/**
	 * Get unique Affine Relation identifier.
	 * 
	 * @return Channel's ID.
	 */
	public int getID() {
		return instanceID;
	}
	
	/**
	 * Compare equality of the affine relations,
	 * regardless to the phase.
	 * 
	 * @param r Affine relation to be compared with.
	 * @return True if same relative rates, false otherwise.
	 * Phi values are also compared, when both are defined.
	 */
	public boolean isConsistentWith(AffineRelation r) {
		if ((!isPhiDefined && r.isPhiDefined) || (isPhiDefined && !r.isPhiDefined)) 
			return n == r.n && d == r.d;
		return n == r.n && phi == r.phi && d == r.d;
	}

	/**
	 * Create a new affine relation which is
	 * reversed: n and d are exchanged, phi is
	 * opposed if defined. Adjustment boolean
	 * is set accordingly to the previous value.
	 * 
	 * @return New reversed affine relation.
	 */
	public AffineRelation getReverse() {
		AffineRelation r = new AffineRelation(this);
		return r.setReverse();
	}	

	/**
	 * The affine relation which is
	 * reversed: n and d are exchanged, phi is
	 * opposed if defined.
	 * 
	 * @return Current object.
	 */
	public AffineRelation setReverse() {
		int tmp = n;
		n = d;
		d = tmp;
		phi = -phi;
		return this;
	}	
	
	/**
	 * Get source actor relative clock rate.
	 * 
	 * @return Source relative clock rate.
	 */
	public int getN() {
		return n;
	}

	/**
	 * Unset the phase.
	 */
	public void unsetPhi() {
		isPhiDefined = false;
	}
		
	/**
	 * Set the phase.
	 * 
	 * @param b New phase.
	 */
	public void setPhi(int b) {
		isPhiDefined = true;
		cstrSubroutine(n, b, d);
	}

	/**
	 * Wheter or not phi value has been defined yet.
	 * 
	 * @return True if phi has been computed, false otherwise.
	 */
	public boolean isPhiDefined() {
		return isPhiDefined;
	}
	
	/**
	 * Get the phase between the two actors.
	 * 
	 * @return Phase.
	 */
	public int getPhi() {
		return phi;
	}

	/**
	 * Get destination actor relative clock rate.
	 * 
	 * @return Destination relative clock rate.
	 */
	public int getD() {
		return d;
	}
	
	/**
	 * Set the deadline adjustment policy.
	 * 
	 * @param b If false, deadlines will be adjusted.
	 * If true, deadlines will not be adjusted (because
	 * this would jeopardize the throughput.
	 */
	public void setNoAdjust(boolean b) {
		noAdjust = b;
	}

	/**
	 * Get the deadline adjustment policy.
	 * 
	 * @return False if adjustment is allowed, true otherwise.
	 */
	public boolean isNoAdjust() {
		return noAdjust;
	}

	/**
	 * Compute the actors' activation order, over their activation
	 * hyperperiod. Phi must have been defined before calling this method.
	 * <p>
	 * In the two sequences: a 1 codes an activation, a 0 an absence of 
	 * activation (which implies a 1 at the same index of the other sequence).
	 * Example for an (3,1,4) affine relation:
	 * seq1 = 1 (0 1 0 1 1 0) and 
	 * seq2 = 0 (1 0 1 1 0 1)
	 * Note that if both clocks are unactivated at the same time, the
	 * sequences do not contain this time instant (i. e. there is no
	 * possible way of having seq1[i] = 0 and seq2[i] = 0).
	 * <p>
	 * See Adnan Bouakaz thesis p. 68-69 (particularly the example 2.13 and
	 * the paragraph above it; we recall that every affine 
	 * relation is in the canonical form in this program).
	 * 
	 * @return Pair of sequences, the first being the source clock, 
	 * the second being the destination one. Both have the same length.
	 */
	public Pair<UPIS> binaryEncoding() {
		boolean reversed = phi < 0;
		int  n1, phi1, d1;
		if (reversed) { 
			n1 = d; 
			phi1 = -phi; 
			d1 = n;
		} else { 
			n1 = n; 
			phi1 = phi; 
			d1 = d;
		}
		/* the length of the prefix */
		int c1 = (int) Math.ceil((double) phi1 / n1); 
		int c2; /* the length of the period */
		if (BasicArithmetic.funcGCD(n1, d1) == 1) {
			c2 = n1+d1-1; 
		}
		else {
			c2 = (n1+d1) / 2; 
		}

		long[] sequence1 = new long[c1+c2], sequence2 = new long[c1+c2];
		for (int i = 0; i < c1; ++i) {
			sequence1[i] = 1; 
			sequence2[i] = 0; 
		} 

		int added = 0, k1 = c1*n1, k2 = phi1;
		while (added < c2) {
			if (k1 > k2) { 
				sequence1[c1+added] = 0; 
				sequence2[c1+added] = 1; 
				k2 += d1; 
			} else if (k1 < k2) { 
				sequence1[c1+added] = 1; 
				sequence2[c1+added] = 0; 
				k1 += n1;
			} else { 
				sequence1[c1+added] = 1; 
				sequence2[c1+added] = 1; 
				k1 += n1; 
				k2 += d1;
			}
			++added;
		}
		return (reversed) ? new Pair<>(new UPIS(sequence2,c1), new UPIS(sequence1,c1)) 
						  : new Pair<>(new UPIS(sequence1,c1), new UPIS(sequence2,c1));
	}

	/**
	 * Reset to 0 the next AffineRelation's ID. This method
	 * must be called between each new Channel's graph
	 * problem representation creation. Indeed Affine Relations'
	 * names are not used to identified them
	 * internally.
	 * 
	 */
	public static void resetIDs() {
		IDcount = 0;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof AffineRelation) {
			return ((AffineRelation) obj).getID() == instanceID;
		}
		return false;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode(java.lang.Object)
	 */	
	@Override
	public int hashCode() {
		return instanceID;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "("+n+", "+ (isPhiDefined ? phi : "*") +", "+d+")";
	}
	
}
