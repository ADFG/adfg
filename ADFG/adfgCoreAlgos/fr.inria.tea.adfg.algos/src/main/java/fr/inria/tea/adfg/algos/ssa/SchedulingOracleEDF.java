/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.ssa;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.logging.Logger;

import fr.inria.tea.adfg.algos.common_data_rep.Actor;
import fr.inria.tea.adfg.algos.common_data_rep.GraphProbData;
import fr.inria.tea.adfg.algos.common_data_rep.Partition;
import fr.inria.tea.adfg.algos.common_data_rep.WeaklyConnectedComponent;

/**
 * This class contains several methods to compute
 * the best period of a system
 * for EDF scheduling, and ensures that the system
 * is schedulable.
 * 
 * @author Adnan Bouakaz
 * @author Alexandre Honorat
 */
public class SchedulingOracleEDF {

	private static final Logger LOGGER = Logger.getLogger("AdfgLog");	 

	public static final long incoherentPeriod = 0;

	// GraphProbData
	private int nbProcessor;
	private GraphProbData gpr;

	
	/**
	 * Store the graph problem useful information.
	 * 
	 * @param gpr Graph to work with.
	 * @param mustUseAllProcessors True if the analysis must be done with
	 * all available processors, or false if with the number of non empty partitions.
	 */
	public SchedulingOracleEDF(GraphProbData gpr, boolean mustUseAllProcessors) {
		nbProcessor = gpr.getNbProcessor();
		if (!mustUseAllProcessors) {
			nbProcessor = gpr.getNumberOfUsedProcessors();
			
		}
		this.gpr = gpr;
	}	


	/**
	 * Compute the best possible period according to the
	 * SQPA schedulability test.
	 * <p>
	 * See Adnan Bouakaz thesis p. 107.
	 * <p>
	 * All the partition actor periods are set by this method.
	 * 
	 * @param partition Actors to work on, being a subset of
	 * only one weakly connected component.
	 * @return Period of the basis actor, or {@code incoherentPeriod}
	 * if the set couldn't be scheduled.
	 */
	public long standardSQPA(Partition partition) {
		if (partition.getActors().isEmpty()) {
			return incoherentPeriod;
		}

		WeaklyConnectedComponent wcp = partition.getWCPs().iterator().next();
		long divFactor = wcp.getDivFactor();
		long plb = wcp.getPeriodLowerBound();
		long pub = wcp.getPeriodUpperBound();
		double psi = partition.getSymbolicLoad(); 
		long inf = Math.max(plb, (long) Math.ceil(psi)); 
		long k = (inf + divFactor - 1) / divFactor; 
		long basisPeriod = k * divFactor;
		if (basisPeriod > pub) {
			return incoherentPeriod;
		}
		GraphProbData.setPeriods(basisPeriod, partition.getActors());

		Metrics met = new Metrics(partition.getActors(), nbProcessor);
		long dMin = met.edfDMin();
		long timeInterval = met.edfDMax(met.edfL(), false);

		long currentH;
		while ((currentH = met.edfH(timeInterval)) > dMin) {			
			if (currentH < timeInterval) {
				timeInterval = currentH;
			} else if (currentH == timeInterval) {
				/* interval = max{d | d<l} */
				timeInterval = met.edfDMax(timeInterval, true);
			} else {
				/* increase T */
				++k;
				basisPeriod = k * divFactor;
				if (basisPeriod > pub) {
					return incoherentPeriod;
				}
				GraphProbData.setPeriods(basisPeriod, partition.getActors());
				/* l = min{l, max{d |d < L*(T)} */
				met = new Metrics(partition.getActors(), nbProcessor);
				long dMaxTemp = met.edfDMax(met.edfL(), false);
				timeInterval = Math.min(dMaxTemp, currentH);	
				dMin = met.edfDMin();
			}
		}
		return basisPeriod;
	}

	/**
	 * Compute the best possible period according to the
	 * PSQPA schedulability test, only for a connected
	 * graph.
	 * <p>
	 * See Adnan Bouakaz thesis p. 110, point 6.
	 * <p>
	 * All the actor periods are set by this method.
	 * 
	 * @return Period of the basis actor, or {@code incoherentPeriod}
	 * if the set couldn't be scheduled.
	 */
	public long standardPSQPA() {
		long maxT = 0;
		for (Partition partition: gpr.getMappedPartitions().values()) {
			if (partition.getActors().isEmpty()) {
				continue;
			}
			long basisPeriod = standardSQPA(partition);
			if (basisPeriod == incoherentPeriod) {
				return basisPeriod;
			}
			if (basisPeriod > maxT) {
				maxT = basisPeriod;
			}
		}
		GraphProbData.setPeriods(maxT, gpr.getActors());
		return maxT;
	}	
	
	/**
	 * Compute the best possible period according to the
	 * Forced Forward Demand Bound Function schedulability test.
	 * <p>
	 * See Adnan Bouakaz thesis p. 112. However
	 * some parts of this code are undocumented and have been
	 * rewritten by Alexandre Honorat.
	 * <p>
	 * All the WCP actor periods are set by this method.
	 * 
	 * @param wcp Connected actors to work on.
	 * @return Period of the basis actor, or {@code incoherentPeriod}
	 * if the set couldn't be scheduled.
	 */
	public long ffdbfSQPA(WeaklyConnectedComponent wcp) {
		
		// space subintervals between gamma and gammaMax
		int nbSteps = 1000;
		
		long divFactor = wcp.getDivFactor();
		long plb = wcp.getPeriodLowerBound();
		long pub = wcp.getPeriodUpperBound();
		long k = (plb + divFactor - 1) / divFactor; 
		long basisPeriod = k * divFactor;
		if (basisPeriod > pub) {
			return incoherentPeriod;
		}
		wcp.setPeriods(basisPeriod);

		Metrics met = new Metrics(wcp.getActors(), nbProcessor);
		double gamma = met.edfGammaMin();
		double gammaMax = met.edfGammaMax(); 
		
		TreeMap<Double, Long> knownGammaMiss = new TreeMap<>();
		long dMin = met.edfDMin();
		long timeInterval = met.edfDMax(met.edfL(gamma), false);
		double epsilon = (gammaMax - gamma) / nbSteps;
		while (timeInterval >= dMin){
			
			double ffdbf = met.edfFFDBF(timeInterval, gamma);
			long currentH = met.edfH(ffdbf, gamma);
			if (currentH < timeInterval) {
				timeInterval = currentH;
			} else if (currentH == timeInterval) {
				/* l=max{d | d<l} */
				timeInterval = met.edfDMax(timeInterval, true);
			} else {
				double fStar = met.edfF(gamma);
				if (ffdbf >= fStar || gamma + epsilon >= gammaMax) {
					knownGammaMiss.put(gamma, currentH);
					/* increase T */
					++k;
					basisPeriod = k * divFactor;
					if (basisPeriod > pub) {
						return incoherentPeriod;
					}
					wcp.setPeriods(basisPeriod);
					met = new Metrics(wcp.getActors(), nbProcessor);
					gamma = met.edfGammaMin();
					gammaMax = met.edfGammaMax();
					epsilon = (gammaMax - gamma) / nbSteps;
					dMin = met.edfDMin();
				} else {
					gamma += epsilon;
				}

				/* l = min{Prev(gamma), max{d |d < LG(T)} */
				long dMaxTemp = met.edfDMax(met.edfL(gamma), false);
				long previousH = Long.MAX_VALUE;
				Double gammaKey = knownGammaMiss.ceilingKey(gamma);
				if (gammaKey != null) {
					previousH = knownGammaMiss.get(gammaKey);
				}
				timeInterval = Math.min(dMaxTemp, previousH);	
			}
		}
		return basisPeriod;
	}

	/**
	 * Container for QPAstar results
	 * and computed metrics.
	 * 
	 * @author Alexandre Honorat
	 */
	private class ResultQPAstar {
		private long h;
		private long dMin;
		private double utilizationFactor;

		private ResultQPAstar(double utilizationFactor) {
			this.utilizationFactor = utilizationFactor;
		}
	}

	/**
	 * QPAstar is an adaptation of QPA.
	 * <p>
	 * This work is not from the Adnan Bouakaz thesis, is has been
	 * adapted from his article "Earliest-Deadline First Scheduling of Multiple
	 * Independent Dataflow Graphs".
	 * 
	 * @param part Partition to work on.
	 * @param timeInterval Analysis interval.
	 * @return The new time interval, the minimum deadline (or a negative value
	 * if not schedulable) and the utilization factor.
	 */
	private ResultQPAstar standardQPAstar(Partition part, long timeInterval) {
		Metrics met = new Metrics(part.getActors(), 1);
		ResultQPAstar result = new ResultQPAstar(met.edfU());
		if (result.utilizationFactor > 1.0) {
			result.h = timeInterval;
			result.dMin = incoherentPeriod;
			return result;
		}
		long tempInterval = met.edfDMax(Math.min(met.edfL(), timeInterval), false);
		long h = met.edfH(tempInterval);
		long dMin = met.edfDMin();
		while (h <= tempInterval && h >= dMin) {
			if (h < tempInterval) {
				tempInterval = h;
			} else {
				tempInterval = met.edfDMax(tempInterval, true);
			}
			h = met.edfH(tempInterval);
		}
		result.h = h;
		result.dMin = dMin;
		return result;
	}

	/**
	 * Performs QPAstar on all partitions.
	 * 
	 * @param timeInterval Analysis interval array
	 * (one element per partition).
	 * @return New analysis time interval array if
	 * all components are schedulable, 
	 * {@code null} otherwise.
	 */
	private long[] allPartsQPAstarTest(long[] timeInterval) {
		long[] newTimeInterval = new long[timeInterval.length];
		for (Entry<Integer, Partition> partition: gpr.getMappedPartitions().entrySet()) {
			Partition part = partition.getValue();
			int index = partition.getKey();
			if (part.getActors().isEmpty()) {
				newTimeInterval[index] = timeInterval[index];				
				continue;
			}
			long partTimeInterval = timeInterval[index];
			ResultQPAstar qpa = standardQPAstar(part, partTimeInterval);
			if (qpa.h >= qpa.dMin) {
				return null;
			}
			newTimeInterval[index] = qpa.h;
		}
		return newTimeInterval;
	}

	/**
	 * Check if a weakly connected component reduced periods
	 * array is the children of an element of the list 
	 * (so if the child has all its component periods greater than
	 * the element ones).
	 * 
	 * @param schedulableK List of forbidden parents.
	 * @param nextK Node to test 
	 * (indexed weakly connected components reduced periods).
	 * @return {@code true} if {@code nextK} is a children of 
	 * at least one element of the list, {@code false} otherwise.
	 */
	private boolean isChildrenOf(List<long[]> schedulableK, long[] nextK) {
		int length = nextK.length;
		for (long[] possibleParent: schedulableK) {
			int i;
			for (i = 0; i < length; ++i) {
				if (nextK[i] < possibleParent[i]) {					
					break;
				}
			}
			if (length == i) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Generate the next list of nodes to visit:
	 * children nodes of the current not schedulable one.
	 * 
	 * @param toNotVisitChildrenK Nodes that cannot have children.
	 * @param toVisitChildrenK Nodes that may have children.
	 * @return List of all possible children nodes.
	 */
	private List<long[]> generateNextLevelNodes(List<long[]> toNotVisitChildrenK, List<long[]> toVisitChildrenK) {
		List<long[]> list = new ArrayList<>();
		for (long[] toVisit: toVisitChildrenK) {
			for (int i = 0; i < toVisit.length; ++i) {
				long[] nextK = Arrays.copyOf(toVisit, toVisit.length);
				nextK[i] += 1;
				if (!isChildrenOf(toNotVisitChildrenK, nextK)) {
					list.add(nextK);
				}
			}
			toNotVisitChildrenK.add(toVisit);
		}
		if (list.isEmpty()) {
			return null;
		}
		return list;
	}

	/**
	 * Performs the PQPA (parametric QPA) schedulability analysis.
	 * <p>
	 * Works for independant dataflow graphs and multiprocessor.
	 * <p>
	 * This work is not from the Adnan Bouakaz thesis, is has been
	 * adapted from his article "Earliest-Deadline First Scheduling of Multiple
	 * Independent Dataflow Graphs".
	 * 
	 * @return Indexed weakly connected components reduced periods if schedulable,
	 * {@code null} otherwise.
	 */
	public long[] modifiedPQPA() {
		int nbWCPs = gpr.getWeaklyConnectedComponents().size();
		WeaklyConnectedComponent[] indexedWCPs = new WeaklyConnectedComponent[nbWCPs];
		long[] indexedPUBs = new long[nbWCPs];
		long[] indexedDivFactor = new long[nbWCPs];
		double[] indexedPSIs = new double[nbWCPs];
		long[] indexedStartK = new long[nbWCPs];
		long[] indexedBestK = null;
		long[] indexedBestPeriods = new long[nbWCPs];
		long[] partLastTimeInterval = new long[gpr.getNbProcessor()];
		double psiCoef = ((double) nbWCPs / nbProcessor);
		double bestU = 0;
		int i = 0;

		for (WeaklyConnectedComponent wcp: gpr.getWeaklyConnectedComponents()) {
			indexedWCPs[i] = wcp;
			indexedPUBs[i] = wcp.getPeriodUpperBound();
			indexedDivFactor[i] = wcp.getDivFactor();
			indexedPSIs[i] = wcp.getSymbolicLoad() * psiCoef; 
			long inf = Math.max(wcp.getPeriodLowerBound(), (long) Math.floor(indexedPSIs[i])); 
			long k = inf / indexedDivFactor[i]; 
			indexedStartK[i] = k;
			long basisPeriod = k * indexedDivFactor[i];
			if (basisPeriod > indexedPUBs[i]) {
				return null;
			}
			indexedBestPeriods[i] = basisPeriod;
			wcp.setPeriods(basisPeriod);
			++i;
		}

		for (i = 0; i < gpr.getNbProcessor(); ++i) {
			partLastTimeInterval[i] = Actor.MAX_PERIOD;
		}
		if (allPartsQPAstarTest(partLastTimeInterval) != null) {
			return indexedBestPeriods;
		}

		int reachedDepth = 0;
		List<long[]> nodesToVisit = new ArrayList<>();
		nodesToVisit.add(indexedStartK);
		while (nodesToVisit != null) {
			List<long[]> nodesToVisitChildren = new ArrayList<>();
			Iterator<long[]> currentIt = nodesToVisit.iterator();
			while (currentIt.hasNext()) {
				long[] currentK = currentIt.next();
				long[] currentPeriods = new long[nbWCPs];
				for (i = 0; i < nbWCPs; ++i) {
					long basisPeriod = currentK[i] * indexedDivFactor[i];
					if (basisPeriod > indexedPUBs[i]) {
						break;
					}
					indexedWCPs[i].setPeriods(basisPeriod);
					currentPeriods[i] = basisPeriod;
				}
				if (i < nbWCPs) {
					continue;
				}
				long[] newIntervals = allPartsQPAstarTest(partLastTimeInterval);
				if (newIntervals != null) {
					double newU = gpr.getTotalUtilizationFactor();
					if (newU > bestU) {
						bestU = newU;
						indexedBestK = currentK;
						indexedBestPeriods = currentPeriods;
						partLastTimeInterval = newIntervals;
					}
				} else {
					nodesToVisitChildren.add(currentK);
					currentIt.remove();
				}
			}
			if (indexedBestK != null) {
				nodesToVisit.add(indexedBestK);
			}
			nodesToVisit = generateNextLevelNodes(nodesToVisit, nodesToVisitChildren);
			++reachedDepth;
		}
		LOGGER.fine("PQPA reached depth: " + reachedDepth);
		if (indexedBestK != null) {
			for (i = 0; i < nbWCPs; ++i) {
				indexedWCPs[i].setPeriods(indexedBestPeriods[i]);
			}
			return indexedBestPeriods;
		}
		return null;
	}

	/**
	 * Generate the next list of nodes to visit:
	 * children nodes of the current not schedulable one.
	 * 
	 * @param toNotVisitChildrenK Nodes that cannot have children.
	 * @param toVisitChildrenK Nodes that may have children.
	 * @param toIncrementWCPs List of WCPs indexes which can be incremented.
	 * @return List of all possible children nodes.
	 */
	private List<long[]> generatePartNextLevelNodes(List<long[]> toNotVisitChildrenK, List<long[]> toVisitChildrenK,
			List<Integer> toIncrementWCPs) {
		List<long[]> list = new ArrayList<>();
		for (long[] toVisit: toVisitChildrenK) {
			for (Integer i: toIncrementWCPs) {
				long[] nextK = Arrays.copyOf(toVisit, toVisit.length);
				nextK[i] += 1;
				if (!isChildrenOf(toNotVisitChildrenK, nextK)) {
					list.add(nextK);
				}
			}
			toNotVisitChildrenK.add(toVisit);
		}
		if (list.isEmpty()) {
			return null;
		}
		return list;
	}

	/**
	 * Performs the PQPA (parametric QPA) schedulability analysis
	 * on a single partition.
	 * <p>
	 * This work is not from the Adnan Bouakaz thesis, is has been
	 * adapted from his article "Earliest-Deadline First Scheduling of Multiple
	 * Independent Dataflow Graphs".
	 * 
	 * @param part Partition to work on.
	 * @param startingK Current indexed weakly connected components reduced periods.
	 * @param indexedWCPs Indexed weakly connected components of the system (with
	 * the same indexing).
	 * @return Indexed weakly connected components reduced periods if schedulable,
	 * {@code null} otherwise.
	 */	
	public long[] modifiedPQPA(Partition part, long[] startingK, WeaklyConnectedComponent[] indexedWCPs) {
		int nbWCPs = gpr.getWeaklyConnectedComponents().size();
		long[] indexedPUBs = new long[nbWCPs];
		long[] indexedDivFactor = new long[nbWCPs];
		long[] indexedBestK = null;
		List<Integer> listIndex = new ArrayList<>();
		long partLastTimeInterval = Actor.MAX_PERIOD;
		double bestU = 0;
		int i = 0;
		for (i = 0; i < nbWCPs; ++i) {
			WeaklyConnectedComponent wcp = indexedWCPs[i];
			if (part.getWCPs().contains(wcp)) {
				listIndex.add(i);
			}
			indexedPUBs[i] = wcp.getPeriodUpperBound();
			indexedDivFactor[i] = wcp.getDivFactor();
			long basisPeriod = indexedDivFactor[i] * startingK[i];
			if (basisPeriod > indexedPUBs[i]) {
				return null;
			}
			wcp.setPeriods(basisPeriod);
		}

		long[] ourStartingK = Arrays.copyOf(startingK, startingK.length);
		ResultQPAstar resQPA = standardQPAstar(part, partLastTimeInterval);
		if (resQPA.dMin > resQPA.h) {
			return ourStartingK;
		} else {
			partLastTimeInterval = resQPA.h;
		}
		if (resQPA.utilizationFactor > 1.0) {
			for (Integer index: listIndex) {
				WeaklyConnectedComponent wcp = indexedWCPs[index];
				long basisPeriod = (long) Math.floor(wcp.getBasis().getPeriod() * resQPA.utilizationFactor);
				if (basisPeriod > indexedPUBs[index]) {
					return null;
				}
				wcp.setPeriods(basisPeriod);
				ourStartingK[index] = basisPeriod / indexedDivFactor[index];
			}
		}

		int reachedDepth = 0;
		List<long[]> nodesToVisit = new ArrayList<>();
		nodesToVisit.add(ourStartingK);
		while (nodesToVisit != null) {
			List<long[]> nodesToVisitChildren = new ArrayList<>();
			Iterator<long[]> currentIt = nodesToVisit.iterator();
			while (currentIt.hasNext()) {
				long[] currentK = currentIt.next();
				i = 0;
				for (Integer index: listIndex) {
					long basisPeriod = currentK[index] * indexedDivFactor[index];
					if (basisPeriod > indexedPUBs[index]) {
						break;
					}
					indexedWCPs[index].setPeriods(basisPeriod);
					++i;
				}
				if (i < listIndex.size()) {
					continue;
				}
				resQPA = standardQPAstar(part, partLastTimeInterval);
				if (resQPA.dMin > resQPA.h) {
					double newU = resQPA.utilizationFactor;
					if (newU > bestU) {
						bestU = newU;
						indexedBestK = currentK;
					}
				} else {
					nodesToVisitChildren.add(currentK);
					currentIt.remove();
				}
				partLastTimeInterval = resQPA.h;
			}
			if (indexedBestK != null) {
				nodesToVisit.add(indexedBestK);
			}
			nodesToVisit = generatePartNextLevelNodes(nodesToVisit, nodesToVisitChildren, listIndex);
			++reachedDepth;
		}
		LOGGER.fine("PQPA reached depth: " + reachedDepth);
		return indexedBestK;
	}


}
