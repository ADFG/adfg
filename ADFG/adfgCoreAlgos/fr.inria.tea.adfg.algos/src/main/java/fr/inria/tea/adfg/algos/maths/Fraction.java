/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.maths;

import fr.inria.tea.adfg.algos.common_base.AdfgInternalException;

/** 
 * Mathematical fraction representation with numerator and denominator.
 * Basically do same things as {@code org.apache.commons.lang.math.Fraction}.
 * 
 * @author Adnan Bouakaz
 * @author Alexandre Honorat
 */
public class Fraction {

	/*
	 * {@code a}: Numerator.
	 * {@code b}: Denominator.
	 */
	private long a, b;

	/**
	 * Construct a reduced fraction from numerator and denominator.
	 * The reduced fraction's denominator is always positive.
	 * 
	 * @param x Numerator.
	 * @param y Denominator.
	 */
	public Fraction(long x, long y) {
		this(x, y, true);
	}

	/**
	 * Construct a reduced fraction from numerator and denominator.
	 * The reduced fraction's denominator is always positive.
	 * 
	 * @param x Numerator.
	 * @param y Denominator.
	 * @param mustBeReduced True if the fraction must be reduced, false otherwise.
	 */
	public Fraction(long x, long y, boolean mustBeReduced) {
		if (y == 0) {
			throw new AdfgInternalException("Division by zero in a fraction.");
		} else if (y < 0) {
			a = -x;
			b = -y;
		} else {
			a = x;
			b = y;			
		}
		/*Canonical form*/
		if (mustBeReduced) {
			setReduced();
		}
	}
	
	
	/**
	 * Construct a Fraction by deep-copy.
	 * 
	 * @param fr Fraction to copy.
	 */
	public Fraction(Fraction fr) {
		a = fr.a;
		b = fr.b;
	}

	/**
	 * Create a fraction that approximates the value in Q.
	 * Based on the continued fractions theory.
	 * 
	 * @param value Number to approximate.
	 */
	public Fraction(double value) {
		long max = (long) Math.pow(10, Math.abs(Math.ceil(Math.log10(value))) + 1.0);   
		// previous approximation
		long num0 = 1;
		long den0 = 0;
		// current approximation
		long num1 = (long) Math.floor(value);
		long den1 = 1;
		// next approximation
		long num2;
		long den2;
	  
	    double r = value - num1;
	    double next_cf;
	    	    
	    while (Math.abs(num1) < max && Math.abs(den1) < max) {
	    	if (r == 0.0)
	    		break;
	    	r = 1.0 / r;
	        next_cf = Math.floor(r);
	  
	        num2 = (long) (next_cf * num1 + num0);
	        den2 = (long) (next_cf * den1 + den0);

	        num0 = num1;
	        num1 = num2;
	        den0 = den1;
	        den1 = den2;
	  
	        r -= next_cf;
	    }
	    if (r == 0.0) {
	    	a = num1;
	    	b = den1;
	    } else {
	    	a = num0;
	    	b = den0;
	    }
	    if (b < 0) {
	    	a = -a;
	    	b = -b;
	    }
	}
	
	/**
	 * Compute the fraction value.
	 * 
	 * @return Value of the fraction.
	 */
	public double getValue() {
		return (double) a / b;
	}

	/**
	 * Create a new fraction being the irreducible form 
	 * of the current object (division by gcd).
	 * 
	 * @return New fraction being the reduced form.
	 */
	public Fraction getReduced() {
		return new Fraction(a, b);
	}

	/**
	 * Reduced the fraction to an irreducible form 
	 * (division by gcd).
	 * 
	 * @return Current object.
	 */
	public Fraction setReduced() {
		long gcd = BasicArithmetic.funcGCD(Math.abs(a), b); 
		a /= gcd;
		b /= gcd;
		return this;
	}
	
	/**
	 * Create new fraction being the inverse of the current object. 
	 * Throw an internal adfg exception if current numerator is zero.
	 * 
	 * @return New fraction being the inverse of the current object.
	 */
	public Fraction getINV() {
		if (a == 0) {
			throw new AdfgInternalException("Division by zero in a fraction");
		}
		return new Fraction(b, a);
	}

	/**
	 * Inverse the fraction, throw an internal adfg exception if current
	 * numerator is zero.
	 * 
	 * @return Current object.
	 */
	public Fraction setINV() {
		if (a == 0) {
			throw new AdfgInternalException("Division by zero in a fraction");
		}
		long tmp = a;
		a = b;
		b = tmp;
		if (b < 0) {
			a = -a;
			b = -b;
		}
		return this;
	}

	/**
	 * Multiply two fractions.
	 * 
	 * @param f Fraction to multiply with.
	 * @return New fraction being the multiplication of the current object
	 * by the parameter.
	 */
	public Fraction getMUL(Fraction f) {
		long gcd1 = BasicArithmetic.funcGCD(Math.abs(a), f.b);
		long gcd2 = BasicArithmetic.funcGCD(Math.abs(f.a), b);
		return new Fraction((a/gcd1)*(f.a/gcd2), (b/gcd2)*(f.b/gcd1));
	}

	/**
	 * Multiply two fractions and store the result in the current object.
	 * 
	 * @param f Fraction to multiply with.
	 * @return Current object.
	 */
	public Fraction setMUL(Fraction f) {
		long gcd1 = BasicArithmetic.funcGCD(Math.abs(a), f.b);
		long gcd2 = BasicArithmetic.funcGCD(Math.abs(f.a), b);
		a = (a/gcd1)*(f.a/gcd2);
		b = (b/gcd2)*(f.b/gcd1);
		return this;
	}
	
	/**
	 * Divide a fraction by another.
	 * 
	 * @param f Fraction to divide by.
	 * @return New fraction being the multiplication of the current fraction
	 * by the inverse of the parameter.
	 */
	public Fraction getDIV(Fraction f) {
		return getMUL(f.getINV());
	}

	/**
	 * Divide a fraction by another and store the result in the current object.
	 * 
	 * @param f Fraction to divide by.
	 * @return Current object.
	 */
	public Fraction setDIV(Fraction f) {
		return setMUL(f.getINV());
	}

	/**
	 * Sum two fractions.
	 * 
	 * @param f Fraction to add with.
	 * @return New fraction being the sum of the current fraction and the parameter.
	 */
	public Fraction getADD(Fraction f) {
		long lcm = BasicArithmetic.funcLCM(b, f.b);
		return new Fraction(a*(lcm/b)+f.a*(lcm/f.b), lcm);
	}

	/**
	 * Sum two fractions and store the result in the 
	 * current object.
	 * 
	 * @param f Fraction to add with.
	 * @return Current object.
	 */
	public Fraction setADD(Fraction f) {
		long lcm = BasicArithmetic.funcLCM(b, f.b);
		a = a*(lcm/b)+f.a*(lcm/f.b);
		b = lcm;
		return this;
	}

	/**
	 * Subtract a fraction.
	 * 
	 * @param f Fraction to subtract.
	 * @return New fraction being the subtraction of the parameter from the current fraction.
	 */
	public Fraction getSUB(Fraction f) {
		long lcm = BasicArithmetic.funcLCM(b, f.b);
		return new Fraction(a*(lcm/b)-f.a*(lcm/f.b), lcm);
	}

	/**
	 * Subtract a fraction and store the result in the
	 * current object.
	 * 
	 * @param f Fraction to subtract.
	 * @return Current object.
	 */
	public Fraction setSUB(Fraction f) {
		long lcm = BasicArithmetic.funcLCM(b, f.b);
		a = a*(lcm/b)-f.a*(lcm/f.b);
		b = lcm;
		return this;
	}
	
	/**
	 * Compare two fractions.
	 * 
	 * @param f Fraction to be compared to.
	 * @return True if current fraction value is 
	 * less than the parameter, or equal to it.
	 * False otherwise.
	 */
	public boolean isLessOrEqual(Fraction f) {
		long gcd1 = BasicArithmetic.funcGCD(Math.abs(a), Math.abs(f.a));
		long gcd2 = BasicArithmetic.funcGCD(b, f.b);
		return (a/gcd1)*(f.b/gcd2) <= (b/gcd2)*(f.a/gcd1);
	}

	/**
	 * Strictly compare two fractions.
	 * 
	 * @param f Fraction to be compared to.
	 * @return True if current fraction value is
	 * less than the parameter. False otherwise.
	 */
	public boolean isLess(Fraction f) {
		long gcd1 = BasicArithmetic.funcGCD(Math.abs(a), Math.abs(f.a));
		long gcd2 = BasicArithmetic.funcGCD(b, f.b);
		return (a/gcd1)*(f.b/gcd2) < (b/gcd2)*(f.a/gcd1);
	}

	/**
	 * Returns numerator part.
	 * 
	 * @return Numerator.
	 */
	public long getNumerator() {
		return a;
	}

	/**
	 * Returns denominator part.
	 * 
	 * @return Denominator.
	 */
	public long getDenominator() {
		return b;
	}

	/**
	 * Compare two fractions. Be aware that it is not the 
	 * equality between the fraction values.
	 * 
	 * @param fr Fraction to be compared to.
	 * @return True if numerator AND denominator 
	 * are equal in both fractions. False otherwise. 
	 */
	public boolean isEqualTo(Fraction fr) {
		return a == fr.a && b == fr.b;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return a+"/"+b;
	}

}
