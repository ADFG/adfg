/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos;

import java.util.Collection;
import java.util.logging.Logger;

import org.jdom2.Document;
import org.jdom2.Element;

import fr.inria.tea.adfg.algos.common_base.AdfgExternalException;
import fr.inria.tea.adfg.algos.common_base.ESchedAlgo;
import fr.inria.tea.adfg.algos.common_base.ESchedPolicy;
import fr.inria.tea.adfg.algos.common_data_rep.Actor;
import fr.inria.tea.adfg.algos.common_data_rep.Channel;
import fr.inria.tea.adfg.algos.common_data_rep.Partition;

/**
 * This class can export results in the Cheddar XMLv3 format.
 * 
 * @author Alexandre Honorat
 */
class CheddarTransformer {

	private static final Logger LOGGER = Logger.getLogger("AdfgLog");
	
	public static final String PREFIX_CORE_NAME = "core";
	public static final String PREFIX_PROC_NAME = "proc";
	public static final String PREFIX_ADDR_SPCE = "adds";
	
	/**
	 * Do nothing.
	 */
	private CheddarTransformer() {
	}

	/**
	 * Write a file (or several if partitioned multi-processor) readable by
	 * Cheddar.
	 * 
	 * @param adfgp Results to export.
	 * @param fileName File to write.
	 * @throws AdfgExternalException If file cound't be written by org.jdom2.
	 */
	static void exportSchedResults(ADFGprob adfgp, String fileName) throws AdfgExternalException {
		ESchedAlgo st = adfgp.getScheduleType();
		if (st == ESchedAlgo.GEDF_MULT) {
			LOGGER.severe("Cheddar does not support global EDF scheduling for now, file not exported.");
			return;
		}
		
		Collection<Partition> parts = adfgp.getPartitions();
		String baseFileName = fileName;
		if (baseFileName.endsWith(".xmlv3")) {
			baseFileName = baseFileName.substring(0, fileName.length()-6);
		}

		Element root = new Element("cheddar");
		Document document = new Document(root);
		
		Element cores = new Element("core_units");
		Element processors = new Element("processors");
		Element addressSpaces = new Element("address_spaces");
		Element tasks = new Element("tasks");
		Element buffers = new Element("buffers");
		Element dependencies = new Element("dependencies");
		int nbActorsTotal = adfgp.getMappedActors().size();
		
		for (Partition part: parts) {
			int nbActorsPart = part.getNbActors();
			if (nbActorsPart < 1) {
				continue;
			} else if (nbActorsPart > 255 && st.schedulingPolicy() == ESchedPolicy.SP) {
				LOGGER.warning("Cheddar manage only 255 actor priorities with POSIX but there is " + nbActorsPart
						+ " on the partition " + part.getID() + ", so you'll have to modify the Cheddar"
								+ "configuration.");
			}
			int partitionID = part.getID();
			
			setAndAddCore(partitionID, st, cores);
			setAndAddProc(partitionID, processors);
			setAndAddAddressSpace(partitionID, addressSpaces);
			
			for (Actor a: part.getActors()) {
				Element task = new Element("periodic_task");
				setGenericElement("a"+a.getID(), a.getName(), "TASK_OBJECT_TYPE", task);
				
				setSingleElement("task_type", "PERIODIC_TYPE", task);
				setSingleElement("cpu_name", PREFIX_PROC_NAME+partitionID, task);
				// if processor is monocore, the only available core will be selected by cheddar 
				// setSingleElement("core_name", PREFIX_CORE_NAME+partitionID, task);
				setSingleElement("address_space_name", PREFIX_ADDR_SPCE+partitionID, task);
				setSingleElement("capacity", Long.toString(a.getWcet()), task);
				setSingleElement("deadline", Long.toString(a.getDeadline()), task);
				setSingleElement("start_time", Long.toString(a.getPhase()), task);
				// priorities are reversed for Cheddar (1 being the lowest for them)
				setSingleElement("priority", Integer.toString(1 + nbActorsTotal - a.getPriority()), task);
				setSingleElement("period", Long.toString(a.getPeriod()), task);

				tasks.addContent(task);
				
				for (Channel channel: adfgp.getAdfgGraph().outgoingEdgesOf(a)) {
					Actor dest = adfgp.getAdfgGraph().getEdgeTarget(channel);
					setAndAddBufferAndDependency(a, dest, channel, buffers, dependencies);
				}								
			}
		}
		
		root.addContent(cores);
		root.addContent(processors);
		root.addContent(addressSpaces);
		root.addContent(tasks);			
		root.addContent(buffers);
		root.addContent(dependencies);
		
		String file = baseFileName+".xmlv3";
		XMLUtils.writeDocument(document, file, "Cheddar", true);
		
	}

	/**
	 * This method creates and add a core element to its parent.
	 * 
	 * @param partitionID Partition of the core.
	 * @param scheduleType Scheduler of the core.
	 * @param cores Parent element.
	 */
	private static void setAndAddCore(int partitionID, ESchedAlgo scheduleType, Element cores) {
		String name = PREFIX_CORE_NAME+partitionID;
		Element core = new Element("core_unit");
		setGenericElement(name, name, "CORE_OBJECT_TYPE", core);

		Element scheduling = new Element("scheduling");
		Element schedulingParameters = new Element("scheduling_parameters");
		Element schedulerType = new Element("scheduler_type");
		if (scheduleType.schedulingPolicy() == ESchedPolicy.EDF) {
			schedulerType.setText("EARLIEST_DEADLINE_FIRST_PROTOCOL");
		} else {
			schedulerType.setText("POSIX_1003_HIGHEST_PRIORITY_FIRST_PROTOCOL");
		}
		schedulingParameters.addContent(schedulerType);
		Element preemptiveType = new Element("preemptive_type");
		preemptiveType.setText("PREEMPTIVE");
		schedulingParameters.addContent(preemptiveType);
		scheduling.addContent(schedulingParameters);
		core.addContent(scheduling);
		
		Element speed = new Element("speed");
		speed.setText("1");
		core.addContent(speed);
		
		cores.addContent(core);		
	}


	/**
	 * This method creates and add a core element to its parent.
	 * 
	 * @param partitionID Partition of the core.
	 * @param processors Parent element.
	 */
	private static void setAndAddProc(int partitionID, Element processors) {
		String name = PREFIX_PROC_NAME+partitionID;
		Element processor = new Element("mono_core_processor");
		setGenericElement(name, name, "PROCESSOR_OBJECT_TYPE", processor);
		
		setSingleElement("processor_type", "MONOCORE_TYPE", processor);
		setSingleElement("migration_type", "NO_MIGRATION_TYPE", processor);
		Element core = new Element("core");
		core.setAttribute("ref", PREFIX_CORE_NAME+partitionID);
		processor.addContent(core);
		
		processors.addContent(processor);
	}

	/**
	 * This method creates and add a core element to its parent.
	 * 
	 * @param partitionID Partition of the core.
	 * @param addressSpaces Parent element.
	 */
	private static void setAndAddAddressSpace(int partitionID, Element addressSpaces) {
		String name = PREFIX_ADDR_SPCE+partitionID;
		Element addressSpace = new Element("address_space");
		setGenericElement(name, name, "ADDRESS_SPACE_OBJECT_TYPE", addressSpace);

		setSingleElement("cpu_name", PREFIX_PROC_NAME+partitionID, addressSpace);
		
		addressSpaces.addContent(addressSpace);
	}

	/**
	 * Add a buffer and a the related dependencies in the output, given
	 * a channel.
	 * 
	 * @param src Producer.
	 * @param dest Consumer.
	 * @param chan Channel to add.
	 * @param buffers Buffer parent element.
	 * @param dependencies Dependency parent element.
	 */
	private static void setAndAddBufferAndDependency(Actor src, Actor dest, Channel chan, Element buffers, Element dependencies) {
		Element buffer = new Element("buffer");
		setGenericElement("c"+chan.getID(), chan.getName(), "BUFFER_OBJECT_TYPE", buffer);
		setSingleElement("cpu_name", PREFIX_PROC_NAME+src.getPartitionID(), buffer);
		setSingleElement("address_space_name", PREFIX_ADDR_SPCE+src.getPartitionID(), buffer);
		setSingleElement("queueing_system_type", "QS_PP1", buffer);
		setSingleElement("buffer_size", Long.toString(chan.getSize()), buffer);
		setSingleElement("buffer_initial_data_size", Long.toString(chan.getInitial()), buffer);
		Element roles = new Element("roles");
		setSingleElement("task_name", src.getName(), roles);
		Element roleP = new Element("buffer_role");
		setSingleElement("the_role", "UCSDF_PRODUCER", roleP);
		setSingleElement("size", Long.toString(chan.getProduce().constantBounds().getSecond()), roleP);
		setSingleElement("time", Long.toString(src.getWcet()), roleP);
		setSingleElement("timeout", "0", roleP);
		setSingleElement("amplitude_function", chan.getProduce().toCheddarString(), roleP);
		roles.addContent(roleP);
		setSingleElement("task_name", dest.getName(), roles);
		Element roleC = new Element("buffer_role");
		setSingleElement("the_role", "UCSDF_CONSUMER", roleC);
		setSingleElement("size", Long.toString(chan.getConsume().constantBounds().getSecond()), roleC);
		setSingleElement("time", "1", roleC);
		setSingleElement("timeout", "0", roleC);
		setSingleElement("amplitude_function", chan.getConsume().toCheddarString(), roleC);
		roles.addContent(roleC);
		buffer.addContent(roles);
		buffers.addContent(buffer);
		
		
		Element depC = new Element("dependency");
		setSingleElement("type_of_dependency", "QUEUEING_BUFFER_DEPENDENCY", depC);
		Element depCtaskID = new Element("buffer_dependent_task");
		depCtaskID.setAttribute("ref", "a"+dest.getID());
		depC.addContent(depCtaskID);
		setSingleElement("buffer_orientation", "FROM_OBJECT_TO_TASK", depC);
		Element depCbufID = new Element("buffer_dependency_object");
		depCbufID.setAttribute("ref", "c"+chan.getID());
		depC.addContent(depCbufID);

		Element depP = new Element("dependency");
		setSingleElement("type_of_dependency", "QUEUEING_BUFFER_DEPENDENCY", depP);
		Element depPtaskID = new Element("buffer_dependent_task");
		depPtaskID.setAttribute("ref", "a"+src.getID());
		depP.addContent(depPtaskID);
		setSingleElement("buffer_orientation", "FROM_TASK_TO_OBJECT", depP);
		Element depPbufID = new Element("buffer_dependency_object");
		depPbufID.setAttribute("ref", "c"+chan.getID());
		depP.addContent(depPbufID);
		
		dependencies.addContent(depC);
		dependencies.addContent(depP);
	}
	
	
	/**
	 * This method set the generic details of an element:
	 * id, name and object type.
	 * 
	 * @param id Used for the id attribute.
	 * @param name Used for the name element.
	 * @param objectType Used for the related attribute. 
	 * @param element Element to set.
	 */
	private static void setGenericElement(String id, String name, String objectType, Element element) {
		element.setAttribute("id", id);
		setSingleElement("object_type", objectType, element);
		setSingleElement("name", name, element);
	}

	/**
	 * Create a new element, set its text, and add it to its parent.
	 * 
	 * @param name Element name.
	 * @param text Text contained by the element.
	 * @param parent Element containing the element created.
	 */
	private static void setSingleElement(String name, String text, Element parent) {
		Element element = new Element(name);
		element.setText(text);
		parent.addContent(element);
	}
	
}
