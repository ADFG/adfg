/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.ssa;

import java.util.logging.Logger;

import fr.inria.tea.adfg.algos.common_data_rep.Actor;
import fr.inria.tea.adfg.algos.common_data_rep.GraphProbData;
import fr.inria.tea.adfg.algos.common_data_rep.Partition;
import fr.inria.tea.adfg.algos.common_data_rep.WeaklyConnectedComponent;
import fr.inria.tea.adfg.algos.maths.Pair;

/**
 * This class contains several methods to compute
 * the best period of a system
 * for Static Priority scheduling, and ensures that the system
 * is schedulable.
 *  
 * @author Adnan Bouakaz
 * @author Alexandre Honorat
 */
public class SchedulingOracleSP {

	
	private static final Logger LOGGER = Logger.getLogger("AdfgLog");
	
	public static final long incoherentPeriod = -1;
	
	// GraphProbData
	private int nbProcessor;
	private GraphProbData gpr;
	
	/**
	 * Store the graph problem useful information.
	 * 
	 * @param gpr Graph to work with.
	 * @param mustUseAllProcessors True if the analysis must be done with
	 * all available processors, or false if with the number of non empty partitions.
	 */
	public SchedulingOracleSP(GraphProbData gpr, boolean mustUseAllProcessors) {
		nbProcessor = gpr.getNbProcessor();
		if (!mustUseAllProcessors) {
			nbProcessor = gpr.getNumberOfUsedProcessors();
			
		}
		this.gpr = gpr;
	}	
	
	/**
	 * Compute a reduced basis period interval where
	 * the actor may be scheduled.
	 * <p>
	 * See Adnan Bouakaz thesis p. 97-98. The lower bound
	 * equation has been modified because useless, the
	 * original one is used instead, see p. 43.
	 * <p>
	 * All the parameter arrays are indexed by
	 * increasing tasks priorities.
	 * 
	 * @param actor Actor to consider.
	 * @param actorIndex Index of the actor to consider in the 
	 * array of actors sorted by priorities (starting from 0).
	 * @param sumWCET Each cell of this array is the sum of the
	 * higher priority tasks WCET and the own task WCET.
	 * @param sumUF Each cell of this array is the sum of the
	 * higher priority tasks symbolic load and the one of the
	 * current task.
	 * @param completeSumCoefC Actor coefficients for the third
	 * coefficient of the polynomial giving the upper bound of T.
	 * @return An interval of possible T values (the basis period)
	 * for the specified actor. Lower bound is necessary only, upper
	 * bound (second element) is sufficient only.
	 */
	private Pair<Long> reduceSearchSpaceActor(Actor actor, int actorIndex, 
												double[] sumWCET, double[] sumUF, double[] completeSumCoefC) {
		double deadline = actor.getSymbolicDeadline().setMUL(actor.getCoefficient()).getValue();
		
		
		/* period lower bound */
		long inf1 = (long) Math.ceil(sumUF[actorIndex] + actor.getWcet() / deadline);
		long inf2 = (long) Math.ceil((sumWCET[actorIndex] + actor.getWcet()) / deadline);
		long inf = Math.max(inf1, inf2);	
		
		/* period upper bound: 
		 * solve second degree equation a2 T^2 + b2 T + c2 >= 0
		 */
		double a = deadline;
		double b = - (deadline * sumUF[actorIndex]) - sumWCET[actorIndex] - actor.getWcet();
		double c = completeSumCoefC[actorIndex];
		double delta = b * b - (4 * a * c);
		long sup = inf;
		if (delta >= 0) {
			double root2 = (-b + Math.sqrt(delta)) / (2 * a);
			sup = Math.max(inf, (long) Math.ceil(root2));
		}
		return new Pair<>(inf,sup);
	}

	/**
	 * Compute the maximum of basis period upper
	 * bounds and lower bounds over all actors.
	 * <p>
	 * See Adnan Bouakaz thesis p. 97-98.
	 * 
	 * @param partition Actors to work on.
	 * @param infSchedulable Basis period schedulable interval of each actor.
	 * @return Maximum basis period lower bound and upper bound (over all actors).
	 */
	private Pair<Long> reduceSearchSpace(Partition partition, Pair<Long>[] infSchedulable) {
		int size = partition.getNbActors();

		/* precompute some values */
		double[] sumWCET = new double[size];
		double[] sumUF = new double[size];
		double[] completeSumCoefC = new double[size];
		long sumExecutionTime = 0;
		double sumUtilizationFactor = 0;
		double c = 0;
		int j = 0;
		for (Actor actor: partition.getPriorityOrderedActors()) {
			sumWCET[j] = sumExecutionTime;
			sumUF[j] = sumUtilizationFactor;
			completeSumCoefC[j] = c;
			
			long wcet = actor.getWcet();
			sumExecutionTime += wcet;
			double coef = actor.getCoefficient().getValue();
			double utilizationFactor = wcet / coef;
			sumUtilizationFactor += utilizationFactor;
			c += wcet * utilizationFactor;
			
			++j;
		}
		
		/* get the maximum bounds found (lower and upper) */
		long inf = 0, sup = 0;
		int i = 0;
		for (Actor actor: partition.getPriorityOrderedActors()) {
			Pair<Long> pair = reduceSearchSpaceActor(actor, i, sumWCET, sumUF, completeSumCoefC);
			infSchedulable[i] = pair;
			if (inf < pair.getFirst())  {
				inf = pair.getFirst();
			}
			if (sup < pair.getSecond()) {
				sup = pair.getSecond();
			}
			++i;
		}
		return new Pair<>(inf, sup);
	}

	/**
	 * Compute the response time of each actor and set
	 * its minimum acceptable basis period, if not already
	 * set. The method stops if one the actor response time
	 * is greater than its deadline.
	 * <p>
	 * This method is not described in the Adnan Bouakaz thesis.
	 * 
	 * @param basisPeriod Current instance WCP basis period.
	 * @param partition Actors to work on, being a subset of the
	 * WCP used to build this instance.
	 * @param infSchedulable Map of actors and their minimal needed
	 * basis period to meet their deadline.
	 * @return False if a deadline was missed, true otherwise.
	 */
	private boolean standardRTA(long basisPeriod, Partition partition, Pair<Long>[] infSchedulable) {
		GraphProbData.setPeriods(basisPeriod, partition.getActors());
		int j = 0;
		for (Actor actor: partition.getPriorityOrderedActors()) {
			long infSchedLow = infSchedulable[j].getFirst();
			long infSchedUp = infSchedulable[j].getSecond();
			
			if (basisPeriod >= infSchedUp) {
				continue;
			} else if (basisPeriod < infSchedLow) {
				return false;
			} else {
				long responseTime = new Metrics(partition.getPriorityOrderedActors(), nbProcessor)
											.spResponseTime(actor); 
				if (responseTime > actor.getDeadline()) {
					return false;
				} else {
					infSchedulable[j] = new Pair<>(Math.min(basisPeriod, infSchedLow), basisPeriod);
				}
			}
			++j;
		}
		return true;
	}


	/**
	 * Compute the best possible period according to the
	 * SRTA schedulability test.
	 * <p>
	 * See Adnan Bouakaz thesis p. 98.
	 * <p>
	 * All the partition actor periods are set by this method.
	 * 
	 * @param partition Actors to work on, being a subset of
	 * only one weakly connected component.
	 * @param minBasisPeriod A minimum basis period enforced by caller.
	 * If greater than maxBasisPeriod, then returns {@code incoherentPeriod}.
	 * @param maxBasisPeriod A maximum basis period enforced by caller.
	 * If lower than the lower bound found by RTA anaylisis, then returns {@code incoherentPeriod}. 
	 * @return Period of the basis actor, or {@code incoherentPeriod}
	 * if the set couldn't be scheduled.
	 */
    public long standardSRTA(Partition partition, long minBasisPeriod, long maxBasisPeriod) { 
		if (partition.getActors().isEmpty() || maxBasisPeriod < minBasisPeriod) {
			return incoherentPeriod;
		}

		
		WeaklyConnectedComponent wcp = partition.getWCPs().iterator().next();
		@SuppressWarnings("unchecked")
		Pair<Long>[] infSchedulable = (Pair<Long>[]) new Pair[partition.getNbActors()];
		Pair<Long> p = reduceSearchSpace(partition, infSchedulable);
		long divFactor = wcp.getDivFactor();
		long plb = Math.max(wcp.getPeriodLowerBound(), minBasisPeriod);
		long pub = Math.min(wcp.getPeriodUpperBound(), maxBasisPeriod);
		long lower = p.getFirst(), lowerSafe = p.getSecond();
		double psi = partition.getSymbolicLoad();
		LOGGER.fine("Partition n°" + partition.getID() + 
					"; reduce lower: " + lower + 
					"\treduce upper: "+ lowerSafe +
			    		"\tpsi: " + psi +
					"\tlowerbound: " + plb +
					"\tupperbound: "+ pub);
		long inf = Math.max(Math.max(plb, lower), (long) Math.ceil(psi));
		long sup = Math.min(pub, lowerSafe); 
		long reducedBasisPeriod = (inf + divFactor - 1) / divFactor;
		long basisPeriod = reducedBasisPeriod * divFactor; 
		if (basisPeriod > pub /* could happen with maxBasisPeriod*/ ||
		    basisPeriod < inf /* should not happen */) {
			return incoherentPeriod;
		}
		
		/* dichotomic search */
		long start = reducedBasisPeriod;
		long end = (sup + divFactor - 1) / divFactor;
		StringBuilder sb = new StringBuilder();
		sb.append("Partition n°" + partition.getID() + "; SRTA dichotomy:\n" + 
			  "start: " + start + "\tend: " + end + "\n");
		int numberOfDichotomyDown = 0;
		int numberOfDichotomyUp = 0;
		while (start <= end) {
			long middle = (start + end) / 2;
			long tempT = middle * divFactor;
			if (standardRTA(tempT, partition, infSchedulable)) {
					basisPeriod = tempT;
					end = middle - 1;
					sb.append("hit (go down), affected T: " + tempT + "\tdichotomy middle: " + middle + "\n");
					++numberOfDichotomyDown;
			} else {
					basisPeriod = incoherentPeriod;
					start = middle + 1;
					sb.append("miss (go up), unaffected T: " + tempT + "\tdichotomy middle: " + middle + "\n");
					++numberOfDichotomyUp;
			}
		}
		int numberOfDichotomy = numberOfDichotomyDown + numberOfDichotomyUp;
		sb.append("Number of SRTA dichotomy: " + numberOfDichotomy +" (down: " + numberOfDichotomyDown + ", up: " + numberOfDichotomyUp + ")\n");
		LOGGER.fine(sb.toString());

		GraphProbData.setPeriods(basisPeriod, partition.getActors());
		return basisPeriod;
	}

	/**
	 * Compute the best possible period according to the
	 * PSRTA schedulability test, only for a connected
	 * graph.
	 * <p>
	 * All the actor periods are set by this method.
	 *
	 * @todo
	 * Link to the thesis.
	 * 
	 * @return Period of the basis actor, or {@code incoherentPeriod}
	 * if the set couldn't be scheduled.
	 */
	public long standardPSRTA() {
		long maxT = 0L;
		for (Partition partition: gpr.getMappedPartitions().values()) {
			if (partition.getActors().isEmpty()) {
				continue;
			}
			long basisPeriod = standardSRTA(partition, maxT, Actor.MAX_PERIOD);
			if (basisPeriod == incoherentPeriod) {
				return basisPeriod;
			}
			if (basisPeriod > maxT) {
				maxT = basisPeriod;
			}
		}
		GraphProbData.setPeriods(maxT, gpr.getActors());
		return maxT;
	}	
	
}
