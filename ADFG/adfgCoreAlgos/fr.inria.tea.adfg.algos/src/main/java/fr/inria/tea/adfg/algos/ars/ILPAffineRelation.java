/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.ars;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jgrapht.graph.DirectedMultigraph;
import org.jgrapht.graph.SimpleGraph;

import de.vandermeer.asciitable.AsciiTable;
import fr.inria.tea.adfg.algos.common_base.AbstractSolver;
import fr.inria.tea.adfg.algos.common_base.AdfgInternalException;
import fr.inria.tea.adfg.algos.common_base.PrettyTable;
import fr.inria.tea.adfg.algos.common_data_rep.Actor;
import fr.inria.tea.adfg.algos.common_data_rep.AffineRelation;
import fr.inria.tea.adfg.algos.common_data_rep.Channel;
import fr.inria.tea.adfg.algos.common_data_rep.GraphProbData;
import fr.inria.tea.adfg.algos.maths.BasicArithmetic;
import fr.inria.tea.adfg.algos.maths.Fraction;
import fr.inria.tea.adfg.algos.maths.Pair;
import fr.inria.tea.adfg.algos.maths.UPIS;
import lpsolve.LpSolve;
import lpsolve.LpSolveException;

/**
 * This class in the affine relation synthesis
 * is in charge of calling the ILP solver to compute
 * affine relations' phases.
 *  
 * @author Adnan Bouakaz
 * @author Alexandre Honorat
 */
class ILPAffineRelation extends AbstractSolver {

	private static final Logger LOGGER = Logger.getLogger("AdfgLog");	 
	
	// variables to contain the generated constraints (LP FORMAT)
	private StringBuilder subjectTo, minimize, bounds, general;

	private GraphProbData gpr;
	private SimpleGraph<Actor, Object> undirectedGraph;
	private DirectedMultigraph<Actor,Channel> adfgGraph;
	private DirectedMultigraph<Actor, AffineRelation> affineGraph;
	
	private Set<List<Actor>> cycleBasis;
	
	/**
	 * @param gpr Graph Problem Representation.
	 * @param cycleBasis Basis of the cycles.
	 */
	protected ILPAffineRelation(GraphProbData gpr, Set<List<Actor>> cycleBasis) {
		super("ARS", "ILP-ARS.lp");
		this.gpr = gpr;
		undirectedGraph = gpr.getUndirectedGraph();
		adfgGraph = gpr.getAdfgGraph();
		affineGraph = gpr.getAffineGraph();
		
		this.cycleBasis = cycleBasis;
	}

	/**
	 * Solve the problem regarding to the current properties.
	 * Compute affine relations' phases.
	 * <p>
	 * This method should follow immediately the object construction,
	 * any changes on GPR between constructor call and this method call
	 * could lead to inconsistencies.
	 * 
	 */
	@Override
	protected void solving() {
		
		if (affineGraph.vertexSet().isEmpty()) {
			LOGGER.info("There is no channels, so no ILP solver call is done for Affine Relation synthesis.");
			return;
		}
		
		subjectTo = new StringBuilder("\n/* Subject To */\n\n/* -- cycles constraints */\n\n");
		minimize = new StringBuilder("Minimize: \n");
		bounds = new StringBuilder("\n/* Bounds */\n");
		general = new StringBuilder("\n/* General */\n");

		generateILP();
		
		try (FileWriter lpProblem = new FileWriter(fileName)) {
			lpProblem.write(minimize.toString()); 
			lpProblem.write(subjectTo.toString());
			lpProblem.write(bounds.toString()); 
			lpProblem.write(general.toString());
		} catch (IOException e) {
			throw new AdfgInternalException("Problem during "+name+" ILP file wrting.", e);
		}
		
		LpSolve ilp = solveILP(LOGGER);
		
		/* save the solution */
		Map<String,Integer> varsPhi = new HashMap<>();
		Map<String,Long> varsh = new HashMap<>();
		Map<String,Long> varsC = new HashMap<>();
		try {						
			double[] vals = ilp.getPtrVariables();
			for (int i = 0; i < vals.length; ++i) {
				String varName = ilp.getColName(i+1); 
				long varValue = Math.round(vals[i]); 
				if (varName.charAt(0) == 'p') {
					varsPhi.put(varName, (int) varValue);
				} else if (varName.charAt(0) == 'h') {
					varsh.put(varName, varValue);
				} else if (varName.charAt(0) == 'C') {
					varsC.put(varName, varValue);
				}
			}
		} catch (LpSolveException e) {
			throw new AdfgInternalException("Problem during "+name+" ILP results reading.", e);
		}

		deleteILP(LOGGER, ilp);
		
		StringBuilder sb = new StringBuilder(
				"Exiting:\n========== Affine Relation Synthesis -- ILP solution ==========\n");
		long approximateBufferSize = 0L;
		for (Channel edge: adfgGraph.edgeSet()) {
		    String edgeSizeName = "h_" + edge.getID();
		    long edgeWeightedSize =  varsh.get(edgeSizeName) * edge.getDataSize();
		    approximateBufferSize += edgeWeightedSize;		    
		}
		sb.append("ILP Approximate value of the sum of buffer sizes = " + approximateBufferSize + "\n");


		AsciiTable atAR = PrettyTable.create();
		atAR.addRule();
		atAR.addRow("src --> tgt", "(n, phi, d)");
		atAR.addStrongRule();
		for (Object o: undirectedGraph.edgeSet()) {
			Actor src = undirectedGraph.getEdgeSource(o);
			Actor dest = undirectedGraph.getEdgeTarget(o);
			int srcID = src.getID(), destID = dest.getID();
			/* save phis in affineGraph */
			String varphi = "phi_" + srcID + "_" + destID;
			int phi = varsPhi.get(varphi+"_pos") - varsPhi.get(varphi+"_neg");
			AffineRelation r = affineGraph.getEdge(src, dest);
			r.setPhi(phi);
			affineGraph.getEdge(dest, src).setPhi(-phi);
			/* print */
			String arName = src.getName()+" --> "+dest.getName();
			atAR.addRow(arName, r);
		}
		atAR.addRule();

		AsciiTable atCh = PrettyTable.create();
		atCh.addRule();
		atCh.addRow("src --> tgt", "Channel name", "init/size");
		atCh.addStrongRule();
		for (Channel edge: adfgGraph.edgeSet()) {
			Actor src = adfgGraph.getEdgeSource(edge), dest = adfgGraph.getEdgeTarget(edge);
			long init = varsC.get("C_" + edge.getID());
			long size = varsh.get("h_" + edge.getID());
			String arName = src.getName()+" --> "+dest.getName();
			atCh.addRow(arName, edge.getName(), init + "/" + size);
		}
		atCh.addRule();
		LOGGER.info(sb.toString() + atAR.render() + "\n" + atCh.render());
		
	}

	/**
	 * Generate the ILP strings.
	 */
	private void generateILP() {
		/* consistency constraints */
		for (List<Actor> cycle: cycleBasis) {
			generateCycleConstraints(cycle);
		}
		
		/* Overflow and underflow constraints */
		String channelInfos = "Channel parameters:\n";
		AsciiTable atCC = PrettyTable.create();
		atCC.addRule();
		atCC.addRow("Channel name [phi]", "(prod., cons. linear bounds)", 
					"(p., c. firing ratio)", "(affine relation)");
		atCC.addStrongRule();
		subjectTo.append("\n/* -- channels constraints */\n\n");
		for (Channel edge: adfgGraph.edgeSet()) {
			Actor src = adfgGraph.getEdgeSource(edge), dest = adfgGraph.getEdgeTarget(edge); 
			AffineRelation r = affineGraph.getEdge(src, dest);
			generateChannelConstraints(edge, src, dest, r, atCC);
			subjectTo.append("\n");
		}
		atCC.addRule();
		LOGGER.fine(channelInfos + atCC.render());
		
		/* Add phi variables */
		subjectTo.append("\n/* -- dummy phi constraints */\n\n");
		for (Object o: undirectedGraph.edgeSet()) {
			Actor src = undirectedGraph.getEdgeSource(o);
			Actor dest = undirectedGraph.getEdgeTarget(o);
			int srcID = src.getID(), destID = dest.getID();
			String varphi1 = "phi_" + srcID + "_" + destID;
			String varphi2 = "phi_" + destID + "_" + srcID;
			general.append("int "+varphi1+"_pos;\n");
			general.append("int "+varphi1+"_neg;\n");
			general.append("int "+varphi2+"_pos;\n");
			general.append("int "+varphi2+"_neg;\n");
			subjectTo.append(varphi1+"_pos = "+varphi2+"_neg;\n");
			subjectTo.append(varphi2+"_pos = "+varphi1+"_neg;\n");
		}
		
		minimize.deleteCharAt(minimize.length()-1).append(";\n"); 
	}
	
	
	/**
	 * Generate ILP constraints for a relation between two actors. Append
	 * the constraints to the related class members.
	 * <p>
	 * See Adnan Bouakaz thesis p. 72-78, section 2.3.3 for SP and
	 * section 2.3.4 for EDF, underflow and overflow constraints.
	 * 
	 * @param edge Between the two actors.
	 * @param src Source actor.
	 * @param dest Destination actor.
	 * @param r Between the two actors.
	 * @param channelInfos Log container to contribute to.
	 */
	private void generateChannelConstraints(Channel edge, Actor src, Actor dest, AffineRelation r, 
											 AsciiTable channelInfos) {
		UPIS prate = edge.getProduce(), crate = edge.getConsume();
		/* initial tokens */
		boolean enforceLCM = gpr.getUserParams().isForceTokenLCM();
		general.append("int C_"+edge.getID()+";\n");
		if (edge.getInitial() != Channel.UNDEFINED) {
			subjectTo.append(" C_"+edge.getID()+" = "+edge.getInitial()+";\n");
		} else if (enforceLCM) {
			general.append("int lcm_"+edge.getID()+";\n");
			subjectTo.append("C_"+edge.getID()+" = "+
								edge.getRatesLCM()+" lcm_"+edge.getID()+";\n");
		}

		/* size */
		general.append("int h_"+edge.getID()+";\n");
		if (edge.getSize() != Channel.UNDEFINED) {
			subjectTo.append("h_"+edge.getID()+" = "+edge.getSize()+";\n");
		}
		long minSize = Math.max(prate.constantBounds().getSecond(), crate.constantBounds().getSecond());
		bounds.append("h_"+edge.getID()+" >= " + minSize + ";\n");
		subjectTo.append("C_"+edge.getID()+" - h_"+edge.getID()+" <= 0;\n");

		String varphi = "phi_"+src.getID()+"_"+dest.getID();

		Pair<Fraction> pLB = prate.linearBounds(), cLB = crate.linearBounds();
		Fraction pFR = prate.getFiringRatio(), cFR = crate.getFiringRatio();
		if (LOGGER.isLoggable(Level.FINE)) {
			channelInfos.addRow(edge.getName()+" ["+varphi+"]", "("+pLB+", "+cLB+")", "("+pFR+", "+cFR+")", r);
		}
		
		minimize.append(" "+edge.getDataSize()+" h_"+edge.getID()+" +");
		long ceilPhiRatio = (long) edge.getDataSize() * ((prate.getSumPeriod() + r.getD() - 1) / r.getD());
		//int floorPhiRatio = prate.getSumPeriod() / r.getD();
		minimize.append(" "+ceilPhiRatio+" "+varphi+"_pos"+" + "+ceilPhiRatio+" "+varphi+"_neg"+" +");

		
		/* overflow and underflow subjectTo */
		Fraction over = gpr.getUOequation(src, dest, r, prate, crate, true);
		writeUOequations(over, crate, r.getD(), Integer.toString(edge.getID()), varphi, true);			
		Fraction under = gpr.getUOequation(src, dest, r, prate, crate, false);
		writeUOequations(under, prate, r.getN(), Integer.toString(edge.getID()), varphi, false);
	}

	/**
	 * Write overflow or underflow equation in the linear problem constraints
	 * ({@code subjectTo} class field).
	 * <p>
	 * See Adnan Bouakaz thesis p. 72-78, section 2.3.3 for SP and
	 * section 2.3.4 for EDF, underflow and overflow constraints.
	 * 
	 * @param uo Constant part of the equation (lambdas and n/d).
	 * @param rate Production or consumption rate.
	 * @param nd The considered affine relation coefficient (n or d).
	 * @param edgeID ID of the considered channel.
	 * @param phiName Complete phi name ("p_x_y").
	 * @param isOver True if overflow equation, false otherwise (underflow).
	 */
	private void writeUOequations(Fraction uo, UPIS rate, int nd, 
								  String edgeID, String phiName, boolean isOver) {
		long common = BasicArithmetic.funcLCM(nd* rate.getPeriodLength(), uo.getDenominator());
		long phiRatio = (common*rate.getSumPeriod()) / (rate.getPeriodLength()*nd);
		if (isOver) {			
			subjectTo.append(common + " h_" + edgeID + " >= ");
		} else {
			subjectTo.append("0 <= ");
		}
		subjectTo.append(phiRatio + " " + phiName + "_pos ");
		subjectTo.append("- " + phiRatio + " " + phiName + "_neg ");
		subjectTo.append(" + " + common + " C_" + edgeID);			
		
		subjectTo.append(" + " + (common*uo.getNumerator() / uo.getDenominator()) +" ;\n");

	}
	
	
	/**
	 * Check consistency and generate constraints for an affine relation cycle.
	 * Append the constraints to the related class members.
	 * <p>
	 * See Adnan Bouakaz thesis p. 70, proposition 2.8 .
	 * 
	 * @param cycle Cycle to check.
	 */
	private void generateCycleConstraints(List<Actor> cycle) {
		int cycleSize = cycle.size() - 1;
		if (cycleSize <= 2) {
			return;
		}
		
		String[] varsPhi = new String[cycleSize]; 
		long[]  coefsPhi = new long[cycleSize]; 
		for (int i = 0; i < coefsPhi.length; ++i) {
			coefsPhi[i] = 1;
		}
		int nbPhi = 0;
		long mulN = 1, mulD = 1;

		// cycle is redundant (last == first)
		Iterator<Actor> nextActor = cycle.iterator();
		Actor dest = nextActor.next();
		while (nextActor.hasNext()) {
			Actor src = dest;
			dest = nextActor.next();
			AffineRelation r = affineGraph.getEdge(src, dest);
			String varphi = "phi_"+src.getID()+"_"+dest.getID();
			varsPhi[nbPhi] = varphi;

			for (int i = 0; i < nbPhi; ++i) {
				coefsPhi[i] *= r.getN();
			}
			for (int i = nbPhi+1; i < coefsPhi.length; ++i) {
				coefsPhi[i] *= r.getD();
			}
			mulN *= r.getN();
			mulD *= r.getD();
			long g = BasicArithmetic.funcGCD(mulN, mulD); 
			mulN /= g; 
			mulD /= g;
			g = coefsPhi[0]; 
			for (int i = 0; i < coefsPhi.length; ++i) {
				g = BasicArithmetic.funcGCD(g, coefsPhi[i]);
			}
			for (int i = 0; i < coefsPhi.length; ++i) {
				coefsPhi[i] = coefsPhi[i] / g;
			}
			++nbPhi;
		}
		
		if (mulN != mulD) {
			throw new AdfgInternalException("Some cycles do not satisfy consistency Part 1."); 
		}
		StringBuilder constraint = new StringBuilder();
		constraint.append(coefsPhi[0] + " "+varsPhi[0] + "_pos ");
		constraint.append("- " + coefsPhi[0] + " "+varsPhi[0] + "_neg ");
		for (int i = 1; i < varsPhi.length; ++i) { 
			constraint.append("+ " + coefsPhi[i] + " "+varsPhi[i] + "_pos ");
			constraint.append("- " + coefsPhi[i] + " "+varsPhi[i] + "_neg ");
		}
		constraint.append(" = 0;\n"); 
		subjectTo.append(constraint.toString());
	}


}
