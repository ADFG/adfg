/**
 * This package contains the main classes to use as an API.
 */
package fr.inria.tea.adfg.algos;