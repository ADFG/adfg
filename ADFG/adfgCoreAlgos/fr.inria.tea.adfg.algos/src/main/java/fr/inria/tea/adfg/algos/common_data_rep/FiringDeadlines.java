/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.algos.common_data_rep;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.jgrapht.graph.DirectedMultigraph;

import fr.inria.tea.adfg.algos.maths.BasicArithmetic;
import fr.inria.tea.adfg.algos.maths.Fraction;
import fr.inria.tea.adfg.algos.maths.Pair;
import fr.inria.tea.adfg.algos.maths.UPIS;

/** 
 * The symbolic deadline class store all deadlines
 * and dependencies relative to an actor in a given
 * affine relation graph.
 * 
 * @author Adnan Bouakaz
 * @author Alexandre Honorat
 */
public class FiringDeadlines {
	
	private class Dependency implements Comparable<Dependency> {
		Actor actor;
		int job;
		Dependency(Actor a, int i) {
			actor = a;
			job = i;
		}
		
		@Override
		public String toString() {
			return "("+actor.getName()+", "+job+")";
		}
		
		@Override
		public int compareTo(Dependency arg0) {
			return actor.getName().compareTo(arg0.actor.getName());
		}
	}

	private DirectedMultigraph<Actor, AffineRelation> affineGraph;
	private Actor actor;
	private Fraction[] deadlines;
	private Map<Integer, Set<Dependency>> dependencies;
	private int prefixLength, periodLength;

	
	/**
	 * Construct a new symbolic deadline instance  specific to the 
	 * actor parameter. The affine relation graph will be used to 
	 * compute deadlines and dependencies for this actor.
	 * 
	 * @param gpr Graph and problem data.
	 * @param wcp Weakly connected component to consider.
	 * @param actor Actor for which deadlines are computed.
	 */
	public FiringDeadlines(GraphProbData gpr, WeaklyConnectedComponent wcp, Actor actor) {
		this.affineGraph = gpr.getAffineGraph();
		this.actor = actor;

		Set<AffineRelation> neighbors = affineGraph.outgoingEdgesOf(actor);

		/* compute the number of jobs in the hyper period */
		int phase = 0, period = 1;
		for (AffineRelation r: neighbors) {
			period = (int) BasicArithmetic.funcLCM(period, r.getD());
			int ph = 0;
			if (r.getPhi() > 0) {
				ph = (int) Math.ceil((double) r.getPhi() / r.getN());
			}
			phase = Math.max(phase, ph);
		}

		periodLength = period; 
		prefixLength = phase;
		deadlines = new Fraction[prefixLength + periodLength];
		dependencies = new TreeMap<>();

		for (int i = 0; i < deadlines.length; ++i) {
			deadlines[i] = new Fraction(1,1);
		}

		/* compute deadline dependencies */
		for (AffineRelation r: neighbors) {
			Actor neighbor = affineGraph.getEdgeTarget(r); 
			if (r.isNoAdjust() || !Actor.onSameProcessor(actor, neighbor)) {
				continue;			
			}
			
			Pair<UPIS> encoding = r.binaryEncoding();
			UPIS pClock = encoding.getFirst(), cClock = encoding.getSecond(); 
			// there are only 0 and 1 in the UPIS, so there is no point to use long (for now).
			int shift = (int) Math.max(0, prefixLength - pClock.getSumPrefix());
			int unfold = periodLength / (int) pClock.getSumPeriod();
			if (shift > 0) {
				pClock = pClock.shift(shift); 
				cClock = cClock.shift(shift);
			}
			if (unfold > 1) {
				pClock = pClock.unfold(unfold);
				cClock = cClock.unfold(unfold);
			}
			
			/* construct dependencies */
			int i = 0, p = 0, c = 0;
			while (i < pClock.getLength()) {
				if (pClock.elementAt(i) == 1 && cClock.elementAt(i) == 0) {
					++p;
					if (pClock.elementAt(i+1) == 0 && cClock.elementAt(i+1) == 1) {
						addPrecedenceConstraint(p, neighbor, c+1);
					}
				} else if (pClock.elementAt(i) == 1 && cClock.elementAt(i) == 1) { 
					++p;
					++c;
					if (pClock.elementAt(i+1) == 1 && cClock.elementAt(i+1) == 0) {
						addPrecedenceConstraint(p, neighbor, c);
					} else if (pClock.elementAt(i+1) == 1 && cClock.elementAt(i+1) == 1) {
						if (actor.getID() > neighbor.getID()) {
							addPrecedenceConstraint(p, neighbor, c);
						} else {
							addPrecedenceConstraint(p, neighbor, c+1);							
						}
					} else if (pClock.elementAt(i+1) == 0 && cClock.elementAt(i+1) == 1) {
						addPrecedenceConstraint(p, neighbor, c+1);
					}
				} else if (cClock.elementAt(i) == 1) {
					++c;
				}
				++i;
			}
		}
	}

	/**
	 * Add a precedence constraint in the instance dependencies map.
	 * 
	 * @param index Firing index of the instance actor.
	 * @param neighbor Neighbor actor (toward the dependency is directed).
	 * @param nindex Firing index of the neighbor actor.
	 */
	private void addPrecedenceConstraint(int index, Actor neighbor, int nindex) {
		if (dependencies.get(index) == null) {
			dependencies.put(index, new TreeSet<Dependency>());
		}
		dependencies.get(index).add(new Dependency(neighbor, nindex));		
	}
	
	
	/**
	 * Delta of an affine relation w.r.t
	 * job parameters.
	 * <p>
	 * This delta is not exactly the one presented in the
	 * Adnan Bouakaz thesis p. 105, but it is a part of it.
	 * 
	 * @param r Affine relation.
	 * @param p Job.
	 * @param c A job dependency of {@code p}.
	 * @return Delta of the affine relation,
	 * weighted by job {@code p} and 
	 * job dependency {@code c}.
	 */
	private static int delta(AffineRelation r, int p, int c) {
		return r.getD()*(c-1)+r.getPhi()-r.getN()*(p-1);
	}

	/**
	 * Get deadline of a specific firing for the current
	 * actor symbolic deadline.
	 * 
	 * @param i Index of the firing, starting at 0.
	 * @return Deadline fraction representation.
	 */
	public Fraction get(int i) {
		if (i < deadlines.length) {
			return deadlines[i];
		}
		int index = (i-prefixLength) % (periodLength);
		return deadlines[index+prefixLength];
	}

	/**
	 * Set deadline  of a specific firing for the current
	 * actor symbolic deadline.
	 * 
	 * @param i Index of the firing, starting at 0.
	 * @param f New deadline value.
	 */
	public void set(int i, Fraction f) {
		if (i < deadlines.length) {
			deadlines[i] = f;
		} else { 
			int index = (i-prefixLength) % (periodLength);
			deadlines[index+prefixLength] = f;
		}
	}

	/**
	 * Update actor deadlines regarding to the 
	 * job dependencies' deadlines. 
	 * <p>
	 * This algorithm corresponds to pages
	 * 104-105 in the Adnan Bouakaz thesis.
	 * Delta(i,k) in the thesis corresponds in the code
	 * to {@code new Fraction(delta(r, job, d.job), r.getN())}.
	 * 
	 * @return True if a modification occurred,
	 * false otherwise.
	 */
	public boolean update() {
		boolean change = false;
		for (Entry<Integer, Set<Dependency>> entry: dependencies.entrySet()) {
			int job = entry.getKey();
			Fraction min = get(job);
			for (Dependency d: entry.getValue()) {
				Actor neighbor = d.actor;
				AffineRelation r = affineGraph.getEdge(actor, neighbor);
				Fraction alpha = neighbor.getFiringDeadlines().get(d.job-1);
				Fraction tempf = new Fraction(delta(r, job, d.job), r.getN());
				tempf.setADD(new Fraction(r.getD(), r.getN()).setMUL(alpha));
				if (tempf.isLess(min)) {
					change = true;
					min = tempf;
				}
			}
			set(job, min);			
		}
		return change;
	}

	/**
	 * Update all firing deadlines to the minimum
	 * deadline value.
	 * <p>
	 * Not called.
	 * 
	 * @return True if a modification occurred, 
	 * false otherwise.
	 */
	public boolean updateMinimum() {
		boolean change = false;
		Fraction min = getMinimum();
		for (int i = 0; i < deadlines.length; i++) {
			Fraction tempf = get(i);
			if (min.isLess(tempf)) {
				change = true;
				set(i, new Fraction(min));
			}
		}
		return change;
	}

	/**
	 * Get minimum deadline over all actor firings'
	 * deadlines.
	 * 
	 * @return The minimum deadline (as a fraction).
	 */
	public Fraction getMinimum() {
		Fraction min = new Fraction(1,1);
		for (Fraction f: deadlines) {
			if (f.isLess(min)) {
				min = f;
			}
		}
		return min;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < prefixLength; ++i) {
			sb.append(deadlines[i] + " "); 
		}
		sb.append("(");
		for (int i = prefixLength; i < deadlines.length; ++i) {
			sb.append(deadlines[i] + " ");
		}
		sb.append(")");
		return sb.toString();
	}

}
