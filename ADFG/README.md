# ADFG

This folder contains the source code of the ADFG scheduler/analyzer.
This folder can be part of the Eclipse repository.
Please read instructions from root folder to have further details.

## Instructions

Run ```./install-projects.sh```.

This will build and install the Java part of ADFG, 
and its dependencies.

This script is called automatically by the upper
script ```install.sh``` located in the root folder.

