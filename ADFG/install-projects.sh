#!/bin/sh

## $1 can be the "clean" directive

# Projects compilation

if [ "$1" = "clean" ] ; then
    directive="clean"
    mess="Cleaning"
else
    directive="install"
    mess="Compiling and installing"
fi

echo ""
echo "==> $mess core modules ..."
echo ""
mvn -q -f adfgCoreAlgos/aggregator/pom.xml ${directive} || exit 1
echo ""
echo "==> $mess Eclipse plugins ..."
echo ""
mvn -q -f adfgEclipsePlugins/aggregator/pom.xml ${directive} || exit 1
if [ "$1" = "nam" ] ; then
    echo ""
    echo "==> $mess Nam's module ..."
    echo ""
    mvn -q -f adfgNamExtension/aggregator/pom.xml ${directive} || exit 1
fi
