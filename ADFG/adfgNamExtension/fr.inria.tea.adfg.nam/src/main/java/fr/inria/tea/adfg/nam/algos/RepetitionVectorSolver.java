/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.nam.algos;

import java.util.Collection;
import java.util.HashSet;
import java.util.logging.Logger;

import org.jgrapht.graph.DirectedMultigraph;

import fr.inria.tea.adfg.algos.common_data_rep.Channel;
import fr.inria.tea.adfg.algos.common_data_rep.GraphProbData;
import fr.inria.tea.adfg.algos.maths.BasicArithmetic;
import fr.inria.tea.adfg.algos.maths.Fraction;;

public class RepetitionVectorSolver {

	private static final Logger LOGGER = Logger.getLogger("AdfgLog");
	private GraphProbData gpr;
	private DirectedMultigraph<ActorPEG, Channel> graphPEG;
	
	public RepetitionVectorSolver(GraphProbData gpr) {
		this.gpr = gpr;
		this.graphPEG = ExpansionRateSolver.transformGraphActorToGraphActorPEG(gpr.getAdfgGraph());
	}

	/**
	 * Compute the repetition vector of acyclic-SDF by applying the DFS based
	 * algorithm in Battacharyya et al., 1996
	 */
	public void solving() {

		for (ActorPEG actor : graphPEG.vertexSet()) {
			if (actor.getRepetitionCount().getNumerator() == 0 && actor.getRepetitionCount().getDenominator() == 1) {
				setReps(actor, new Fraction(1, 1));
			}
		}

		long x = 1;
		for (ActorPEG actor : graphPEG.vertexSet()) {
			x = BasicArithmetic.funcLCM(x, actor.getRepetitionCount().getDenominator());
		}

		for (ActorPEG actor : graphPEG.vertexSet()) {
			long new_num = actor.getRepetitionCount().getNumerator() * x;
			long old_denum = actor.getRepetitionCount().getDenominator();
			actor.setRepetitionCount(new Fraction(new_num, old_denum));
		}
		//TODO: Fix printDataSubRoutine
		//printDataSubRoutine(gpr);
	}

	public void setReps(ActorPEG actor, Fraction n) {
		actor.setRepetitionCount(n);

		Collection<Channel> predDeps = new HashSet<Channel>();
		Collection<Channel> postDeps = new HashSet<Channel>();

		for (Channel c : graphPEG.edgeSet()) {
			ActorPEG src = graphPEG.getEdgeSource(c);
			ActorPEG snk = graphPEG.getEdgeTarget(c);
			if (src.getName().equals(actor.getName())) {
				postDeps.add(c);
			}
			if (snk.getName().equals(actor.getName())) {
				predDeps.add(c);
			}
		}

		for (Channel alpha : postDeps) {
			ActorPEG snk = graphPEG.getEdgeTarget(alpha);
			if (snk.getRepetitionCount().getValue() == 0) {
				long prd = alpha.getProduce().elementAt(0);
				long cns = alpha.getConsume().elementAt(0);
				// LOGGER.info("+Value of prd:" + prd +
				// "-- Value of cns:" + cns +
				// "-- Value of n:" + n.getNumerator() + "/" +
				// n.getDenominator());
				setReps(snk, new Fraction(n.getNumerator() * prd, n.getDenominator() * cns));
			}
		}

		for (Channel alpha : predDeps) {
			ActorPEG src = graphPEG.getEdgeSource(alpha);
			if (src.getRepetitionCount().getValue() == 0) {
				long prd = alpha.getProduce().elementAt(0);
				long cns = alpha.getConsume().elementAt(0);
				// LOGGER.info("-Value of prd:" + prd +
				// "-- Value of cns:" + cns +
				// "-- Value of n:" + n.getNumerator() + "/" +
				// n.getDenominator());
				setReps(src, new Fraction(n.getNumerator() * cns, n.getDenominator() * prd));
			}
		}
	}

	/**
	 * TODO: Fox AsciiTable
	 * @param graphPEG
	 */
	//	public static void printDataSubRoutine(DirectedMultigraph<ActorPEG, Channel> graphPEG) {
	//		StringBuilder sb = new StringBuilder();
	//		V2_AsciiTable atAR = new V2_AsciiTable();
	//		atAR.addRule();
	//		atAR.addRow("Actor Name", "Resp Num", "Resp Denum");
	//		atAR.addStrongRule();
	//		for (ActorPEG actor : graphPEG.vertexSet()) {
	//			atAR.addRow(actor.getName(), actor.getRepetitionCount().getNumerator(),actor.getRepetitionCount().getDenominator());
	//		}
	//		atAR.addRule();
	//		LOGGER.info(sb.toString() + PrettyTableRenderer.getRenderer().render(atAR));
	//	}
}
