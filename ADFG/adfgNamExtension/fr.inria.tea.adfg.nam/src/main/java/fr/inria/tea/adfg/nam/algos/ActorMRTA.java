/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.nam.algos;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import fr.inria.tea.adfg.algos.common_base.AdfgExternalException;
import fr.inria.tea.adfg.algos.common_data_rep.Actor;
import fr.inria.tea.adfg.algos.maths.Fraction;

@SuppressWarnings("serial")
public class ActorMRTA extends Actor {
	
	//Architectural model
	public long memoryDemand;	
	
	//Analysis model
	public Collection<Actor> predDeps;
	public Collection<Actor> postDeps;
	public Collection<Integer> memoryBanks;
	public Collection<MemoryAccess> memoryAccesses;			
	public int responseTime;
	public int responseL1;
	public int phaseL1;	
	public Fraction repetitionCount;
	
	//Graph sorting
	public long tLevel;
	public long bLevel;
	public boolean visited;
	
	//Graph scheduling
	public boolean scheduled;
	
	/**
	 * 
	 * @return
	 */
	public long getMemoryDemand() {
		return memoryDemand;
	}
	
	/**
	 * 
	 * @param memoryDemand
	 */
	public void setMemoryDemand(long memoryDemand) {
		this.memoryDemand = memoryDemand;
	}
	
	/**
	 * 
	 * @param name
	 * @param wcet
	 * @param userPeriod
	 * @param memoryDemand
	 * @throws AdfgExternalException
	 */
	public ActorMRTA(String name, long wcet, long userPeriod, long memoryDemand) throws AdfgExternalException {
		super(name, wcet, wcet, userPeriod);
		this.memoryDemand = memoryDemand;
		
		this.predDeps = new HashSet<Actor>();			
		this.postDeps = new HashSet<Actor>();
		this.memoryBanks = new HashSet<Integer>();
		this.memoryAccesses = new HashSet<MemoryAccess>();
		this.responseTime = 0;
		this.responseL1 = 0;
		this.phaseL1 = 0;
		this.tLevel = 0;
		this.bLevel = 0;
		this.visited = false;
		this.scheduled = false;
	}
	
	public ActorMRTA(Actor a) {
		super(a,true,true);
		this.predDeps = new HashSet<Actor>();			
		this.postDeps = new HashSet<Actor>();
		this.memoryBanks = new HashSet<Integer>();
		this.memoryAccesses = new HashSet<MemoryAccess>();
		this.responseTime = 0;
		this.responseL1 = 0;
		this.phaseL1 = 0;
		this.tLevel = 0;
		this.bLevel = 0;
		this.visited = false;
		this.scheduled = false;
	}
	

	/**
	 * 
	 */
	//	public ActorMRTA clone(boolean keepPeriod, boolean keepMapping) throws AdfgExternalException {
	//		ActorMRTA actor = new ActorMRTA(this.getName(), this.getWcet(), this.getUserPeriod(), this.getMemoryDemand());
	//		cloneSubroutine(actor, keepPeriod, keepMapping);
	//		return actor;
	//	}		
		

	/**
	 * 
	 * @return
	 */
	public Collection<Actor> getPredDeps() {
		return predDeps;
	}
	
	/**
	 * 
	 * @return
	 */
	public Collection<Actor> getPostDeps() {
		return postDeps;
	}
		
	/**
	 * 
	 * @return
	 */
	public int getResponseTime() {
		return responseTime;
	}

	/**
	 * 
	 * @param responseTime
	 */
	public void setResponseTime(int responseTime) {
		this.responseTime = responseTime;
	}

	/**
	 * 
	 * @return
	 */
	public int getResponseL1() {
		return responseL1;
	}

	/**
	 * 
	 * @param responseL1
	 */
	public void setResponseL1(int responseL1) {
		this.responseL1 = responseL1;
	}

	/**
	 * 
	 * @return
	 */
	public int getPhaseL1() {
		return phaseL1;
	}

	/**
	 * 
	 * @param phaseL1
	 */
	public void setPhaseL1(int phaseL1) {
		this.phaseL1 = phaseL1;
	}
	
	/**
	 * 
	 * @return
	 */
	public Collection<MemoryAccess> getMemoryAccesses() {
		return memoryAccesses;
	}

	/**
	 * 
	 * @param remoteAccesses
	 */
	public void setMemoryAccesses(Collection<MemoryAccess> remoteAccesses) {
		this.memoryAccesses = remoteAccesses;
	}
	
	/**
	 * 
	 * @return
	 */
	public Collection<Integer> getMemoryBanks() {
		return memoryBanks;
	}

	/**
	 * 
	 * @param memoryBanks
	 */
	public void setMemoryBanks(Collection<Integer> memoryBanks) {
		this.memoryBanks = memoryBanks;
	}

	/**
	 * @return the bLevel
	 */
	public long getBLevel() {
		return bLevel;
	}

	/**
	 * @param bLevel the bLevel to set
	 */
	public void setBLevel(long bLevel) {
		this.bLevel = bLevel;
	}

	/**
	 * @return the tLevel
	 */
	public long getTLevel() {
		return tLevel;
	}

	/**
	 * @param tLevel the tLevel to set
	 */
	public void setTLevel(long tLevel) {
		this.tLevel = tLevel;
	}

	/**
	 * @return the visited
	 */
	public boolean isVisited() {
		return visited;
	}

	/**
	 * @param visited the visited to set
	 */
	public void setVisited(boolean visited) {
		this.visited = visited;
	}

	/**
	 * @return the scheduled
	 */
	public boolean isScheduled() {
		return scheduled;
	}

	/**
	 * @param scheduled the scheduled to set
	 */
	public void setScheduled(boolean scheduled) {
		this.scheduled = scheduled;
	}
	
	/***
	 * 
	 * @param actorPEGs
	 * @param name
	 * @return
	 */
	public static ActorMRTA getActorMRTAByName(Set<ActorMRTA> actorMRTAs, String name) {
		for (ActorMRTA a : actorMRTAs) {
			if (a.getName().equals(name)) {
				return a;
			}
		}
		return null;
	}

	//TODO: Remove static keyword
	public static class MemoryAccess {
		private int accessBank;
		private long accessDemand;
		
		public MemoryAccess(int remoteMemoryBank, long remoteMemoryDemand) {
			this.accessBank = remoteMemoryBank;
			this.accessDemand = remoteMemoryDemand;
		}
		
		public int getAccessBank() {
			return accessBank;
		}
		
		public void setAccessBank(int remoteMemoryBank) {
			this.accessBank = remoteMemoryBank;
		}
		
		public long getAccessDemand() {
			return accessDemand;
		}
		
		public void setAccessDemand(long remoteMemoryDemand) {
			this.accessDemand = remoteMemoryDemand;
		}
	}

}
