/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.nam.algos;

import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Logger;

import org.jgrapht.graph.DirectedMultigraph;

import fr.inria.tea.adfg.algos.common_base.AbstractSolver;
import fr.inria.tea.adfg.algos.common_base.AdfgInternalException;
import fr.inria.tea.adfg.algos.common_data_rep.Actor;
import fr.inria.tea.adfg.algos.common_data_rep.Channel;
import fr.inria.tea.adfg.algos.common_data_rep.GraphProbData;

public class ILPAcyclicPartitioning extends AbstractSolver {

	private static final Logger LOGGER = Logger.getLogger("AdfgLog");

	// variables to contain the generated constraints (LP FORMAT)
	private StringBuilder subjectTo, maximize, integer, binary;

	private GraphProbData gpr;
	private DirectedMultigraph<Actor, Channel> adfgGraph;

	public ILPAcyclicPartitioning(GraphProbData gpr) {
		super("AP", "ILP-AP.lp");
		this.gpr = gpr;
		adfgGraph = gpr.getAdfgGraph();
	}

	@Override
	public void solving() {
		subjectTo = new StringBuilder("\n/* Subject To */\n");
		maximize = new StringBuilder("Maximize: \n");
		//bounds = new StringBuilder("\n/* Bounds */\n");
		integer = new StringBuilder("\n/* Integer */\n");
		binary = new StringBuilder("\n/* Binary */\n");

		int nbActors = gpr.getNbActors();
		int nbProcs = gpr.getNbProcessor();

		/*
		 * Objective function
		 */
		for (int s = 1; s <= nbProcs; ++s) {
			for (Channel ch : gpr.getMappedChannels().values()) {
				/*
				 * TODO: use ID instead of name of actor.
				 */
				String src = adfgGraph.getEdgeSource(ch).getName();
				String tar = adfgGraph.getEdgeTarget(ch).getName();

				// String src =
				// Integer.toString(adfgGraph.getEdgeSource(ch).getID());
				// String tar =
				// Integer.toString(adfgGraph.getEdgeTarget(ch).getID());
				maximize.append("+ " + ch.getSize() + " z" + src + tar + s + " ");
				binary.append("bin z" + src + tar + s + ";\n");
			}
		}
		maximize.append(";\n");

		/*
		 * 2.2
		 */
		for (int i = 1; i <= nbActors; ++i) {
			for (int s = 1; s <= nbProcs; ++s) {

				binary.append("bin x" + i + s + ";\n");
				subjectTo.append("+ x" + i + s + " ");
			}
			subjectTo.append("= 1;\n");
		}

		/*
		 * 2.3
		 */
		for (int s = 1; s <= nbProcs; ++s) {
			for (int i = 1; i <= nbActors; ++i) {
				subjectTo.append("+ "
						+ String.format("%.2f",
								((float) gpr.getMappedActors().get(Integer.toString(i)).getWcet())
										/ ((float) gpr.getMappedActors().get(Integer.toString(i)).getUserPeriod()))
						+ " x" + i + s + " ");
			}
			subjectTo.append("<= 1;\n");
		}

		/*
		 * 2.4, 2.5
		 */

		for (int s = 1; s <= nbProcs; ++s) {
			for (Channel ch : gpr.getMappedChannels().values()) {
				/*
				 * TODO: use ID instead of name of actor.
				 */
				String src = adfgGraph.getEdgeSource(ch).getName();
				String tar = adfgGraph.getEdgeTarget(ch).getName();

				subjectTo.append(" z" + src + tar + s + " <= x" + src + s + ";\n");
				subjectTo.append(" z" + src + tar + s + " <= x" + tar + s + ";\n");
			}
		}

		/*
		 * 2.6
		 */
		for (int s = 1; s <= nbProcs; ++s) {
			for (int t = 1; t <= nbProcs; ++t) {
				if (s != t) {
					for (Channel ch : gpr.getMappedChannels().values()) {
						/*
						 * TODO: use ID instead of name of actor.
						 */
						String src = adfgGraph.getEdgeSource(ch).getName();
						String tar = adfgGraph.getEdgeTarget(ch).getName();

						subjectTo.append(" x" + src + s + " + x" + tar + t + " - 1 <= y"+s+t+";\n");
					}
					binary.append("bin y" + s + t + ";\n");
				}				
			}			
		}
		
		/*
		 * 2.7
		 */
		for (int s = 1; s <= nbProcs; ++s) {
			for (int t = 1; t <= nbProcs; ++t) {
				if (s != t) {
					subjectTo.append("p" + s + " - p" + t + " + " + nbProcs + " y" + s + t + " <= " + nbProcs + " - 1;\n");
				}
			}
			integer.append("int p" + s + ";\n");
		}
		
		/*
		 * 2.8
		 */
		for (int s = 1; s < nbProcs; ++s) {
			for (int i = 1; i <= nbActors; ++i) {
				subjectTo.append(" + x" + i + s);
			}
			subjectTo.append(" >= ");			
			for (int i = 1; i <= nbActors; ++i) {
				subjectTo.append(" + x" + i + (s+1));
			}
			subjectTo.append(";\n");
		}

		LOGGER.info("Number of actor: " + nbActors);
		LOGGER.info("Number of processor: " + nbProcs);

		try (FileWriter lpProblem = new FileWriter(fileName)) {
			lpProblem.write(maximize.toString());
			lpProblem.write(subjectTo.toString());
			//lpProblem.write(bounds.toString());
			lpProblem.write(integer.toString());
			lpProblem.write(binary.toString());
		} catch (IOException e) {
			throw new AdfgInternalException("Problem during " + name + " ILP file wrting.", e);
		}

		//LpSolve ilp = solveILP(LOGGER);
	}

}
