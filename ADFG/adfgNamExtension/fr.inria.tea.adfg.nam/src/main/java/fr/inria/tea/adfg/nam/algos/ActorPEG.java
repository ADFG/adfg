/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.nam.algos;

import java.util.Set;

import fr.inria.tea.adfg.algos.common_data_rep.Actor;
import fr.inria.tea.adfg.algos.maths.Fraction;

@SuppressWarnings("serial")
public class ActorPEG extends Actor {

	enum ActorType {
		SRC, FIL, SNK
	}

	enum ActorTemperature {
		HOT, COLD
	}

	// Data rate transfer function
	public double alpha;
	// Closed form
	public double alphaPrime;
	//
	public double gamma;
	// w_i
	public int outputDataRate;
	// l_i
	public int inputDataRate;
	//
	public double ub_i;
	//
	public int duplicity;
	//
	public ActorType type;
	//
	public ActorTemperature temperature;
	//
	public Fraction repetitionCount;
	//
	public int memoryDemand;	
	//
	public int state;

	/***
	 * 
	 * @param a
	 */
	public ActorPEG(Actor a) {
		super(a,true,true);
		this.alpha = 0.0;
		this.alphaPrime = 0.0;
		this.gamma = 0.0;
		this.outputDataRate = 0;
		this.inputDataRate = 0;
		this.ub_i = 0;
		this.duplicity = 1;
		this.repetitionCount = new Fraction(1.0);
		this.type = ActorType.FIL;
		this.temperature = ActorTemperature.COLD;	
		this.state = 0;
	}

	/***
	 * 
	 * @param actorPEGs
	 * @param name
	 * @return
	 */
	public static ActorPEG getActorPEGByName(Set<ActorPEG> actorPEGs, String name) {
		for (ActorPEG a : actorPEGs) {
			if (a.getName().equals(name)) {
				return a;
			}
		}
		return null;
	}

	/***
	 * 
	 */
	@Override
	public String toString() {
		return "(" + this.getName() + ", " + this.getWcet() + ", " + ((this.getPeriod() != UNDEFINED) ? this.getPeriod() : "*") + ", "
				+ ((this.getPhase() != UNDEFINED) ? this.getPhase() : "*") + ", " + ((this.getDeadline() != UNDEFINED) ? this.getDeadline() : "*") + " | "
				+ this.repetitionCount.getNumerator() + " | " + inputDataRate + ", " + outputDataRate + ", " + type + " | "
				+ alphaPrime + ", " + gamma + " | " + ub_i + ", " + duplicity + " | " + temperature + ")";
	}
	
	public int getMemoryDemand() {
		return memoryDemand;
	}

	public void setMemoryDemand(int memoryDemand) {
		this.memoryDemand = memoryDemand;
	}

	public Fraction getRepetitionCount() {
		return repetitionCount;
	}

	public void setRepetitionCount(Fraction repetitionCount) {
		this.repetitionCount = repetitionCount;
	}
	
	
}
