/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.nam.algos;

import java.util.ArrayList;
import java.util.logging.Logger;

import org.jgrapht.graph.DirectedMultigraph;

import fr.inria.tea.adfg.algos.common_data_rep.Actor;
import fr.inria.tea.adfg.algos.common_data_rep.Channel;
import fr.inria.tea.adfg.algos.common_data_rep.GraphProbData;

public class SchedSDFMinBuffer {

	private static final Logger LOGGER = Logger.getLogger("AdfgLog");

	// GraphProbData
	private GraphProbData gpr;
	ArrayList<ActorPEG> fireableActors;
	ArrayList<ActorPEG> deferableActors;
	private DirectedMultigraph<ActorPEG, Channel> graphPEG;
	
	/**
	 * Store graph problem useful information.
	 * 
	 * @param gpr
	 *            Graph to work with.
	 */
	public SchedSDFMinBuffer(GraphProbData gpr) {
		this.gpr = gpr;
		this.graphPEG = ExpansionRateSolver.transformGraphActorToGraphActorPEG(gpr.getAdfgGraph());
		LOGGER.info("PEG: Transformed " + this.graphPEG.vertexSet().size() + " nodes and "
				+ this.graphPEG.edgeSet().size() + " edges");
	}

	public void computeMinBufferSched() {
		int i = 1;
		while(!isSchedComplete()) {			
			fireableActors = new ArrayList<ActorPEG>();
			deferableActors = new ArrayList<ActorPEG>();

			// Compute fireable and deferable actors;
			for (ActorPEG a : graphPEG.vertexSet()) {
				if (isFireable(a))
					fireableActors.add(a);
				if (isDeferable(a))
					deferableActors.add(a);
			}

			LOGGER.info("Iteration:" + i);
			LOGGER.info("Fireable Actors:");
			LOGGER.info(fireableActors.toString());
			LOGGER.info("Deferable Actors:");
			LOGGER.info(deferableActors.toString());

			fireableActors.removeAll(deferableActors);
			//Fire all actor in Fireable Set
			if (!fireableActors.isEmpty()) {
				while (!fireableActors.isEmpty()) {
					ActorPEG f = fireableActors.get(fireableActors.size()-1);
					fireActor(f);
					fireableActors.remove(f);
					LOGGER.info("Fire:" + f.getName());
				}
			} else {
				LOGGER.info("All deferable");
				if(!deferableActors.isEmpty()){
					ActorPEG f = deferableActors.get(0);									
					int totalToken = getTotalToken();
					int minTotalToken = totalToken + getTokenAfterFiring(deferableActors.get(0));
					for(ActorPEG d : deferableActors){						
						int nextTotalToken = totalToken + getTokenAfterFiring(d);
						if(nextTotalToken < minTotalToken) {
							minTotalToken = nextTotalToken;
							f = d;
						}
					}
					fireActor(f);
					LOGGER.info("Fire:" + f.getName());
				}
			}
			
			i++;
		}
		
		LOGGER.info("======Buffer size requirement======");
		for (Channel c : gpr.getAdfgGraph().edgeSet()){
			LOGGER.info(c.getName() + " - " + c.getSize());
		}
	}

	public boolean isFireable(ActorPEG a) {
		if (a.state == a.getRepetitionCount().getNumerator()) {
			return false;
		}

		boolean fireable = true;
		for (Channel c : graphPEG.edgeSet()) {
			Actor dest = graphPEG.getEdgeTarget(c);
			//TODO: channel state ?
//			if (dest.getName().equals(a.getName())) {
//				if (c.getConsume().elementAt(0) > c.state) {
//					fireable = false;
//				}
//			}
		}
		return fireable;
	}

	public boolean isDeferable(ActorPEG a) {
		if (a.state == a.getRepetitionCount().getNumerator()) {
			return false;
		}

		boolean deferable = false;
		for (Channel c : graphPEG.edgeSet()) {
			ActorPEG src = graphPEG.getEdgeSource(c);
//			if (src.getName().equals(a.getName())) {
//				if (c.getConsume().elementAt(0) < c.state) {
//					deferable = true;
//				}
//			}
		}
		return deferable;
	}

	public boolean isSchedComplete() {
		boolean schedComplete = true;
		for (ActorPEG a : graphPEG.vertexSet()) {
			if (a.state < a.getRepetitionCount().getNumerator()) {
				schedComplete = false;
			}
		}
		return schedComplete;
	}

	public int getTotalToken(){
		int totalToken = 0;
		//TODO: channel state
//		for (Channel c : gpr.getAdfgGraph().edgeSet()){
//			totalToken += c.getState();
//		}		
		return totalToken;
	}
	
	public int getTokenAfterFiring(Actor a){
		int tokenAfterFire = 0;
		for (Channel c : graphPEG.edgeSet()) {
			ActorPEG src = graphPEG.getEdgeSource(c);
			ActorPEG dest = graphPEG.getEdgeTarget(c);			
			if (src.getName().equals(a.getName())) {
				tokenAfterFire += c.getProduce().elementAt(0);				
			} else if (dest.getName().equals(a.getName())) {
				tokenAfterFire -= c.getConsume().elementAt(0);
			}
		}
		return tokenAfterFire;
	}
	
	public void fireActor(ActorPEG a) {
		//TODO : channel state
//		a.state = a.state + 1;
//		for (Channel c : gpr.getAdfgGraph().edgeSet()) {
//			Actor src = gpr.getAdfgGraph().getEdgeSource(c);
//			Actor dest = gpr.getAdfgGraph().getEdgeTarget(c);
//			if (src.getName().equals(a.getName())) {
//				c.setState(c.getState() + c.getProduce().elementAt(0));
//				if(c.getSize() < c.getState()) c.setSize(c.getState());				
//			} else if (dest.getName().equals(a.getName())) {
//				c.setState(c.getState() - c.getConsume().elementAt(0));
//				if(c.getSize() < c.getState()) c.setSize(c.getState());
//			}
//		}
	}

}
