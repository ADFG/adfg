/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.nam.algos;

import java.util.ArrayList;
import java.util.logging.Logger;

import org.jgrapht.graph.DirectedMultigraph;

import fr.inria.tea.adfg.algos.common_data_rep.Actor;
import fr.inria.tea.adfg.algos.common_data_rep.Channel;

/***
 * 
 * @author hatran
 *
 */
public class SchedListCommon {

	private static final Logger LOGGER = Logger.getLogger("AdfgLog");

	public enum MemoryArchitecture {
		MULTI_BANK, SINGLE_BANK
	};

	public SchedListCommon() {

	}

	/***
	 * 
	 * @param queueReady
	 * @param queueWait
	 */
	public static void updateQueues(ArrayList<Firing> queueReady, ArrayList<Firing> queueWait) {
		for (Firing f : queueWait) {
			boolean isReady = true;
			for (Firing predDepF : f.predDeps) {
				if (!predDepF.isScheduled) {
					isReady = false;
				}
			}

			if (isReady) {
				queueReady.add(f);
			}
		}

		for (Firing f : queueReady) {
			queueWait.remove(f);
		}
	}

	/***
	 * 
	 * @param queueComplete
	 * @param delay
	 * @param memArch
	 */
	public static void updateSchedule(ArrayList<Firing> queueComplete, int delay, MemoryArchitecture memArch) {
		boolean flag = true;
		do {
			SchedListCommon.multiCoreRTA(queueComplete, delay, memArch);
			SchedListCommon.updateRelease(queueComplete);

			flag = false;

			for (Firing f : queueComplete) {
				if (f.startTimeL1 != f.startTime) {
					flag = true;
				}
				f.startTime = f.startTimeL1;
			}
		} while (flag);
	}

	/***
	 * 
	 * @param queueComplete
	 */
	public static void updateRelease(ArrayList<Firing> queueComplete) {

		for (Firing f : queueComplete) {
			int iOffset = 0;
			for (Firing predDepF : f.predDeps) {
				if (iOffset < predDepF.startTime + predDepF.responseTime) {
					iOffset = predDepF.startTime + predDepF.responseTime + 1;
				}
			}
			f.startTimeL1 = iOffset;
		}
	}

	/***
	 * 
	 * @param queueComplete
	 * @param memDelay
	 * @param memArch
	 */
	public static void multiCoreRTA(ArrayList<Firing> queueComplete, int memDelay, MemoryArchitecture memArch) {
		boolean flag = true;
		do {
			for (Firing f : queueComplete) {
				int busInterference = SchedListCommon.computeBusInterference(f, queueComplete, memDelay, memArch);
				int wcet = (int) f.actor.getWcet();
				f.responseTimeL1 = busInterference + wcet;
			}

			flag = false;
			for (Firing f : queueComplete) {
				if (f.responseTime != f.responseTimeL1) {
					flag = true;
				}
				f.responseTime = f.responseTimeL1;
				f.endTime = f.startTime + f.responseTime;
			}
		} while (flag);
	}

	/***
	 * 
	 * @param f
	 * @param queueComplete
	 * @param delay
	 * @param memArch
	 * @return
	 */
	public static int computeBusInterference(Firing f, ArrayList<Firing> queueComplete, int delay,
			MemoryArchitecture memArch) {
		int busInterference = 0;
		for (Firing contentionF : queueComplete) {

			if (memArch == MemoryArchitecture.MULTI_BANK) {
				if (f.actor.getPartitionID() != contentionF.actor.getPartitionID()
						&& SchedListCommon.isMemContention(f.memAccess, contentionF.memAccess)
						&& SchedListCommon.isOverlap(f, contentionF)) {
					// LOGGER.info(
					// "-" + f.name + "-" + f.actor.getName() + "(" +
					// f.actor.getPartitionID() + "-" + f.actor.getMD() + ")" +
					// "\t" +
					// "-" + contentionF.name + "-" +
					// contentionF.actor.getName() + "(" +
					// contentionF.actor.getPartitionID() + "-" +
					// contentionF.actor.getMD() + ")"
					// );
					busInterference += Math.min(f.actor.getMemoryDemand(), contentionF.actor.getMemoryDemand());
				}
			}

			if (memArch == MemoryArchitecture.SINGLE_BANK) {
				if (f.actor.getPartitionID() != contentionF.actor.getPartitionID()
						&& SchedListCommon.isOverlap(f, contentionF)) {
					busInterference += Math.min(f.actor.getMemoryDemand(), contentionF.actor.getMemoryDemand());
				}
			}

		}
		return busInterference * delay;
	}

	/***
	 * 
	 * @param f1
	 * @param f2
	 * @return
	 */
	public static boolean isOverlap(Firing f1, Firing f2) {
		if (f1.startTime < f2.endTime && f1.endTime > f2.startTime) {
			return true;
		}
		return false;
	}

	/***
	 * 
	 * @param memDemand1
	 * @param memDemand2
	 * @return
	 */
	public static boolean isMemContention(ArrayList<Processor> memDemand1, ArrayList<Processor> memDemand2) {
		boolean flag = false;
		for (Processor p : memDemand1) {
			if (memDemand2.contains(p)) {
				flag = true;
			}
		}
		return flag;
	}

	/***
	 * 
	 * 
	 * @param queueComplete
	 * @return
	 */
	public static int computeMakeSpan(ArrayList<Firing> queueComplete) {
		int lastCompleteTime = 0;
		for (Firing f : queueComplete) {
			if (f.endTime > lastCompleteTime) {
				lastCompleteTime = f.endTime;
			}
		}
		return lastCompleteTime;
	}

	/***
	 * 
	 * @param f
	 */
	public static void printFiringInfo(Firing f){
		LOGGER.info("Print Firing Info");
		StringBuilder sb = new StringBuilder();
		sb.append("Firing: ");
		sb.append(f.name);
		for (Processor p : f.memAccess) {
			sb.append(" (" + p.processorId + ")");
		}
		LOGGER.info(sb.toString());
		for (Firing fdep : f.predDeps) {
			LOGGER.info("\t <- " + fdep.name);
		}
	}
	
	/***
	 * 
	 * @param adfgGraph
	 * @return
	 */
	public static DirectedMultigraph<ActorMRTA, Channel> transformGraphActorToGraphActorMRTA(DirectedMultigraph<Actor, Channel> adfgGraph) {
		DirectedMultigraph<ActorMRTA, Channel> graphMRTA = new DirectedMultigraph<>(Channel.class);
		for (Actor a : adfgGraph.vertexSet()) {
			ActorMRTA actorMRTA = new ActorMRTA(a);
			graphMRTA.addVertex(actorMRTA);
		}
		for (Channel e : adfgGraph.edgeSet()) {
			Actor src = adfgGraph.getEdgeSource(e);
			Actor snk = adfgGraph.getEdgeTarget(e);

			ActorMRTA srcPEG = ActorMRTA.getActorMRTAByName(graphMRTA.vertexSet(), src.getName());
			ActorMRTA snkPEG = ActorMRTA.getActorMRTAByName(graphMRTA.vertexSet(), snk.getName());

			graphMRTA.addEdge(srcPEG, snkPEG, e);
		}
		return graphMRTA;
	}
	
	/***
	 * 
	 * @param firings
	 */
	public static void printFiringsInfo(ArrayList<Firing> firings) {
		for (Firing f : firings) {
			StringBuilder sb = new StringBuilder();
			sb.append(f.name);
			for (Processor p : f.memAccess) {
				sb.append(" (" + p.processorId + ")");
			}
			LOGGER.info(sb.toString());
			for (Firing fdep : f.predDeps) {
				LOGGER.info("\t <- " + fdep.name);
			}
		}
	}

	/***
	 * 
	 * @param qReady
	 * @param qWait
	 * @param qComplete
	 * @param schedule
	 */
	public static void printStatusSubroutine(ArrayList<Firing> qReady, ArrayList<Firing> qWait,
			ArrayList<Firing> qComplete, ArrayList<ArrayList<Firing>> schedule) {
		LOGGER.info("===Schedule Status:");

		StringBuilder sb = new StringBuilder();
		sb.append("\t - Ready Queue: ");
		for (Firing f : qReady) {
			sb.append(f.name + " ");
		}
		sb.append("\n");

		sb.append("\t - Waiting Queue: ");
		for (Firing f : qWait) {
			sb.append(f.name + " ");
		}
		sb.append("\n");

		sb.append("\t - Complete Queue: ");
		for (Firing f : qComplete) {
			sb.append(f.name + " ");
		}
		sb.append("\n");

		sb.append("\t - Schedule: ");
		sb.append("\n");
		int i = 0;
		for (ArrayList<Firing> procQueue : schedule) {
			sb.append("\t \t - Schedule " + i + " :");
			for (Firing f : procQueue) {
				sb.append(" " + f.name + "(" + f.startTime + "-" + f.endTime + ")");
			}
			sb.append("\n");
			i++;
		}
		sb.append("\n");

		LOGGER.info(sb.toString());
	}
}
