/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.nam.algos;

import java.util.concurrent.ThreadLocalRandom;
import org.jgrapht.graph.DirectedMultigraph;
import fr.inria.tea.adfg.algos.common_data_rep.Channel;

public class SchedTTUtility {

	public SchedTTUtility() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	public static void generateMemoryAcessProfileSubroutine(DirectedMultigraph<ActorMRTA, Channel> gpr) {
		int mD = 0;
		int min = 1;
		int max = 10;
		for (ActorMRTA a : gpr.vertexSet()) {
			if (a.getMemoryDemand() == 0) {
				mD = ThreadLocalRandom.current().nextInt(min, max + 1);
				a.setMemoryDemand(mD);
			}
		}
	}
}
