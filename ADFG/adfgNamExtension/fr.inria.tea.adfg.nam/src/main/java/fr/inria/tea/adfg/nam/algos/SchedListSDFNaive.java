/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.nam.algos;

import java.util.ArrayList;

import fr.inria.tea.adfg.algos.common_data_rep.Actor;
import fr.inria.tea.adfg.algos.common_data_rep.Channel;
import fr.inria.tea.adfg.algos.common_data_rep.GraphProbData;
import fr.inria.tea.adfg.nam.algos.SchedListCommon.MemoryArchitecture;

public class SchedListSDFNaive {

	private int nbProcessor = 16;
	private MemoryArchitecture memArch;
	private int memDelay;
	private GraphProbData gpr;
	private ArrayList<Processor> processors;
	private ArrayList<ArrayList<Firing>> schedule;
	private ArrayList<Firing> firings;
	public int makespan = 0;

	/***
	 * 
	 * @param gpr
	 * @param firings
	 * @param memArch
	 * @param memDelay
	 */
	public SchedListSDFNaive(GraphProbData gpr, ArrayList<Firing> firings, MemoryArchitecture memArch, int memDelay) {
		this.gpr = gpr;
		this.firings = firings;
		this.memArch = memArch;
		this.memDelay = memDelay;

		for (Actor a : gpr.getActors()) {
			a.setPartitionID(-1);
		}

		processors = new ArrayList<Processor>();
		// nbProcessor = gpr.getNbProcessor();
		for (int i = 0; i < nbProcessor; i++) {
			Processor p = new Processor(i);
			processors.add(p);
		}

		schedule = new ArrayList<ArrayList<Firing>>();
		for (int i = 0; i < nbProcessor; i++) {
			ArrayList<Firing> proc = new ArrayList<Firing>();
			schedule.add(proc);
		}
	}

	/***
	 * 
	 */
	public void synthesizeListSchedTableHLF() {
		ArrayList<Firing> queueReady = new ArrayList<Firing>();
		ArrayList<Firing> queueWait = new ArrayList<Firing>();
		ArrayList<Firing> queueComplete = new ArrayList<Firing>();

		/*
		 * Init 3 queues
		 */
		for (Firing f : firings) {
			boolean isReady = true;
			for (Firing predDepF : f.predDeps) {
				if (!predDepF.isScheduled) {
					isReady = false;
				}
			}
			if (isReady) {
				queueReady.add(f);
			} else {
				queueWait.add(f);
			}
		}

		/*
		 * Need to have two condition because queueWait can be empty after
		 * calling updateQueues while queueReady has some elements
		 */
		while (!queueWait.isEmpty() || !queueReady.isEmpty()) {
			while (!queueReady.isEmpty()) {
				Firing f = queueReady.get(0);
				scheduleFiring(f);
				queueComplete.add(f);
				SchedListCommon.updateSchedule(queueComplete, memDelay, this.memArch);
				queueReady.remove(f);
			}
			SchedListCommon.updateQueues(queueReady, queueWait);
		}

		SchedListCommon.printStatusSubroutine(queueReady, queueWait, queueComplete, schedule);
		this.makespan = SchedListCommon.computeMakeSpan(queueComplete);
	}

	/***
	 * 
	 * @param f
	 */
	private void scheduleFiring(Firing f) {
		int earliestStartTime = 0;
		int earliestAvailableTime = 0;
		Processor allocateProc;

		for (Firing predDepF : f.predDeps) {
			if (earliestStartTime < predDepF.endTime) {
				earliestStartTime = predDepF.endTime;
			}
		}

		if (f.actor.getPartitionID() == -1) {
			allocateProc = processors.get(0);
			earliestAvailableTime = processors.get(0).earliestAvailableTime;

			/*
			 * Find a processor that allows earliestStartTime
			 */
			for (int i = 0; i < nbProcessor; i++) {
				if (processors.get(i).earliestAvailableTime < earliestAvailableTime) {
					allocateProc = processors.get(i);
					earliestAvailableTime = processors.get(i).earliestAvailableTime;
				}
			}

			/*
			 * Enforce dependency
			 */
			if (schedule.get(allocateProc.processorId).size() >= 1) {
				f.predDeps.add(
						schedule.get(allocateProc.processorId).get(schedule.get(allocateProc.processorId).size() - 1));
			}

			/*
			 * At the time we allocate a actor,we update all memory access
			 * references of all previous scheduled that related to this actor.
			 */
			for (Channel e : gpr.getAdfgGraph().edgeSet()) {
				Actor src = gpr.getAdfgGraph().getEdgeSource(e);
				Actor dest = gpr.getAdfgGraph().getEdgeTarget(e);
				if (dest == f.actor) {
					for (Firing prodF : firings) {
						if (prodF.actor == src) {
							prodF.addNonDuplicateMemAccess(allocateProc);
						}
					}
				}
			}

			/*
			 * Update actor partition ID
			 */
			f.actor.setPartitionID(allocateProc.processorId);

		} else {
			allocateProc = processors.get(f.actor.getPartitionID());
			earliestAvailableTime = allocateProc.earliestAvailableTime;
		}

		/*
		 * Schedule f, set f attributes
		 */
		f.actor.setPartitionID(allocateProc.processorId);
		f.isScheduled = true;
		if (earliestStartTime < earliestAvailableTime) {
			f.startTime = earliestAvailableTime;
		} else {
			f.startTime = earliestStartTime;
		}
		f.addNonDuplicateMemAccess(allocateProc);
		f.endTime = f.startTime + (int) f.actor.getWcet();

		/*
		 * Update processor and scheduling queue
		 */
		allocateProc.earliestAvailableTime = f.endTime;
		schedule.get(allocateProc.processorId).add(f);
	}
}
