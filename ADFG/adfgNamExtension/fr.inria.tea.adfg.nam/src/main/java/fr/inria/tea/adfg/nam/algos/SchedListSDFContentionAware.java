/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.nam.algos;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Logger;

import org.jgrapht.graph.DirectedMultigraph;

import fr.inria.tea.adfg.algos.common_data_rep.Actor;
import fr.inria.tea.adfg.algos.common_data_rep.Channel;
import fr.inria.tea.adfg.algos.common_data_rep.GraphProbData;
import fr.inria.tea.adfg.nam.algos.SchedListCommon.MemoryArchitecture;

/***
 * 
 * @author hatran
 *
 */
public class SchedListSDFContentionAware {

	public static final long incoherentPeriod = -1;
	private static final Logger LOGGER = Logger.getLogger("AdfgLog");
	
	private int nbProcessor = 16;
	private MemoryArchitecture memArch;
	private int memDelay;
	private GraphProbData gpr;
	private DirectedMultigraph<ActorMRTA, Channel> graphMRTA;
	private ArrayList<Processor> processors;
	private ArrayList<ArrayList<Firing>> schedule;
	private ArrayList<Firing> firings;
	public int makespan = 0;

	/***
	 * 
	 * @param gpr
	 * @param firings
	 * @param memArch
	 */
	public SchedListSDFContentionAware(GraphProbData gpr, MemoryArchitecture memArch, int memDelay) {				
		this.gpr = gpr;
		this.nbProcessor = gpr.getNbProcessor();		
		this.graphMRTA = SchedListCommon.transformGraphActorToGraphActorMRTA(this.gpr.getAdfgGraph());
		
		LOGGER.info("##=== Generating Memory Access Profile: START ===");
		SchedTTUtility.generateMemoryAcessProfileSubroutine(graphMRTA);
		LOGGER.info("##=== Generating Memory Access Profile: END ===");		
		
		LOGGER.info("##=== Job Dependency Solver ===");
		JobDependencySolver jds = new JobDependencySolver(graphMRTA);
		jds.solving();
		LOGGER.info("##=== Job Dependency Solver ===");
		
		this.firings = jds.getFirings();
		this.memArch = memArch;
		this.memDelay = memDelay;
		
		for (ActorMRTA a : this.graphMRTA.vertexSet()) {
			a.setPartitionID(-1);
		}

		processors = new ArrayList<Processor>();		
		for (int i = 0; i < nbProcessor; i++) {
			Processor p = new Processor(i);
			processors.add(p);
		}

		schedule = new ArrayList<ArrayList<Firing>>();
		for (int i = 0; i < nbProcessor; i++) {
			ArrayList<Firing> proc = new ArrayList<Firing>();
			schedule.add(proc);
		}
	}

	/***
	 * 
	 */
	public void synthesizeListSchedTableHLF() {
		ArrayList<Firing> queueReady = new ArrayList<Firing>();
		ArrayList<Firing> queueWait = new ArrayList<Firing>();
		ArrayList<Firing> queueComplete = new ArrayList<Firing>();

		/*
		 * Init 3 queues
		 */
		for (Firing f : firings) {
			boolean isReady = true;
			for (Firing predDepF : f.predDeps) {
				if (!predDepF.isScheduled) {
					isReady = false;
				}
			}
			if (isReady) {
				queueReady.add(f);
			} else {
				queueWait.add(f);
			}
		}

		/*
		 * Need to have two condition because queueWait can be empty after
		 * calling updateQueues while queueReady has some elements
		 */
		while (!queueWait.isEmpty() || !queueReady.isEmpty()) {
			while (!queueReady.isEmpty()) {
				Firing f = queueReady.get(0);
				scheduleFiring(f, queueComplete);
				queueComplete.add(f);
				SchedListCommon.updateSchedule(queueComplete, memDelay, this.memArch);
				queueReady.remove(f);
			}
			SchedListCommon.updateQueues(queueReady, queueWait);
		}

		SchedListCommon.printStatusSubroutine(queueReady, queueWait, queueComplete, schedule);
		this.makespan = SchedListCommon.computeMakeSpan(queueComplete);
	}

	/***
	 * 
	 * @param fCopy
	 * @param queueComplete
	 */
	@SuppressWarnings("unchecked")
	private void scheduleFiring(Firing f, ArrayList<Firing> queueComplete) {
		Processor allocateProc = null;
		int earliestStartTime = 0;
		int earliestAvailableTime = 0;
		int minMakespan = Integer.MAX_VALUE;

		/*
		 * Compute earliestStartTime if the firing
		 */
		for (Firing predDepF : f.predDeps) {
			if (earliestStartTime < predDepF.endTime) {
				earliestStartTime = predDepF.endTime;
			}
		}

		if (f.actor.getPartitionID() == -1) {

			/*
			 * If the actor is not allocated to a processor then we find a
			 * processor that allows minMakespan. We iterate through the list of
			 * processor. For each iteration, we assign the firing to a
			 * processor and compute the makespan. A copy of queueComplete is
			 * created at each iteration.
			 */

			for (Processor p : processors) {

				Firing fCopy;
				ArrayList<Firing> queueCopy = null;
				earliestAvailableTime = p.earliestAvailableTime;

				/*
				 * Deep clone f to fCopy and queueComplete to queueCopy
				 */
				fCopy = f.deepClone();
				SchedListCommon.printFiringInfo(fCopy);

				/*
				 * Deep clone queueComplete to queueCopy
				 */
				try {
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					ObjectOutputStream oos = new ObjectOutputStream(baos);
					oos.writeObject(queueComplete);

					ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
					ObjectInputStream ois = new ObjectInputStream(bais);
					queueCopy = (ArrayList<Firing>) ois.readObject();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}

				/*
				 * Enforce dependency
				 */
				if (schedule.get(p.processorId).size() >= 1) {
					fCopy.predDeps.add(schedule.get(p.processorId).get(schedule.get(p.processorId).size() - 1));
				}

				/*
				 * At the time we allocate an actor,we update all memory access
				 * references of all previous scheduled that related to this
				 * actor.
				 */
				for (Channel e : gpr.getAdfgGraph().edgeSet()) {
					Actor src = gpr.getAdfgGraph().getEdgeSource(e);
					Actor dest = gpr.getAdfgGraph().getEdgeTarget(e);
					if (dest == fCopy.actor) {
						for (Firing prodF : firings) {
							if (prodF.actor == src) {
								prodF.addNonDuplicateMemAccess(allocateProc);
							}
						}
					}
				}

				/*
				 * Update actor partition ID
				 */
				fCopy.actor.setPartitionID(p.processorId);

				/*
				 * Recompute schedule and see whether we can achieve a shorter
				 * makespan
				 */
				queueCopy.add(fCopy);
				SchedListCommon.updateSchedule(queueCopy, memDelay, this.memArch);
				if (SchedListCommon.computeMakeSpan(queueComplete) < minMakespan) {
					minMakespan = SchedListCommon.computeMakeSpan(queueComplete);
					allocateProc = p;
				}
			}
		} else {
			/*
			 * If the actor is already allocated to a processor, we just need to
			 * assign the firing to this processor
			 */
			allocateProc = processors.get(f.actor.getPartitionID());
			earliestAvailableTime = allocateProc.earliestAvailableTime;
		}

		/*
		 * Schedule f, set f attributes
		 */
		f.actor.setPartitionID(allocateProc.processorId);
		f.isScheduled = true;
		if (earliestStartTime < earliestAvailableTime) {
			f.startTime = earliestAvailableTime;
		} else {
			f.startTime = earliestStartTime;
		}
		f.addNonDuplicateMemAccess(allocateProc);
		f.endTime = f.startTime + (int) f.actor.getWcet();

		/*
		 * Update processor and scheduling queue
		 */
		allocateProc.earliestAvailableTime = f.endTime;
		schedule.get(allocateProc.processorId).add(f);
	}
}
