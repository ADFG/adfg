/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.nam.algos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import org.jgrapht.graph.DirectedMultigraph;

import fr.inria.tea.adfg.algos.common_data_rep.Channel;

public class JobDependencySolver {

	private static final Logger LOGGER = Logger.getLogger("AdfgLog");
	private DirectedMultigraph<ActorMRTA, Channel> graphMRTA;
	private List<String> cplexData;
	private ArrayList<Firing> firings;
	
	public JobDependencySolver(DirectedMultigraph<ActorMRTA, Channel> graphMRTA) {
		this.graphMRTA = graphMRTA;
		this.firings = new ArrayList<Firing>();
	}

	/**
	 * Compute and generate all job level dependencies in one graph iteration
	 * by using - Affine relations (Adnan et al., 2012) - Occurence functions
	 * (Smarandache et Guernic., 1997) - Repetition vector (Battacharyya et al.,
	 * 1996)
	 */
	public void solving() {
		
		String cplexActors = "Jobs = {\n";		
		for (ActorMRTA a : graphMRTA.vertexSet()){
			for(int i=1; i <= a.repetitionCount.getNumerator(); i++) {
				cplexActors += "<"+ a.getName() + i + ", " + a.getWcet() + ", " + a.getMemoryDemand() + ", " + a.getName() + ", " + a.getID() + ", " + i +">,\n";
				
				Firing f = new Firing();
				f.actor = a;
				f.id = i;
				f.name = a.getName() + i;
				f.startTime = 0;
				f.endTime = 0;			
				if (i > 1) {
					f.predDeps.add(Firing.getFiringByActorAndId(firings, a, i-1));
				}
				firings.add(f);
			}					
		}
		
		cplexActors = cplexActors.substring(0,cplexActors.length()-2);
		cplexActors += "\n};";
		LOGGER.info(cplexActors);
		LOGGER.info(firings.toString());
		
		String cplexDeps = "Dependencies = {\n";
		for (Channel r : graphMRTA.edgeSet()) {
			ActorMRTA h = graphMRTA.getEdgeSource(r);
			ActorMRTA k = graphMRTA.getEdgeTarget(r);
			
			int repH = (int) h.repetitionCount.getNumerator();
			int repK = (int) k.repetitionCount.getNumerator();

			int[] depH = new int[repH];
			int[] depK = new int[repK];

			long p = r.getProduce().elementAt(0);
			long q = r.getConsume().elementAt(0);
			//int buffer_size = (int) (p + q - BasicArithmetic.funcGCD((long)p, (long)q));
			long buffer_size = r.getSize();
			
			LOGGER.info("Actor: " + h.getName());
			
			for (int i = 0; i < repH; i++) {
					depH[i] = (int) (Math.ceil( ( (double) (i+1) *(double) p - (double) buffer_size) /  (double)q));
					if(depH[i]>= 1){
						LOGGER.info(k.getName() + depH[i] + "->" + h.getName() + ((i+1)));
						cplexDeps += 
									"<<" 
									+ k.getName() + depH[i] + ", " + k.getWcet() + ", " + k.getMemoryDemand() + ", " + k.getName() + ", " + k.getID() + ", " + depH[i] +
									">,<" 
									+ h.getName() + (i+1) + ", " + h.getWcet() + ", " + h.getMemoryDemand() + ", " + h.getName() + ", " + h.getID() + ", " + (i+1) +
									">>,\n";
						Firing firingK = Firing.getFiringByActorAndId(firings, k, depH[i]);
						Firing firingH = Firing.getFiringByActorAndId(firings, h, i+1);
						firingH.predDeps.add(firingK);
					}
			}	
			
			LOGGER.info("Actor: " + k.getName());

			for (int i = 0; i < repK; i++) {				
				depK[i] = (int) (Math.ceil (((double)(i+1)*(double)q) / (double) p));
				
				LOGGER.info(h.getName() + depK[i] + "->" + k.getName() + (i + 1));
				cplexDeps += 
							"<<" 
							+ h.getName() + depK[i] + ", " + h.getWcet() + ", " + h.getMemoryDemand() + ", " + h.getName() + ", " + h.getID() + ", " + depK[i] +
							">,<" 
							+ k.getName() + (i+1) + ", " + k.getWcet() + ", " + k.getMemoryDemand() + ", " + k.getName() + ", " + k.getID() + ", " + (i+1) +  
							">>,\n";
				Firing firingH = Firing.getFiringByActorAndId(firings, h, depK[i]);
				Firing firingK = Firing.getFiringByActorAndId(firings, k, i+1);
				firingK.predDeps.add(firingH);
			}	
		}		
		
		
		cplexDeps = cplexDeps.substring(0,cplexDeps.length()-2);
		cplexDeps += "\n};";
		LOGGER.info(cplexDeps);			
		
		String cplexProcs = "Processors	= {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};";
		
		String cplexMemDeps = "MemDeps = {\n";
		for (Channel c : graphMRTA.edgeSet()){
			ActorMRTA src = graphMRTA.getEdgeSource(c);
			ActorMRTA dest = graphMRTA.getEdgeTarget(c);
			cplexMemDeps += "<" + src.getID() + ", " + dest.getID() + ">,\n";
		}
		cplexMemDeps = cplexMemDeps.substring(0,cplexMemDeps.length()-2);
		cplexMemDeps += "\n};";
		
		cplexData = Arrays.asList(cplexActors, cplexDeps, cplexMemDeps, cplexProcs);
		
		LOGGER.info(firings.toString());
	}	
	
	/**
	 * @return the cplexResult
	 */
	public List<String> getCplexResult() {
		return cplexData;
	}

	/**
	 * @param cplexResult the cplexResult to set
	 */
	public void setCplexResult(List<String> cplexResult) {
		this.cplexData = cplexResult;
	}
	
	public ArrayList<Firing> getFirings() {
		return firings;
	}

	public void setFirings(ArrayList<Firing> firings) {
		this.firings = firings;
	}
}
