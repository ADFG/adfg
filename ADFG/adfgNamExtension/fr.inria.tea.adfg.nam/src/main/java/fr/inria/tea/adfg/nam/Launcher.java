/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.nam;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.ParserProperties;

import fr.inria.tea.adfg.algos.ADFGprob;
import fr.inria.tea.adfg.algos.ADFGraph;
import fr.inria.tea.adfg.algos.common_base.AdfgExternalException;
import fr.inria.tea.adfg.algos.common_base.DirectStreamHandler;
import fr.inria.tea.adfg.algos.common_base.MinimalFormatter;
import fr.inria.tea.adfg.algos.common_base.Parameters;
import fr.inria.tea.adfg.algos.common_base.TradeOffUDH;
import fr.inria.tea.adfg.algos.common_data_rep.GraphProbData;
import fr.inria.tea.adfg.nam.algos.CodeGeneratorLIDEC;
import fr.inria.tea.adfg.nam.algos.CodeGeneratorLIDECRTEMS;
import fr.inria.tea.adfg.nam.algos.DotParser;
import fr.inria.tea.adfg.nam.algos.ExpansionRateSolver;
/**
 * Main class to run the command line interface.
 * Parses options, then run the analysis and returns. 
 * 
 * @author ahonorat
 */
public class Launcher {
    
    private static final Logger LOGGER = Logger.getLogger("AdfgLog");
    
    public static final int CONSOLE_WIDTH = 128;
    private static GraphProbData gpr;
    	
    /**
     * Standard constructor, do nothing.
     */
    private Launcher() {
    	
    }
    
    /**
     * Prints the usage then exit.
     * 
     * @param parser The complete usage is printed thanks to the parser.
     * @param exitCode 0 if normal exiting, 1 or more if exiting because of
     * 	an error.
     */
    private static void printUsageExit(CmdLineParser parser, int exitCode) {
    	System.out.println("\n\tSyntax: adfg_sched_nam FILENAME ANALYSIS [OPTIONS]\n");
    	parser.printUsage(System.out);
    	System.exit(exitCode);
    }

    /**
     * Parse argument and run the analysis.
     * 
     * @param args Program arguments.
     */
    public static void main(String[] args) {
    	Options opt = new Options();
		ParserProperties pp = ParserProperties.defaults().withUsageWidth(CONSOLE_WIDTH);
		CmdLineParser parser = new CmdLineParser(opt, pp);	
				
		try {
			parser.parseArgument(args);
		} catch (CmdLineException e) {
			System.out.println("Bad argument format: "+e.getMessage());
			printUsageExit(parser, 1);
		}

		if (opt.isHelp()) {
		    printUsageExit(parser, 0);
		}
						
		if (opt.isVerbose()) {
			Level level = opt.isQuiet() ? Level.SEVERE : Level.FINE;
			LOGGER.setLevel(level);
			for (Handler h: LOGGER.getParent().getHandlers()) {
				h.setLevel(level);
			}
		} else {
			Level level = opt.isQuiet() ? Level.SEVERE : Level.INFO;
			LOGGER.setUseParentHandlers(false);
			Handler handler = new DirectStreamHandler(System.err, new MinimalFormatter());
			handler.setLevel(level);
			LOGGER.addHandler(handler);
			LOGGER.setLevel(level);
		}
			
		long startTime = System.currentTimeMillis();
			
		try {
			
			//=====================
			// Construction from sdf3/adfg xml format
			//=====================
			String fileName = opt.getFile();
			Path currentRelativePath = Paths.get("");
			LOGGER.config("Current relative path is: " + currentRelativePath.toAbsolutePath());
			LOGGER.info("Filename: " + opt.getFile());
			ADFGraph graph = null;
			if (fileName.endsWith(".xml")) {
				graph = new ADFGraph(opt.getFile());
			} else if (fileName.endsWith(".adfg")) {
				graph = new ADFGraph(opt.getFile());
			} else if (fileName.endsWith(".tur")) {
				graph = new ADFGraph(opt.getFile());
			} else if  (fileName.endsWith(".dot")) {
				graph = new ADFGraph(opt.getFile());
				DotParser.parse(graph);
			}
			//====================
			// Analysis
			//====================
			
			Parameters params = new Parameters(opt.keepPeriod(), opt.keepInitToken(), 
												opt.keepMaxToken(), opt.keepMapping(), 
												opt.getStrategy(), opt.isReduceWCET(), false,
												new TradeOffUDH(TradeOffUDH.DEFAULT_TRADEOFF_UDH));
			ADFGprob prob = new ADFGprob(opt.getScheduleType(), opt.getNbProcs(), graph, params);
			if (opt.zeroDelay()) {
				prob.setAllInitialTokensToZero();
				LOGGER.warning("Channels initial number of tokens (i. e. delays) have been set to zero.");
			}
						
			gpr = new GraphProbData(prob, opt.getScheduleType(), opt.getNbProcs(), params);
			
			final String optAnalysisString = opt.getAnalysis();
			if("expansionratesolver".equalsIgnoreCase(optAnalysisString) || 
					"ers".equalsIgnoreCase(optAnalysisString)) {
				LOGGER.info("===Expansion Rate Solver: START===");				
				ExpansionRateSolver expSolver = new ExpansionRateSolver(gpr);
				expSolver.solving();
				LOGGER.info("===Expansion Rate Solver: END===");
			}
			else if("codegeneration".equalsIgnoreCase(optAnalysisString) || 
					"cgn".equalsIgnoreCase(optAnalysisString)) {
				LOGGER.info("===LIDE C Code Generation: START===");		
				CodeGeneratorLIDEC cgl = new CodeGeneratorLIDEC(gpr);
				cgl.generateLIDEC();
				
				LOGGER.info("===LIDE C Code Generation: END===");
			}
			else if("codegenerationrtems".equalsIgnoreCase(optAnalysisString) || 
					"crt".equalsIgnoreCase(optAnalysisString)) {
				LOGGER.info("===LIDE C RTEMS Code Generation: START===");		
				CodeGeneratorLIDECRTEMS cglrt = new CodeGeneratorLIDECRTEMS(gpr);
				cglrt.generateLIDECRTEMS();
				
				LOGGER.info("===LIDE C RTEMS Code Generation: END===");
			}
			else if("dottoadfg".equalsIgnoreCase(optAnalysisString) || 
					"dta".equalsIgnoreCase(optAnalysisString)) {
				LOGGER.info("===Dot to ADFG: START===");		
				prob.updateOrExportFile("output.adfg");
				
				LOGGER.info("====Dot to ADFG: END===");
			}
			else if("contentionawarescheduling".equalsIgnoreCase(optAnalysisString) || 
					"cas".equalsIgnoreCase(optAnalysisString)) {		
				
				LOGGER.info("#===Contention Aware Scheduling: START===");
//				LOGGER.info("Entering:\n=== Repetition Vector Solver ===");
//				RepetitionVectorSolver rvs = new RepetitionVectorSolver(gpr);
//				rvs.solving();
//				LOGGER.info("Exiting:\n=== Repetition Vector Solver ===");
//	
//				LOGGER.info("Entering:\n=== Min Buffer Sched ===");
//				SchedSDFMinBuffer somb = new SchedSDFMinBuffer(gpr);
//				somb.computeMinBufferSched();
//				LOGGER.info("Exiting:\n=== Min Buffer Sched ===");
//	
//	
//				LOGGER.info("Entering:\n=== List Scheduling Synthesis ===");
//				SchedListSDFContentionAware solv2 = new SchedListSDFContentionAware(gpr, jds.getFirings(),
//						MemoryArchitecture.MULTI_BANK, 10);
//				solv2.synthesizeListSchedTableHLF();
//				LOGGER.info("Makespan: " + solv2.makespan);
//				LOGGER.info("Exiting:\n=== List Scheduling Synthesis ===");
//
//				 LOGGER.info("Entering:\n=== List Scheduling Synthesis===");
//				 SchedListSDFContentionAware solv3 = new
//				 SchedListSDFContentionAware(gpr, jds.getFirings(),
//				 MemoryArchitecture.SINGLE_BANK, 10);
//				 solv3.synthesizeListSchedTableHLF();
//				 LOGGER.info("Makespan: " + solv3.makespan);
//				 LOGGER.info("Exiting:\n=== List Scheduling Synthesis===");
				 LOGGER.info("===Contention Aware Scheduling: END===");
			}
									
		} catch (AdfgExternalException e) {
			if (opt.isVerbose()) {
				e.printStackTrace();
			}
			System.exit(1);
		}
			
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		double sum = (double) elapsedTime / 1000;
			
		LOGGER.fine("\n# Time (s): "+sum);
		System.exit(0);
	}
	
}
