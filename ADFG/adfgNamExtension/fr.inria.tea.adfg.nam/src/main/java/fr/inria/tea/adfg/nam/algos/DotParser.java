/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.nam.algos;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.logging.Logger;

import com.paypal.digraph.parser.GraphEdge;
import com.paypal.digraph.parser.GraphNode;
import com.paypal.digraph.parser.GraphParser;
import com.paypal.digraph.parser.GraphParserException;

import fr.inria.tea.adfg.algos.ADFGraph;
import fr.inria.tea.adfg.algos.common_base.AdfgExternalException;
import fr.inria.tea.adfg.algos.common_data_rep.Actor;
import fr.inria.tea.adfg.algos.common_data_rep.Channel;
import fr.inria.tea.adfg.algos.maths.UPIS;

public class DotParser {
	private static final Logger LOGGER = Logger.getLogger("AdfgLog");

	public DotParser() {

	}

	public static void parse(ADFGraph adfgr) {
		String fileName = adfgr.getFileName();
		try {
			LOGGER.info("Filename: " + fileName);
			GraphParser parser = new GraphParser(new FileInputStream(fileName));

			Map<String, GraphNode> nodes = parser.getNodes();
			Map<String, GraphEdge> edges = parser.getEdges();

			try {
				parseActors(adfgr, nodes);
				parseEdges(adfgr, nodes, edges);
				int sjNumber = countSplitJoin(nodes);
				LOGGER.info("Number of split/join actors: " + sjNumber);
				
				int bufferReg = countBufferSize(nodes,edges);
				LOGGER.info("Buffer requirement: " + bufferReg);
				
				int bufferRegCSDF = countBufferSizeCSDF(nodes,edges);
				LOGGER.info("Estimated CSDF buffer size: " + bufferRegCSDF);				
			} catch (AdfgExternalException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (GraphParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void parseActors(ADFGraph adfgr, Map<String, GraphNode> nodes) throws AdfgExternalException {
		LOGGER.info("--- nodes:");
		for (GraphNode node : nodes.values()) {
			if (!node.getId().toString().equals("nodenull")) {
				String WCET = null;
				String name = null;
				String pushType = null;
				String popType = null;
				String push = null;
				String pop = null;
				String codeSize = null;
				String stackSize = null;

				int popSize = 0;
				int pushSize = 0;
				int mD = 0;

				name = node.getAttribute("label").toString();
				WCET = node.getAttribute("work").toString();
				push = node.getAttribute("push").toString();
				pushType = node.getAttribute("pushType").toString();
				pop = node.getAttribute("pop").toString();
				popType = node.getAttribute("popType").toString();
				codeSize = node.getAttribute("codeSize").toString();
				codeSize = codeSize.substring(0, codeSize.length()-2);
				stackSize = node.getAttribute("stackSize").toString();
				stackSize = stackSize.substring(0, stackSize.length()-2);
				
				if (pushType.equals("void")) {
					pushSize = 0;
				} else if (pushType.equals("int")) {
					pushSize = 2;
				}
				if (pushType.equals("float")) {
					pushSize = 4;
				} else if (pushType.equals("complex")) {
					pushSize = 16;
				}

				if (popType.equals("void")) {
					popSize = 0;
				} else if (popType.equals("int")) {
					popSize = 2;
				}
				if (popType.equals("float")) {
					popSize = 4;
				} else if (popType.equals("complex")) {
					popSize = 16;
				}

				mD = (int) Math.ceil((double)(Integer.parseInt(stackSize) + Integer.parseInt(codeSize) + Integer.parseInt(pop) * popSize
						+ Integer.parseInt(push) * pushSize)/(double)64);
				
				ActorMRTA actor;
				long wcet2 = Long.parseLong(WCET);
				if (wcet2 == 0) {
					wcet2 = 1;
				}
				actor = new ActorMRTA(name, wcet2, 0, mD);				
				
				adfgr.addActor(actor);

				//LOGGER.info(actor.getName()+ " " + actor.getMD());
			}
		}
	}

	private static void parseEdges(ADFGraph adfgr, Map<String, GraphNode> nodes, Map<String, GraphEdge> edges) throws AdfgExternalException {
		LOGGER.info("--- edges:");
		for (GraphEdge edge : edges.values()) {
			if (!edge.getNode1().getId().toString().equals("nodenull")) {
				String name = edge.getNode1().getAttribute("label").toString() + "-" + edge.getNode2().getAttribute("label").toString();
				String pRate = "(" + edge.getNode1().getAttribute("push").toString() + ")";
				String cRate = "(" + edge.getNode2().getAttribute("pop").toString() + ")";
				String srcName = edge.getNode1().getAttribute("label").toString();
				String tgtName = edge.getNode2().getAttribute("label").toString();

				UPIS prate = new UPIS(pRate);
				UPIS crate = new UPIS(cRate);

				Channel channel = new Channel(name, prate, crate, Integer.MAX_VALUE, 0, 1);

				Actor src = adfgr.getMappedActors().get(srcName);
				Actor tgt = adfgr.getMappedActors().get(tgtName);

				if (src == null || tgt == null) {
					throw new AdfgExternalException("Source/target unknown: channel <" + name + ">.");
				}
				//LOGGER.info(name + " " + pRate + " " + cRate);
				adfgr.addChannel(channel, src, tgt);			
			}
		}
	}
	
	private static int countSplitJoin(Map<String, GraphNode> nodes) {
		int sjNumber = 0;
		for (GraphNode node : nodes.values()) {
			String name = node.getAttribute("label").toString();
			if(name.startsWith("WEIGHTED_ROUND_ROBIN")){
				sjNumber++;
			}
		}
		return sjNumber;
	}
	

	private static int countBufferSize(Map<String, GraphNode> nodesnodes, Map<String, GraphEdge> edges) {
		int bufferSize = 0;
		for (GraphEdge edge : edges.values()) {
			if (!edge.getNode1().getId().toString().equals("nodenull")) {				
				int pRate = Integer.parseInt(edge.getNode1().getAttribute("push").toString());
				int cRate = Integer.parseInt(edge.getNode2().getAttribute("pop").toString());
				int dataSize = 0;
				String pType = edge.getNode2().getAttribute("pushType").toString();
				
				if(pType.equals("int")){
					dataSize = 2;
				}else if(pType.equals("float")){
					dataSize = 4;
				}else if(pType.equals("double")){
					dataSize = 8;
				}else if(pType.equals("complex")){
					dataSize = 16;
				}
				//String name = edge.getNode1().getAttribute("label").toString() + "-" + edge.getNode2().getAttribute("label").toString();
				//LOGGER.info(name + " " + pRate + " " + cRate);	
				//int tokenNumber =  pRate + cRate - (int)(BasicArithmetic.funcGCD(pRate,cRate));
				bufferSize = bufferSize + Integer.max(pRate, cRate) * dataSize;			
			}
		}
		return bufferSize;
	}
	
	private static int countBufferSizeCSDF(Map<String, GraphNode> nodesnodes, Map<String, GraphEdge> edges) {
		int bufferSize = 0;
		for (GraphEdge edge : edges.values()) {
			if (!edge.getNode1().getId().toString().equals("nodenull")) {
				
				int pRate = Integer.parseInt(edge.getNode1().getAttribute("push").toString());
				int cRate = Integer.parseInt(edge.getNode2().getAttribute("pop").toString());
				int dataSize = 0;
				String pType = edge.getNode2().getAttribute("pushType").toString();
				
				if(pType.equals("int")){
					dataSize = 2;
				}else if(pType.equals("float")){
					dataSize = 4;
				}else if(pType.equals("double")){
					dataSize = 8;
				}else if(pType.equals("complex")){
					dataSize = 16;
				}
				
				//String name = edge.getNode1().getAttribute("label").toString() + "-" + edge.getNode2().getAttribute("label").toString();
				//LOGGER.info(name + " " + pRate + " " + cRate);
				if(!edge.getNode1().getAttribute("label").toString().startsWith("WEIGHTED_ROUND_ROBIN")){
					bufferSize = bufferSize + Integer.max(pRate, cRate) * dataSize;			
				}
			}
		}
		return bufferSize;
	}
}
