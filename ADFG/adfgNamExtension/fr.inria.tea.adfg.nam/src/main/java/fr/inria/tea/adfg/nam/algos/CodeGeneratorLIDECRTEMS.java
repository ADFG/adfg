/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.nam.algos;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Logger;

import fr.inria.tea.adfg.algos.common_data_rep.GraphProbData;

public class CodeGeneratorLIDECRTEMS {
	private static final Logger LOGGER = Logger.getLogger("AdfgLog");
	private GraphProbData gpr;
	private StringBuilder sbLIDECRTEMSDriver;
	private CodeGeneratorLIDEC cgLIDEC;
	
	private int indentationLevel = 0;

	public CodeGeneratorLIDECRTEMS(GraphProbData gpr) {
		this.gpr = gpr;
		this.cgLIDEC = new CodeGeneratorLIDEC(this.gpr);
		sbLIDECRTEMSDriver = new StringBuilder();
	}
	
	public void generateLIDECRTEMS() {
		cgLIDEC.generateHeaderInclude(indentationLevel, sbLIDECRTEMSDriver);
		cgLIDEC.addLine(sbLIDECRTEMSDriver);
		cgLIDEC.generateHeaderChannel(indentationLevel, sbLIDECRTEMSDriver);
		cgLIDEC.addLine(sbLIDECRTEMSDriver);
		cgLIDEC.generateHeaderActor(indentationLevel, sbLIDECRTEMSDriver);
		cgLIDEC.addLine(sbLIDECRTEMSDriver);
		cgLIDEC.addLine(indentationLevel, sbLIDECRTEMSDriver, "void *POSIX_Init(void *argument) {");
		indentationLevel++;
		
		cgLIDEC.addLine(sbLIDECRTEMSDriver);
		cgLIDEC.addLine(indentationLevel,sbLIDECRTEMSDriver,"struct sched_param param;");
		cgLIDEC.addLine(indentationLevel,sbLIDECRTEMSDriver,"pthread_attr_t attr;");
		cgLIDEC.addLine(indentationLevel,sbLIDECRTEMSDriver,"pthread_t id;");
		cgLIDEC.addLine(indentationLevel,sbLIDECRTEMSDriver,"cpu_set_t cpuset;");
		cgLIDEC.addLine(sbLIDECRTEMSDriver);
		cgLIDEC.addLine(indentationLevel,sbLIDECRTEMSDriver,"/* Main thread priority */");
		cgLIDEC.addLine(indentationLevel,sbLIDECRTEMSDriver,"param.sched_priority=99;");
		cgLIDEC.addLine(indentationLevel,sbLIDECRTEMSDriver,"pthread_setschedparam(pthread_self(), SCHED_OTHER, &param);");
		cgLIDEC.addLine(sbLIDECRTEMSDriver);
					
		cgLIDEC.generateChannels(indentationLevel, sbLIDECRTEMSDriver);
		cgLIDEC.addLine(sbLIDECRTEMSDriver);
		
		cgLIDEC.generateActors(indentationLevel, sbLIDECRTEMSDriver);
		cgLIDEC.addLine(sbLIDECRTEMSDriver);
		
		cgLIDEC.generateActorsSchedulingParams(indentationLevel, sbLIDECRTEMSDriver);
		cgLIDEC.addLine(sbLIDECRTEMSDriver);
		
		cgLIDEC.generateRTEMSActors(indentationLevel, sbLIDECRTEMSDriver);
		cgLIDEC.addLine(sbLIDECRTEMSDriver);
		
		cgLIDEC.addLine(indentationLevel, sbLIDECRTEMSDriver, "/* Start time. Used later to compute the periodic releases */");
		cgLIDEC.addLine(indentationLevel, sbLIDECRTEMSDriver, "clock_gettime(CLOCK_REALTIME, &critical_instant);");
		cgLIDEC.addLine(indentationLevel, sbLIDECRTEMSDriver, "pthread_exit(NULL);");
		cgLIDEC.addLine(indentationLevel, sbLIDECRTEMSDriver, "return 0");
		
		indentationLevel--;
		cgLIDEC.addLine(indentationLevel, sbLIDECRTEMSDriver, "}");
		LOGGER.info(sbLIDECRTEMSDriver.toString());
				
	    try {
	    	FileOutputStream outputStream = new FileOutputStream("lide_c_rtems_adfg_driver.c");
		    byte[] strToBytes = sbLIDECRTEMSDriver.toString().getBytes();
			outputStream.write(strToBytes);
			outputStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
