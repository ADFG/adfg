/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.nam.algos;

import java.util.Collection;
import java.util.logging.Logger;

import org.jgrapht.graph.DirectedMultigraph;

//import de.vandermeer.asciitable.v2.V2_AsciiTable;
import fr.inria.tea.adfg.algos.common_base.AdfgInternalException;
//import fr.inria.tea.adfg.algos.common_base.PrettyTableRenderer;
import fr.inria.tea.adfg.algos.common_data_rep.Actor;
import fr.inria.tea.adfg.algos.common_data_rep.Channel;
import fr.inria.tea.adfg.algos.common_data_rep.GraphProbData;
import fr.inria.tea.adfg.nam.algos.ActorMRTA.MemoryAccess;

public class ReponseTimeAnalysisDAG {

	private static final Logger LOGGER = Logger.getLogger("AdfgLog");

	private int nbProcessor;
	private int memDelay;
	private GraphProbData gpr;

	
	public ReponseTimeAnalysisDAG(GraphProbData gpr, int memDelay) {
		this.nbProcessor = gpr.getNbProcessor();
		this.gpr = gpr;
		this.memDelay = memDelay;
	}


	public long experimentalMethod() {
		/*
		 * Check if all actors are ActorMRTA
		 */
		for (Actor actor : gpr.getActors()) {
			if (!(actor instanceof ActorMRTA)) {
				throw new AdfgInternalException("TT_MC_RTA: All actors must be ActorMRTA");
			}
		}

		/*
		 * Compute actor dependencies, offsets, initial response time
		 */
		for (Actor actor : gpr.getActors()) {
			ActorMRTA actorM = (ActorMRTA) actor;
			actorM.setDeadline(actorM.getUserPeriod());
			actorM.setPhase(0);
			actorM.getMemoryBanks().add(actor.getPartitionID());
			actorM.getMemoryAccesses().add(new fr.inria.tea.adfg.nam.algos.ActorMRTA.MemoryAccess(actor.getPartitionID(), actorM.getMemoryDemand()));
			actorM.setResponseTime((int) (actorM.getWcet() + (actorM.getMemoryDemand() * memDelay)));
			computePredDeps(actorM, gpr.getAdfgGraph());
			computePostDeps(actorM, gpr.getAdfgGraph());

			for (Channel c : gpr.getAdfgGraph().edgeSet()) {
				Actor src = gpr.getAdfgGraph().getEdgeSource(c);
				Actor dest = gpr.getAdfgGraph().getEdgeTarget(c);

				if (src.getName().equals(actorM.getName())) {
					/*-
					 * Check if SRC actor already accessed to a memory bank of DEST actor. 
					 * If NO (memoryAccess == null): create add a record of memory access.
					 * If YES: increase the memory demand to this bank.
					 * Note:
					 * (1) Access to remote memory bank is computed by partitionID of an actor.
					 * (2) Remote memory demand is computed by the number of token produced. 
					 */
					MemoryAccess memAccess = getMemoryAccess(dest.getPartitionID(), actorM.getMemoryAccesses());
					if (memAccess == null) {
						MemoryAccess m = new MemoryAccess(dest.getPartitionID(), c.getProduce().getSumPeriod());
						actorM.getMemoryAccesses().add(m);
						actorM.getMemoryBanks().add(dest.getPartitionID());
					} else {
						memAccess.setAccessDemand(memAccess.getAccessDemand() + c.getProduce().getSumPeriod());
					}	
				}
			}
		}

		LOGGER.info("\n---------- INPUT ----------");
		//TODO: fix printDataSubRoutine
		//printDataSubRoutine(gpr);

		/*
		 * Implementation of ComputeRT algorithm in Rihani et al., 2016
		 */
		boolean flag = true;
		do {
			MultiCoreRTA();
			UpdateRelease();
			
			flag = false;

			for (Actor actor : gpr.getActors()) {
				if (((ActorMRTA) actor).getPhaseL1() != ((ActorMRTA) actor).getPhase()) {
					flag = true;
				}
				((ActorMRTA) actor).setPhase(((ActorMRTA) actor).getPhaseL1());
			}
		} while (flag);

		LOGGER.info("\n---------- OUTPUT ----------");
		//TODO: fix printDataSubRoutine
		//printDataSubRoutine(gpr);

		throw new AdfgInternalException("TT_MC_RTA is not fully implemented, abandon.");
	}

	/**
	 * Response time analysis given a set of release dates (offsets)
	 */
	private void MultiCoreRTA() {
		boolean flag = true;
		do {

			for (Actor actor : gpr.getActors()) {
				ActorMRTA actorM = (ActorMRTA) actor;
				int busInterference = ComputeBusInterference(actorM, gpr.getActors());
				int wcet = (int) actorM.getWcet();
				actorM.setResponseL1(wcet + busInterference);
			}

			flag = false;
			for (Actor actor : gpr.getActors()) {
				ActorMRTA actorM = (ActorMRTA) actor;
				if (actorM.getResponseL1() != actorM.getResponseTime()) {
					flag = true;
				}
				actorM.setResponseTime(actorM.getResponseL1());
			}

		} while (flag);
	}

	/**
	 * Update release times to start after all dependencies
	 */
	private void UpdateRelease() {

		for (Actor iActor : gpr.getActors()) {
			ActorMRTA iActorM = (ActorMRTA) iActor;
			int iOffset = 0;
			for (Actor kActor : iActorM.getPredDeps()) {
				ActorMRTA kActorM = (ActorMRTA) kActor;
				if (iOffset < kActorM.getPhase() + kActorM.getResponseTime()) {
					iOffset = (int) (kActorM.getPhase() + kActorM.getResponseTime());
				}
			}
			iActorM.setPhaseL1(iOffset);
		}
	}

	/**
	 * Compute Bus Interference following equation 11 in Rihani et al., 2016
	 * 
	 * @return
	 */
	private int ComputeBusInterference(ActorMRTA iActorM, Collection<Actor> actors) {
		int busInterference = 0;

		/*
		 * Memory bank, duration used by iActor
		 */
		int iStart = (int) iActorM.getPhase();
		int iEnd = iStart + iActorM.getResponseTime();

		/*
		 *  Memory bank b loop
		 */
		for (int b : iActorM.getMemoryBanks()) {

			long bankInterference = 0;
			long task_bank_inf = 0;

			for (MemoryAccess memAccess : iActorM.getMemoryAccesses()) {
				if (memAccess.getAccessBank() == b) {
					bankInterference = memAccess.getAccessDemand();
					task_bank_inf = memAccess.getAccessDemand();
				}
			}

			/*
			 *  Processor y loop
			 */
			for (int y = 0; y < nbProcessor; y++) {

				if (y == iActorM.getPartitionID()) {
					continue;
				}

				/*
				 *  Compute Level 2 interference
				 *  sum_(y\in G1)^(y!=x)(min(A_i_y_b, S_i_x_b))
				 */				 
				int busAccesses = 0;
				
				for (Actor jActor : actors) {
					ActorMRTA jActorM = (ActorMRTA) jActor;
					/*
					 * jActors on processor i (Cond-2) that can interfere actor
					 * iActor if: (1) jActor access a memory bank used by i
					 * Actor (2) Must be in a different processor (3) There is
					 * no dependency between the two actors
					 */
					if ((jActorM != iActorM) && (jActorM.getPartitionID() == y)
							&& (jActorM.getPartitionID() != iActorM.getPartitionID())
							&& (jActorM.getMemoryBanks().contains(b)) && !(iActorM.getPredDeps().contains(jActorM))
							&& !(jActorM.getPredDeps().contains(iActorM))) {
						
						long MD_b_k = 0;

						for (MemoryAccess memAccess : jActorM.getMemoryAccesses()) {
							if (memAccess.getAccessBank() == b) {
								MD_b_k = memAccess.getAccessDemand();
							}
						}

						int kStart = (int) jActorM.getPhase();
						int kEnd = kStart + jActorM.getResponseTime();

						if ((iStart < kEnd) && (iEnd > kStart)) {
							int overlap = Math.min(iEnd - iStart,
									Math.min(iEnd - kStart, Math.min(kEnd - kStart, kEnd - iStart)));
							busAccesses += Math.min(MD_b_k, Math.ceil((double) overlap / (double) memDelay));
							// LOGGER.info(iActor.getName() + "-" +
							// jActor.getName() + "-" +
							// Integer.toString(overlap) + "-" +
							// Integer.toString(busAccesses) + "-" + MD_b_k
							// + "
							// " + bankInterference);
						}
					}
				}
				bankInterference += Math.min(task_bank_inf, busAccesses);
			}
			busInterference += bankInterference;
		}
		return busInterference * memDelay;
	}

	/**
	 * Compute the precedent dependencies of actor in adfgGraph
	 * 
	 * @param actorM
	 * @param adfgGraph
	 */
	public static void computePredDeps(ActorMRTA actorM, DirectedMultigraph<Actor, Channel> adfgGraph) {
		for (Channel c : adfgGraph.edgeSet()) {
			Actor src = adfgGraph.getEdgeSource(c);
			Actor dest = adfgGraph.getEdgeTarget(c);
			if (dest.getName().equals(actorM.getName())) {
				actorM.getPredDeps().add(src);
			}
		}
	}

	/**
	 * Compute the post dependencies of actor in adfgGraph
	 * 
	 * @param actorM
	 * @param adfgGraph
	 */
	public static void computePostDeps(ActorMRTA actorM, DirectedMultigraph<Actor, Channel> adfgGraph) {
		for (Channel c : adfgGraph.edgeSet()) {
			Actor src = adfgGraph.getEdgeSource(c);
			Actor dest = adfgGraph.getEdgeTarget(c);
			if (src.getName().equals(actorM.getName())) {
				actorM.getPostDeps().add(dest);
			}
		}
	}

	/**
	 * Print attributes of actors in grp Graph
	 */
	//	public static void printDataSubRoutine(GraphProbData gpr) {
	//		StringBuilder sb = new StringBuilder();
	//		V2_AsciiTable atAR = new V2_AsciiTable();
	//		atAR.addRule();
	//		atAR.addRow("Actor Name", "WCET", "Period", "Deadline", "Memory Demand", "Offset", "Pred Deps","Post Deps",
	//				"Remote Bank", "Response Time", "t-L", "b-L");
	//		atAR.addStrongRule();
	//		for (Actor actor : gpr.getActors()) {
	//			ActorMRTA actorM = (ActorMRTA) actor;
	//
	//			StringBuilder strPredDeps = new StringBuilder();
	//			for (Actor predDep : actorM.getPredDeps()) {
	//				strPredDeps.append(predDep.getName());
	//				strPredDeps.append(' ');
	//			}
	//			
	//			StringBuilder strPostDeps = new StringBuilder();
	//			for (Actor predDep : actorM.getPostDeps()) {
	//				strPostDeps.append(predDep.getName());
	//				strPostDeps.append(' ');
	//			}
	//
	//			StringBuilder strWriteMemoryBank = new StringBuilder();
	//			for (MemoryAccess m : actorM.getMemoryAccesses()) {
	//				strWriteMemoryBank.append(m.getAccessBank() + "-" + m.getAccessDemand());
	//				strWriteMemoryBank.append(' ');
	//			}
	//
	//			atAR.addRow(actorM.getName(), actorM.getWcet(), actorM.getUserPeriod(), actorM.getDeadline(),
	//					actorM.getMemoryDemand(), actorM.getPhase(), strPredDeps.toString(), 
	//					strPostDeps.toString(), strWriteMemoryBank.toString(),
	//					actorM.getResponseTime(), actorM.getTLevel(), actorM.getBLevel());
	//
	//		}
	//		atAR.addRule();
	//		LOGGER.info(sb.toString() + PrettyTableRenderer.getRenderer().render(atAR));
	//	}

	/**
	 * 
	 * @param bank
	 * @param memAccesses
	 * @return
	 */
	private MemoryAccess getMemoryAccess(int bank, Collection<MemoryAccess> memAccesses) {
		for (MemoryAccess m : memAccesses) {
			if (m.getAccessBank() == bank) {
				return m;
			}
		}
		return null;
	}
}
