/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.nam.algos;

import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import org.jgrapht.alg.connectivity.ConnectivityInspector;
import org.jgrapht.graph.DirectedMultigraph;

import fr.inria.tea.adfg.algos.common_data_rep.Actor;
import fr.inria.tea.adfg.algos.common_data_rep.Channel;
import fr.inria.tea.adfg.algos.common_data_rep.GraphProbData;
import fr.inria.tea.adfg.nam.algos.ActorPEG.ActorTemperature;
import fr.inria.tea.adfg.nam.algos.ActorPEG.ActorType;

public class ExpansionRateSolver {

	private static final Logger LOGGER = Logger.getLogger("AdfgLog");
	@SuppressWarnings("unused")
	private GraphProbData gpr;
	private int nbProcs;
	private double ub_s;
	private DirectedMultigraph<ActorPEG, Channel> graphPEG;
	private String msgPrefix = "ERS: ";

	private long srcRate;
	private long srcRepetitionCount;

	/***
	 * Implementation of the approach to compute expansion rates by identifying hot actors and hot regions.
	 * Sardar M. Farhad, Yousun Ko, Bernd Burgstaller, and Bernhard Scholz. 2011. 
	 * Orchestration by approximation: mapping stream programs onto multicore architectures. 
	 * SIGPLAN Not. 46, 3 (March 2011), 357-368. 
	 * DOI: https://doi.org/10.1145/1961296.1950406 
	 * 
	 * @param gpr
	 */
	public ExpansionRateSolver(GraphProbData gpr) {
		this.gpr = gpr;
		this.nbProcs = gpr.getNbProcessor();
		LOGGER.info(msgPrefix + "Number of processors: " + nbProcs);
		this.graphPEG = transformGraphActorToGraphActorPEG(gpr.getAdfgGraph());
		LOGGER.info(msgPrefix + "Transformed " + this.graphPEG.vertexSet().size() + " nodes and "
				+ this.graphPEG.edgeSet().size() + " edges");

		computeOutputInputDataRates(graphPEG);
		identifySrcAndSnk(graphPEG);
		setSourceRateAndRepetitionCount(graphPEG);
		setSinkRateAndRepetitionCount(graphPEG);
		LOGGER.info(msgPrefix + "Computed input and output data rate, identified SRC and SNK");
		LOGGER.info(msgPrefix + "Source Rate: " + srcRate + ", Source Repetition Count: " + srcRepetitionCount);
	}

	/***
	 * 
	 * - RepetitionCount of all actors must be computed
	 */
	public void solving() {
		computeAlphaPrime(graphPEG);
		computeGamma(graphPEG);
		computeUBi(graphPEG);
		this.ub_s = computeUBs(graphPEG);
		computeDuplicity(graphPEG);
		LOGGER.info(msgPrefix + "System upper bound (ub_s): " + ub_s);
		LOGGER.info(msgPrefix + "Result");
		displayGraphInfo(graphPEG);

		List<Set<ActorPEG>> hotRegions = computeHotRegions(graphPEG);
		LOGGER.info(msgPrefix + "Hot regions");
		for (Set<ActorPEG> connectedSet : hotRegions) {
			LOGGER.info(connectedSet.toString());
		}
	}

	private List<Set<ActorPEG>> computeHotRegions(DirectedMultigraph<ActorPEG, Channel> graph) {
		DirectedMultigraph<ActorPEG, Channel> hotGraph = new DirectedMultigraph<>(Channel.class);

		for (ActorPEG actorPEG : graph.vertexSet()) {
			if (actorPEG.temperature == ActorTemperature.HOT) {
				hotGraph.addVertex(actorPEG);
			}
		}

		for (Channel e : graph.edgeSet()) {
			ActorPEG src = graph.getEdgeSource(e);
			ActorPEG snk = graph.getEdgeTarget(e);

			if (src.temperature == ActorTemperature.HOT && snk.temperature == ActorTemperature.HOT) {
				hotGraph.addEdge(src, snk, e);
			}
		}

		ConnectivityInspector<ActorPEG, Channel> inspector = new ConnectivityInspector<>(hotGraph);
		return inspector.connectedSets();
	}

	/***
	 * 
	 * @param graph
	 */
	private void computeAlphaPrime(DirectedMultigraph<ActorPEG, Channel> graph) {
		for (ActorPEG actorPEG : graph.vertexSet()) {
			int w_i = actorPEG.outputDataRate;
			int q_i = (int) actorPEG.getRepetitionCount().getNumerator();
			actorPEG.alphaPrime = ((double) w_i * (double) q_i) / ((double) srcRate * (double) srcRepetitionCount);
		}
	}

	/***
	 * 
	 * @param graph
	 */
	private void computeGamma(DirectedMultigraph<ActorPEG, Channel> graph) {
		for (ActorPEG actorPEG : graph.vertexSet()) {
			actorPEG.gamma = (double) actorPEG.getWcet() / (double) actorPEG.inputDataRate;
		}
	}

	/***
	 * 
	 * @param graph
	 */
	private void computeUBi(DirectedMultigraph<ActorPEG, Channel> graph) {
		for (ActorPEG actorPEG : graph.vertexSet()) {

			if (actorPEG.type == ActorType.SRC) {

				actorPEG.ub_i = 1.0 / actorPEG.gamma;
			} else {

				double denominator = 0.0;
				for (Channel e : graph.edgeSet()) {

					if (graph.getEdgeTarget(e) == actorPEG) {
						ActorPEG j = graph.getEdgeSource(e);
						double w_ji = (double) e.getProduce().elementAt(0) / (double) j.outputDataRate;
						denominator += w_ji * j.alphaPrime * actorPEG.gamma;
					}
				}
				actorPEG.ub_i = 1.0 / denominator;
			}
		}
	}

	/***
	 * 
	 * @param graph
	 */
	private double computeUBs(DirectedMultigraph<ActorPEG, Channel> graph) {
		ActorPEG actorSrc = getSourceActor(graph);
		double ub_s = 0.0;
		double gamma_1 = actorSrc.gamma;
		double sum_1 = 0.0;
		double denominator = 0.0;

		for (ActorPEG actorPEG : graph.vertexSet()) {
			if (actorPEG.type != ActorType.SRC) {
				double sum_2 = 0.0;
				for (Channel e : graph.edgeSet()) {

					if (graph.getEdgeTarget(e) == actorPEG) {
						ActorPEG j = graph.getEdgeSource(e);
						double w_ji = e.getProduce().elementAt(0) / j.outputDataRate;
						sum_2 += w_ji * j.alphaPrime;
					}
				}
				
				sum_1 += actorPEG.gamma * sum_2;
			}		
		}
		denominator = gamma_1 + sum_1;
		
		LOGGER.info(msgPrefix + "nbProcs: " + this.nbProcs + ", denominator: " + denominator);
		ub_s = (double) this.nbProcs / denominator;
		return ub_s;
	}

	private void computeDuplicity(DirectedMultigraph<ActorPEG, Channel> graph) {
		for (ActorPEG actorPEG : graph.vertexSet()) {

			double sum = 0.0;
			if (actorPEG.type == ActorType.SRC) {
				actorPEG.duplicity = 1;
				actorPEG.temperature = ActorTemperature.COLD;
			} else {

				if (actorPEG.ub_i < this.ub_s) {
					actorPEG.temperature = ActorTemperature.HOT;
				} else {
					actorPEG.temperature = ActorTemperature.COLD;
				}

				for (Channel e : graph.edgeSet()) {

					if (graph.getEdgeTarget(e) == actorPEG) {
						ActorPEG j = graph.getEdgeSource(e);
						double w_ji = (double) e.getProduce().elementAt(0) / (double) j.outputDataRate;
						sum += w_ji * j.alphaPrime;
					}
				}

				actorPEG.duplicity = (int) Math.ceil(actorPEG.gamma * sum * this.ub_s);
			}
		}
	}

	/***
	 * 
	 * @param adfgGraph
	 * @return
	 */
	public static DirectedMultigraph<ActorPEG, Channel> transformGraphActorToGraphActorPEG(
			DirectedMultigraph<Actor, Channel> adfgGraph) {
		DirectedMultigraph<ActorPEG, Channel> graphPEG = new DirectedMultigraph<>(Channel.class);
		for (Actor a : adfgGraph.vertexSet()) {
			ActorPEG actorPEG = new ActorPEG(a);
			graphPEG.addVertex(actorPEG);
		}
		for (Channel e : adfgGraph.edgeSet()) {
			Actor src = adfgGraph.getEdgeSource(e);
			Actor snk = adfgGraph.getEdgeTarget(e);

			ActorPEG srcPEG = ActorPEG.getActorPEGByName(graphPEG.vertexSet(), src.getName());
			ActorPEG snkPEG = ActorPEG.getActorPEGByName(graphPEG.vertexSet(), snk.getName());

			graphPEG.addEdge(srcPEG, snkPEG, e);
		}
		return graphPEG;
	}

	/***
	 * 
	 * @param graph
	 */
	private void computeOutputInputDataRates(DirectedMultigraph<ActorPEG, Channel> graph) {
		for (ActorPEG actorPEG : graph.vertexSet()) {
			int outputDataRate = 0;
			int inputDataRate = 0;
			for (Channel e : graph.edgeSet()) {
				if (graph.getEdgeSource(e) == actorPEG) {
					outputDataRate += e.getProduce().elementAt(0);
				} else if (graph.getEdgeTarget(e) == actorPEG) {
					inputDataRate += e.getConsume().elementAt(0);
				}
			}
			actorPEG.outputDataRate = outputDataRate;
			actorPEG.inputDataRate = inputDataRate;
		}
	}

	/***
	 * 
	 * @param graph
	 */
	private void identifySrcAndSnk(DirectedMultigraph<ActorPEG, Channel> graph) {
		for (ActorPEG actorPEG : graph.vertexSet()) {
			int srcCount = 0;
			int snkCount = 0;
			for (Channel e : graph.edgeSet()) {
				if (graph.getEdgeSource(e) == actorPEG) {
					srcCount++;
				} else if (graph.getEdgeTarget(e) == actorPEG) {
					snkCount++;
				}
			}
			if (snkCount == 0) {
				actorPEG.type = ActorType.SRC;
			} else if (srcCount == 0) {
				actorPEG.type = ActorType.SNK;
			} else if (snkCount > 0 && srcCount > 0) {
				actorPEG.type = ActorType.FIL;
			}
		}
	}

	/***
	 * TODO: Find a method to get sourceRate as a SDF graph specification
	 * 
	 * @param graph
	 */
	private void setSourceRateAndRepetitionCount(DirectedMultigraph<ActorPEG, Channel> graph) {
		for (ActorPEG actorPEG : graph.vertexSet()) {
			if (actorPEG.type == ActorType.SRC) {
				actorPEG.inputDataRate = actorPEG.outputDataRate;
				this.srcRate = actorPEG.outputDataRate;
				this.srcRepetitionCount = actorPEG.getRepetitionCount().getNumerator();
			}
		}
	}

	/***
	 * TODO: Find a method to get the outputDataRate of the source actor from a
	 * SDF graph specification
	 * 
	 * @param graph
	 */
	private void setSinkRateAndRepetitionCount(DirectedMultigraph<ActorPEG, Channel> graph) {
		for (ActorPEG actorPEG : graph.vertexSet()) {
			if (actorPEG.type == ActorType.SNK) {
				actorPEG.outputDataRate = actorPEG.inputDataRate;
			}
		}
	}

	/***
	 * 
	 * @param graph
	 * @return
	 */
	private ActorPEG getSourceActor(DirectedMultigraph<ActorPEG, Channel> graph) {
		for (ActorPEG actorPEG : graph.vertexSet()) {
			if (actorPEG.type == ActorType.SRC) {
				return actorPEG;
			}
		}
		return null;
	}

	/***
	 * 
	 * @param graph
	 * @return
	 */
	@SuppressWarnings("unused")
	private ActorPEG getSinkActor(DirectedMultigraph<ActorPEG, Channel> graph) {
		for (ActorPEG actorPEG : graph.vertexSet()) {
			if (actorPEG.type == ActorType.SNK) {
				return actorPEG;
			}
		}
		return null;
	}

	/***
	 * 
	 * @param graph
	 */
	private void displayGraphInfo(DirectedMultigraph<ActorPEG, Channel> graph) {
		for (ActorPEG actorPEG : graph.vertexSet()) {
			LOGGER.info(msgPrefix + actorPEG.toString());
		}
	}
}
