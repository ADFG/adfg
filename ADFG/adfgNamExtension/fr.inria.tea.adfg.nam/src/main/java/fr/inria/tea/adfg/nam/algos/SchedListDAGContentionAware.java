/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.nam.algos;

import java.util.ArrayList;
import java.util.Stack;
import java.util.logging.Logger;

import fr.inria.tea.adfg.algos.common_base.AdfgInternalException;
import fr.inria.tea.adfg.algos.common_data_rep.Actor;
import fr.inria.tea.adfg.algos.common_data_rep.GraphProbData;
import fr.inria.tea.adfg.nam.algos.ActorMRTA.MemoryAccess;

/***
 * 
 * @author hatran
 *
 */
public class SchedListDAGContentionAware {

	private static final Logger LOGGER = Logger.getLogger("AdfgLog");

	private int nbProcessor;
	private int d = 10;
	private GraphProbData gpr;

	public SchedListDAGContentionAware(GraphProbData gpr) {
		nbProcessor = gpr.getNbProcessor();
		this.gpr = gpr;
	}

	public long synthesizeListSchedTableHLF() {

		/**
		 * STEP 0: Initializing Data
		 */

		ArrayList<ActorMRTA> readyJobs = new ArrayList<ActorMRTA>();
		ArrayList<ActorMRTA> pendingJobs = new ArrayList<ActorMRTA>();
		ArrayList<ActorMRTA> scheduledJobs = new ArrayList<ActorMRTA>();
		ArrayList<Processor> processors = new ArrayList<Processor>();

		Stack<ActorMRTA> stackT = new Stack<ActorMRTA>();
		Stack<ActorMRTA> stackB = new Stack<ActorMRTA>();

		for (int i = 1; i <= nbProcessor; i++) {
			Processor p = new Processor(i);
			processors.add(p);
		}

		/**
		 * STEP 1: Preparation Computing t-level and b-level.
		 */
		LOGGER.info("Pre-processing graph data");

		for (Actor actor : gpr.getActors()) {
			ActorMRTA actorM = (ActorMRTA) actor;
			actorM.setDeadline(actorM.getUserPeriod());
			actorM.setPhase(0);
			actorM.getMemoryBanks().add(actor.getPartitionID());
			actorM.getMemoryAccesses().add(new MemoryAccess(actor.getPartitionID(), actorM.getMemoryDemand()));
			actorM.setResponseTime((int) (actorM.getWcet() + (actorM.getMemoryDemand() * d)));
			actorM.setVisited(false);
			ReponseTimeAnalysisDAG.computePredDeps(actorM, gpr.getAdfgGraph());
			ReponseTimeAnalysisDAG.computePostDeps(actorM, gpr.getAdfgGraph());
		}

		LOGGER.info("Sorting graph following topological order");
		stackT = topologicalSort(gpr);

		LOGGER.info("Computing t-level and b-level !");
		while (stackT.empty() == false) {
			long max = 0;
			ActorMRTA actorN = stackT.pop();
			stackB.push(actorN);

			for (Actor parentActor : actorN.getPredDeps()) {
				ActorMRTA parentActorM = (ActorMRTA) parentActor;
				if (parentActorM.getTLevel() + parentActorM.getWcet() > max) {
					max = parentActorM.getTLevel() + parentActorM.getWcet();
				}
			}
			actorN.setTLevel(max);
		}

		while (stackB.empty() == false) {
			long max = 0;
			ActorMRTA actorN = stackB.pop();

			for (Actor childActor : actorN.getPostDeps()) {
				ActorMRTA childActorM = (ActorMRTA) childActor;
				if (childActorM.getBLevel() > max) {
					max = childActorM.getBLevel();
				}
			}
			actorN.setBLevel(actorN.getWcet() + max);
		}

		//TODO: fix printDataSubRoutine
		//ReponseTimeAnalysisDAG.printDataSubRoutine(gpr);

		/**
		 * STEP 2: Compute List scheduling
		 */

		LOGGER.info("Computing List scheduling !");

		/*
		 * --INITILIZATION-- Initialize two sets of jobs: -- Ready to be
		 * scheduled: no dependency or all precedence dependencies are
		 * scheduled. -- Not ready to be scheduled: not all precedence
		 * dependencies are scheduled.
		 */

		for (Actor actor : gpr.getActors()) {
			ActorMRTA actorM = (ActorMRTA) actor;
			boolean isReady = true;
			for (Actor predDepsActor : actorM.getPredDeps()) {
				ActorMRTA predActorM = (ActorMRTA) predDepsActor;
				if (!predActorM.isScheduled()) {
					isReady = false;
				}
			}

			if (isReady) {
				readyJobs.add(actorM);
			} else {
				pendingJobs.add(actorM);
			}
		}

		printStatusSubroutine(readyJobs, pendingJobs);

		/*
		 * --LIST SCHEDULING-- Perform scheduling until all jobs are scheduled
		 */

		while (!pendingJobs.isEmpty() || !readyJobs.isEmpty()) {

			/*
			 * --ELECTION-- Elect the next job to be scheduled Election is done
			 * by taking into account task t level and b level
			 */
			ActorMRTA electedActor = Elect(readyJobs);
			LOGGER.info("Elected task:" + electedActor.getName());

			/*
			 * --COMPUTING MIN START TIME-- Compute min start time for the
			 * elected job a task cannot run parallel if there is another task
			 * accessing to the same memory bank is running. In other words, a
			 * task that has the same post dependency with the elected task.
			 */
			int minStartTime = (int) electedActor.getPhase();

			for (Actor actor : electedActor.getPredDeps()) {
				if (minStartTime < actor.getPhase() + actor.getWcet()) {
					minStartTime = (int) (actor.getPhase() + actor.getWcet());
				}
			}

			for (ActorMRTA actorS : scheduledJobs) {
				ArrayList<Actor> common = new ArrayList<Actor>(actorS.getPostDeps());
				common.retainAll(electedActor.getPostDeps());
				if (!common.isEmpty()) {
					if (minStartTime < actorS.getPhase() + actorS.getWcet()) {
						minStartTime = (int) (actorS.getPhase() + actorS.getWcet());
					}
				}
			}
			LOGGER.info("Min start time: " + minStartTime);
			readyJobs.remove(electedActor);

			/*
			 * --ALLOCATION-- Allocate the actor to a processor which allows
			 * earliest start time.
			 */
			boolean allocationFlag = false;
			for (Processor proc : processors) {
				if (proc.earliestAvailableTime <= minStartTime) {
					electedActor.setScheduled(true);
					electedActor.setPhase(minStartTime);
					electedActor.setPartitionID(proc.processorId);

					proc.allocatedActors.add(electedActor);
					proc.earliestAvailableTime = (int) (minStartTime + electedActor.getWcet());

					readyJobs.remove(electedActor);
					scheduledJobs.add(electedActor);

					allocationFlag = true;

					LOGGER.info("Allocate task: " + electedActor.getName() + " to processor: " + proc.processorId);
					break;
				}
			}

			/*
			 * If we arrive here, it means that there is no processor available
			 * to execute the task at minStartTime -- Search for a processor
			 * that can ensure earliest start time.
			 */
			if (!allocationFlag) {
				Processor allocateProc = processors.get(0);
				int earlistStartTime = processors.get(0).earliestAvailableTime;
				for (Processor proc : processors) {
					if (proc.earliestAvailableTime < earlistStartTime) {
						allocateProc = proc;
						earlistStartTime = proc.earliestAvailableTime;
					}
				}

				electedActor.setScheduled(true);
				electedActor.setPhase(minStartTime);
				electedActor.setPartitionID(allocateProc.processorId);

				allocateProc.allocatedActors.add(electedActor);
				allocateProc.earliestAvailableTime = (int) (earlistStartTime + electedActor.getWcet());

				readyJobs.remove(electedActor);
				scheduledJobs.add(electedActor);

				allocationFlag = true;

				LOGGER.info("Allocate task: " + electedActor.getName() + " to processor: " + allocateProc.processorId);

			}

			UpdateQueues(readyJobs, pendingJobs);
			printStatusSubroutine(readyJobs, pendingJobs);
			LOGGER.info("");
		}

		throw new AdfgInternalException("TT_LIST is not fully implemented, abandon.");
	}

	private void UpdateQueues(ArrayList<ActorMRTA> readyJobs, ArrayList<ActorMRTA> pendingJobs) {
		int s = pendingJobs.size();
		for (int i = 0; i < s; i++) {
			ActorMRTA actorM = pendingJobs.get(i);

			boolean isReady = true;
			for (Actor predDepsActor : actorM.getPredDeps()) {
				ActorMRTA predActorM = (ActorMRTA) predDepsActor;
				if (!predActorM.isScheduled()) {
					isReady = false;
				}
			}

			if (isReady) {
				readyJobs.add(actorM);
				pendingJobs.remove(actorM);
				s = s - 1;
			}
		}
	}

	private ActorMRTA Elect(ArrayList<ActorMRTA> readyJobs) {
		ActorMRTA electedActor = readyJobs.get(0);

		for (int i = 1; i < readyJobs.size(); i++) {
			if (electedActor.getTLevel() < readyJobs.get(i).getTLevel()) {
				electedActor = readyJobs.get(i);
			} else if (electedActor.getTLevel() == readyJobs.get(i).getTLevel()) {
				if (electedActor.getBLevel() > readyJobs.get(i).getBLevel()) {
					electedActor = readyJobs.get(i);
				}
			}
		}

		return electedActor;
	}

	private Stack<ActorMRTA> topologicalSort(GraphProbData gpr) {
		Stack<ActorMRTA> stack = new Stack<ActorMRTA>();
		for (Actor actor : gpr.getActors()) {
			ActorMRTA actorM = (ActorMRTA) actor;
			if (!actorM.isVisited()) {
				topologicalSortUtil(actorM, gpr, stack);
			}
		}
		return stack;
	}

	private void printStatusSubroutine(ArrayList<ActorMRTA> readyJobs, ArrayList<ActorMRTA> pendingJobs) {
		StringBuilder sb = new StringBuilder();
		sb.append("Ready Jobs: ");
		for (ActorMRTA actorM : readyJobs) {
			sb.append(actorM.getName() + " ");
		}
		sb.append("\n");
		sb.append("Pending Jobs: ");
		for (ActorMRTA actorM : pendingJobs) {
			sb.append(actorM.getName() + " ");
		}
		sb.append("\n");
		LOGGER.info(sb.toString());
	}

	private void topologicalSortUtil(ActorMRTA actorM, GraphProbData gpr, Stack<ActorMRTA> stack) {
		actorM.setVisited(true);
		for (Actor adjacentActor : actorM.getPostDeps()) {
			ActorMRTA adjacentActorM = (ActorMRTA) adjacentActor;
			if (!adjacentActorM.isVisited()) {
				topologicalSortUtil(adjacentActorM, gpr, stack);
			}
		}
		stack.push(actorM);
	}
}
