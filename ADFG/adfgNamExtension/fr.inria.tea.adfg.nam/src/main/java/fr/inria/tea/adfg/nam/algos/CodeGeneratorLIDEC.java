/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.nam.algos;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Logger;

import fr.inria.tea.adfg.algos.common_data_rep.Actor;
import fr.inria.tea.adfg.algos.common_data_rep.Channel;
import fr.inria.tea.adfg.algos.common_data_rep.GraphProbData;

/***
 * Naming convention for actors
 * - example filename should be used to generate the lide_c_..._driver.c file
 * - lide_c_[actor_name]_[new|invoke|enable|terminate]
 * - define incoming edges then outgoing edges
 * - each actor has a name (for debugging purpose)
 */
public class CodeGeneratorLIDEC {
	public static final char SPACE = ' ';
	public static final String NEWLINE = System.getProperty("line.separator");
	public static final String ACTOR_COUNT = "ACTOR_COUNT";
	public static final String DEF_ACTOR = "ACTOR_";
	public static final String ARR_ACTOR = "actors";
	public static final String ARR_RTEMS_ACTOR = "rtems_actors";
	public static final String LIDEC_ACTOR_CONTEXT_TYPE = "lide_c_actor_context_type";
	public static final String LIDEC_FIFO_POINTER = "lide_c_fifo_pointer";
	public static final String LIDEC_FIFO_NEW = "lide_c_fifo_new";
	public static final String C_POINTER = "*";
	
	private static final Logger LOGGER = Logger.getLogger("AdfgLog");
	private GraphProbData gpr;
	private StringBuilder sbLIDECDriver;
	
	private int indentationLevel = 0;

	public CodeGeneratorLIDEC(GraphProbData gpr) {
		this.gpr = gpr;
		sbLIDECDriver = new StringBuilder();
	}
	
	public void generateLIDEC() {
		generateHeaderInclude(indentationLevel, sbLIDECDriver);
		addLine(sbLIDECDriver);
		generateHeaderChannel(indentationLevel, sbLIDECDriver);
		addLine(sbLIDECDriver);
		generateHeaderActor(indentationLevel, sbLIDECDriver);
		addLine(sbLIDECDriver);
		addLine(indentationLevel, sbLIDECDriver, "int main(int argc, char **argv) {");
		indentationLevel++;
		addLine(sbLIDECDriver);
		generateChannels(indentationLevel, sbLIDECDriver);
		addLine(sbLIDECDriver);
		generateActors(indentationLevel, sbLIDECDriver);
		addLine(sbLIDECDriver);		
		generateLIDECSimpleScheduler(indentationLevel, sbLIDECDriver);
		addLine(sbLIDECDriver);
		indentationLevel--;
		addLine(indentationLevel, sbLIDECDriver, "}");
		LOGGER.info(sbLIDECDriver.toString());
				
	    try {
	    	FileOutputStream outputStream = new FileOutputStream("lide_c_adfg_driver.c");
		    byte[] strToBytes = sbLIDECDriver.toString().getBytes();
			outputStream.write(strToBytes);
			outputStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void generateChannels(int indentationLevel, StringBuilder sbCode) {
		addLine(indentationLevel, sbCode, "/* LIDE-C Channels */");
		for(Channel c : gpr.getAdfgGraph().edgeSet()) {
			addLine(indentationLevel, sbCode, LIDEC_FIFO_POINTER+SPACE+c.getName()+SPACE+"="+SPACE+"NULL;");
		}
		addLine(sbCode);
		for(Channel c : gpr.getAdfgGraph().edgeSet()) {
			addLine(indentationLevel, sbCode, c.getName()+SPACE+"="+SPACE+LIDEC_FIFO_NEW+"("+c.getName().toUpperCase()+"_CAPACITY"+", "+c.getDataSize()+");");
		}
	}
	
	public void generateActors(int indentationLevel, StringBuilder sbCode) {		
		String strActor = "";
		addLine(indentationLevel, sbCode, "/* LIDE-C Actors */");
		
		String actors = "";
		actors+="char *descriptors[ACTOR_COUNT] = {";
		for(Actor a : gpr.getActors()) {
			actors+=("\""+a.getName()+"\",");
		}
		actors = actors.substring(0,actors.length()-1);
		actors+="};";
		
		addLine(indentationLevel, sbCode, actors);
		
		addLine(indentationLevel, sbCode, LIDEC_ACTOR_CONTEXT_TYPE + C_POINTER + SPACE + ARR_ACTOR + "["+ACTOR_COUNT +"];");        
		for(Actor a : gpr.getActors()) {
			strActor+=ARR_ACTOR+"["+DEF_ACTOR+a.getName().toUpperCase()+"]"+SPACE+"="+SPACE;
			strActor+="("+LIDEC_ACTOR_CONTEXT_TYPE+C_POINTER+")" + SPACE;
			strActor+="(";
			strActor+="lide_c_"+a.getName()+"_new" + SPACE;
			strActor+="(";
			strActor+="descriptors["+DEF_ACTOR+a.getName().toUpperCase()+"], ";
			strActor+=getChannels(a);
			strActor+=")";
			strActor+=")";
			strActor+=";";			
			addLine(indentationLevel, sbCode, strActor);
			strActor = "";
		}
	}
	
	public void generateActorsSchedulingParams(int indentationLevel, StringBuilder sbCode) {
		addLine(indentationLevel, sbCode, "/* LIDE-C Actors scheduling */");		
		
		String strActorPriorities = "int priorities["+ACTOR_COUNT+"]"+SPACE+"= {";
		String strActorPeriods = "int periods["+ACTOR_COUNT+"]"+SPACE+"= {";
		String strActorProcessors = "int processors["+ACTOR_COUNT+"]"+SPACE+"= {";
		
		for(Actor a : gpr.getActors()) {
			strActorPriorities += a.getPriority()+",";
			strActorPeriods += a.getPeriod()+",";
			strActorProcessors += a.getPartitionID()+",";
		}
		
		strActorPriorities = strActorPriorities.substring(0,strActorPriorities.length()-1);
		strActorPriorities+="};";
		strActorPeriods = strActorPeriods.substring(0,strActorPeriods.length()-1);
		strActorPeriods+="};";
		strActorProcessors = strActorProcessors.substring(0,strActorProcessors.length()-1);
		strActorProcessors+="};";
		
		addLine(indentationLevel, sbCode, strActorPriorities);
		addLine(indentationLevel, sbCode, strActorPeriods);
		addLine(indentationLevel, sbCode, strActorProcessors);
	}
	
	public void generateRTEMSActors(int indentationLevel, StringBuilder sbCode) {
		addLine(indentationLevel, sbCode, "/* LIDE-C Actors --> RTEMS Actors*/");
		
		addLine(indentationLevel, sbCode, "rtems_actor_type* "+ARR_RTEMS_ACTOR+";");
		addLine(indentationLevel, sbCode, ARR_RTEMS_ACTOR + " = malloc (sizeof (rtems_actor_type)*+"+ACTOR_COUNT+");");
		addLine(sbCode);
		
		String rtems_actors = "";
		
		for(Actor a : gpr.getActors()) {
			rtems_actors = ARR_RTEMS_ACTOR+"["+DEF_ACTOR+a.getName().toUpperCase()+"].context"+SPACE+"="+SPACE+ARR_ACTOR+"["+DEF_ACTOR+a.getName().toUpperCase()+"];";
			addLine(indentationLevel, sbCode, rtems_actors);
			rtems_actors = ARR_RTEMS_ACTOR+"["+DEF_ACTOR+a.getName().toUpperCase()+"].name"+SPACE+"="+SPACE+"descriptors["+DEF_ACTOR+a.getName().toUpperCase()+"];";
			addLine(indentationLevel, sbCode, rtems_actors);
			rtems_actors = ARR_RTEMS_ACTOR+"["+DEF_ACTOR+a.getName().toUpperCase()+"].priority"+SPACE+"="+SPACE+"priorities["+DEF_ACTOR+a.getName().toUpperCase()+"];";
			addLine(indentationLevel, sbCode, rtems_actors);
			rtems_actors = ARR_RTEMS_ACTOR+"["+DEF_ACTOR+a.getName().toUpperCase()+"].period.tv_sec"+SPACE+"="+SPACE+"periods["+DEF_ACTOR+a.getName().toUpperCase()+"];";
			addLine(indentationLevel, sbCode, rtems_actors);
			rtems_actors = ARR_RTEMS_ACTOR+"["+DEF_ACTOR+a.getName().toUpperCase()+"].processor"+SPACE+"="+SPACE+"processors["+DEF_ACTOR+a.getName().toUpperCase()+"];";
			addLine(indentationLevel, sbCode, rtems_actors);
			addLine(sbCode);
		}
		
		
	}
	
	public void generateRTEMSThreads(int indentationLevel, StringBuilder sbCode) {
		addLine(indentationLevel, sbCode, " /* Priority-based, preemptive scheduling with timeslicing */");
		addLine(indentationLevel, sbCode, "pthread_attr_init(&attr);");
		addLine(indentationLevel, sbCode, "pthread_attr_setinheritsched(&attr,PTHREAD_EXPLICIT_SCHED);");
		addLine(indentationLevel, sbCode, "pthread_attr_setschedpolicy(&attr,SCHED_OTHER);");
		addLine(sbCode);
		
		addLine(indentationLevel, sbCode, "/* Create a thread for each actor: Set priority --> Create thread --> Mapping */");		
		for(Actor a : gpr.getActors()) {		    
			addLine(indentationLevel, sbCode, "param.sched_priority=rtems_actors["+DEF_ACTOR+a.getName().toUpperCase()+"].priority;");
			addLine(indentationLevel, sbCode, "pthread_attr_setschedparam(&attr,&param);");
			addLine(indentationLevel, sbCode, "pthread_create(&id,&attr,lide_c_actor_start_routine,&rtems_actors["+DEF_ACTOR+a.getName().toUpperCase()+"]);");
			addLine(indentationLevel, sbCode, "CPU_ZERO(&cpuset);");
			addLine(indentationLevel, sbCode, "CPU_SET(processors["+DEF_ACTOR+a.getName().toUpperCase()+"], &cpuset);");
			addLine(indentationLevel, sbCode, "pthread_setaffinity_np(id, sizeof(cpu_set_t), &cpuset);");
			addLine(sbCode);
		}
		
	}
	
	public void generateLIDECSimpleScheduler(int indentationLevel, StringBuilder sbCode) {		
		addLine(indentationLevel, sbCode, "/* Execute the graph. */");					
		addLine(indentationLevel, sbCode, "lide_c_util_simple_scheduler(actors, ACTOR_COUNT, descriptors);");
		addLine(indentationLevel, sbCode, "return 0;");
	}
		
	
	public void generateHeaderInclude(int indentationLevel, StringBuilder sbCode) {
		addLine(indentationLevel, sbCode, "#include <stdio.h>");
		addLine(indentationLevel, sbCode, "#include <stdlib.h>");
		addLine(indentationLevel, sbCode, "#include \"lide_c_fifo.h\"");
		addLine(indentationLevel, sbCode, "#include \"lide_c_util.h\"");
		for(Actor a : gpr.getActors()) {
			addLine(indentationLevel, sbCode, "#include \"lide_c_"+a.getName()+".h\"");
		}
	}
	
	public void generateHeaderActor(int indentationLevel, StringBuilder sbCode) {
		int i = 0;
		addLine(indentationLevel, sbCode,"/* An enumeration of the actors in this application */");
		for(Actor a : gpr.getActors()) {
			addLine(indentationLevel, sbCode, "#define"+SPACE+DEF_ACTOR+a.getName().toUpperCase()+SPACE+i);
			i++;
		}		
		addLine(sbCode);
		addLine(indentationLevel, sbCode, "/* The total number of actors in the application */");
		addLine(indentationLevel, sbCode, "#define"+SPACE+ACTOR_COUNT+SPACE+i);
	}
	
	public void generateHeaderChannel(int indentationLevel, StringBuilder sbCode) {		
		addLine(indentationLevel, sbCode, "/* Channels in this application */");
		for(Channel c : gpr.getAdfgGraph().edgeSet()) {
			addLine(indentationLevel, sbCode, "#define"+SPACE+c.getName().toUpperCase()+"_CAPACITY"+SPACE+c.getSize());
		}		
	}
	
	public void addLine(int indentationLevel, StringBuilder sbCode, String str) {
		for(int i = 0; i < indentationLevel; i++) {
			sbCode.append("    ");
		}
		sbCode.append(str);
		addLine(sbCode);
	}
	
	public void addLine(StringBuilder sbCode) {
		sbCode.append(NEWLINE);
	}
	
	public String getChannels(Actor a) {
		String channels="";
		for(Channel c : gpr.getAdfgGraph().edgeSet()) {
			if(gpr.getAdfgGraph().getEdgeTarget(c) == a) {
				channels+= (c.getName());
				channels+= ", ";
			}
		}
		for(Channel c : gpr.getAdfgGraph().edgeSet()) {
			if(gpr.getAdfgGraph().getEdgeSource(c) == a) {
				channels+= (c.getName());
				channels+= ", ";
			}
		}
		channels = channels.substring(0,channels.length()-2);
		return channels;
	}
}
