/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.nam.algos;

import java.util.ArrayList;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import fr.inria.tea.adfg.algos.common_data_rep.Actor;

/***
 * 
 * @author hatran
 *
 */
@SuppressWarnings("serial")
public class Firing implements Serializable {

	public ActorMRTA actor;
	public String name;
	public int id;
	public int startTime;
	public int endTime;
	public boolean isScheduled;
	public int startTimeL1;
	public int responseTime;
	public int responseTimeL1;
	public ArrayList<Firing> predDeps;
	public ArrayList<Processor> memAccess;

	/***
	 * 
	 */
	public Firing() {

		this.actor = null;
		this.name = "";
		this.id = 0;
		this.startTime = 0;
		this.endTime = 0;
		this.responseTime = 0;
		this.responseTimeL1 = 0;
		this.isScheduled = false;
		this.predDeps = new ArrayList<Firing>();
		this.memAccess = new ArrayList<Processor>();
	}

	/***
	 * 
	 * @param actor
	 * @param name
	 * @param id
	 * @param startTime
	 * @param endTime
	 * @param isScheduled
	 * @param startTimeL1
	 * @param responseTime
	 * @param responseTimeL1
	 */
	public Firing(ActorMRTA actor, String name, int id, int startTime, int endTime, boolean isScheduled, int startTimeL1,
			int responseTime, int responseTimeL1) {

		this.actor = actor;
		this.name = name;
		this.id = id;
		this.startTime = startTime;
		this.endTime = endTime;
		this.isScheduled = isScheduled;
		this.startTime = startTimeL1;
		this.responseTime = responseTime;
		this.responseTimeL1 = responseTimeL1;
		this.predDeps = new ArrayList<Firing>();
		this.memAccess = new ArrayList<Processor>();
	}

	/***
	 * 
	 * @param firings
	 * @param actor
	 * @param id
	 * @return
	 */
	public static Firing getFiringByActorAndId(ArrayList<Firing> firings, Actor actor, int id) {

		for (Firing f : firings) {
			if (f.actor == actor && f.id == id) {
				return f;
			}
		}
		return null;
	}

	
	public static void getFiringInfo(Firing f){
		
	}
	
	/***
	 * 
	 * @param processor
	 */
	public void addNonDuplicateMemAccess(Processor processor) {

		boolean flag = true;
		for (Processor pr : memAccess) {
			if (pr.processorId == processor.processorId) {
				flag = false;
			}
		}
		if (flag) {
			memAccess.add(processor);
		}
	}

	/***
	 * 
	 * @return
	 */
	public Firing deepClone() {
		try {
			SchedListCommon.printFiringInfo(this);
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.writeObject(this);

			ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
			ObjectInputStream ois = new ObjectInputStream(bais);
			Firing fCopy = (Firing) ois.readObject();
			
			SchedListCommon.printFiringInfo(fCopy);
			
			
			return fCopy;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}
}
