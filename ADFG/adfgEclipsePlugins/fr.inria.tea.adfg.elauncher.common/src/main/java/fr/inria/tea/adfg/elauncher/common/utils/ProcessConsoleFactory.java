/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.elauncher.common.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleFactory;
import org.eclipse.ui.console.IConsoleManager;

/**
 * Create a unique instance of a {@code ProcessConsole}.
 * 
 * @author ahonorat
 */
public class ProcessConsoleFactory implements IConsoleFactory {

	/**
	 * Shared {@code ProcessConsole} instance.
	 */
	private static ProcessConsole processConsole = null;
	private static IConsoleManager consoleManager = ConsolePlugin.getDefault().getConsoleManager();
	
	/**
	 * Standard constructor, do nothing.
	 */
	public ProcessConsoleFactory() {
	}

	/**
	 * Get the current console and show the view.
	 */
	@Override
	public void openConsole() {
		showConsole();
	}

	/**
	 * Show the console view, create the console if none.
	 */
	public static void showConsole() {
		consoleManager.showConsoleView(getProcessConsole());		
	}
	
	/**
	 * Return the shared console for all ADFG analysis,
	 * creates one if none, always check if this console is registered.
	 * 
	 * @return Shared console to write analysis results on.
	 */
	public static ProcessConsole getProcessConsole() {
		if (processConsole == null) {
			processConsole = new ProcessConsole("ADFG analysis", null);
		}
	    List<IConsole> consoles = new ArrayList<IConsole>(Arrays.asList(consoleManager.getConsoles()));
	    if (!consoles.contains(processConsole)) {
	    	consoleManager.addConsoles( new IConsole[] { processConsole } );
	    }
		return processConsole;
	}
	
}
