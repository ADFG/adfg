/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.elauncher.common.utils;

/**
 * Class containing all stringID used for dialogs 
 * and objectsID passed through events.
 * 
 * @author ahonorat
 */
public class Constants {

	/**
	 * Scheduling policy selected in the dynamic menu.
	 */
	public static final String widgetSchedDataID = "adfg.elauncher.sdf.scheuleType";

	
	/**
	 * Input file (SDF3 or ADFG).
	 */
	public static final String inputFileID = "inputFile";
	/**
	 * Update user choice for input file.
	 */
	public static final String mustUpdateID = "mustUpdate";
	/**
	 * Keep actors processor binding/mapping provided by the user (if not SDF3).
	 */
	public static final String keepMappingID = "keepMapping";	
	/**
	 * Keep periods provided by the user (if not SDF3).
	 */
	public static final String keepPeriodID = "keepPeriod";
	/**
	 * Keep initial token buffer sizes provided by the user.
	 */
	public static final String keepInitTokenID = "keepInitToken";
	/**
	 * Keep maximum token buffer sizes provided by the user.
	 */
	public static final String keepMaxTokenID = "keepMaxToken";
	/**
	 * New results' file, SDF3 (if input is also SDF3) or ADFG.
	 */
	public static final String newFileID = "newFile";
	/**
	 * Number of processors in the modelized system.
	 */
	public static final String nbProcsID = "nbProcs";
	/**
	 * Scheduling policy to run.
	 */
	public static final String schedTypeID = "schedType";
	/**
	 * If the scheduling must be exported in the Yartiss file format.
	 */
	public static final String exportYartissID = "exportYartiss";
	/**
	 * If the scheduling must be exported in the Cheddar file format.
	 */
	public static final String exportCheddarID = "exportCheddar";
	/**
	 * If the dataflow graph must be exported in the Dot file format.
	 */
	public static final String exportDotID = "exportDot";
	/**
	 * If the results must be compared to the initial graph.
	 */
	public static final String compareResultsID = "compareResults";
	/**
	 * If all channels initial number of tokens (delays) must be zero.
	 */
	public static final String zeroDelayID = "zeroDelay";
	/**
	 *  If all WCET must be divided by their greatest common divisor (GCD).
	 */
	public static final String reduceWCETpgcdID = "reduceWCETpgcd";
	/**
	 * Strategy for SCOTCH partitioning.
	 */
	public static final String strategyScotchID = "strategyScoth";
	/**
	 * If init token must be multiple of their rates.
	 */
	public static final String lcmInitTokenID = "lcmToken";

	/**
	 * Trade off for SP_UNI_UDH algorithm.
	 */
	public static final String tradeoffUDHID = "tradeoffUDH";	
}
