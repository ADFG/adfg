/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.elauncher.common.utils;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleConstants;
import org.eclipse.ui.console.IConsolePageParticipant;
import org.eclipse.ui.part.IPageBookViewPage;
import org.eclipse.ui.part.IPageSite;

import fr.inria.tea.adfg.algos.ADFGprob;

/**
 * Class used to create the cancel button on
 * {@code ProcessConsole} instances.
 * 
 * @author ahonorat
 */
public class ProcessConsoleParticipant implements IConsolePageParticipant {

	private static Action stop = null;
	private static IActionBars bars = null;
	private static ProcessConsole pc = null;

	/**
	 * Standard constructor, do nothing.
	 */
	public ProcessConsoleParticipant() {
	}

	/**
	 * No adapter exists yet, always returns {@code null}.
	 */
	@Override
	public <T> T getAdapter(Class<T> adapter) {
		return null;
	}

	/**
	 * Create the cancel button and set the static private members,
	 * only if the given console is of type {@code ProcessConsole}
	 * and if no other instance is open.
	 */
	@Override
	public void init(IPageBookViewPage page, IConsole console) {
		// TODO Auto-generated method stub
		if (console instanceof ProcessConsole && pc == null) {
			IPageSite site = page.getSite();
			bars = site.getActionBars();
			pc = (ProcessConsole) console;
			createStopButton();

			pc.setStop(stop);
			pc.setActionBars(bars);
			
			bars.getMenuManager().add(new Separator());
			bars.getMenuManager().add(stop);
			IToolBarManager toolbarManager = bars.getToolBarManager();
			toolbarManager.appendToGroup(IConsoleConstants.LAUNCH_GROUP, stop);

			bars.updateActionBars();
		}
	}

	/**
	 * Effective code to create the cancel button.
	 * Disable it by default.
	 */
	private void createStopButton() {
		// TODO Auto-generated method stub
		ImageDescriptor imageDescriptor = null;
		//ImageDescriptor.createFromFile(getClass(), "/icons/stop_all_active.gif");
		stop = new Action("Cancel analysis", imageDescriptor) {
			public void run() {
				IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
				Shell shell = window.getShell();
				ADFGprob process = ProcessConsole.getProcess();
				if (process != null) {
					MessageDialog.openError(shell, "ADFG Exception", "Cancel method is not fully implemented, "+
							"restart eclipse if analysis pursues or cancellation hasn't be confirmed.");
					if (!process.cancel()) {
						MessageDialog.openError(shell, "ADFG Exception", "Analysis couldn't be cancelled, "
								+ "wait or restart eclipse.");
					} else {
						stop.setEnabled(false);
						bars.updateActionBars();
					}
				}
			}
		};
		stop.setEnabled(false);
	}

	/**
	 * Set the static parameters to {@code null}.
	 */
	@Override
	public void dispose() {
		pc = null;
		stop = null;
		bars = null;
	}

	@Override
	public void activated() {
		// TODO Auto-generated method stub

	}

	@Override
	public void deactivated() {
		// TODO Auto-generated method stub

	}
	
}
