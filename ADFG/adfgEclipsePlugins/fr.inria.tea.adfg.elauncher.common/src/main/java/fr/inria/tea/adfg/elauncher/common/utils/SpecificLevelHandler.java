/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.elauncher.common.utils;

import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.ErrorManager;
import java.util.logging.Filter;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;
import java.util.logging.StreamHandler;

import org.eclipse.ui.console.IOConsoleOutputStream;

/**
 * This class implement an handler that publishes only
 * records of the set level (and not all records more
 * important than this level).
 * 
 * @author ahonorat
 */
public class SpecificLevelHandler extends StreamHandler {

	/**
	 * Stream used in this handler.
	 */
	OutputStream oS;
	
	/**
	 * Set the stream and call super constructor.
	 * 
	 * @param out Stream to publish on.
	 * @param formatter Formatter used for publishing.
	 */
	public SpecificLevelHandler(OutputStream out, Formatter formatter) {
		super(out, formatter);
		oS = out;
	}

	/*
	 * Publish and forces to flush both handler and stream.
	 * 
	 * (non-Javadoc)
	 * @see java.util.logging.StreamHandler#publish(java.util.logging.LogRecord)
	 */
	public void publish(LogRecord record) {
		super.publish(record);
		super.flush();
		try {
			if (oS != null && (oS instanceof IOConsoleOutputStream) && !((IOConsoleOutputStream) oS).isClosed()) {
				oS.flush();				
			}
		} catch (IOException e) {
			e.printStackTrace();
			reportError("Output stream flush after publish failed.", e, ErrorManager.GENERIC_FAILURE);
		}
	}	
	
	/*
	 * Update our stream member.
	 * 
	 * (non-Javadoc)
	 * @see java.util.logging.StreamHandler#setOutputStream(java.io.OutputStream)
	 */
	protected synchronized void setOutputStream(OutputStream out) throws SecurityException {
		super.setOutputStream(out);
		oS = out;
	}
	
	/* 
	 * This method returns true only if the LogRecord has
	 * the same level as the handler (and false if it is
	 * greater than ..., at the opposite of a standard handler).
	 * 
	 * (non-Javadoc)
	 * @see java.util.logging.StreamHandler#isLoggable(java.util.logging.LogRecord)
	 */
	public boolean isLoggable(LogRecord record) {
		boolean sameLevel = record.getLevel() == this.getLevel();
		Filter filter = this.getFilter();
		boolean passFilter = true;
		if (filter != null) {
			passFilter = filter.isLoggable(record);
		}
		return sameLevel && passFilter;
	}
	
}
