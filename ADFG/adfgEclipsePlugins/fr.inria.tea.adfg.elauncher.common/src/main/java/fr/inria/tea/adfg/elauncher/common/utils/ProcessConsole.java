/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.elauncher.common.utils;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.console.MessageConsole;

import fr.inria.tea.adfg.algos.ADFGprob;

/**
 * Console potentially linked to an ADFG problem (called a process).
 * 
 * @author ahonorat
 */
public class ProcessConsole extends MessageConsole {

	private static ADFGprob process = null;
	private IAction stop;
	private IActionBars actionsBars;
	
	public ProcessConsole(String name, ImageDescriptor imageDescriptor) {
		super(name, imageDescriptor);
		stop = null;
	}

	public ProcessConsole(String name, ImageDescriptor imageDescriptor, boolean autoLifecycle) {
		super(name, imageDescriptor, autoLifecycle);
		process = null;
		stop = null;
	}

	public ProcessConsole(String name, String consoleType, ImageDescriptor imageDescriptor, boolean autoLifecycle) {
		super(name, consoleType, imageDescriptor, autoLifecycle);
		process = null;
		stop = null;
	}

	public ProcessConsole(String name, String consoleType, ImageDescriptor imageDescriptor, String encoding,
			boolean autoLifecycle) {
		super(name, consoleType, imageDescriptor, encoding, autoLifecycle);
		process = null;
		stop = null;
	}

	/**
	 * Return the ADFG problem linked to the console.
	 * 
	 * @return {@code null} if none registered.
	 */
	public static ADFGprob getProcess() {
		return process;
	}

	/**
	 * Set the current running ADFG problem.
	 * 
	 * @param pb Problem that will be enabled to be cancel.
	 */
	public static void setProcess(ADFGprob pb) {
		process = pb;
	}	

	/**
	 * Set if the cancel button must be enabled or not.
	 * 
	 * @param enabled True if enabled, false otherwise.
	 * @return True if there was a cancel button,
	 * false otherwise.
	 */
	public boolean setStop(boolean enabled) {
		if (stop != null) {
			stop.setEnabled(enabled);
			actionsBars.updateActionBars();
			return true;
		}
		return false;
	}

	/**
	 * Set the cancel button/action instance to use.
	 * 
	 * @param stop Cancel action.
	 */
	void setStop(IAction stop) {
		this.stop = stop;
	}
	
	/**
	 * Set the whole action bar containing the cancel
	 * button.
	 * 
	 * @param bars Action bar of the cancel button.
	 */
	void setActionBars(IActionBars bars) {
		this.actionsBars = bars;
	}
}
