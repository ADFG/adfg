/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.elauncher.common.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

import fr.inria.tea.adfg.algos.common_base.MinimalFormatter;
import fr.inria.tea.adfg.elauncher.common.Activator;

public class ConsoleWriter {

	private Map<Level,MessageConsoleStream> mcStreams;
	private List<Handler> handlers;
	private MessageConsole mc;
	private Logger logger;

	public ConsoleWriter(MessageConsole mc, Logger logger) {
		this.mcStreams = new HashMap<Level,MessageConsoleStream>();
		this.handlers = new ArrayList<Handler>();
		this.mc = mc;
		this.logger = logger;
		
		init();
	}
	
	private void init() {
		MessageConsoleStream mcsStandard = mc.newMessageStream();
		mcStreams.put(Level.INFO, mcsStandard);
		MessageConsoleStream mcsWarning = mc.newMessageStream();
		mcStreams.put(Level.WARNING, mcsWarning);
		MessageConsoleStream mcsSevere = mc.newMessageStream();
		mcStreams.put(Level.SEVERE, mcsSevere);
		MessageConsoleStream mcsPluginInfo = mc.newMessageStream();
		mcStreams.put(Level.OFF, mcsPluginInfo);

		RGB red = new RGB(255, 2, 2);
		RGB orange = new RGB(255, 165, 2);
		RGB blue = new RGB(2, 2, 205);
		
		Display display = Display.getDefault();	
		Color warningColor = new Color(display, orange);
		Color severeColor = new Color(display, red);
		Color pluginInfoColor = new Color(display, blue);
		
		display.syncExec(new Runnable() {
			@Override
			public void run() {
				mcsWarning.setColor(warningColor);
				mcsSevere.setColor(severeColor);
				mcsPluginInfo.setColor(pluginInfoColor);
			}
		});

		Handler standardHandler = new SpecificLevelHandler(mcsStandard, new MinimalFormatter());
		standardHandler.setLevel(Level.INFO);
		handlers.add(standardHandler);
		logger.addHandler(standardHandler);						

		Handler warningHandler = new SpecificLevelHandler(mcsWarning, new MinimalFormatter());
		warningHandler.setLevel(Level.WARNING);
		handlers.add(warningHandler);
		logger.addHandler(warningHandler);						

		Handler severeHandler = new SpecificLevelHandler(mcsSevere, new MinimalFormatter());
		severeHandler.setLevel(Level.SEVERE);
		handlers.add(severeHandler);
		logger.addHandler(severeHandler);						
	}
	
	
	public MessageConsoleStream getConsoleStream(Level lvl) {
		return mcStreams.get(lvl);
	}
	
	public Status closeConsolesResources() {
		for (Handler handler: handlers) {
			// handler.flush();
			handler.close();
			logger.removeHandler(handler);
		}
		// if not already closed by the handler
		for (MessageConsoleStream stream: mcStreams.values()) {
			if (!stream.isClosed()) {
				try {
					stream.flush();
					stream.close();
				} catch (Exception ex) {
					Status status = new Status(IStatus.ERROR, Activator.PLUGIN_ID, 
							"One or more console stream coudn't be close properly.", ex);
					return status;
				}
			}
		}
		return null;
	}

}
