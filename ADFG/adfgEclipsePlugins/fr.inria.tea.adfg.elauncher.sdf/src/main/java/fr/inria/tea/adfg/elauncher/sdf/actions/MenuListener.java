/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.elauncher.sdf.actions;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import fr.inria.tea.adfg.algos.common_base.ESchedAlgo;
import fr.inria.tea.adfg.elauncher.common.utils.Constants;

/**
 * This class provides a listener to trigger the ADFG
 * analysis from the ADFG submenu. Notice that the 
 * analysis is launched in a separate thread to yield 
 * to the UI thread.
 * 
 * @author Alexandre Honorat
 */
class MenuListener implements SelectionListener {

	/**
	 * Standard constructor, do nothing.
	 */
	public MenuListener() {
	}
	
	@Override
	public void widgetSelected(SelectionEvent e) {
		ESchedAlgo st = (ESchedAlgo) (e.widget.getData(Constants.widgetSchedDataID));
		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		Shell shell = window.getShell();
		IWorkbenchPage page = window.getActivePage();
		IStructuredSelection selection = (IStructuredSelection) page.getSelection();
		if (selection != null) {
			Object selObject = selection.getFirstElement();
			if (selObject != null) {
				IFile resource = (IFile) Platform.getAdapterManager().getAdapter(selObject, IFile.class);

				LaunchAnalysis ana = LaunchAnalysis.getRunnableAnalysis(st, resource, window);
				if (ana != null ) {
					new Thread(ana).start();
				} else {
					MessageDialog.openError(shell, "ADFG", "Could not launch an new analysis: "+
											"one is already running.");
				}
			}
		}
	}

	@Override
	public void widgetDefaultSelected(SelectionEvent e) {
	}	

}
