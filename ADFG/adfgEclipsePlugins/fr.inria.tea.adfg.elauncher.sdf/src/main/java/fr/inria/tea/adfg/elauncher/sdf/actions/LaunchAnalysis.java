/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.elauncher.sdf.actions;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleConstants;
import org.eclipse.ui.console.IConsoleView;
import org.eclipse.ui.console.MessageConsoleStream;

import fr.inria.tea.adfg.algos.ADFGprob;
import fr.inria.tea.adfg.algos.ADFGraph;
import fr.inria.tea.adfg.algos.common_base.AdfgExternalException;
import fr.inria.tea.adfg.algos.common_base.ESchedAlgo;
import fr.inria.tea.adfg.algos.common_base.ETradeOffType;
import fr.inria.tea.adfg.algos.common_base.Parameters;
import fr.inria.tea.adfg.algos.common_base.TradeOffUDH;
import fr.inria.tea.adfg.algos.common_data_rep.Actor;
import fr.inria.tea.adfg.algos.common_data_rep.AffineRelation;
import fr.inria.tea.adfg.algos.common_data_rep.Channel;
import fr.inria.tea.adfg.elauncher.common.utils.ConsoleWriter;
import fr.inria.tea.adfg.elauncher.common.utils.Constants;
import fr.inria.tea.adfg.elauncher.common.utils.ProcessConsole;
import fr.inria.tea.adfg.elauncher.common.utils.ProcessConsoleFactory;
import fr.inria.tea.adfg.elauncher.sdf.Activator;

/**
 * Main class which call the dialog bow and
 * then call ADFG algos. This class is intended to be called
 * in a separate thread since the analysis can take a long time.
 * <p>
 * Some threads are used internally in order to have access
 * to the Eclipse UI outside of the UI thread.
 * 
 * @author Alexandre Honorat
 */
class LaunchAnalysis implements Runnable {

	private static final Logger LOGGER = Logger.getLogger("AdfgLog");

	/**
	 * Own reference to avoid several analysis at the same time.
	 */
	private static LaunchAnalysis itself = null;

	private ESchedAlgo st;
	private IFile resource;
	private IWorkbenchWindow window;

	private int nbProcs;
	private String scotchStrat;
	private String rawNewFile;
	private String rawYartissFile;
	private String rawCheddarFile;
	private String rawDotFile;
	private boolean mustUpdate;
	private boolean keepInitToken;
	private boolean keepMaxToken;
	private boolean keepPeriod;
	private boolean keepMapping;
	private boolean compareResults;
	private boolean reduceWCET;
	private boolean zeroDelay;
	private boolean lcmToken;
	private String toUDH;
	
	/**
	 * Internal error status.
	 */
	private Status status;
	private int retDiagOpen;

	/**
	 * Create a new instance thanks to a scheduling policy,
	 * an SDF3 file, and the current window. Do not perform
	 * the analysis yet.
	 * 
	 * @param st Scheduling policy to use.
	 * @param resource File to analyse.
	 * @param window Current Eclipse window.
	 */
	private LaunchAnalysis(ESchedAlgo st, IFile resource, IWorkbenchWindow window) {
		this.st = st;
		this.resource = resource;
		this.window = window;

		status = null;

		// to avoid printing log on System.err
		LOGGER.setUseParentHandlers(false);
		LOGGER.setLevel(Level.INFO);
	}

	/**
	 * Return an instance only if one is not
	 * already running.
	 * 
	 * @param st Scheduling policy to use.
	 * @param resource File to analyse.
	 * @param window Current Eclipse window.
	 * @return A new instance or {@code null} if one
	 * is already running.
	 */
	public static synchronized LaunchAnalysis getRunnableAnalysis(ESchedAlgo st, IFile resource, IWorkbenchWindow window) {
		if (itself == null) {
			itself =  new LaunchAnalysis(st, resource, window);
			return itself;
		}
		return null;
	}

	@Override
	public synchronized void run() {
		ProcessConsole mc = ProcessConsoleFactory.getProcessConsole();
		ConsoleWriter csw = new ConsoleWriter(mc, LOGGER);

		IWorkbenchPage page = window.getActivePage();
		Shell currentShell = window.getShell();
		Display display = Display.getDefault();					

		URI sdfFileURI = resource.getLocationURI();
		String sdfFile = sdfFileURI.getPath();

		/* open the first dialog box */
		display.syncExec(new Runnable() {
			@Override
			public void run() {
				IDialogSettings settings = Activator.getDefault().getDialogSettings();
				settings.put(Constants.inputFileID, sdfFile);
				settings.put(Constants.schedTypeID, st.name());

				LaunchDialog diag = new LaunchDialog(currentShell, 
						st.isMultiprocessor(), st.implementsStrategies(),
						st.equals(ESchedAlgo.SP_UNI_UDH));
				retDiagOpen = diag.open();
				if (retDiagOpen != Window.OK) {
					return;
				}

				nbProcs = settings.getInt(Constants.nbProcsID);
				scotchStrat = settings.get(Constants.strategyScotchID);
				rawNewFile = settings.get(Constants.newFileID);
				rawYartissFile = settings.get(Constants.exportYartissID);
				rawCheddarFile = settings.get(Constants.exportCheddarID);
				rawDotFile = settings.get(Constants.exportDotID);			
				mustUpdate =  settings.getBoolean(Constants.mustUpdateID);
				keepInitToken = settings.getBoolean(Constants.keepInitTokenID);
				keepMaxToken = settings.getBoolean(Constants.keepMaxTokenID);
				keepPeriod = settings.getBoolean(Constants.keepPeriodID);
				keepMapping = settings.getBoolean(Constants.keepMappingID);
				compareResults = settings.getBoolean(Constants.compareResultsID);
				reduceWCET = settings.getBoolean(Constants.reduceWCETpgcdID);
				lcmToken = settings.getBoolean(Constants.lcmInitTokenID);
				toUDH = settings.get(Constants.tradeoffUDHID);
				zeroDelay = settings.getBoolean(Constants.zeroDelayID);
				
				String id = IConsoleConstants.ID_CONSOLE_VIEW;
				IConsoleView consoleView = null;
				try {
					consoleView = (IConsoleView) page.showView(id);
					consoleView.display((IConsole) mc);
				} catch (PartInitException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}		
		});

		if (retDiagOpen != Window.OK) {
			itself = null;
			Status consoleStatus = csw.closeConsolesResources();
			openError(display, currentShell, consoleStatus);
			return;
		}

		MessageConsoleStream mcsPluginInfo = csw.getConsoleStream(Level.OFF);

		display.syncExec(new Runnable() {
			@Override
			public void run() {
				mcsPluginInfo.println("...");
				mcsPluginInfo.println("Analysing: " + sdfFile + " " + st.name() + " -p" + nbProcs);
				mcsPluginInfo.println("Parameters: keepInit,MaxToken<" + keepInitToken + "," + 
						keepMaxToken + "> keepPeriod<" + keepPeriod + "> keepMapping<" + keepMapping + 
						"> zeroDelay<" + zeroDelay + "> reduceWCET<" + reduceWCET + ">, lcmToken<" +
						lcmToken + "> tradeoffUDH<" + toUDH + ">.");
				try {
					mcsPluginInfo.flush();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		try {

		    ADFGprob.checkSchedulingParams(st, nbProcs, null);
		    Parameters params = new Parameters(keepPeriod, keepInitToken, keepMaxToken, keepMapping,
		    									ETradeOffType.valueOf(scotchStrat), reduceWCET, lcmToken,
		    									new TradeOffUDH(toUDH));
			ADFGraph gr = new ADFGraph(sdfFile);
			ADFGprob pb = new ADFGprob(st, nbProcs, gr, params);
			if (zeroDelay) {
				pb.setAllInitialTokensToZero();
			}
			
			ProcessConsole.setProcess(pb);

			display.syncExec(new Runnable() {
				@Override
				public void run() {
					mc.setStop(true);
				}
			});

			pb.start();
			pb.join();					

			// congratulate the user, positive thinking
			// no need to manage the non schedulable case which
			// always raises an exception
			boolean isSchedulable = pb.isSchedulable();
			boolean comparison = (compareResults) ? pb.compareProperties(gr) : false;
			
			display.syncExec(new Runnable() {
				@Override
				public void run() {
					mc.setStop(false);
					mcsPluginInfo.println("Analysis terminated, schedulable system? " + isSchedulable + ".");
					if (compareResults) {
						mcsPluginInfo.println("Input=Output? " + comparison +".");
					}
					try {
						mcsPluginInfo.flush();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (isSchedulable) {
						MessageDialog.openInformation(currentShell, "ADFG result", "Your system is schedulable! Congratulations :)");
					}
				}
			});

			if (isSchedulable && mustUpdate) {
				pb.updateOrExportFile(null);
				resource.refreshLocal(IResource.DEPTH_ZERO, null);
			}			

			if (!"".equals(rawNewFile)) {
				URI newFileURI = null;
				newFileURI = new URI(rawNewFile);
				String newFile = newFileURI.getPath();

				if (isSchedulable && !"".equals(newFile)) {
					if (newFile.equals(sdfFile)) {
						throw new AdfgExternalException("You selected the same file for input and copy.");
					} else {
						pb.updateOrExportFile(newFile);
						IResource parentResource = resource.getParent();
						if (parentResource != null) {
							parentResource.refreshLocal(IResource.DEPTH_ONE, null);
						}
					}
				}						
			}

			if (!"".equals(rawYartissFile)) {
				URI yartissFileURI = new URI(rawYartissFile);
				String yartissFile = yartissFileURI.getPath();

				if (isSchedulable && !"".equals(yartissFile)) {
					pb.exportYartissResults(yartissFile);
					IResource parentResource = resource.getParent();
					if (parentResource != null) {
						parentResource.refreshLocal(IResource.DEPTH_ONE, null);
					}
				}
			}						
			
			if (!"".equals(rawCheddarFile)) {
				URI cheddarFileURI = new URI(rawCheddarFile);
				String cheddarFile = cheddarFileURI.getPath();

				if (isSchedulable && !"".equals(cheddarFile)) {
					pb.exportCheddarResults(cheddarFile);
					IResource parentResource = resource.getParent();
					if (parentResource != null) {
						parentResource.refreshLocal(IResource.DEPTH_ONE, null);
					}
				}
			}			
			
			if (!"".equals(rawDotFile)) {
				URI dotFileURI = new URI(rawDotFile);
				String dotFile = dotFileURI.getPath();

				if (isSchedulable && !"".equals(dotFile)) {
					pb.exportDotResults(dotFile);
					IResource parentResource = resource.getParent();
					if (parentResource != null) {
						parentResource.refreshLocal(IResource.DEPTH_ONE, null);
					}
				}
			}			

		} catch (Exception ex) {

			display.syncExec(new Runnable() {
				@Override
				public void run() {
					mc.setStop(false);
				}
			});
			
			List<Status> causes = new ArrayList<Status>();
			Throwable t = ex.getCause();
			// the error dialog is not opened when given a multistatus with empty children
			// be aware that if the message given to the exception was null, its cause will
			// be printed instead (when ex.getMessage() is called)
			if (t != null) {
				while (t != null) {
					causes.add(new Status(IStatus.ERROR, Activator.PLUGIN_ID, t.toString()));
					t = t.getCause();
				}				
				status = new MultiStatus(Activator.PLUGIN_ID, IStatus.ERROR, 
						causes.toArray(new Status[] {}), ex.getMessage(), ex);
			} else {
				status = new Status(IStatus.ERROR, Activator.PLUGIN_ID, ex.toString());
			}
			openError(display, currentShell, status);
		}

		ProcessConsole.setProcess(null);
		Status consoleStatus = csw.closeConsolesResources();
		openError(display, currentShell, consoleStatus);
		itself = null;

		Actor.resetIDs();
		AffineRelation.resetIDs();
		Channel.resetIDs();

	}
	
	/**
	 * Open the dialog box to alert the user about the given error.
	 * <p>
	 * This method do nothing if one of its parameters is null.
	 * 
	 * @param display Used to trigger a call in GUI thread.
	 * @param shell Parent shell of the dialog.
	 * @param status Error to display.
	 */
	private static void openError(Display display, Shell shell, Status status) {
		if (display == null || shell == null || status == null) {
			return;
		}
		display.syncExec(new Runnable() {
			@Override
			public void run() {
				ErrorDialog.openError(shell, "ADFG Exception", "An error occured while running ADFG, see details below.", status);
			}
		});
	}

}
