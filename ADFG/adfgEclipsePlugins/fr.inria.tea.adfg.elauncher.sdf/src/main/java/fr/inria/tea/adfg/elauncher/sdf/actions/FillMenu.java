/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.elauncher.sdf.actions;

import org.eclipse.jface.action.ContributionItem;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;

import fr.inria.tea.adfg.algos.common_base.ESchedAlgo;
import fr.inria.tea.adfg.algos.common_base.ESchedPolicy;
import fr.inria.tea.adfg.elauncher.common.utils.Constants;

/*
 * This class provide a dynamic contribution to the ADFG menu: 
 * it fills it with all available scheduling policies.
 * 
 * @author Alexandre Honorat
 */
public class FillMenu extends ContributionItem {

	/**
	 * Listener to use when clicking on one of the
	 * provided scheduling policies.
	 */
	static MenuListener listener = null;

	/**
	 * Constructor, automatically called by the extension point.
	 */
	public FillMenu () {
		if (listener == null) {
			listener = new MenuListener();
		}
	}

	/**
	 * Create a menu item for each scheduling policy, and
	 * associate this policy as an item internal data.
	 */
	public final void fill(final Menu menu, final int index) {

		for (ESchedAlgo st: ESchedAlgo.values()) {	
			if (st.schedulingPolicy() != ESchedPolicy.TT) {
				MenuItem menuItem = new MenuItem(menu, SWT.CHECK, index);
				menuItem.setData(Constants.widgetSchedDataID, st);
				menuItem.setText(st.name());
				menuItem.addSelectionListener(listener);
				menuItem.setToolTipText(st.getFullName());
			}
		}
	}

}
