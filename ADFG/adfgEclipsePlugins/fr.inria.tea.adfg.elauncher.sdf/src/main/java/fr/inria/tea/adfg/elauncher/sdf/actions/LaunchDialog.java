/* 
   Authors: Loïc Besnard, Adnan Bouakaz, Thierry Gautier, Alexandre Honorat, Jean-Pierre Talpin, Hai Nam Tran. 
   Copyright (C) by INRIA-CNRS - developed by TEA project - GPL v2 License - 2016.
   Web site: http://polychrony.inria.fr/ADFG
   Contact: polychronycontact@inria.fr

   ADFG (Affine Data Flow Graph) is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License Version 2 as published by the Free
   Software Foundation.

   ADFG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License Version 2 for more details.

   You should have received a copy of the GNU General Public License Version 2
   along with ADFG; see the file COPYING. If not, consult the url 
   http://www.gnu.org/licenses/gpl-2.0.html, or write to the Free
   Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
*/
package fr.inria.tea.adfg.elauncher.sdf.actions;

import java.util.Arrays;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.FontMetrics;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import fr.inria.tea.adfg.elauncher.sdf.Activator;
import fr.inria.tea.adfg.algos.common_base.ETradeOffType;
import fr.inria.tea.adfg.algos.common_base.TradeOffUDH;
import fr.inria.tea.adfg.elauncher.common.utils.Constants;

/**
 * This class provides a custom dialog box for
 * ADFG analysis. It enables to set the output
 * file and to specify the number of processor.
 * Then these data are stored in a dialog
 * settings object.
 * 
 * @author Alexandre Honorat
 */
class LaunchDialog extends TitleAreaDialog {

	
	Text vleProcs;
	Text vleFile;
	Text vleYartiss;
	Text vleCheddar;
	Text vleDot;
	Text vleToUDH;
	Button btnUpdate;
	Button btnInitToken;
	Button btnMaxToken;
	Button btnPeriod;
	Button btnMapping;
	Button btnCompare;
	Button btnZeroDelays;
	Button btnWCETpgcd;
	Button btnLcmToken;
	boolean enableStrat;
	boolean enableProcs;
	boolean enableToUDH;
	Combo cbScotchStart;
	
	/**
	 * Initialize some variables, especially turn off
	 * the help.
	 * 
	 * @param parentShell To get access to UI methods.
	 * @param enableProcs If the processor field must be activated (multiprocessor).
	 * @param enableStrat If the strategy combo must be activated (SCOTCH).
	 * @param enableToUDH If the tradeoff field for UDH must be activated.
	 */
	public LaunchDialog(Shell parentShell, boolean enableProcs, boolean enableStrat, boolean enableToUDH) {
		super(parentShell);
		setHelpAvailable(false);
		vleProcs = null;
		vleFile = null;
		vleYartiss = null;
		vleCheddar = null;
		vleToUDH = null;
		btnUpdate = null;
		btnInitToken = null;
		btnMaxToken = null;
		btnPeriod = null;
		btnMapping = null;
		btnCompare = null;
		this.enableProcs = enableProcs;
		this.enableStrat = enableStrat;
		this.enableToUDH = enableToUDH;
	}

	@Override
	public void create() {
		super.create();
		setTitle("ADFG analysis configuration");
		IDialogSettings settings = Activator.getDefault().getDialogSettings();
		String schedType = settings.get(Constants.schedTypeID);
		StringBuilder sb = new StringBuilder();
		sb.append("Please set parameters for the ADFG analysis ("+schedType+").");
		setMessage(sb.toString(), IMessageProvider.NONE);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Shell currentShell = this.getShell();
		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NONE);
		container.setLayout(new GridLayout(3, false));
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		Composite leftColumn = new Composite(container, SWT.NONE);
		leftColumn.setLayout(new GridLayout(1, false));
		leftColumn.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		Label verticalSeparator = new Label(container, SWT.SEPARATOR|SWT.VERTICAL);
		verticalSeparator.setLayoutData(new GridData(SWT.CENTER, SWT.FILL, false, true));
		
		Composite rightColumn = new Composite(container, SWT.NONE);
		rightColumn.setLayout(new GridLayout(1, false));
		rightColumn.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		
		// left panel:
		
		btnUpdate = new Button(leftColumn, SWT.CHECK);
		btnUpdate.setText("Update current data file? (see path on the right)");
		btnUpdate.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));

		Composite procRow = new Composite(leftColumn, SWT.NONE);
		procRow.setLayout(new GridLayout(2, false));
		procRow.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true));
		Label lblProcs = new Label(procRow, SWT.NONE);
		lblProcs.setText("Processors available for scheduling:");
		vleProcs = new Text(procRow, SWT.RIGHT | SWT.BORDER);
	    GC gc = new GC(vleProcs);
	    FontMetrics fm = gc.getFontMetrics();
	    double width = 15 * fm.getAverageCharacterWidth();
	    //int height = fm.getHeight();
	    gc.dispose();	    
		vleProcs.setText(enableProcs ? "2" : "1");
		vleProcs.setTextLimit(8);
		vleProcs.setEnabled(enableProcs);
		GridData textGD = new GridData(SWT.RIGHT, SWT.CENTER, true, false);
		//textGD.heightHint = height;
		textGD.widthHint = (int) Math.ceil(width);
		vleProcs.setLayoutData(textGD);		
		//vleProcs.setSize(width, height);
		// LayoutData take priority over Control's size.
		
		
		Composite stratRow = new Composite(leftColumn, SWT.NONE);
		stratRow.setLayout(new GridLayout(2, false));
		stratRow.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true));
		Label lblStrat = new Label(stratRow, SWT.NONE);
		lblStrat.setText("SCOTCH partitioning strategy:");
		cbScotchStart = new Combo(stratRow, SWT.READ_ONLY);
		cbScotchStart.setItems(Arrays.stream(ETradeOffType.values()).map(Enum::name).toArray(String[]::new));
		cbScotchStart.setText(ETradeOffType.getDefault().name());
		cbScotchStart.setEnabled(enableStrat);

		//Composite tradeoffUDHRow = new Composite(rightColumn, SWT.NONE);
		//tradeoffUDHRow.setLayout(new GridLayout(2, false));
		//tradeoffUDHRow.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true));
		Label lblToUDH = new Label(leftColumn, SWT.NONE);
		lblToUDH.setText("Tradeoff for SP_UNI_UDH policy (default " + 
							TradeOffUDH.DEFAULT_TRADEOFF_UDH + "):");
		vleToUDH = new Text(leftColumn, SWT.RIGHT | SWT.BORDER);
		vleToUDH.setEnabled(enableToUDH);
		vleToUDH.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		
		btnInitToken = new Button(leftColumn, SWT.CHECK);
		btnInitToken.setText("Keep initial buffer sizes? (i. e. computed only if undefined)");
		btnInitToken.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		btnMaxToken = new Button(leftColumn, SWT.CHECK);
		btnMaxToken.setText("Keep max buffer sizes? (i. e. computed only if undefined)");
		btnMaxToken.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		btnPeriod = new Button(leftColumn, SWT.CHECK);
		btnPeriod.setText("Keep actor periods? (i. e. computed only if undefined)");
		btnPeriod.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		btnMapping = new Button(leftColumn, SWT.CHECK);
		btnMapping.setText("Keep actor mapping? (i.e. computed only if undefined)");
		btnMapping.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		btnCompare = new Button(leftColumn, SWT.CHECK);
		btnCompare.setText("Compare results with input?");
		btnCompare.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		btnZeroDelays = new Button(leftColumn, SWT.CHECK);
		btnZeroDelays.setText("Enforce zero delays (initial nb. of tokens) on channels?");
		btnZeroDelays.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		btnWCETpgcd = new Button(leftColumn, SWT.CHECK);
		btnWCETpgcd.setText("Divide all actors WCET by their greatest common divisor?");
		btnWCETpgcd.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		btnLcmToken = new Button(leftColumn, SWT.CHECK);
		btnLcmToken.setText("Force all initial tokens to be a multiple of the channel rates?");
		btnLcmToken.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));

		// right panel:
		
		StyledText lblUpdate = new StyledText(rightColumn, SWT.NONE);
		IDialogSettings settings = Activator.getDefault().getDialogSettings();
		String inputFile = settings.get(Constants.inputFileID);
		lblUpdate.setBackground(currentShell.getBackground());
	    lblUpdate.setEditable(false);
	    lblUpdate.setCaret(null);
		lblUpdate.setText("("+inputFile+")");
		lblUpdate.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		Composite resultRow = new Composite(rightColumn, SWT.NONE);
		resultRow.setLayout(new GridLayout(2, false));
		resultRow.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true));
		
		Label lblFile = new Label(resultRow, SWT.NONE);
		lblFile.setText("New result file to create: (leave blank if none)");
		Button btnFile = new Button(resultRow, SWT.PUSH);
		btnFile.setText("Browse");
		btnFile.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false));
		btnFile.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog fileDialog = new FileDialog(currentShell, SWT.SAVE);
				fileDialog.setFilterExtensions(new String[] {"*.xml", "*.adfg"});
				String path = fileDialog.open();
				if (path != null) {
					vleFile.setText(path);
				}
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		vleFile = new Text(rightColumn, SWT.BORDER);
		vleFile.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		
		Composite yartissRow = new Composite(rightColumn, SWT.NONE);
		yartissRow.setLayout(new GridLayout(2, false));
		yartissRow.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true));
		
		Label lblYartiss = new Label(yartissRow, SWT.NONE);
		lblYartiss.setText("New Yartiss result file to create: (leave blank if none)");
		Button btnYartiss = new Button(yartissRow, SWT.PUSH);
		btnYartiss.setText("Browse");
		btnYartiss.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false));
		btnYartiss.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog yartissDialog = new FileDialog(currentShell, SWT.SAVE);
				yartissDialog.setFilterExtensions(new String[] {"*.xml"});
				String path = yartissDialog.open();
				if (path != null) {
					vleYartiss.setText(path);
				}
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		vleYartiss = new Text(rightColumn, SWT.BORDER);
		vleYartiss.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		Composite cheddarRow = new Composite(rightColumn, SWT.NONE);
		cheddarRow.setLayout(new GridLayout(2, false));
		cheddarRow.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true));
		
		Label lblCheddar = new Label(cheddarRow, SWT.NONE);
		lblCheddar.setText("New Cheddar result file to create: (leave blank if none)");
		Button btnCheddar = new Button(cheddarRow, SWT.PUSH);
		btnCheddar.setText("Browse");
		btnCheddar.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false));
		btnCheddar.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog cheddarDialog = new FileDialog(currentShell, SWT.SAVE);
				cheddarDialog.setFilterExtensions(new String[] {"*.xmlv3"});
				String path = cheddarDialog.open();
				if (path != null) {
					vleCheddar.setText(path);
				}
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		vleCheddar = new Text(rightColumn, SWT.BORDER);
		vleCheddar.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		Composite dotRow = new Composite(rightColumn, SWT.NONE);
		dotRow.setLayout(new GridLayout(2, false));
		dotRow.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true));
		
		Label lblDot = new Label(dotRow, SWT.NONE);
		lblDot.setText("New Dot result file to create: (leave blank if none)");
		Button btnDot = new Button(dotRow, SWT.PUSH);
		btnDot.setText("Browse");
		btnDot.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false));
		btnDot.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog dotDialog = new FileDialog(currentShell, SWT.SAVE);
				dotDialog.setFilterExtensions(new String[] {"*.dot"});
				String path = dotDialog.open();
				if (path != null) {
					vleDot.setText(path);
				}
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		vleDot = new Text(rightColumn, SWT.BORDER);
		vleDot.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		
		return area;
	}

	@Override
	protected boolean isResizable() {
		return true;
	}

	@Override
	protected void okPressed() {
		if (saveInput()) {
			super.okPressed();
		}
	}

	/**
	 * Store the field values into the dialog settings object 
	 * shared by the plugin.
	 * 
	 * @return False if processor number cannot be parsed, true otherwise.
	 */
	private boolean saveInput() {
		// put these informations somewhere
		IDialogSettings settings = Activator.getDefault().getDialogSettings();
		if (vleProcs != null) {
			try {
				String nbProcsWithoutSpaces = vleProcs.getText().trim();
				settings.put(Constants.nbProcsID, Integer.parseInt(nbProcsWithoutSpaces));
			} catch (NumberFormatException ex) {
				IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID, 
								ex.toString());
				ErrorDialog.openError(this.getShell(), "ADFG Parameter exception", 
										"Bad processor number.", status);
				return false;
			}
		}
		if (cbScotchStart != null) {
			settings.put(Constants.strategyScotchID, cbScotchStart.getText());
		}
		
		if (vleFile != null) {
			settings.put(Constants.newFileID, vleFile.getText());
		}
		if (vleYartiss != null) {
			settings.put(Constants.exportYartissID, vleYartiss.getText());
		}
		if (vleCheddar != null) {
			settings.put(Constants.exportCheddarID, vleCheddar.getText());
		}
		if (vleDot != null) {
			settings.put(Constants.exportDotID, vleDot.getText());
		}
		if (vleToUDH != null) {
			settings.put(Constants.tradeoffUDHID, vleToUDH.getText());
		}

		if (btnUpdate != null) {
			settings.put(Constants.mustUpdateID, btnUpdate.getSelection());
		}
		if (btnInitToken != null) {
			settings.put(Constants.keepInitTokenID, btnInitToken.getSelection());
		}
		if (btnMaxToken != null) {
			settings.put(Constants.keepMaxTokenID, btnMaxToken.getSelection());
		}
		if (btnPeriod != null) {
			settings.put(Constants.keepPeriodID, btnPeriod.getSelection());
		}
		if (btnMapping != null) {
			settings.put(Constants.keepMappingID, btnMapping.getSelection());
		}
		if (btnZeroDelays != null) {
			settings.put(Constants.zeroDelayID, btnZeroDelays.getSelection());
		}
		if (btnWCETpgcd != null) {
			settings.put(Constants.reduceWCETpgcdID, btnWCETpgcd.getSelection());
		}
		if (btnLcmToken != null) {
			settings.put(Constants.lcmInitTokenID, btnLcmToken.getSelection());
		}
		if (btnCompare != null) {
			settings.put(Constants.compareResultsID, btnCompare.getSelection());
		}
		
		return true;
	}

}
