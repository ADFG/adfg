#!/bin/sh

RTSCHEDULING_ROOT="$(pwd)"
ADFG="${RTSCHEDULING_ROOT}/ADFG"
RES="${RTSCHEDULING_ROOT}/RESOURCES"
RESD="${RTSCHEDULING_ROOT}/RESOURCES-DEV"
SCRIPTS="${RTSCHEDULING_ROOT}/scriptsDeps"

# Checking the environment.

if [ ! -f "${SCRIPTS}/requires_func.sh" ]; then
    echo "A script is missing, leaving."
    exit 1
fi

. "${SCRIPTS}/requires_func.sh"
requireDir "${ADFG}"
requireDir "${RES}"
requireDir "${RESD}"

# Generating the configuration file
CONFIG_FILE="${RTSCHEDULING_ROOT}/devel.config"

if [ ! -f "${CONFIG_FILE}" ]; then
    if [ ! "${JAVA_HOME}" = "" ]; then
	if [ -d "${JAVA_HOME}" ]; then
	    JDK_ADFG="${JAVA_HOME}"
	fi
    fi
    echo "#!/bin/sh" > "${CONFIG_FILE}"
    echo "# Configuration used in Makefiles *and* shell scripts" >> "${CONFIG_FILE}"
    echo "# - do not use relative paths (but the default ones), neither commands" >> "${CONFIG_FILE}"
    echo "" >> "${CONFIG_FILE}"
    echo "# Compulsory paths description:" >> "${CONFIG_FILE}"
    echo "# a. to the jvm or jni or jdk" >> "${CONFIG_FILE}"
    echo "# b. to the scotch installation folder" >> "${CONFIG_FILE}"
    echo "# c. to the lpsolve installation folder" >> "${CONFIG_FILE}"
    echo "JDK_ADFG=\"${JDK_ADFG}\"" >> "${CONFIG_FILE}"
    echo "SCOTCH_ADFG=\"${RES}/libs_to_install/scotch_6.0.6\"" >> "${CONFIG_FILE}"
    echo "LPSOLVE_ADFG=\"${RES}/libs_to_install/lp_solve_5.5\"" >> "${CONFIG_FILE}"
    echo "" >> "${CONFIG_FILE}"
    echo "# Optional paths description:" >> "${CONFIG_FILE}"
    echo "# a. to the .bashrc file (install alias), can be blank" >> "${CONFIG_FILE}"
    echo "# b. to the .tcshrc file (install alias), can be blank" >> "${CONFIG_FILE}"
    echo "# c. to the eclipse installation folder" >> "${CONFIG_FILE}"
    echo "BASHRC_ADFG=\"\"" >> "${CONFIG_FILE}"
    echo "TCSHRC_ADFG=\"\"" >> "${CONFIG_FILE}"
    echo "ECLIPSE_ADFG=\"\"" >> "${CONFIG_FILE}"
    if [ "${JDK_ADFG}" = "" ] ; then
	echo "Fill the devel.config generated file and run this script again."
	exit 1
    fi
fi

# Complete compilation

cd "${RES}"
. "${RES}/install-wrappers.sh" $*

cd "${RESD}"
. "${RESD}/install-artifacts.sh" $*

cd "${ADFG}"
. "${ADFG}/install-projects.sh" $*

CMD_FILE="${RTSCHEDULING_ROOT}/adfg_sched"
CMD_FILE_NAM="${RTSCHEDULING_ROOT}/adfg_sched_nam"
if [ ! "$1" = "clean" ] ; then

    # Install libs
    cd "${RTSCHEDULING_ROOT}"
    mkdir -p "${RTSCHEDULING_ROOT}/libs"
    cp -Lf "${RES}/wrappers_C/lpsolve/"*.${LIB_SUFF} "${RTSCHEDULING_ROOT}/libs"
    cp -Lf "${RES}/wrappers_C/scotch/"*.${LIB_SUFF} "${RTSCHEDULING_ROOT}/libs"
    
    # Generating the adfg_sched command.
    if [ ! -f "${CMD_FILE}" ]; then
	. "${RTSCHEDULING_ROOT}/scriptsDeps/create_cmd.sh"
	adfgExe "${CMD_FILE}" "${ADFG}/adfgCoreAlgos/fr.inria.tea.adfg.cli/target/cli-0.0.1-SNAPSHOT-jar-with-dependencies.jar"
	chmod +x "${CMD_FILE}"
    fi
    if [ ! -f "${CMD_FILE_NAM}" ] && [ "$1" = "nam" ]; then
	. "${RTSCHEDULING_ROOT}/scriptsDeps/create_cmd.sh"
	adfgExe "${CMD_FILE_NAM}" "${ADFG}/adfgNamExtension/fr.inria.tea.adfg.nam/target/nam-0.0.1-SNAPSHOT-jar-with-dependencies.jar"
	chmod +x "${CMD_FILE_NAM}"
    fi
    
    
    # Aliases for the CLI
    . ./devel.config

    JAR="${ADFG}/adfgCoreAlgos/fr.inria.tea.adfg.cli/target/cli-0.0.1-SNAPSHOT-jar-with-dependencies.jar"
    JLP="${RTSCHEDULING_ROOT}/libs"
    JLP_FULL="-Djava.library.path=${JLP}"
    ## bashrc alias
    if [ ! "${BASHRC_ADFG}" = "" ] && [ -f "${BASHRC_ADFG}" ] ; then
	echo ""
	echo "==> Installing alias *adfg_sched* in ${BASHRC_ADFG}"
	echo ""
	ALIAS_BASHRC="alias adfg_sched=\"java ${JLP_FULL} -jar ${JAR}\""
	grep -q 'alias adfg_sched=' "${BASHRC_ADFG}"
	if [ $? -eq 0 ] ; then
	    sed -i 's!alias adfg_sched=.*$!'"${ALIAS_BASHRC}!" "${BASHRC_ADFG}"
	else
	    echo "${ALIAS_BASHRC}" >> "${BASHRC_ADFG}"
	fi
    fi
    ## tcshrc alias
    if [ ! "${TCSHRC_ADFG}" = "" ] && [ -f "${TCSHRC_ADFG}" ] ; then
	echo ""
	echo "==> Installing alias *adfg_sched* in ${TCSHRC_ADFG}"
	echo ""
	ALIAS_TCSHRC="alias adfg_sched \"java ${JLP_FULL} -jar ${JAR}\""
	grep -q 'alias adfg_sched ' "${TCSHRC_ADFG}"
	if [ $? -eq 0 ] ; then
	    sed -i 's!alias adfg_sched.*$!'"${ALIAS_TCSHRC}!" "${TCSHRC_ADFG}"
	else
	    echo "${ALIAS_TCSHRC}" >> "${TCSHRC_ADFG}"
	fi
    fi

    ## Eclipse java.library.path
    if [ ! "${ECLIPSE_ADFG}" = "" ] && [ -f "${ECLIPSE_ADFG}/eclipse.ini" ] && [ ! "$1" = "clean" ]; then
	grep -q "\-Djava.library.path" "${ECLIPSE_ADFG}/eclipse.ini"
	if [ $? -eq 0 ]; then
	    grep -q "\-Djava.library.path=${JLP}" "${ECLIPSE_ADFG}/eclipse.ini"
	    if [ $? -eq 0 ]; then 
		echo "The eclipse.ini file in ${ECLIPSE_ADFG} is already configured."
	    else
		echo "The eclipse.ini file in ${ECLIPSE_ADFG} already contains" \
		    "a java.library.path option, please add yourself the right paths" \
		    "to run ADFG: ${JLP}"
		exit 1
	    fi
	else
	    echo "Updating eclipse.ini."
	    echo "${JLP_FULL}" >> "${ECLIPSE_ADFG}/eclipse.ini"
	fi
    fi

else
    rm -rf ${CMD_FILE} ${CMD_FILE_NAM}
fi

