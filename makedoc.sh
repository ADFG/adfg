#!/bin/sh

HERE=${PWD}
docDir=$HERE/doc

#Clean
mkdir -p ${docDir}
\rm -Rf ${docDir}/UserGuide.pdf ${docDir}/ContributorGuide.pdf ${docDir}/site

# Build doclet
cd $HERE/ADFG/adfgCoreAlgos/fr.inria.tea.adfg.doc
\rm -f install.log
mvn install > install.log

# Run standard javadoc and doclet
cd $HERE/ADFG/adfgCoreAlgos/fr.inria.tea.adfg.algos
echo "javadoc in $HERE/ADFG/adfgCoreAlgos/fr.inria.tea.adfg.algos"
\rm -f site.log 
mvn site > site.log
cp -r target/site ${docDir}/.

# Build Latex contributor guide
# It depends on the result of the previous mvn site (which generated todo9.tex)
todoAppendixGen=$HERE/ADFG/adfgCoreAlgos/fr.inria.tea.adfg.algos/target/latexSite/todo9.tex
cd $HERE/ADFG/adfgCoreAlgos/fr.inria.tea.adfg.doc/latex/ContributorGuide
cp ${todoAppendixGen} appendix/
echo " Contributor Guide documentation"
\rm -f Makefile.log; make $*  > Makefile.log
if [ -f ContributorGuide.pdf ]; then
    \cp ContributorGuide.pdf ${docDir}/
    mkdir -p ${docDir}/latexSite
    cp appendix/index.html ${docDir}/latexSite
fi

# Build Latex user guide
cd $HERE/ADFG/adfgCoreAlgos/fr.inria.tea.adfg.doc/latex/UserGuide
echo " User Guide documentation"
\rm -f Makefile.log; make $*  > Makefile.log
if [ -f UserGuide.pdf ]; then
    \cp UserGuide.pdf ${docDir}/
fi

cd ${HERE}
echo ""
echo ">>> Documentation generated in $HERE/doc"
echo ""





