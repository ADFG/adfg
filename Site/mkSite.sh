#!/bin/sh

if [ $# -gt 1 ]; then
	echo "Syntax: ./mkSite.sh [clean]"
	exit
fi

if [ $# -eq 1 ]; then
  if [ "$1" != "clean" ]; then
      echo "Syntax:  ./mkSite.sh [clean]"
      exit 1
  fi
  echo "Cleaning..."
  rm -f -R exported/SRC
  rm -f exported/*.zip
  rm -R -f exported/update
  rm -f exported/index.html
  rm -f exported/UserGuide/index.html
  rm -f exported/*.xsd
  rm -f -R examples
  exit 0
fi 

rm -f -R tmp
mkdir -p tmp
echo "   * Generating exported/index.html"
pandoc Site.md -t html -c style_sheet.css -o tmp/index.html
cat header.html tmp/index.html  foot.html > exported/index.html

# Sources
# -------
rm -f -R exported/SRC
mkdir -p exported/SRC

cd ..
git archive -o adfg_src.zip HEAD
mv adfg_src.zip Site/exported/SRC/.
cd Site/exported/SRC
unzip adfg_src.zip
\rm -R examples adfg_src.zip
zip -r adfg_src.zip *
mv adfg_src.zip ../.
cd ../..
\rm -R exported/SRC

# Getting the ADFG versions : MacOs and Linux
# -------------------------
echo "   * Getting the binary versions produced by the continuous integration."
echo "     ** MacOs " 
#scp -i ~/.ssh/id_rsa_ci polychronyuser@scm.gforge.inria.fr:/svnroot/polychrony/toto/adfg_Darwin_x86_64.zip exported/
sshpass -p ci scp -oCiphers=-aes256-gcm@openssh.com ci@adfg-macos1.ci:/builds/workspace/adfg-macos/export/adfg_Darwin_x86_64.zip  exported/
echo "     ** Linux " 
sshpass -p ci scp ci@adfg-new-ubuntu64.ci:/builds/workspace/adfg-linux64/export/*.zip  exported/

# export prédéfinis.
cp -R TO_BE_EXPORTED/* exported/.

\rm -R -f exported/update
cd exported
mkdir -p update
cd update
unzip ../adfg_eclipse_site.zip
cd ../..

# Our xsd files
# -------------
echo "   * Getting the XSD files to export on the website"
find ../ADFG/adfgCoreAlgos/fr.inria.tea.adfg.algos/src/main/resources -name *.xsd | xargs -I {} cp {} exported/

# The user's guide
# ----------------
echo "   * Exporting the User'guide in exported/UserGuide"
mkdir -p exported/UserGuide
pandoc UserGuide.md -t html -c style_sheet.css -o tmp/UserGuide_index.html
cat header.html tmp/UserGuide_index.html  foot.html > exported/UserGuide/index.html
cp -f TO_BE_EXPORTED/style_sheet.css exported/UserGuide

echo "The local ADFG site is now uptodate"
chmod -R go+rx exported
