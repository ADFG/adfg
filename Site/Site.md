
<!---
This file is formatted in Markdown .
The HTML file is generated with pandoc:

pandoc Site.md -t html -c style_sheet.css -o exported/index.html
-->

<div style="text-align:center"><img src ="adfg.png"/></div>

<p>
<!-- # **ADFG TOOL**-->
<meta charset="UTF-8">

ADFG, developed by [INRIA](https://www.inria.fr/en)-[IRISA](http://www.irisa.fr/en) project-team <a href="https://team.inria.fr/tea/presentation/">TEA</a>, checks the 
scheduling feasibility of an Ultimately Cyclo-Static DataFlow (UCSDF) graph 
which represents a system of actors (or processes) and their dependencies (data production and consumption rates), aka channels
between them. If the graph is schedulable, regarding to a specified policy, runtime parameters are computed to help
the system implementation or build (if it is an embedded system).
The UCSDF graph can be passed to ADFG as a SDF3 xml file or can be constructed through the Java library.

Parameters computed by ADFG are:

 * the firing period of each actor, and their phases;
 * each channel maximum size, and initial tokens number;
 * processor allocation of each actor (in case of homogeneous multi-processor system).

Some useful metrics are also computed as the processor utilization factor.

ADFG comes with a very simple API, a command-line tool to analyse sdf files, and some eclipse plugins.

ADFG has originally been written by Adnan Bouakaz as an experimental tool for his [PhD thesis](http://hal.inria.fr/tel-00916515).
	
</p>

# **SOFTWARE IMPLEMENTATION**

ADFG depends on two external software: [SCOTCH](https://www.labri.fr/perso/pelegrin/scotch/scotch_fr.html) used for graph partitioning, 
[LpSolve](http://lpsolve.sourceforge.net) used for Integer Linear Problem (ILP) solving. 

Some dynamical libraries of SCOTCH and lp_solve are integrated to the distribution.
It includes also a [Java wrapper for the lp_solve](http://lpsolve.sourceforge.net/5.5/Java/README.html).

ADFG can be used as a command line (adfg_sched) or under Eclipse.

<!---
 lpsolve is distributed under LGPL for the GNU lesser general public license.
 SCOTCH is distributed under CeCILL-C licence.
 Java wrapper for the lp_solve is distributed under the LGPL license Juergen Ebert (juergen.ebert@web.de)
-->

## Licences

The installing and use of the ADFG toolset suppose the agreement to the license.
ADFG is distributed under the [GPL v2](http://www.gnu.org/licenses/gpl-2.0.html) License terms.

## Distributions (version 3.2, January 2022)
 * [Source distribution](adfg_src.zip)
 * [Binary distribution for linux-x86_64](adfg_Linux_x86_64.zip)
 * [Binary distribution for macosx-x86_64](adfg_Darwin_x86_64.zip)

## Installation

After the **unzip** of the downloaded archive, a directory with adfg_ as prefix is created (adfg_src, adfg_Linux_x86_64 for Linux, ...). 
Let **ADFG_DIR** be this directory.

  * goto the **ADFG_DIR**  directory
  * then execute the script: **install.sh**; it will generate a script called **adfg_sched** in order to use adfg.


## ADFG under Eclipse
   To use ADFG under Eclipse:

   * Add the **ADFG_DIR/libs** value to the **java.library.path**, in the **eclipse.ini** file of your eclipse (see [here](eclipse.ini) for an example).
   * Run eclipse and install the plugins for ADFG by the &quot;Help->Install New Software...&quot; dialog, with the address:

     **http://polychrony.inria.fr/ADFG/update**

## Documentation

 The [User&apos;s Guide](UserGuide/index.html) provides information on how to use ADFG tool.

## Test

To test the ADFG tool, you can download the [examples](examples.tar.gz) provided by the [DARTS tool](http://daedalus.liacs.nl).

## Publications

   * Hai Nam Tran, Alexandre Honorat, Shuvra S. Bhattacharyya, Jean-Pierre Talpin, Thierry Gautier, Lo&iuml;c Besnard. A Framework for Periodic Scheduling Synthesis of Synchronous Data-Flow Graphs. SAMOS XXI 2021 - International Conference on embedded computer Systems: Architectures, Modeling and Simulation. Springer, 2021.  [https://hal.inria.fr/hal-03488217v1](https://hal.inria.fr/hal-03488217v1)

   * Hai Nam Tran, Alexandre Honorat, Jean-Pierre Talpin, Thierry Gautier, Lo&iuml;c Besnard. Efficient Contention-Aware Scheduling of SDF Graphs on Shared Multi-bank Memory. ICECCS 2019 - 24th International Conference on Engineering of Complex Computer Systems, Nov 2019, Hong Kong, China.  [https://hal.inria.fr/hal-02193639v2](https://hal.inria.fr/hal-02193639v2)

   * Alexandre Honorat, Hai Nam Tran, Lo&iuml;c Besnard, Thierry Gautier, Jean-Pierre Talpin, Adnan Bouakaz. ADFG: a scheduling synthesis tool for dataflow graphs in real-time systems. International Conference on Real-Time Networks and Systems, Oct 2017, Grenoble, France. [https://hal.inria.fr/hal-01615142](https://hal.inria.fr/hal-01615142)


## Contact

  For any contact, send a mail to polychronycontact@inria.fr
