# ADFG User's guide


**ADFG (Affine DataFlow Graph)** tool checks the scheduling feasibility of an Ultimately Cyclo-Static DataFlow (UCSDF) graph which represents a system of actors (or processes) 
and their dependencies (data production and consumption rates), aka channels between them. If the graph is schedulable, regarding to a specified policy, runtime parameters are computed 
to help the system implementation or build (if it is an embedded system)

ADFG accepts two kind of input files: [SDF3](http://www.es.ele.tue.nl/sdf3/manuals/xml/) standard (modified for csdf) and .adfg. You also can export the provided SDF3 examples into .adfg
with the results (adfg model enables to store the computed periods, at the opposite of SDF3).
ADFG comes with a command line interface tool and some Eclipse plugins.


##Using ADFG tool as a command line

**Syntax**:

 * *adfg_sched* **FILENAME** **ANALYSIS** [**OPTIONS**]
 * *adfg_sched* -h  for help

This command takes a SDF3 file **FILENAME** (describing the system) and a scheduling policy **ANALYSIS** as mandatory arguments. 
The number of processors available in the system is also mandatory if a multi-processor policy is chosen, it is set with the option -p.

**FILENAME**: ADFG or SDF3 file to analyse (*.adfg  or *.xml). 

**ANALYSIS**: specifies the scheduling policy (several variants of EDF or SP policies, UNI or MULTI processors). See below for details. The accepted values are:
 
 * EDF, Mono processor: EDF_UNI, EDF_UNI_DA, EDF_UNI_HDA, EDF_MULT_MBS_HDA, EDF_UNI_PQPA
 * EDF, Multi processors: EDF_MULT_MBS_ID, EDF_MULT_BF_UF_ID, EDF_MULT_BF_SQPA_CD, EDF_MULT_BF_PQPA, GEDF_MULT  
 * SP, Mono processor: SP_UNI, SP_UNI_LOP, SP_UNI_UDH 
 * SP, Multi processors: SP_MULT_MBS, SP_MULT_BF_FBBFFD, SP_MULT_BF_SRTA

**OPTIONS**:

 * -b (--keepMapping)                                             : Use the given actor partition (i. e. computed only if not provided). (default: false)
 * -c (--cheddar) FILENAME                                        : Export actors properties in the XML Cheddar format.
 * -d (--dot) FILENAME                                            : Export graph properties in the DOT format.
 * -g (--WCETgcd)                                                 : Reduce all WCET by their greatest common divisor (for simulation). (default: false)
 * -h (--help)                                                    : Print usage. (default: true)
 * -i (--keepInitToken)                                           : Use the given initial buffer tokens (i. e. computed only if not provided). (default: false)
 * -m (--keepMaxToken)                                            : Use the given max buffer sizes (i. e. computed only if not provided). (default: false)
 * -n (--newFile) FILENAME                                        : Create a results file (ADFG .adfg format, or SDF3 .xml if constructed by a SDF3 file).
 * -p (--nbProcs) POS INT                                         : Number of processors. (default: 1)
 * -q (--quiet)                                                   : Print only errors (disable verbose mode). (default: false)
 * -r (--compareResult)                                           : Compare result with the given input. (default: false)
 * -s (--strategy) [BUFFER_MIN | THROUGHPUT_MAX | BALANCED]       : Solving objective.
 * -t (--keepPeriod)                                              : Use the given actor periods (i. e. computed only if not provided). (default: false)
 * -u (--updateXML)                                               : Update intial file with computed results. (default: false)
 * -v (--verbose)                                                 : Print more detailed information. (default: false)
 * -y (--yartiss) FILENAME                                        : Export actors properties in the XML Yartiss format.
 * -z (--zeroDelay)                                               : All channel initial number of tokens (delays) must be zero. (default: false)


###Example

It is assumed that the setup of ADFG has been executed. The command 

 *adfg_sched* Beamformer.xml EDF_UNI 

provides this [result](Beamformer.res).



##Using ADFG tool under Eclipse

It is assumed that you have installed ADFG according to the installation procedure, and the setup of ADFG has been executed.
Using adfg under Eclipse is quite similar to the adfg_sched command line.

Run Eclipse,

 * select a sdf3 file and right click, 
 * select in the menu the line "ADFG for SDF" and select a scheduling policy ([example](adfg_sched1.png)),
 * set the options and run ADFG ([example](adfg_sched2.png))
 * the result is written in the console windows ([example](adfg_sched3.png)).


##About the scheduling policies:

 * EDF (Early Deadline First) 
     * Mono processor (not restricted to weakly connected DFG):
        * EDF_UNI 
        * EDF_UNI_DA	 *Deadline Adjustment to avoid preemptions*
        * EDF_UNI_HDA 	 *Hybbrid Deadline Adjustment*
        * EDF_UNI_PQPA   *Parametric Quick Processor demand Analysis*

     * Multi processors (restricted to weakly connected DFG except EDF_MULT_PQPA):
        * EDF_MULT_MBS_HDA	 *Scotch, Hybrid Deadline Adjustment*
        * EDF_MULT_MBS_ID 	 *Scotch, Implicit Deadline*
        * EDF_MULT_BF_UF_ID 	 *Best Fit, Implicit Deadline*
        * EDF_MULT_BF_SQPA_CD 	 *Best Fit, Constrained Deadline*
        * EDF_MULT_BF_PQPA       *Best Fit, Parametric Quick Processor demand Analysis*
	
     * Global multi processors (restricted to weakly connected DFG):	
        * GEDF_MULT 

 * SP (Static Priority)
    * Mono processor (restricted to weakly connected DFG):
        * SP_UNI	 *Deadline Monotonic*
        * SP_UNI_LOP  	 *Linear Odering Problem to minimize total buffer size*
        * SP_UNI_UDH  	 *Utilization Distance Heuristic to minimize total buffer size*

    * Multi processors (restricted to weakly connected DFG):
        * SP_MULT_MBS	 	*Scotch*
        * SP_MULT_BF_FBBFFD	*Best Fit, Fisher Baruah Baker First Fit Decreasing*
        * SP_MULT_BF_SRTA 	*Best Fit, Symbolic Response Time Analysis*




