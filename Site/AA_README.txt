The site of the project (Site.md) is formatted in Markdown.

To build the exported site, run
    ./mkSite.sh
the result is generated in the exported directory.
To clean, run
    ./mkSite.sh clean


WARNING:
   DON'T DELETE the directory TO_BE_EXPORTED.
   It contains files that are exported on the web site after a rebuild of the site.
---------------------------------------------------------------------------------------------
LBesnard : 
    To install the site (http://polychrony.inria.fr/ADFG), use the command
    scp -p -r exported web-espresso@apachedmz:~/www/polychrony.inria.fr/htdocs/ADFG/NEW_VERSION
 
    Then, to change the version on the web:
        ssh web-espresso@apachedmz.irisa.fr
        cd www
        cd polychrony.inria.fr/htdocs

  
    You can, for example, use the following commands
        mv ADFG ADFG_
        mv ADFG_/NEW_VERSION  ADFG
    *** To keep the old version:
        mv ADFG_/PREV_VERSIONS ADFG/.
        mv ADFG_   ADFG/PREV_VERSIONS/XXX   where XXX is a name that identifies the version (currently ADFG/PREV_VERSIONS contains VersionApr2019 and VersionNov2019)

  


    
    
     

