#!/bin/sh

RTSCHEDULING_ROOT="$(pwd)"
ADFG="${RTSCHEDULING_ROOT}/ADFG"
SCRIPTS="${RTSCHEDULING_ROOT}/scriptsDeps"

# Checking the environment.

if [ ! -f "${SCRIPTS}/requires_func.sh" ]; then
    echo "A scrpit is missing, leaving."
    exit 1
fi

. "${SCRIPTS}/requires_func.sh"
requireFile "${ADFG}/adfgCoreAlgos/fr.inria.tea.adfg.cli/target/cli-0.0.1-SNAPSHOT-jar-with-dependencies.jar"
requireFile "${ADFG}/adfgEclipsePlugins/site/target/fr.inria.tea.adfg.elauncher.site-0.0.1-SNAPSHOT.zip"
requireFile "${ADFG}/adfgCoreAlgos/fr.inria.tea.adfg.doc/latex/ContributorGuide/ContributorGuide.pdf"
requireFile "${ADFG}/adfgCoreAlgos/fr.inria.tea.adfg.doc/latex/UserGuide/UserGuide.pdf"


# Current version.

. "${SCRIPTS}/OSdependent.sh"

EXPORT_NAME="adfg_${KERNEL_ADFG}_${MACHINE_ADFG}"
EXPORT_PATH="${RTSCHEDULING_ROOT}/export/${EXPORT_NAME}"

# Export.

rm -rf "${RTSCHEDULING_ROOT}/export"
mkdir -p "${EXPORT_PATH}/libs"
mkdir -p "${EXPORT_PATH}/scriptsDeps"
mkdir -p "${EXPORT_PATH}/examples/sdf3"
mkdir -p "${EXPORT_PATH}/examples/sdf3/csdf"
mkdir -p "${EXPORT_PATH}/examples/sdf3/sdf"

cp -f "${ADFG}/adfgCoreAlgos/fr.inria.tea.adfg.cli/target/cli-0.0.1-SNAPSHOT-jar-with-dependencies.jar" "${EXPORT_PATH}/cli.jar"
cp -f "${ADFG}/adfgEclipsePlugins/site/target/fr.inria.tea.adfg.elauncher.site-0.0.1-SNAPSHOT.zip" "${EXPORT_PATH}/adfg_eclipse_site.zip"
cp -f "${ADFG}/adfgCoreAlgos/fr.inria.tea.adfg.doc/latex/ContributorGuide/ContributorGuide.pdf" "${EXPORT_PATH}/docDev.pdf"
cp -f "${ADFG}/adfgCoreAlgos/fr.inria.tea.adfg.doc/latex/UserGuide/UserGuide.pdf" "${EXPORT_PATH}/docUser.pdf"


# Added for web export. It is required for aadl2signal export.
# FOR_WEB_EXPORT=${HOME}/tmp/osate/imports/adfg
# \rm -Rf ${FOR_WEB_EXPORT}
# mkdir -p ${FOR_WEB_EXPORT}
# unzip "${RTSCHEDULING_ROOT}/export/adfg_eclipse_site.zip" -d ${FOR_WEB_EXPORT}


. "${RTSCHEDULING_ROOT}/scriptsDeps/OSdependent.sh"
cp -Lf "${RTSCHEDULING_ROOT}/libs/"*.${LIB_SUFF} "${EXPORT_PATH}/libs"
cp -Lf "${RTSCHEDULING_ROOT}/examples/sdf3/sdf/"*.xml "${EXPORT_PATH}/examples/sdf3/sdf"
cp -Lf "${RTSCHEDULING_ROOT}/examples/sdf3/csdf/"*.xml "${EXPORT_PATH}/examples/sdf3/csdf"
cp -Lf "${RTSCHEDULING_ROOT}/scriptsDeps/create_cmd.sh" "${EXPORT_PATH}/scriptsDeps/"
cp -Lf "${RTSCHEDULING_ROOT}/scriptsDeps/requires_func.sh" "${EXPORT_PATH}/scriptsDeps/"
cp -Lf "${RTSCHEDULING_ROOT}/scriptsDeps/OSdependent.sh" "${EXPORT_PATH}/scriptsDeps/"
cp -Lf "${RTSCHEDULING_ROOT}/scriptsDeps/install_export.sh" "${EXPORT_PATH}/install.sh"
chmod +x "${EXPORT_PATH}/install.sh"

cd "${RTSCHEDULING_ROOT}/export"
zip -q -r "${EXPORT_NAME}.zip" "${EXPORT_NAME}"
# \rm -R -f "${EXPORT_PATH}"
cd "${RTSCHEDULING_ROOT}"

echo "[DONE] files exported in: ${EXPORT_PATH}"
